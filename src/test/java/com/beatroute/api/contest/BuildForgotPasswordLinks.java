package com.beatroute.api.contest;


import com.beatroute.api.model.User;
import com.beatroute.api.repository.ContestUserRepository;
import com.beatroute.api.service.EnterpriseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class BuildForgotPasswordLinks {
    @Autowired
    private ContestUserRepository userRepository;

    @Autowired
    private EnterpriseService enterpriseService;

    @Value("${app.mail.forgot.password.body}")
    private String forgotPasswordBody;

    @Test
    public void execute() {
        PrintWriter pw = null;
        try {
            String date = Calendar.getInstance().getTime().toString();
            pw = new PrintWriter(new File("/tmp/FPLinks_"+date+".csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Name");
        sb.append(',');
        sb.append("Email");
        sb.append(',');
        sb.append("Forgot password link");
        sb.append('\n');

        List<User> usersList = (List<User>) userRepository.findAllByPasswordAndCreatedDate();
        for(User user : usersList){
            UUID uuidOne = UUID.randomUUID();
            String randomUUIDOne = uuidOne.toString().replaceAll("-", "");
            UUID uuidTwo = UUID.randomUUID();
            String randomUUIDTwo = uuidTwo.toString().replaceAll("-", "");
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.DATE, 1);
            dt = c.getTime();
            long dateTimeStamp = dt.getTime();
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(5);
            String hashedPassword = passwordEncoder.encode(user.getId().toString());
            user.setPassword(hashedPassword);
            user = userRepository.save(user);

            String host = "https://www.beatroute.com";
            String activationLink = host.concat(forgotPasswordBody);
            String forgotPasswordLink = activationLink+randomUUIDOne+"-"+"fcyu"+user.getId()+"uko"+"-"+randomUUIDTwo+"-"+dateTimeStamp;
            //System.out.println("Forgot password link for User : "+user.getName()+" is "+forgotPasswordLink);

            sb.append(user.getName());
            sb.append(',');
            sb.append(user.getEmail());
            sb.append(',');
            sb.append(forgotPasswordLink);
            sb.append('\n');
        }
        pw.write(sb.toString());
        pw.close();
    }
}
