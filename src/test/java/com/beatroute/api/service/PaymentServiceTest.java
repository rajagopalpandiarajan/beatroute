
package com.beatroute.api.service;

import com.beatroute.api.dto.PaymentResponse;
import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.Event;
import com.beatroute.api.model.Transaction;
import com.beatroute.api.model.User;
import com.beatroute.api.model.UserBagPack;
import com.beatroute.api.repository.EventRepository;
import com.beatroute.api.repository.TransactionRepository;
import com.beatroute.api.repository.UserBagPackRepository;
import com.beatroute.api.repository.UserRepository;
import com.beatroute.api.rest.template.TempLockRequest;
import com.beatroute.api.service.builder.EventBuilder;
import com.beatroute.api.service.builder.UserBuilder;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.net.ssl.SSLSocketFactory;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class PaymentServiceTest {

    @Autowired
    private TicketService ticketService;

    @Autowired
    private UserBagPackService userBagPackService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private UserBagPackRepository userBagPackRepository;

    @Autowired
    private UserBuilder userBuilder;

    @Autowired
    private EventBuilder eventBuilder;

    @Autowired
    private TransactionRepository transactionRepository;

    @MockBean
    private GenericAsyncService genericAsyncService;

    private User user;

    private List<SecureTicket> secureTickets;

    @Autowired
    private PaymentService paymentService;

    //@Before
    public void setup() {
        // create user
        user = userBuilder.getAppUser();
        userRepository.save(user);

        secureTickets = new ArrayList<>();
    }

    private void createBackPack(String sessionReferenceId) {
        UserBagPack userBagPack = new UserBagPack();
        userBagPack.setUser(user);
        userBagPack.setJsonData("{\n" +
                "\t\"eventId\": \"1\",\n" +
                "\t\"currency\": \"USD\",\n" +
                "\t\"numberOfTickets\": \"2\",\n" +
                "\t\"row\": \"A\",\n" +
                "\t\"seatNo\": \"1\",\n" +
                "\t\"section\": \"A\",\n" +
                "\t\"sessionReferenceId\":"+ "\""+sessionReferenceId+"\",\n" +
                "\t\"supplierName\": \"TicketNetwork\",\n" +
                "\t\"ticketBookingPrice\": \"10.50\",\n" +
                "\t\"ticketGroupId\": \"1235\"\n" +
                "}\n");
        userBagPackRepository.save(userBagPack);
    }

  /*  @Test
    public void should_process_secure_tickets_and_generate_payment_hash_successfully() throws Exception {
        getSecureTickets();

        // check for hash code
        PaymentResponse paymentResponse = paymentService.processPayment(secureTickets);
        Assert.assertNotNull(paymentResponse.getSecureHash());

        // check for 3 transactions
        List<Transaction> transactions = transactionRepository.findAllByUserOrderByCreatedDateDesc(user);
        Assert.assertEquals(3,transactions.size());

        // all transactions should have same payment ref id
        String paymentReferenceId = transactions.get(0).getPaymentReferenceId();
        for(Transaction transaction : transactions) {
            Assert.assertEquals(paymentReferenceId, transaction.getPaymentReferenceId());
        }
    }*/

    /*private void getSecureTickets() throws Exception {
        Event event = eventBuilder.getSampleEvent();
        eventRepository.save(event);

        // create bag pack
        String sessionRefId = event.getSessions().iterator().next().getSessionReferenceId();
        createBackPack(sessionRefId);
        createBackPack(sessionRefId);
        createBackPack(sessionRefId);

        given(genericAsyncService.lockTicket(any(TempLockRequest.class))).willReturn(new AsyncResult<>("{\"lockStatus\":\"success\"}"));

        List<UserBagPack> userBackPacks = userBagPackService.fetchBagPack(user.getId());
        secureTickets = ticketService.lockTickets(userBackPacks);

        for(SecureTicket secureTicket : secureTickets) {
            secureTicket.setEvent(event);
        }
    }*/

    /** RETAINED FOR REFRENCE ONLY. THIS IS NO LONGER NEEDED. */
//    @SuppressWarnings({ "restriction", "deprecation" })
//	@Test
//    public void post( ) {
//        String ip_postData = "merchantId=12104978&orderRef=9859341150678&amount=100&currCode=840&paymentMethod=Master&epMonth=07&epYear=2020&cardNo=4918914107195005&cardHolder=MasterCard&remark=test&securityCode=123&payType=N";
////        String live_ip_postData = "merchantId=12113650&orderRef=9859341150678&amount=100&currCode=840&paymentMethod=Master&epMonth=07&epYear=2020&cardNo=4918914107195005&cardHolder=MasterCard&remark=test&securityCode=123";
//        
//        try
//        {
//            String strResult = "";
////            URL url = new URL("https://test.paydollar.com/b2cDemo/eng/directPay/payComp.jsp"); //old
//            URL url = new URL("https://test.paydollar.com/b2cDemo/eng/dPayment/payComp.jsp");
////            URL url = new URL("https://www.paydollar.com/b2c2/eng/directPay/payComp.jsp"); //old
////            URL url = new URL("https://www.paydollar.com/b2c2/eng/dPayment/payComp.jsp");
//            URLConnection con = url.openConnection(); //from secure site
//            if(con instanceof com.sun.net.ssl.HttpsURLConnection){
//                ((com.sun.net.ssl.HttpsURLConnection)con).setSSLSocketFactory
//                        ((SSLSocketFactory) SSLSocketFactory.getDefault());
//            }
//            con.setDoOutput(true);
//            con.setDoInput(true);
//            // Set request headers for content type and length
//            con.setRequestProperty(
//                    "Content-type",
//                    "application/x-www-form-urlencoded");
//            con.setRequestProperty(
//                    "Content-length",
//                    String.valueOf(ip_postData.length()));
////            con.setRequestProperty(
////                  "Content-length",
////                  String.valueOf(live_ip_postData.length()));
//            // Issue the POST request
//            OutputStream outStream = con.getOutputStream();
//            outStream.write(ip_postData.getBytes());
//            outStream.flush();
//            // Read the responsePayDollar PayGate Integration Guide (v3.52)
//
//            InputStream inStream = con.getInputStream();
//            while (true)
//            {
//                int c = inStream.read();
//                if (c == -1)
//                    break;
//                strResult = strResult + String.valueOf((char)c);
//            }
//            inStream.close();
//            outStream.close();
//
//            System.out.println("strResult : "+ strResult);
//
//        }
//        catch (Exception e)
//        {
//            System.out.print(e.toString());
//
//        }
//    }

}

