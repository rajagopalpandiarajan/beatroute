package com.beatroute.api.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;

import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Slf4j
@SuppressWarnings("rawtypes")
public class EnterpriseServiceTest {

    @Autowired
    private EnterpriseService enterpriseService;

    @Test
    public void get_enterprise_Info_on_tenant_name_and_return_success() throws Exception {
    	ContextData cd=new ContextData();
        cd.setEnterpriseId(new Long(1));
        cd.setOriginHost("dev.beatroute.com");
        cd.setOriginSubDomain("dev");
		ContextStorage.set(cd);
		
		Map response = enterpriseService.getEnterpriseInfo();
        log.info("response - "+response);
        Assert.assertNotNull(response);
        Assert.assertEquals(new Long(1), response.get("domainTenantId"));
    }

    @Test
    public void get_enterprise_Info_on_tenant_name_and_return_failure() throws Exception {
    	ContextStorage.unset();
        Map response = enterpriseService.getEnterpriseInfo();
        log.info("response - "+response);
        Assert.assertNotNull(response);
        Assert.assertEquals("failure", response.get("status"));
        Assert.assertNotNull(response.get("errors"));
    }

}
