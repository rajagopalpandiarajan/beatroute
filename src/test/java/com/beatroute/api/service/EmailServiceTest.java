package com.beatroute.api.service;

import com.beatroute.api.dto.EmailContent;
import com.beatroute.api.dto.Enquiry;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class EmailServiceTest {

    @Autowired
    private EmailService emailService;

    @Autowired
    private EmailHelperService emailHelperService;


    @Test
    public void should_send_user_registration_email() throws Exception {
        EmailContent emailContent = new EmailContent();
        emailContent.setDisplayName("Infosys");
        emailContent.setCompanyName("Infosys Pvt Ltd");
        emailContent.setSupportContactName("Anu");
        emailContent.setSupportContactNo("9988776655");
        emailContent.setSupportEmailId("support@infosys.com");
        emailService.sendUserRegistartionEmail("Rakshith", "rakshith@technome.co", "http://www.beatroute.com:80/user/activate/" ,"Beatroute.com - Registration Confirmation", emailContent);
        log.info("completed");
    }

    @Test
    public void should_send_forgot_password_email() throws Exception {
        EmailContent emailContent = new EmailContent();
        emailContent.setDisplayName("Infosys");
        emailContent.setCompanyName("Infosys Pvt Ltd");
        emailContent.setSupportContactName("Anu");
        emailContent.setSupportContactNo("9988776655");
        emailContent.setSupportEmailId("support@infosys.com");
        emailService.sendForgotPasswordEmail("Rakshith", "rakshith@technome.co", "http://www.beatroute.com:80/user/activate/" ,"Beatroute.com - Forgot Password", emailContent);
    }

    /*@Test
    public void should_send_reset_password_email() throws Exception {
        emailService.sendResetPasswordEmail("Savitha", "savitha@technome.co","Beatroute.com - Reset Password");
    }*/

    @Test
    public void should_send_payment_confirmation_email() throws Exception {
        EmailContent emailContent = new EmailContent();
        emailContent.setDisplayName("Infosys");
        emailContent.setCompanyName("Infosys Pvt Ltd");
        emailContent.setSupportContactName("Anu");
        emailContent.setSupportContactNo("9988776655");
        emailContent.setSupportEmailId("support@infosys.com");
        emailService.sendPaymentConfirmation("Savitha", "rakshith@technome.co", "Beatroute.com - Payment confirmation", "Wicked", "Apollo Victoria Theatre, London United Kingdom", "Sep 3, 2016 1:00:00 AM", "50", "1", "10", "2703861", "60", "", "", "", "supplierName", "additionalNotes", "additionalInfo", emailContent);
    }

    @Test
    public void should_send_order_failure_email() throws Exception {
        EmailContent emailContent = new EmailContent();
        emailContent.setDisplayName("Infosys");
        emailContent.setCompanyName("Infosys Pvt Ltd");
        emailContent.setSupportContactName("Anu");
        emailContent.setSupportContactNo("9988776655");
        emailContent.setSupportEmailId("support@infosys.com");
        emailService.sendOrderFailure("Savitha", "rakshith@technome.co", "Beatroute.com - Dev Transaction Failure: Reference no. " + System.currentTimeMillis(), String.valueOf(System.currentTimeMillis()), "Test Event", "1", "60", new Date(), "supplierName", "", emailContent);
    }

    @Test
    public void get_status_of_enquiry()
    {
        String input=getStringOfInput();
        JSONObject json=new JSONObject(input);
        Enquiry enquiry=new Enquiry();
        enquiry.setFirstName(json.getString("firstName"));
        enquiry.setLastName(json.getString("lastName"));
        enquiry.setEmail(json.getString("email"));
        enquiry.setContactNumber(json.getString("contactNumber"));
        enquiry.setDesignation(json.getString("designation"));
        enquiry.setCompanyName(json.getString("companyName"));
        enquiry.setAddress(json.getString("address"));
        enquiry.setRegId(json.getString("regId"));
        enquiry.setTypeOfSolution(json.getString("typeOfSolution"));
        // System.err.println(json.getString("firstName")+" : "+json.getString("lastName")+" : "+json.getString("email")+" : "+json.getString("contactNumber")+" : "+json.getString("designation")+" : "+json.getString("companyName")+" : "+json.getString("address")+" : "+json.getLong("regId")+""+" : "+json.getString("typeOfSolution"));
        System.out.println("enquiry : ->  "+enquiry);
        Map response=this.emailHelperService.sendEnquiryEmail(enquiry);
        System.out.println("response : "+response);
    }

    private String getStringOfInput()
    {
        String input="{\n" +
                "\t\"firstName\": \"Vinod\",\n" +
                "\t\"lastName\": \"Kumar\",\n" +
                "\t\"email\": \"pankajsoni@technome.co\",\n" +
                "\t\"contactNumber\": \"9024684071\",\t\n" +
                "\t\"designation\": \"what is this?\",\n" +
                "\t\"companyName\": \"Beatroute\",\n" +
                "\t\"address\": \"#3/11th vasant vihar, child colony, banglore\",\n" +
                "\t\"regId\": \"1123\",\n" +
                "\t\"typeOfSolution\": \"this useful solution i required.\"\n" +
                "}";
        System.out.println("inget : input "+input);
        return input;
    }


}
