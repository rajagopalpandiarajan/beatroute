package com.beatroute.api.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class AmazonS3ServiceTest {

    @Autowired
    private AmazonS3Service amazonS3Service;

    @Test
    public void should_upload_asset_to_s3() throws Exception {
        String imageUrl = amazonS3Service.upload("https://upload.wikimedia.org/wikipedia/commons/5/5b/JordanEberlePats.JPG");
        log.info(imageUrl);
        Assert.assertNotNull(imageUrl);
    }
}
