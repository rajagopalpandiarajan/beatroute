/*
package com.beatroute.api.service;

import com.beatroute.api.dto.OrderResponse;
import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.User;
import com.beatroute.api.model.UserBagPack;
import com.beatroute.api.repository.UserBagPackRepository;
import com.beatroute.api.repository.UserRepository;
import com.beatroute.api.rest.template.OrderRequest;
import com.beatroute.api.rest.template.TempLockRequest;
import com.beatroute.api.service.builder.EventBuilder;
import com.beatroute.api.service.builder.UserBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.any;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class TicketServiceTest {

    @Autowired
    private TicketService ticketService;

    @Autowired
    private UserBagPackService userBagPackService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserBagPackRepository userBagPackRepository;

    @Autowired
    private UserBuilder userBuilder;

    @Autowired
    private EventBuilder eventBuilder;

    @MockBean
    private GenericAsyncService genericAsyncService;

    private User user;

    private List<SecureTicket> secureTickets;

    @Before
    public void setup() {
        // create user
        user = userBuilder.getAppUser();
        userRepository.save(user);
        secureTickets = new ArrayList<>();
    }

    private void createBackPack() {
        UserBagPack userBagPack = new UserBagPack();
        userBagPack.setUser(user);
        userBagPack.setJsonData("{\n" +
                "\t\"eventId\": \"1\",\n" +
                "\t\"currency\": \"USD\",\n" +
                "\t\"numberOfTickets\": \"2\",\n" +
                "\t\"row\": \"A\",\n" +
                "\t\"seatNo\": \"1\",\n" +
                "\t\"section\": \"A\",\n" +
                "\t\"sessionReferenceId\": \"21323213243\",\n" +
                "\t\"supplierName\": \"TicketNetwork\",\n" +
                "\t\"ticketBookingPrice\": \"10.50\",\n" +
                "\t\"ticketGroupId\": \"1235\"\n" +
                "}\n");
        userBagPackRepository.save(userBagPack);
    }


    @Test
    public void should_successfully_lock_ticket_from_backpack_of_the_user() throws Exception {

        // send the user details to fetch the backpack
        getSecureTickets();
        Assert.assertTrue(secureTickets.size() == 3);
    }

    private void getSecureTickets() throws Exception {
        createBackPack();
        createBackPack();
        createBackPack();

        given(genericAsyncService.lockTicket(any(TempLockRequest.class))).willReturn(new AsyncResult<>("{\"lockStatus\":\"success\"}"));

        List<UserBagPack> userBackPacks = userBagPackService.fetchBagPack(user.getId());
        secureTickets = ticketService.lockTicketsForSupplier(userBackPacks);
    }

    @Test
    public void should_fail_when_ticket_lock_fails() throws Exception {

        createBackPack();
        given(genericAsyncService.lockTicket(any(TempLockRequest.class))).willThrow(Exception.class);

        List<UserBagPack> userBackPacks = userBagPackService.fetchBagPack(user.getId());
        secureTickets = ticketService.lockTickets(userBackPacks);
        Assert.assertFalse(secureTickets.get(0).getLockStatus());
        Assert.assertNull(secureTickets.get(0).getLockResponse());
    }

    @Test
    public void should_continue_when_ticket_lock_response_is_negative() throws Exception {

        createBackPack();
        given(genericAsyncService.lockTicket(any(TempLockRequest.class))).willReturn(new AsyncResult<>("{\"message\":\"could not lock ticket\"}"));

        List<UserBagPack> userBackPacks = userBagPackService.fetchBagPack(user.getId());
        List<SecureTicket> SecureTicket = ticketService.lockTickets(userBackPacks);
        Assert.assertFalse(SecureTicket.get(0).getLockStatus());
        Assert.assertEquals("{\"message\":\"could not lock ticket\"}", SecureTicket.get(0).getLockResponse());
    }

    @Test
    public void should_process_order_successfully() throws Exception{
        getSecureTickets();
        for(SecureTicket secureTicket : secureTickets) {
            secureTicket.setEvent(eventBuilder.getSampleEvent());
        }
        given(genericAsyncService.createOrder(any(OrderRequest.class))).willReturn(new AsyncResult<>(getOrderResponse()));
        List<OrderResponse> orderResponses = ticketService.placeOrder(secureTickets);
        Assert.assertEquals(3, orderResponses.size());
        for(OrderResponse orderResponse : orderResponses) {
            Assert.assertEquals(true, orderResponse.isSuccess());
        }
    }

    private String getOrderResponse() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        OrderResponse orderResponse = new OrderResponse();
        orderResponse.setSuccess(true);
        return objectMapper.writeValueAsString(orderResponse);
    }
}
*/
