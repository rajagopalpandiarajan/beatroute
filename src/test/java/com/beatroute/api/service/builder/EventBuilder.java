package com.beatroute.api.service.builder;

import com.beatroute.api.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class EventBuilder {

    @Autowired
    private VenueBuilder venueBuilder;

    @Autowired
    private SessionBuilder sessionBuilder;

    private Event event;

    public EventBuilder newEvent() {
        event = new Event();
        return this;
    }

    public EventBuilder withName(String name) {
        event.setName(name);
        return this;
    }

    public EventBuilder andCategory(String category) {
        Category newCategory = new Category();
        newCategory.setName(category);
        event.setPrimaryCategory(newCategory);
        return this;
    }

    public Event build() {
        return event;
    }

    public EventBuilder andTag(String name) {
        Tag tag = new Tag();
        tag.setName(name);
        tag.getEvents().add(event);
        event.getTags().add(tag);
        return this;
    }

    public EventBuilder andTenant(String name) {
        Tenant tenant = new Tenant();
        tenant.setName(name);
        tenant.getEvents().add(event);
        event.getTenants().add(tenant);
        return this;
    }

    public Event getSampleEventWithName(String name) {
        Event event = getSampleEvent();
        event.setName(name);
        return event;
    }

    public Event getSampleEvent() {
        Event event = newEvent().withName(RandomWordGenerator.getInstance().makeHeadline()).andCategory(RandomWordGenerator.getInstance().makeHeadline()).andTag(RandomWordGenerator.getInstance().makeHeadline()).andTenant(RandomWordGenerator.getInstance().makeHeadline()).build();

        Session morningSession = sessionBuilder.newSession().withName(RandomWordGenerator.getInstance().makeHeadline()).andStartTime(new Date()).andEndTime(new Date()).andDescription(RandomWordGenerator.getInstance().makeHeadline()).withEvent(event).build();

        Venue bristow = venueBuilder.newVenue().withName(RandomWordGenerator.getInstance().makeHeadline()).andAddress(RandomWordGenerator.getInstance().makeHeadline()).andCity(RandomWordGenerator.getInstance().makeHeadline()).andState(RandomWordGenerator.getInstance().makeHeadline()).andCountry(RandomWordGenerator.getInstance().makeHeadline()).andSession(morningSession).build();
        Venue newYork = venueBuilder.newVenue().withName(RandomWordGenerator.getInstance().makeHeadline()).andAddress(RandomWordGenerator.getInstance().makeHeadline()).andCity(RandomWordGenerator.getInstance().makeHeadline()).andState(RandomWordGenerator.getInstance().makeHeadline()).andCountry(RandomWordGenerator.getInstance().makeHeadline()).andSession(morningSession).build();

        morningSession.getVenues().add(bristow);
        morningSession.getVenues().add(newYork);

        event.getSessions().add(morningSession);
        return event;
    }
}
