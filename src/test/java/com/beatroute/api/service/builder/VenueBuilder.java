package com.beatroute.api.service.builder;

import com.beatroute.api.model.Event;
import com.beatroute.api.model.Session;
import com.beatroute.api.model.Venue;
import org.springframework.stereotype.Component;

@Component
public class VenueBuilder {

    private Venue venue;

    public VenueBuilder newVenue() {
        venue = new Venue();
        return this;
    }

    public VenueBuilder withName(String name) {
        venue.setName(name);
        return this;
    }

    public VenueBuilder andCity(String city) {
        venue.setCity(city);
        return this;
    }

    public Venue build() {
        return venue;
    }

    public VenueBuilder andSession(Session session) {
        venue.setSession(session);
        return this;
    }

    public VenueBuilder andAddress(String address) {
        venue.setAddress(address);
        return this;
    }

    public VenueBuilder andState(String state) {
        venue.setState(state);
        return this;
    }

    public VenueBuilder andCountry(String country) {
        venue.setCountry(country);
        return this;
    }
}
