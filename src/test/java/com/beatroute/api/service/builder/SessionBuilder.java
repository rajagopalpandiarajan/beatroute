package com.beatroute.api.service.builder;

import com.beatroute.api.model.Event;
import com.beatroute.api.model.Session;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SessionBuilder {

    private Session session;

    public SessionBuilder newSession() {
        session = new Session();
        return this;
    }

    public SessionBuilder withName(String name) {
        session.setName(name);
        return this;
    }

    public SessionBuilder andStartTime(Date date) {
        session.setBeginDateTime(date);
        return this;
    }

    public SessionBuilder andEndTime(Date date) {
        session.setEndDateTime(date);
        return this;
    }

    public SessionBuilder andDescription(String description) {
        session.setDescription(description);
        return this;
    }

    public Session build() {
        return session;
    }

    public SessionBuilder withEvent(Event event) {
        session.setEvent(event);
        return this;
    }
}
