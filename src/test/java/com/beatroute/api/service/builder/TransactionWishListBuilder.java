package com.beatroute.api.service.builder;

import com.beatroute.api.model.Event;
import com.beatroute.api.model.Transaction;
import com.beatroute.api.model.User;
import com.beatroute.api.model.WishList;
import com.beatroute.api.model.enumeration.TransactionStatus;
import com.beatroute.api.rest.template.TransactionTemplate;
import lombok.Builder;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.UUID;

@Component
public class TransactionWishListBuilder {

    String[] currencies = {"INR", "USD"};

    public TransactionTemplate getFailTransaction(long userId, long eventId){
        TransactionTemplate t = new TransactionTemplate();
        t.setAmount(new Random().nextDouble());
        t.setCurrency(currencies[new Random().nextInt(currencies.length)]);
        t.setPaymentReferenceId(UUID.randomUUID().toString());
        t.setTransactionStatus(TransactionStatus.FAIL);
        t.setNumberOfTickets(new Random().nextInt(10));
        t.setTicketBookingPrice(new Random().nextDouble());
        t.setUserId(userId);
        t.setEventId(eventId);

        //persist transaction
        return t;
    }

    public TransactionTemplate getPassTransaction(long userId, long eventId){
        TransactionTemplate t = new TransactionTemplate();
        t.setAmount(new Random().nextDouble());
        t.setCurrency(currencies[new Random().nextInt(currencies.length)]);
        t.setPaymentReferenceId(UUID.randomUUID().toString());
        t.setTransactionStatus(TransactionStatus.SUCCESS);
        t.setNumberOfTickets(new Random().nextInt(10));
        t.setTicketBookingPrice(new Random().nextDouble());
        t.setUserId(userId);
        t.setEventId(eventId);

        //persist transaction
        return t;
    }

    public WishList getWishList(User user, Event event){
        WishList w = new WishList();
        w.setEvent(event);
        w.setUser(user);

        //persist wishlist
        return w;
    }
}
