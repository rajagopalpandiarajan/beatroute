package com.beatroute.api.service.builder;

import com.beatroute.api.model.User;
import com.beatroute.api.model.enumeration.UserType;
import lombok.Builder;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.UUID;

@Component
public class UserBuilder {

    String[] names = {"maddy", "hari", "ranga", "deepak", "manja", "pradyumna", "venki", "anu", "savitha", "pushpa", "namratha"};
    String[] emails = {"maddy@gmail.com", "hari@gmail.com", "ranga@gmail.com", "deepak@gmail.com", "manja@gmail.com", "pradyumna@gmail.com", "venki@gmail.com", "anu@gmail.com", "savitha@gmail.com", "pushpa@gmail.com", "namratha@gmail.com" };
    String[] countries = {"india", "africa", "egypt", "pakistan", "nepal"};
    String[] cities = {"mandya", "mumbai", "bengaluru", "dubai", "madagascar", "vietnam"};
    String[] mobiles = {"9876544351", "9846543359", "9876544756", "9836344156"};
    String[] profilePics = {"http://www.google.com", "http://www.fb.com", "http://www.twitter.com"};

    private User user;

    public User getAppUser(){
        User user = new User();
        user.setName(names[new Random().nextInt(names.length)]);
        user.setEmail(emails[new Random().nextInt(emails.length)]);
        user.setPassword(UUID.randomUUID().toString());
        user.setCountry(countries[new Random().nextInt(countries.length)]);
        user.setUserType(UserType.APP_USER);
        user.setProfilePic(profilePics[new Random().nextInt(profilePics.length)]);
        user.setMobile(mobiles[new Random().nextInt(mobiles.length)]);
        user.setAddress(RandomWordGenerator.getInstance().makeHeadline());
        user.setCity(cities[new Random().nextInt(cities.length)]);

        user.setAuthorised(checkAuthorised(user));
        return user;
    }

    public User getOauthFBUser(){
        User user = new User();
        user.setName(names[new Random().nextInt(names.length)]);
        user.setUserType(UserType.OAUTH_FB_USER);
        user.setProfilePic(profilePics[new Random().nextInt(profilePics.length)]);
        user.setOauthId(UUID.randomUUID().toString());

        user.setAuthorised(checkAuthorised(user));
        return user;
    }

    public User getOauthGoogleUser(){
        User user = new User();
        user.setName(names[new Random().nextInt(names.length)]);
        user.setEmail(emails[new Random().nextInt(emails.length)]);
        user.setUserType(UserType.OAUTH_GOOGLE_USER);
        user.setProfilePic(profilePics[new Random().nextInt(profilePics.length)]);
        user.setOauthId(UUID.randomUUID().toString());

        user.setAuthorised(checkAuthorised(user));
        return user;
    }

    public boolean checkAuthorised(User user){
        if(user.getEmail() == null && user.getUserType() == UserType.APP_USER){
            return false;
        }

        return true;
    }
}
