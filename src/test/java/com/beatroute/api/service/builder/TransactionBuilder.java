package com.beatroute.api.service.builder;

import com.beatroute.api.model.Event;
import com.beatroute.api.model.Session;
import com.beatroute.api.model.Transaction;
import com.beatroute.api.model.User;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

@Component
public class TransactionBuilder {

    private Transaction transaction;

    public TransactionBuilder newTransaction(){
        transaction = new Transaction();
        transaction.setCreatedDate(new Date());
        transaction.setUpdatedDate(new Date());
        return this;
    }

    public TransactionBuilder withPaymentReferenceId(String paymentReferenceId){
        transaction.setPaymentReferenceId(paymentReferenceId);
        return this;
    }

    public TransactionBuilder withNumberOfTickets(int numberOfTickets){
        transaction.setNumberOfTickets(numberOfTickets);
        return this;
    }

    public TransactionBuilder withAmount(BigDecimal amount){
        transaction.setAmount(amount);
        return this;
    }

    public TransactionBuilder withCurrency(String currency){
        transaction.setCurrency(currency);
        return this;
    }

    public TransactionBuilder withTicketBookingPrice(BigDecimal ticketBookingPrice){
        transaction.setTicketBookingPrice(ticketBookingPrice);
        return this;
    }

    public TransactionBuilder withSession(Session session){
        transaction.setSession(session);
        return this;
    }

    public TransactionBuilder withEvent(Event event){
        transaction.setEvent(event);
        return this;
    }

    public TransactionBuilder withUser(User user){
        transaction.setUser(user);
        return this;
    }

    public Transaction build(){
        return transaction;
    }
}
