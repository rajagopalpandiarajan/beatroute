package com.beatroute.api.service.builder;

import com.beatroute.api.model.Event;
import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.User;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

@Component
public class SecureTicketBuilder {

    private SecureTicket secureTicket;

    public SecureTicketBuilder getSecureTicket(){
        secureTicket = new SecureTicket();
        return this;
    }

    public SecureTicketBuilder withUser(User user){
        secureTicket.setUser(user);
        return this;
    }

    public SecureTicketBuilder withEvent(Event event){
        secureTicket.setEvent(event);
        return this;
    }

    public SecureTicketBuilder withTicketBookingPrice(BigDecimal ticketBookingPrice){
        secureTicket.setTicketBookingPrice(ticketBookingPrice);
        return this;
    }

    public SecureTicketBuilder withTicketGroupId(String ticketGroupId){
        secureTicket.setTicketGroupId(ticketGroupId);
        return this;
    }

    public SecureTicketBuilder withPaymentReferenceId(String paymentReferenceId){
        secureTicket.setPaymentReferenceId(paymentReferenceId);
        return this;
    }

    public SecureTicketBuilder withSessionReferenceId(String sessionReferenceId){
        secureTicket.setSessionReferenceId(sessionReferenceId);
        return this;
    }

    public SecureTicketBuilder withLockRequestId() {
        secureTicket.setLockRequestId(String.valueOf(System.nanoTime()));
        return this;
    }

    public SecureTicketBuilder withLockStatus(Boolean status) {
        secureTicket.setLockStatus(status);
        return this;
    }

    public SecureTicketBuilder withCreatedDate() {
        secureTicket.setCreatedDate(new Date());
        return this;
    }

    public SecureTicketBuilder withUpdatedDate() {
        secureTicket.setUpdatedDate(new Date());
        return this;
    }

    public SecureTicketBuilder withExpiryDateTime() {
        secureTicket.setExpiryDateTime(new Date());
        return this;
    }

    public SecureTicketBuilder withNoOfTickets(Integer noOfTickets) {
        secureTicket.setNoOfTickets(noOfTickets);
        return this;
    }

    public SecureTicket build(){
        return secureTicket;
    }
}
