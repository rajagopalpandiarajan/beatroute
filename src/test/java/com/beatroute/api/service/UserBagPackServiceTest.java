package com.beatroute.api.service;

import com.beatroute.api.model.User;
import com.beatroute.api.model.UserBagPack;
import com.beatroute.api.repository.UserBagPackRepository;
import com.beatroute.api.repository.UserRepository;
import com.beatroute.api.service.builder.UserBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class UserBagPackServiceTest {

    @Autowired
    private UserBuilder userBuilder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserBagPackRepository userBagPackRepository;

    @Autowired
    private UserBagPackService userBagPackService;

    @Autowired
    ObjectMapper objectMapper;

    private  User user;

    @Before
    public void setup(){
        user = userBuilder.getAppUser();
        userRepository.save(user);;
    }

    private String createUserBagPack(Long id) throws JsonProcessingException {
        JSONObject obj = new JSONObject();
        obj.put("id", null);
        obj.put("userId", id);
        Map<String, Object> jsonData = new HashMap<>();
        jsonData.put("currency", "USD");
        jsonData.put("eventId", "52");
        jsonData.put("numberOfTickets", "2");
        jsonData.put("row", "330");
        jsonData.put("seatNo", "");
        jsonData.put("section", "K");
        jsonData.put("sessionReferenceId", "2690824");
        jsonData.put("sessionDate", new Date(System.currentTimeMillis() - (7 * (1000 * 60 * 60 * 24))).toString());
        jsonData.put("supplierName", "");
        jsonData.put("ticketBookingPrice", "160");
        jsonData.put("ticketGroupId", "1674655839");
        jsonData.put("latestTicketBookingPrice", "165");
        jsonData.put("expired", false);
        obj.put("jsonData", jsonData);
        return obj.toJSONString();
    }

    @Test
    public void should_add_to_user_bagpack_successfully() throws JsonProcessingException {
        String json = createUserBagPack(user.getId());

        Long userBagPackId = null;
        try {
            userBagPackId = userBagPackService.addToUserBagPack(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(userBagPackId);
    }

    @Test
    public void should_throw_exception_when_adding_bagpack_to_unknown_user() throws JsonProcessingException {
        String json = createUserBagPack(System.nanoTime());
        Long userBagPackId = null;
        try {
            userBagPackId = userBagPackService.addToUserBagPack(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertNull(userBagPackId);
    }

    @Test
    public void should_delete_bag_pack_for_user() throws Exception {
        String json = createUserBagPack(user.getId());
        Long userBagPackId = userBagPackService.addToUserBagPack(json);
        Assert.assertNotNull(userBagPackId);

        userBagPackService.deleteUserBagPack(user.getId());
        Assert.assertEquals(0, user.getUserBagPacks().size());

    }

    @Test
    public void should_delete_event_from_bagpack_for_user() throws Exception {
        String json =createUserBagPack(user.getId());
        Long userBagPackId = userBagPackService.addToUserBagPack(json);
        Assert.assertNotNull(userBagPackId);

        userBagPackService.deleteEventFromUserBagPack(userBagPackId);
        Assert.assertNull(userBagPackRepository.findOne(userBagPackId));
    }

    @Test
    public void should_get_bagpack_for_user_successfully() throws Exception{
        int i = 0;
        Long userBagPackId = null;
        while (i <=2){
            String json = createUserBagPack(user.getId());
            userBagPackId = userBagPackService.addToUserBagPack(json);
            Assert.assertNotNull(userBagPackId);
            i++;
        }

        List userBagPacks = userBagPackService.fetchBagPack(user.getId());
        Assert.assertEquals(3, userBagPacks.size());
    }

    private void createBackPack() {
        UserBagPack userBagPack = new UserBagPack();
        userBagPack.setUser(user);
        userBagPack.setJsonData("{}");
        userBagPackRepository.save(userBagPack);
    }

    @Test
    public void should_return_user_backpack_which_is_empty() throws Exception {

        List<UserBagPack>  userBackPacks = userBagPackService.fetchBagPack(user.getId());
        Assert.assertTrue(userBackPacks.size() == 0);

    }

    @Test
    public void should_return_user_backpack_successfully() throws Exception {

        // create backpack
        createBackPack();
        createBackPack();

        // get back pack
        List<UserBagPack>  userBackPacks = userBagPackService.fetchBagPack(user.getId());
        Assert.assertTrue(userBackPacks.size() == 2);
    }

    /*@Test
    public void should_get_event_expired() throws Exception {
        String json = createUserBagPack(user.getId());
        Long userBagPackId = userBagPackService.addToUserBagPack(json);
        Assert.assertNotNull(userBagPackId);

        List userBagPacks = userBagPackService.fetchBagPack(user.getId());
        Assert.assertEquals(1, userBagPacks.size());

        Map response = userBagPackService.checkAvailabilityOfTickets(userBagPacks);
        System.out.println(response);
        Assert.assertNotNull(response);
    }*/
}
