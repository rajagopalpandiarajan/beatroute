package com.beatroute.api.service;

import com.beatroute.api.elasticsearch.service.ElasticSearchService;
import com.beatroute.api.model.Event;
import com.beatroute.api.model.Session;
import com.beatroute.api.repository.EventRepository;
import com.beatroute.api.service.builder.EventBuilder;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Slf4j
public class EventServiceTest {

    @Autowired
    private EventBuilder eventBuilder;

    @Autowired
    private EventService eventService;

    @Autowired
    private ElasticSearchService elasticSearchService;

    @Autowired
    private EventRepository eventRepository;

    @Test
    public void should_save_event_and_return_success() throws Exception {
        // create event with venues
        Event event = eventBuilder.getSampleEvent();

        // save event and index it on elasticsearch
        eventService.save(event);
        Assert.assertNotNull(event.getId());

        // delete event
        eventService.delete(event.getId(), event.getEventReferenceId());
    }

    @Test
    public void should_update_a_saved_event_and_return_success() throws Exception {

        // create event with venues
        Event event = eventBuilder.getSampleEvent();
        // save event and index it on elasticsearch
        eventService.save(event);
        Assert.assertNotNull(event.getId());

        String uniqueDescription = UUID.randomUUID().toString();
        event.setDescription(uniqueDescription);

        // update the event
        eventService.update(event);
        Event eventJson = (Event) eventService.get(event.getEventReferenceId());
        Assert.assertNotNull(eventJson);
        //Assert.assertTrue(eventJson.contains(uniqueDescription));

        // delete event
        eventService.delete(event.getId(), event.getEventReferenceId());

    }

    @Test
    public void get_sessions_on_demand(){
        Map<String, Object> response = eventService.getSessions("3CVA", "22-01-2017", false);
        log.info("Event Reference Id - "+response.get("eventReferenceId"));
        log.info("Sessions - "+response.get("sessions"));
        List<Session> sessionList = (List) response.get("sessions");
        for(Session session : sessionList){
            log.info("Session ID - "+session.getId());
            log.info("Session Name - "+session.getName());
            log.info("Session Begin Date Time - "+session.getBeginDateTime());
            log.info("");
        }
        Assert.assertNotNull(response);
        Assert.assertEquals("success",response.get("sessions"));
    }

    @Test
    public void get_index_enterprise_events_on_enterprise_id() throws Exception {
        Long enterpriseId = Long.valueOf(2);
        eventService.indexEnterpriseEvents(enterpriseId);

        //Map<String, Object> response = eventService.indexEnterpriseEvents(enterpriseId);
        //Assert.assertNotNull(response);
        //Assert.assertEquals("success",response.get("sessions"));
    }

    @Test
    public void sync_single_event_to_elastic() throws Exception{
        String eventReferenceId = "2W8I";
        Event event = this.eventRepository.findOneByEventReferenceId(eventReferenceId);
        elasticSearchService.update(event, eventReferenceId);
    }
}
