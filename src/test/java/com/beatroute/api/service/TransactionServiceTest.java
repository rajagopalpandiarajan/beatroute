package com.beatroute.api.service;

import com.beatroute.api.model.*;
import com.beatroute.api.model.enumeration.TransactionStatus;
import com.beatroute.api.repository.EventRepository;
import com.beatroute.api.repository.SessionRepository;
import com.beatroute.api.repository.TransactionRepository;
import com.beatroute.api.repository.UserRepository;
import com.beatroute.api.service.builder.*;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class TransactionServiceTest {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private UserBuilder userBuilder;

    @Autowired
    private EventBuilder eventBuilder;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SecureTicketBuilder secureTicketBuilder;

    private User user;

    @Before
    public void setup() {
        // create user
        user = userBuilder.getAppUser();
        user.setMobile("9620480673");
        userRepository.save(user);
    }

    @Test
    public void should_save_transaction_for_user_successfully() throws Exception {
        Assert.assertNotNull(user);
        List<SecureTicket> secureTickets = new ArrayList<>();
        String paymentReferenceId = String.valueOf(System.nanoTime());
        secureTickets.add(buildSecureTicket(paymentReferenceId));
        secureTickets.add(buildSecureTicket(paymentReferenceId));

        transactionService.saveUserTransactions(secureTickets, String.valueOf(System.nanoTime()));
        Assert.assertNotNull(transactionService.getTransaction(secureTickets.get(0)));
        Assert.assertNotNull(transactionService.getTransaction(secureTickets.get(1)));
    }

    @Test
    public void should_get_transaction_successfully(){
        String paymentReferenceId = String.valueOf(System.nanoTime());
        Transaction transaction = transactionService.getTransaction(buildSecureTicket(paymentReferenceId));
        Assert.assertNotNull(transaction);
    }

    private SecureTicket buildSecureTicket(String paymentReferenceId){
        //create event
        Event event = eventBuilder.getSampleEvent();
        eventRepository.save(event);

        SecureTicket secureTicket = secureTicketBuilder.getSecureTicket().withUser(user).withEvent(event).withCreatedDate().withUpdatedDate().withExpiryDateTime().withLockRequestId().withLockStatus(true).withPaymentReferenceId(paymentReferenceId).withSessionReferenceId(event.getSessions().iterator().next().getSessionReferenceId()).withTicketBookingPrice(new BigDecimal(10.00)).withTicketGroupId(String.valueOf(System.nanoTime())).withNoOfTickets(2).build();
        return secureTicket;
    }

}
