package com.beatroute.api.service;

import com.beatroute.api.model.Event;
import com.beatroute.api.model.User;
import com.beatroute.api.service.builder.EventBuilder;
import com.beatroute.api.service.builder.TransactionWishListBuilder;
import com.beatroute.api.service.builder.UserBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class UserServiceTest {

    @Autowired
    private UserBuilder userBuilder;

    @Autowired
    private EventBuilder eventBuilder;

    @Autowired
    private TransactionWishListBuilder transactionWishListBuilder;

    @Autowired
    private EventService eventService;

    @Autowired
    private UserService userService;

    @Test
    public void should_save_app_user_and_return_success(){
        User user = userBuilder.getAppUser();

        user = this.userService.save(user);
        Assert.assertNotNull(user.getId());

        this.userService.delete(user.getId());
    }

    @Test
    public void should_save_oauth_fb_user_and_return_success(){
        User user  = userBuilder.getOauthFBUser();

        user = this.userService.save(user);
        Assert.assertNotNull(user.getId());

        this.userService.delete(user.getId());
    }

    @Test
    public void should_save_oauth_google_user_and_return_success(){
        User user = userBuilder.getOauthGoogleUser();

        user = this.userService.save(user);
        Assert.assertNotNull(user.getId());

        this.userService.delete(user.getId());
    }

    @Test
    public void should_save_user_wish_list_and_return_success() throws Exception {
        User user = userBuilder.getAppUser();
        Event event = eventBuilder.getSampleEvent();

        this.eventService.save(event);
        user = this.userService.save(user);

        user = this.userService.addWishList(user.getId(), event.getId());

        Assert.assertEquals(1, user.getWishlist().size());

        this.userService.delete(user.getId());
        this.eventService.delete(event.getId(), event.getEventReferenceId());
    }
}
