package com.beatroute.api.repository;

import com.beatroute.api.model.Event;
import com.beatroute.api.service.builder.EventBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class EventRepositoryTest {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventBuilder eventBuilder;

    @Test
    public void should_save_event_and_sessions_and_venues_return_success() throws Exception {
        // create event with venues
        Event event = eventBuilder.getSampleEvent();

        // save event and index it on elasticsearch
        eventRepository.save(event);
        Assert.assertNotNull(event.getId());
    }

}
