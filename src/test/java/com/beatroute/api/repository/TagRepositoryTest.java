package com.beatroute.api.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TagRepositoryTest {

    @Autowired
    private TagRepository tagRepository;

    @Test
    public void should_return_valid_tags() throws Exception {
        List<String> tags = tagRepository.findByNameThatStartsWith("the", new Long(1));
        Assert.assertNotNull(tags);
    }
}
