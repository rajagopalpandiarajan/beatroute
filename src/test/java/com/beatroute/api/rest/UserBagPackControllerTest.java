/*
package com.beatroute.api.rest;

import com.beatroute.api.BeatrouteApiApplication;
import com.beatroute.api.model.User;
import com.beatroute.api.service.UserBagPackService;
import com.beatroute.api.service.UserService;
import com.beatroute.api.service.builder.UserBuilder;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BeatrouteApiApplication.class)
@SpringBootTest
@WebAppConfiguration
@Transactional
public class UserBagPackControllerTest {

    @Value("${local.server.port}")   // 6
    int port;

    private MockMvc mockMvc;

    @Autowired
    private UserBuilder userBuilder;

    @Autowired
    private UserService userService;

    @Autowired
    private UserBagPackService userBagPackService;

    private User user;

    @Before
    public void setup(){
        user = userBuilder.getAppUser();

        user = this.userService.save(user);
        Assert.assertNotNull(user.getId());
    }

    private String createUserBagPack(Long id){
        JSONObject obj = new JSONObject();
        obj.put("id", null);
        obj.put("userId", id);
        obj.put("jsonData", " {\"currency\": \"USD\",\"eventId\": 52,\"numberOfTickets\": \"2\",\"row\": \"330\",\"seatNo\": \"\",\"section\": \"K\",\"sessionReferenceId\": \"2690824\",\"supplierName\": \"\",\"ticketBookingPrice\": 160,\"ticketGroupId\": \"1674655839\"}");
        return obj.toJSONString();
    }

    @Test
    public void should_add_event_to_userbagpack(){
        try {
            when(userBagPackService.addToUserBagPack(String.valueOf(user.getId())));

            mockMvc.perform(get("/getBagPack/{id}")).andExpect(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
*/
