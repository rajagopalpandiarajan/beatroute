package com.beatroute.api.elasticsearch;

import com.beatroute.api.elasticsearch.service.ElasticSearchService;
import com.beatroute.api.model.Event;
import com.beatroute.api.repository.EventRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ElasticSearchServiceTest {

    @Autowired
    private ElasticSearchService elasticSearchService;

    @Autowired
    private EventRepository eventRepository;

    @Test
    public void should_add_to_elastic_search_index_and_return_success() throws Exception {
        // create event
        Event event = new Event();
        event.setName("New Test Event 1");

        // save event
        elasticSearchService.save(event, String.valueOf(1));

        // get saved event and validate
        String result = elasticSearchService.get(String.valueOf(1));
        Assert.assertTrue(result.contains("New Test Event"));

        // delete event
        elasticSearchService.delete(String.valueOf(1));
    }

    @Test
    public void should_get_elastic_search_results_and_return_success() throws Exception {
        // create event
        Event event = new Event();
        event.setName("New Test Event ten");

        // save event
        elasticSearchService.save(event, String.valueOf(10));

        // get event and validate
        String result = elasticSearchService.get(String.valueOf(10));
        Assert.assertNotNull(result);
        Assert.assertTrue(result.contains("New Test Event ten"));

        // delete event
        elasticSearchService.delete(String.valueOf(10));
    }

    @Test
    public void should_update_to_elastic_search_index_and_return_success() throws Exception {
        // create event
        Event event = new Event();
        event.setName("New Test Event");

        // save event
        elasticSearchService.save(event, String.valueOf(101));

        // update event
        event.setName("New Updated Test Event");
        elasticSearchService.update(event, String.valueOf(101));

        // get updated event
        String result = elasticSearchService.get(String.valueOf(101));
        Assert.assertTrue(result.contains("New Updated Test Event"));

        // delete event
        elasticSearchService.delete(String.valueOf(101));
    }

    @Test
    public void should_delete_from_elastic_search_and_return_success() throws Exception {
        // create event
        Event event = new Event();
        event.setName("Delete Test Event");

        // save event
        elasticSearchService.save(event, String.valueOf(100));

        // delete event
        elasticSearchService.delete(String.valueOf(100));

        // get deleted event
        String result = elasticSearchService.get(String.valueOf(100));
        Assert.assertNull(result);
    }

    @Test
    public void get_DB_event_should_update_to_elastic_search_index_and_return_success() throws Exception {

        Event event = this.eventRepository.findOneByEventReferenceId("25DR");
        elasticSearchService.update(event, "25DR");

        // get updated event
        String result = elasticSearchService.get("25DR");
        Assert.assertTrue(result.contains("25DR"));

    }
}
