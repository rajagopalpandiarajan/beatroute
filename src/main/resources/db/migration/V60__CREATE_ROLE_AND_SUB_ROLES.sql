ALTER TABLE users ADD COLUMN tabs_active VARCHAR(250) NULL DEFAULT NULL AFTER enterprise_id;

CREATE TABLE tabs_primary (
  id    BIGINT AUTO_INCREMENT PRIMARY KEY,
  tabs VARCHAR(200) NOT NULL UNIQUE,
  alias VARCHAR(200) NOT NULL
  );


INSERT INTO tabs_primary VALUES
(1,'Events','Events'),
(2,'Destinations','Destinations'),
(3,'Promotions','Promotions'),
(4,'Reports','Reports'),
(5,'Customer_info_Management','Customer info Management'),
(6,'Admin_User_Management','Admin User Management'),
(7,'Enterprise','Enterprise'),
(8,'FAQs','FAQs');

CREATE TABLE tabs_secondary (
  id    BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  tabs VARCHAR(200) NOT NULL UNIQUE,
  alias VARCHAR(200) NOT NULL,
  tabs_primary_id BIGINT NOT NULL,
  FOREIGN KEY (tabs_primary_id) REFERENCES tabs_primary (id)
) ;


INSERT INTO `tabs_secondary` VALUES
(1,'Add_an_Event','Add an Event',1),
(2,'Search_and_Edit_an_event','Search and Edit an event',1),
(3,'Event_Status','Event Status',1),
(4,'Inventory_Management','Inventory Management',1),
(5,'Add_Categories','Add Categories',1),
(6,'Categories_List','Categories List',1),
(7,'Unmapped_Categories','Unmapped Categories',1),
(8,'Add_Countries','Add Countries',2),
(9,'List_of_Countries_&_Edit','List of Countries & Edit',2),
(10,'Add_Cities','Add Cities',2),
(11,'List_of_Cities_&_Edit','List of Cities & Edit',2),
(12,'Unrecognized_Destinations','Unrecognized Destinations',2),
(13,'Top_Picks_for_Home_page','Top Picks for Home page',3),
(14,'Add_Category_to_home','Add Category to home',3),
(15,'Add_events_to_a_Category','Add events to a Category',3),
(16,'Home_Page_Primary_Banner','Home Page Primary Banner',3),
(17,'Promotion_Widget','Promotion Widget',3),
(18,'Top_Cities_Promotion','Top Cities Promotion',3),
(19,'Customer_Service_Reports','Customer Service Reports',4),
(20,'Transaction_Report','Transaction Report',4),
(21,'Enterprise_Report','Enterprise Report',4),
(22,'Supplier_Paid_Report','Supplier Paid Report',4),
(23,'Supplier_Summary_Report','Supplier Summary Report',4),
(24,'User_Reports','User Reports',5),
(25,'Add_Admin_User','Add Admin User',6),
(26,'List_of_Admin_&_Edit_rights ','List of Admin & Edit rights ',6),
(27,'Setup','Setup',7),
(28,'List_of_Enterprises','List of Enterprises',7);