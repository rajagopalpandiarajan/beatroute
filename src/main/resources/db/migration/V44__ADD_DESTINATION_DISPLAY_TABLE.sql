drop table if exists destination_display;

CREATE TABLE destination_display (
  id BIGINT NOT NULL AUTO_INCREMENT,
  country VARCHAR(200) DEFAULT NULL,
  country_alias LONGTEXT,
  state VARCHAR(200) DEFAULT NULL,
  state_alias LONGTEXT,
  city VARCHAR(200) DEFAULT NULL,
  city_alias LONGTEXT,
  image VARCHAR(256) DEFAULT NULL,
  video VARCHAR(256) DEFAULT NULL,
  description LONGTEXT,
  user_interest_destinations LONGTEXT,
  created_date DATETIME NOT NULL,
  updated_date DATETIME NOT NULL,
  PRIMARY KEY (id)
) ;
