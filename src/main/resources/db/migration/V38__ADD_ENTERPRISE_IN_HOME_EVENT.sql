ALTER TABLE home_event ADD COLUMN enterprise_id BIGINT NOT NULL DEFAULT 1,
ADD FOREIGN KEY home_event_enterprise(enterprise_id) REFERENCES enterprise(id);
