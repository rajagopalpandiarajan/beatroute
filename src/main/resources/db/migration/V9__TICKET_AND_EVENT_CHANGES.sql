ALTER TABLE events DROP COLUMN generic_detail;

ALTER TABLE events ADD COLUMN generic_detail_title VARCHAR(200) NULL DEFAULT NULL;

ALTER TABLE events ADD COLUMN generic_detail_description LONGTEXT NULL DEFAULT NULL;

ALTER TABLE events ADD COLUMN generic_detail_image VARCHAR(200) NULL DEFAULT NULL;

DROP TABLE IF EXISTS tickets;

DROP TABLE IF EXISTS price;

CREATE TABLE tickets (
  id   BIGINT AUTO_INCREMENT PRIMARY KEY,
  ticket_type VARCHAR(200) NOT NULL,
  ticket_category VARCHAR(200) NOT NULL,
  ticket_inclusion VARCHAR(200) NULL,
  ticket_usp VARCHAR(200) NULL,
  ticket_usp_icon_url VARCHAR(200) NULL,
  age_validation VARCHAR(200) NULL,
  total_quantity INTEGER NOT NULL,
  min_quantity INTEGER NULL,
  max_quantity INTEGER NULL,
  tickets_remaining INTEGER NULL,
  event_id BIGINT NOT NULL,
  session_id BIGINT NULL,
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW(),
  FOREIGN KEY tickets_events(event_id) REFERENCES events(id),
  FOREIGN KEY tickets_sessions(session_id) REFERENCES sessions(id)
);

CREATE TABLE price(
  id   BIGINT AUTO_INCREMENT PRIMARY KEY,
  whole_sale_price DECIMAL(10,4) NOT NULL,
  retail_price DECIMAL(10,4) NOT NULL,
  currency VARCHAR(200) NOT NULL,
  session_id BIGINT NOT NULL,
  ticket_id BIGINT NOT NULL,
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW(),
  FOREIGN KEY price_session(session_id) REFERENCES sessions(id),
  FOREIGN KEY price_tickets(ticket_id) REFERENCES tickets(id)
);
