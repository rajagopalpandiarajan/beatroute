ALTER TABLE sessions MODIFY COLUMN session_reference_id TEXT;

ALTER TABLE secure_tickets MODIFY COLUMN session_reference_id TEXT;

ALTER TABLE secure_tickets MODIFY COLUMN ticket_group_id TEXT;

ALTER TABLE venues CHANGE COLUMN city city VARCHAR(200) NULL ,CHANGE COLUMN state state VARCHAR(200) NULL ;
