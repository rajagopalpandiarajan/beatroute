alter table transactions modify column ticket_booking_price DECIMAL(10,4);
alter table transactions add column ticket_reference_number VARCHAR(200);
alter table secure_tickets add column ticket_reference_number VARCHAR(200);
alter table secure_tickets add column order_reference_number VARCHAR(200);
