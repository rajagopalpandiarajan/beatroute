CREATE TABLE recommendation_events (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  priority int(11) DEFAULT '1',
  event_name varchar(255) NOT NULL,
  event_reference_id varchar(255) NOT NULL,
  enterprise_id bigint(20) NOT NULL DEFAULT '1',
  created_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  recommendation_type varchar(200) NOT NULL,
  recommendation_name varchar(200) NOT NULL,
  PRIMARY KEY (id),
  KEY enterprise_id (enterprise_id),
  CONSTRAINT  FOREIGN KEY (enterprise_id) REFERENCES enterprise (id)
);
