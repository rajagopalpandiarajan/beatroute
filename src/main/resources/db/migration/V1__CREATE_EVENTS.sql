DROP TABLE IF EXISTS events_tenants;
DROP TABLE IF EXISTS tenants;
DROP TABLE IF EXISTS events_tags;
DROP TABLE IF EXISTS tags;
DROP TABLE IF EXISTS sessions;
DROP TABLE IF EXISTS venues;
DROP TABLE IF EXISTS events;
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS wishlists;
DROP TABLE IF EXISTS transactions;
DROP TABLE IF EXISTS navigator;

CREATE TABLE categories (
  id       BIGINT AUTO_INCREMENT PRIMARY KEY,
  name     VARCHAR(200) NOT NULL UNIQUE,
  description VARCHAR(500) NULL,
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW()
);

CREATE TABLE events (
  id       BIGINT AUTO_INCREMENT PRIMARY KEY,
  name     VARCHAR(200) NOT NULL UNIQUE,
  primary_category_id BIGINT NOT NULL,
  secondary_category_id BIGINT NULL,
  supplier_reference_id VARCHAR(50) NULL,
  supplier VARCHAR(50) NULL,
  description LONGTEXT NULL,
  generic_detail VARCHAR(500) NULL,
  venue_map VARCHAR(800) NULL,
  image_gallery TEXT NULL,
  video_gallery VARCHAR(500) NULL,
  event_terms_and_conditions VARCHAR(500) NULL,
  notes VARCHAR(500) NULL,
  event_reference_id VARCHAR(50) NOT NULL,
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW(),
  status  BIGINT NOT NULL,
  FOREIGN KEY events_primay_category(primary_category_id) REFERENCES categories(id),
  FOREIGN KEY events_secondary_category(secondary_category_id) REFERENCES categories(id)
);

CREATE TABLE sessions (
  id   BIGINT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(200) NOT NULL,
  begin_date_time TIMESTAMP NOT NULL,
  end_date_time DATETIME(3),
  event_id   BIGINT NOT NULL,
  description VARCHAR(500) NULL,
  duration INTEGER NULL,
  usp VARCHAR(500) NULL,
  rating VARCHAR(10) NULL,
  session_reference_id VARCHAR(50) NOT NULL,
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW(),
  FOREIGN KEY sessions(event_id) REFERENCES events(id)
);

CREATE TABLE venues (
  id   BIGINT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(200) NOT NULL,
  description VARCHAR(500) NULL,
  address VARCHAR(200) NOT NULL,
  city VARCHAR(200) NOT NULL,
  state VARCHAR(200) NOT NULL,
  country VARCHAR(200) NOT NULL,
  geo_location VARCHAR(200) NULL,
  url VARCHAR(200) NULL,
  zip_code VARCHAR(200) NULL,
  venue_reference_id VARCHAR(50) NOT NULL,
  session_id   BIGINT NOT NULL,
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW(),
  FOREIGN KEY venues(session_id) REFERENCES sessions(id)
);

CREATE TABLE tenants (
  id       BIGINT AUTO_INCREMENT PRIMARY KEY,
  name     VARCHAR(200) NOT NULL,
  api_key  VARCHAR(50) NOT NULL UNIQUE,
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW()
);

CREATE TABLE events_tenants (
  event_id BIGINT NOT NULL,
  tenant_id BIGINT NOT NULL,
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW(),
  FOREIGN KEY events_tenants_event(event_id) REFERENCES events(id),
  FOREIGN KEY events_tenants_tenant(tenant_id) REFERENCES tenants(id)
);

CREATE TABLE tags (
  id       BIGINT AUTO_INCREMENT PRIMARY KEY,
  name     VARCHAR(200) NOT NULL UNIQUE,
  description  VARCHAR(200) NULL,
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW()
);

CREATE TABLE events_tags (
  event_id BIGINT NOT NULL,
  tag_id BIGINT NOT NULL,
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW(),
  FOREIGN KEY events_tags_event(event_id) REFERENCES events(id),
  FOREIGN KEY events_tags_tag(tag_id) REFERENCES tags(id)
);

CREATE TABLE users (
  id   BIGINT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(200) NOT NULL,
  email VARCHAR(200),
  password VARCHAR(200),
  country VARCHAR(200),
  user_type VARCHAR(200) NOT NULL,
  authorised TINYINT(1) NOT NULL,
  oauth_id VARCHAR(200),
  profile_pic VARCHAR(200),
  mobile VARCHAR(200),
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW(),
  address VARCHAR(200),
  city VARCHAR(200)
);

CREATE TABLE transactions (
  id   BIGINT AUTO_INCREMENT PRIMARY KEY,
  user_id  BIGINT NOT NULL,
  event_id BIGINT NOT NULL,
  session_id VARCHAR(200) NOT NULL,
  amount  DECIMAL(10,4) NOT NULL,
  currency VARCHAR(200) NOT NULL,
  mercury_order_id VARCHAR(200),
  number_of_tickets BIGINT NOT NULL,
  ticket_booking_price BIGINT NOT NULL,
  payment_reference_id VARCHAR(200) NOT NULL,
  transaction_status VARCHAR(200),
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW(),
  FOREIGN KEY transactions_users(user_id) REFERENCES users(id),
  FOREIGN KEY transactions_events(event_id) REFERENCES events(id)
);

CREATE TABLE wishlists (
  id   BIGINT AUTO_INCREMENT PRIMARY KEY,
  user_id BIGINT NOT NULL,
  event_id BIGINT NOT NULL,
  FOREIGN KEY wishlists_users(user_id) REFERENCES users(id),
  FOREIGN KEY wishlists_events(event_id) REFERENCES events(id)
);

CREATE TABLE navigator (
  id   BIGINT AUTO_INCREMENT PRIMARY KEY,
  name     VARCHAR(200) NOT NULL UNIQUE,
  description LONGTEXT,
  document LONGBLOB
);

CREATE TABLE bagpack (
 id   BIGINT AUTO_INCREMENT PRIMARY KEY,
 user_id  BIGINT NOT NULL ,
 json_data LONGTEXT,
 FOREIGN KEY (user_id) REFERENCES users(id)
);

INSERT INTO navigator(id, name, description, document) VALUES(1, 'CONCERTS', 'A concert is a live music performance in front of an audience. Beatroute offers to you concerts of various kinds and genres. We have a wide array of live concerts in rock, pop, hip-hop and practically any other popular genre as well as theatrical concerts, benefit concerts and the major music festivals of the world.', '[{
		"name": "MAROON 5",
		"document_id": ""
	}, {
		"name": "SANTANA",
		"document_id": ""
	}, {
		"name": "JUSTIN BEIBER",
		"document_id": ""
	}, {
		"name": "GUNS N'' ROSES",
		"document_id": ""
	}, {
		"name": "ADELE",
		"document_id": ""
	}, {
		"name": "RIOT FEST",
		"document_id": ""
	}, {
		"name": "PITBULL",
		"document_id": ""
	}, {
		"name": "LIONEL RICHIE",
		"document_id": ""
	}, {
		"name": "KISS",
		"document_id": ""
	}, {
		"name": "ELTON JOHN",
		"document_id": ""
	}
]');


INSERT INTO navigator(id, name, description, document) VALUES(2, 'SPORTS', 'Sport can be traced back to ancient Greece. It was an era of admiration for the healthy human body and this passion led to the world''s first athletic meet-The Olympic Games. This passion continues in Beatroute as well. We have on offer all the major sports tournaments like Major League Baseball (MLB), the National Football League (NFL), American college sports, the National Basketball Association (NBA), Hockey, Football, Rugby and a whole lot more!', '[{
	"title": "WWE",
	"list": [{
		"name": "WWE Smackdown",
		"document_id": "203375"
	}, {
		"name": "WWE: Survivor Series",
		"document_id": "219973"
	}, {
		"name": "WWE: Monday Night Raw",
		"document_id": "232886"
	}, {
		"name": "WWE: Raw",
		"document_id": "188398"
	}, {
		"name": "WWE: Smackdown",
		"document_id": "188394"
	}, {
		"name": "WWE: Live",
		"document_id": "188407"
	}]
},
{
	"title": "US OPEN",
	"list": [{
		"name": "US Open Tennis Championship: Session 21 - Women’s Semifinals",
		"document_id": "189060"
	}, {
		"name": "US Open Tennis Championship: Session 20 - Men’s Quarterfinals",
		"document_id": "189061"
	}, {
		"name": "US Open Tennis Championship: Session 22 - Men’s Semifinals",
		"document_id": "189059"
	}, {
		"name": "US Open Tennis Championship: Grounds Pass",
		"document_id": "189062"
	}, {
		"name": "US Open Tennis Championship: Session 24 - Men’s Final/Women’s Doubles Final",
		"document_id": "189057"
	}, {
		"name": "US Open Tennis Championship: Session 23 - Women’s Final/Men’s Doubles Final",
		"document_id": "189058"
	}]
}, {
	"title": "FORMULA ONE",
	"list": [{
		"name": "IPL",
		"document_id": ""
	}, {
		"name": "T-20",
		"document_id": ""
	}, {
		"name": "World Cup",
		"document_id": ""
	}]
}, {
	"title": "UFC",
	"list": [{
		"name": "IPL",
		"document_id": ""
	}, {
		"name": "T-20",
		"document_id": ""
	}, {
		"name": "World Cup",
		"document_id": ""
	}]
}, {
	"title": "ATP WORLD TOUR FINAL",
	"list": [{
		"name": "IPL",
		"document_id": ""
	}, {
		"name": "T-20",
		"document_id": ""
	}, {
		"name": "World Cup",
		"document_id": ""
	}]
}, {
	"title": "MANCHESTER UNITED FC",
	"list": [{
		"name": "IPL",
		"document_id": ""
	}, {
		"name": "T-20",
		"document_id": ""
	}, {
		"name": "World Cup",
		"document_id": ""
	}]
}, {
	"title": "NOVAK DJOKOVIC",
	"list": [{
		"name": "IPL",
		"document_id": ""
	}, {
		"name": "T-20",
		"document_id": ""
	}, {
		"name": "World Cup",
		"document_id": ""
	}]
}, {
	"title": "FIFA WORLD CUP QUALIFIER",
	"list": [{
		"name": "IPL",
		"document_id": ""
	}, {
		"name": "T-20",
		"document_id": ""
	}, {
		"name": "World Cup",
		"document_id": ""
	}]
}, {
	"title": "FIFA WORLD CUP QUALIFIER",
	"list": [{
		"name": "IPL",
		"document_id": ""
	}, {
		"name": "T-20",
		"document_id": ""
	}, {
		"name": "World Cup",
		"document_id": ""
	}]
},
{
	"title": "NBA",
	"list": []
}
]');

INSERT INTO navigator(id, name, description, document) VALUES(3, 'THEATERS', 'Audiences have been enjoying theatre performances for well over 2500 years. Theatre probably arose as a performance of ritual activities that did not require initiation on the part of the spectator. According to the historians Oscar Brockett and Franklin Hildy, rituals typically include elements that entertain or give pleasure, such as costumes and masks as well as skilled performers. As societies grew more complex, these spectacular elements began to be acted out under non-ritualistic conditions. As this occurred, the first steps towards theatre as an autonomous activity were being taken.
Theatre today is much more refined and advanced, and Beatroute has it all. From Broadway to Cirque de Soleil to Opera,and so much more!', '[{
	"name": "WICKED",
	"document_id": ""
}, {
	"name": "THE WIZARD OF OZ",
	"document_id": ""
}, {
	"name": "THE SOUND OF MUSIC",
	"document_id": ""
}, {
	"name": "THE LION KING",
	"document_id": ""
}, {
	"name": "THE BOOK OF MORMON",
	"document_id": ""
}, {
	"name": "PHANTOM OF THE OPERA",
	"document_id": ""
}, {
	"name": "MOTOWN-THE MUSICAL",
	"document_id": ""
}, {
	"name": "MAMMA MIA!",
	"document_id": ""
}, {
	"name": "ALADDIN",
	"document_id": ""
}, {
	"name": "LES MISERABLES",
	"document_id": ""
}]');

INSERT INTO navigator(id, name, description, document) VALUES(4, 'SIGHTSEEING', 'description of sight seeings', '[{
	"name": "Alabama",
	"document_id": ""
}, {
	"name": "Alaska",
	"document_id": ""
}, {
	"name": "Arizona",
	"document_id": ""
}, {
	"name": "Arkansas",
	"document_id": ""
}, {
	"name": "California",
	"document_id": ""
}, {
	"name": "Colorado",
	"document_id": ""
}, {
	"name": "Connecticut",
	"document_id": ""
}, {
	"name": "Delaware",
	"document_id": ""
}, {
	"name": "Florida",
	"document_id": ""
}, {
	"name": "Georgia",
	"document_id": ""
}, {
	"name": "Hawaii",
	"document_id": ""
}, {
	"name": "Idaho",
	"document_id": ""
}]');

INSERT INTO navigator(id, name, description, document) VALUES(5, 'DESTINATION', '', '[
	{
		"name" : "Toronto",
		"description" : "Toronto , the provincial capital of Ontario is also known as New York City run by the Swiss.With its many districts and ethnic diversity , the city caters to all kinds of travellers.Toronto is  widely recognized as one of the most multicultural and cosmopolitan cities in the world.It can be the perfect place for art lovers to adrenaline junkies.When the climate is fine, Toronto is an energetic, big-time city swirling with action.The city is a playground for your taste buds,with so many restaurants to choose from.Canada is a multicultural country and Toronto’s restaurants reflect that. Be sure to try some of the ethnically infused dishes during your stay.",
		"url" : "https://www.youtube.com/embed/CKIqlrpn3bs"
	},
	{
		"name" : "HongKong",
		"description" : "Hong Kong is a city, and former British colony, in southeastern China. Vibrant and densely populated, it’s a major port and global financial center famed for its tower-studded skyline. It’s also known for its lively food scene – from Cantonese dim sum to extravagant high tea – and its shopping, with options spanning chaotic Temple Street Night Market to the city’s innumerable bespoke tailors.",
		"url" : "https://www.youtube.com/embed/NLmz0_oN4pY"
	},
	{
		"name" : "Paris",
		"description" : " If you are lucky enough to have lived in Paris as a young man, then wherever you go for the rest of your life, it stays with you, for Paris is a moveable feast.  - Ernest HemingwayAlso known as ’The City of Light’ ,It is commended for its design, historical centers, Cathedrals , parks, shopping malls , flea marts, bistros’ and for the wide and lavish avenues. The exquisite cuisine of Paris is world known. It was once an island amidst the Seine River, the Ile de la Cité, Paris, established over 2,000 years prior, rapidly spread to both banks of the waterway.The right bank is known for being the business heart of the city while the left bank is home to the University of Paris and all that is scholarly.Paris is also known for being a shopper’s paradise. There are numerous flea markets for the budget minded shoppers and boutiques that cater to the high end market. Leather goods, paintings, perfumes , clothing are available in plenty. Along the banks of the Seine are likewise numerous craftsmen and book shops. On Sunday mornings close Notre Dame Cathedral, one can visit the birds market.. Live confined canaries, finches, and other birds are available to be purchased.  A standout amongst the most novel stores, right around an absolute necessity for travelers, is Le Drugstore. This is a Parisian’s concept of an American drugstore. It has some extravagant products available to be purchased, and also toiletries; be that as it may, the toiletries are excessively costly there for the normal tourist.Paris is surrounded by greenery all over. Bois de Boulogne has more than 809 acres of land , which boasts of beautiful walking trails, a lake and an amusement park for children. Tuileries is a famous garden which is located in front of the world famous art  museum, Louvre. Paris is considered to be a moving city, the city which walks. There are numerous fountains and benches on which tourists can sit and marvel at the architectural genius of the city which is like an open gallery.",
		"url" : "https://www.youtube.com/embed/A2gZYIMp2oc"
	},
	{
		"name" : "London",
		"description" : "London, England’s capital, set on the River Thames, is a 21st-century city with history stretching back to Roman times. At its centre stand the imposing Houses of Parliament, the iconic ‘Big Ben’ clock tower and Westminster Abbey, site of British monarch coronations. Across the Thames, the London Eye observation wheel provides panoramic views of the South Bank cultural complex, and the entire city.",
		"url" : "https://www.youtube.com/embed/PtWeqZsuzpE"
	},
	{
		"name" : "Singapore",
		"description" : "Singapore, an island city-state off southern Malaysia, is a global financial centre with a tropical climate and multicultural population.",
		"url" : "https://www.youtube.com/embed/XTjDU4TiPlg"
	},
	{
		"name" : "Madrid",
		"description" : "Madrid, Spain’s central capital, is a city of elegant boulevards and expansive, manicured parks such as the Buen Retiro. It’s renowned for its rich repositories of European art, including the Prado Museum’s works by Goya, Velázquez and other Spanish masters. The heart of old Hapsburg Madrid is the portico-lined Plaza Mayor, and nearby is the baroque Royal Palace and Armory, displaying historic weaponry.",
		"url" : "https://www.youtube.com/embed/5cGGJ-5kW6g"
	},
	{
		"name" : "Cornellà de Llobregat",
		"description" : "Cornellà de Llobregat is a municipality in the comarca of the Baix Llobregat in Catalonia, Spain. It is situated on the left bank of the Llobregat River.",
		"url" : "https://www.youtube.com/embed/lTB-izxRuNY"
	},
	{
		"name" : "Barcelona",
		"description" : "Barcelona, the cosmopolitan capital of Spain’s Catalonia region, is defined by quirky art and architecture, imaginative food and vibrant street life. It has medieval roots, seen in the maze like Gothic Quarter, but a modernist personality represented by architect Antoni Gaudí’s fantastical Sagrada Família church.",
		"url" : "https://www.youtube.com/embed/L_bgTJkFk3k"
	},
	{
		"name" : "Seville",
		"description" : "Seville is the capital of southern Spain’s Andalusia region and a hotbed for flamenco dance, especially in the Triana neighborhood. The city is known for its ornate Alcázar castle complex, built during the Moorish Almohad dynasty, and its 18th-century Plaza de Toros de la Real Maestranza bullring. The massive Gothic cathedral is the site of Christopher Columbus’s tomb and a famous minaret turned belltower, the Giralda.",
		"url" : "https://www.youtube.com/embed/cuYuEU2WED8"
	},
	{
		"name" : "Valencia",
		"description" : "The port city of Valencia is on Spain’s southeastern Orange Blossom Coast, where the Turia River meets the Mediterranean Sea. It’s known for its City of Arts and Sciences, with futuristic structures including a planetarium, an oceanarium and an interactive museum. Valencia also has several beaches, including some within nearby Albufera park, a wetlands reserve with a lake, walking trails and bird-watching.",
		"url" : "https://www.youtube.com/embed/mnu8Ohq73Tg"
	},
	{
		"name" : "Malaga",
		"description" : "Málaga is a port city on southern Spain’s Costa del Sol, known for its high-rise hotels and resorts jutting up from yellow-sand beaches. But looming over that modern skyline are the city’s 2 massive hilltop citadels, the Alcazaba and ruined Gibralfaro, remnants of Moorish rule, along with a soaring Renaissance cathedral, nicknamed La Manquita (’one-armed woman’) because one of its towers was curiously left unbuilt.",
		"url" : "https://www.youtube.com/embed/YNqMQz18HDM"
	},
	{
		"name" : "Milan",
		"description" : "Milan, a metropolis in Italy’s northern Lombardy region, is a global capital of fashion and design. Home to the national stock exchange, it’s a financial hub also known for its high-end dining and shopping. The Gothic Duomo di Milano cathedral and the Santa Maria delle Grazie convent, housing Leonardo da Vinci’s fresco ’The Last Supper’, testify to centuries of art and culture.",
		"url" : "https://www.youtube.com/embed/GFsWmH81_D4"
	},
	{
		"name" : "Rome",
		"description" : "Rome, Italy’s capital, is a sprawling, cosmopolitan city with nearly 3,000 years of globally influential art, architecture and culture on display. Ancient ruins such as the Roman Forum and the Colosseum evoke the power of the former Roman Empire. Vatican City, headquarters of the Roman Catholic Church, boasts St. Peter’s Basilica and the Vatican Museums, which house masterpieces such as Michelangelo’s Sistine Chapel frescoes.",
		"url" : "https://www.youtube.com/embed/fsLAm8TT4aw"
	},
	{
		"name" : "Turin",
		"description" : "Turin is the capital city of Piedmont in northern Italy, known for its refined architecture and cuisine. The Alps rise to the northwest of the city. Stately baroque buildings and old cafes line Turin’s boulevards and grand squares like Piazza Castello and Piazza San Carlo. Nearby is the soaring spire of the Mole Antonelliana, a 19th-century tower housing the interactive National Cinema Museum.",
		"url" : "https://www.youtube.com/embed/zz-R8Poy5u0"
	},
	{
		"name" : "Naples",
		"description" : "Naples, a big city in southern Italy, sits on the Bay of Naples near Mt. Vesuvius. Dating to the 2nd millennium B.C.E., it has centuries of important art and architecture. Its landmarks include Naples Cathedral, whose Chapel of San Gennaro is filled with frescoes and statues, and the lavish Royal Palace. Naples is also renowned for its distinctive style of thin-crust pizza.",
		"url" : "https://www.youtube.com/embed/EqAoFCVbLdA"
	},
	{
		"name" : "Leverkusen",
		"description" : "Leverkusen is a city in North Rhine-Westphalia, Germany on the eastern bank of the Rhine. To the South, Leverkusen borders the city of Cologne and to the North is the state capital Düsseldorf.",
		"url" : "https://www.youtube.com/embed/gcWMZ751eCk"
	},
	{
		"name" : "Munich",
		"description" : "Munich, the capital of Bavaria, is home to centuries-old buildings and numerous museums. The city is known for its annual Oktoberfest celebration and cavernous beer halls, including the famed Hofbräuhaus, founded in 1589. In the walkable Old Town, Marienplatz is a central square containing landmarks such as Gothic Neues Rathaus (town hall), with a popular glockenspiel show.",
		"url" : "https://www.youtube.com/embed/hCnO7OkbQ10"
	},
	{
		"name" : "Dortmund",
		"description" : "<b style=’background-color: initial;’>Dortmund</b> is an <a href=’https://en.wikipedia.org/wiki/Independent_city’>independent city</a> in <a href=’https://en.wikipedia.org/wiki/North_Rhine-Westphalia’>North Rhine-Westphalia</a>, <a href=’https://en.wikipedia.org/wiki/Germany’>Germany</a>. It is in the middle part of the state and is considered to be the administrative, commercial and cultural centre of the eastern Ruhr area.",
		"url" : "https://www.youtube.com/embed/WmevVQ6cOls"
	},
	{
		"name" : "Gelsenkirchen",
		"description" : "Gelsenkirchen is a city in North Rhine-Westphalia, Germany. It is located in the northern part of the Ruhr area. Its population in 2012 was c. 257,600.",
		"url" : "https://www.youtube.com/embed/gcWMZ751eCk"
	},
	{
		"name" : "Amsterdam",
		"description" : "Amsterdam is the Netherlands’ capital, known for its artistic heritage, elaborate canal system and narrow houses with gabled facades, legacies of the city’s 17th-century Golden Age.",
		"url" : "https://www.youtube.com/embed/cd8gLq6iZg4"
	},
	{
		"name" : "Saint-Denis",
		"description" : "Saint-Denis is a commune in the northern suburbs of Paris, France. It is located 9.4 km from the centre of Paris. Saint-Denis is a subprefecture of the department of Seine-Saint-Denis, being the seat of the arrondissement of Saint-Denis.",
		"url" : "https://www.youtube.com/embed/RgxUxAJgkAY"
	},
	{
		"name" : "Lens Agglo",
		"description" : "Lens is a commune in the Pas-de-Calais department in northern France. It is one of France’s large Picarde cities along with Lille, Valenciennes, Amiens, Roubaix, Tourcoing, Arras, and Douai. The inhabitants are called Lensois",
		"url" : "https://www.youtube.com/embed/RgxUxAJgkAY"
	},
	{
		"name" : "Bordeaux",
		"description" : "Bordeaux, in southwestern France, is a port city on the Garonne and hub of the famed wine-growing region.",
		"url" : "https://www.youtube.com/embed/ajkFWkvAG6U"
	},
	{
		"name" : "Marseille",
		"description" : "Marseille, a port city in southern France, has been a crossroads of immigration and trade since its founding by the Phoenicians in 600 B.C.E. It’s a place of tranquil squares and stepped streets, bustling 19th-century avenues and souklike markets. At its heart is the Vieux Port, where fishmongers sell their daily catch along the boat-lined quayside. La Canebière, the main thoroughfare, runs east from here.",
		"url" : "https://www.youtube.com/embed/fDQ3uCWKVlY"
	},
	{
		"name" : "Nice",
		"description" : "Nice, capital of the French Riviera, skirts the pebbly shores of the Baie des Anges. Founded by the Greeks and later a retreat for 19th-century Europe’s elite, the city today balances old-world decadence with modern urban energy. Its sunshine and liberal attitude have long attracted artists, whose work hangs in its museums. With vibrant markets and diverse restaurants, it’s also renowned for its food.",
		"url" : "https://www.youtube.com/embed/wn89rYh09MM"
	},
	{
		"name" : "Lille",
		"description" : "Lille is the capital of the Nord-Pas-de-Calais region in northern France, near the border with Belgium. A cultural hub and bustling university town today, it was once an important merchant center of French Flanders. Many Flemish influences remain in the city’s culture, cuisine and architecture",
		"url" : "https://www.youtube.com/embed/um8lCs5U1eo"
	},
	{
		"name" : "Toulouse",
		"description" : "Toulouse, capital of France’s southern Midi-Pyrénées region, is bisected by the River Garonne and sits near the Spanish border. It’s known as La Ville Rose due to the pink terra-cotta bricks used in many of its buildings. Its 17th-century Canal du Midi links the Garonne to the Mediterranean Sea, and can be traveled by boat, bike or on foot.",
		"url" : "https://www.youtube.com/embed/1QnkCbgqLZc"
	},
	{
		"name" : "Lyon",
		"description" : "Lyon, a city in France’s Rhône-Alpes region, sits at the confluence of the Rhône and Saône rivers. Its city center reflects 2,000 years of history, with a Roman amphitheater in Fourvière, medieval and Renaissance architecture in Vieux Lyon, and the modern, redeveloped Confluence district on the Presqu’île peninsula between the rivers. Traboules, covered passageways between buildings, connect Vieux Lyon and La Croix-Rousse hill.",
		"url" : "https://www.youtube.com/embed/5mjPAHfyh78"
	},
	{
		"name" : "Saint-Etienne",
		"description" : "Saint-Étienne is a city in eastern central France. It is located in the Massif Central, 50 km southwest of Lyon in the Auvergne-Rhône-Alpes region, along the trunk road that connects Toulouse with Lyon.",
		"url" : "https://www.youtube.com/embed/RgxUxAJgkAY"
	},
	{
		"name" : "Alpes-Maritimes",
		"description" : "Alpes-Maritimes is a department of the Provence-Alpes-Côte d’Azur region in the extreme southeast corner of France.",
		"url" : "https://www.youtube.com/embed/qZe1j7-HVxc"
	},
	{
		"name" : "New York",
		"description" : "Home to the Empire State Building, Times Square, Statue of Liberty and other iconic sites, New York City is a fast-paced, globally influential center of art, culture, fashion and finance. The city’s 5 boroughs sit where the Hudson River meets the Atlantic Ocean, with the island borough of Manhattan at the “Big Apple’s’ core.",
		"url" : "https://www.youtube.com/embed/MtCMtC50gwY"
	},
	{
		"name" : "Berlin",
		"description" : "Berlin, Germany’s capital and cultural center, dates to the 13th century. Divided during the Cold War, today it’s known for its art scene, nightlife and modern architecture, such as Mies van der Rohe’s landmark Neue Nationalgalerie.",
		"url" : "https://www.youtube.com/embed/v=hVfBQNENS9s"
	},
	{
		"name" : "Copenhagen",
		"description" : "Copenhagen, Denmark’s capital, sits on the coastal islands of Zealand and Amager, linked to Malmo in southern Sweden by the Öresund Bridge. Indre By, the central district, contains 18th-century, rococo Frederiksstaden, home to the royal family’s Amalienborg Palace. The city’s center also has the Christiansborg parliament building and the Renaissance Rosenborg Castle, which has a museum of royal artifacts and a popular garden.",
		"url" : "https://www.youtube.com/embed/UVroe3HMlEI"
	},
	{
		"name" : "Stockholm",
		"description" : "Stockholm, the capital of Sweden, encompasses 14 islands of the vast Stockholm archipelago on the Baltic Sea. The cobblestone streets and ochre-colored buildings of medieval Gamla Stan, the old town, are home to a 13th-century cathedral, the royal palace of Kungliga Slottet and its underground armory, cafes and restaurants. Ferries and sightseeing boats shuttle passengers between islands, beneath more than 50 bridges",
		"url" : "https://www.youtube.com/embed/1uSDtfa6kqE"
	},
	{
		"name" : "Hamburg",
		"description" : "Hamburg, a major port city in northern Germany, is connected to the North Sea by the Elbe River. It’s crossed by hundreds of canals, and also contains large areas of parkland. Its central Jungfernstieg boulevard connects the Altstadt (old town) and the Neustadt, passing Binnenalster lake, dotted with boats and surrounded by cafes and restaurants. Oysters and traditional Aalsuppe (soup) are local specialties.",
		"url" : "https://www.youtube.com/embed/ICrLx5aoo9w"
	},
	{
		"name" : "Zurich",
		"description" : "The city of Zurich lies at the north end of Lake Zurich in northern Switzerland, a global center for banking and finance. The picturesque lanes of the central Altstadt (Old Town), on either side of the Limmat River, reflect its premedieval history. Waterfront promenades like the Limmatquai follow the river toward the 17th-century Rathaus (town hall).",
		"url" : "https://www.youtube.com/embed/jMlhL1q-yYM"
	},
	{
		"name" : "Glasgow",
		"description" : "Glasgow is a port city on the River Clyde in Scotland’s western Lowlands. It’s famed for its Victorian and art nouveau architecture, a rich legacy of the city’s 18th-20th-century prosperity due to trade and shipbuilding. Today it’s a national cultural hub, home to institutions including the Scottish Opera, Scottish Ballet and National Theatre of Scotland, acclaimed museums and a thriving music scene.",
		"url" : "https://www.youtube.com/embed/HklVbbq5gsw"
	},
	{
		"name" : "Manchester",
		"description" : "Manchester is a major city in the northwest of England with a rich industrial heritage. The Castlefield Conservation Area’s 18th-century canal system harks back to the city’s days as a textile powerhouse, and you can trace this history at the interactive Museum of Science & Industry. The revitalised Salford Quays dockyards now houses the Daniel Libeskind-designed Imperial War Museum North and The Lowry cultural centre.",
		"url" : "https://www.youtube.com/embed/o_TKIM1vUEs"
	},
	{
		"name" : "Netherlands",
		"description" : "The Netherlands, a country in northwestern Europe, is known for its flat landscape, canals, tulip fields, windmills and cycling routes.",
		"url" : "https://www.youtube.com/embed/p2hmux9jihI"
	},
	{
		"name" : "France",
		"description" : "France, in Western Europe, encompasses medieval and port cities, tranquil villages, mountains and Mediterranean beaches. Paris, its capital, is known worldwide for its couture fashion houses, classical art museums including the Louvre and monuments like the Eiffel Tower.",
		"url" : "https://www.youtube.com/embed/RgxUxAJgkAY"
	},
	{
		"name" : "Spain",
		"description" : "Spain, on Europe’s Iberian Peninsula, is really 17 autonomous regions, each with its own geography and culture. The capital, Madrid, is home to the Royal Palace and singular Prado museum, housing works by European masters, and Segovia to the north has a fairy-tale medieval castle and Roman aqueduct. Catalonia’s capital, Barcelona, is defined by Antoni Gaudí’s quirky modernist architecture, including the Sagrada Família basilica.",
		"url" : "https://www.youtube.com/embed/lTB-izxRuNY"
	},
	{
		"name" : "Germany",
		"description" : "Germany is a Western European country with a terrain of vast forests, rivers and mountain ranges, and 2 millennia of history. Berlin, its capital, is home to thriving art and nightlife scenes, iconic Brandenburg Gate and many sites relating to WWII.",
		"url" : "https://www.youtube.com/embed/gcWMZ751eCk"
	},
	{
		"name" : "Denmark",
		"description" : "Denmark is a country comprising the Jutland peninsula and its offshore islands, linking Northern Europe and Scandinavia via the Öresund bridge. On Zealand, the capital, Copenhagen is home to the rococo Frederiksstaden district and its royal palaces, Tivoli pleasure gardens and the “Little Mermaid” statue.",
		"url" : "https://www.youtube.com/embed/8rORVetg7NU"
	},
	{
		"name" : "United Kingdom",
		"description" : "The U.K., made up of England, Scotland, Wales and Northern Ireland, is an island nation in northwestern Europe. England – birthplace of Shakespeare and The Beatles – is home to the capital, London, a globally influential centre of finance and culture. England is also site of neolithic Stonehenge, Bath’s Roman spa and centuries-old universities at Oxford and Cambridge.",
		"url" : "https://www.youtube.com/embed/PtWeqZsuzpE"
	},
	{
		"name" : "Italy",
		"description" : "Italy, commanding a long Mediterranean coastline, has left a powerful mark on Western culture and cuisine. Its capital, Rome, is home to the Vatican as well as landmark art and ancient ruins. Other major cities include Florence, with Renaissance treasures such as Michelangelo’s ’David’ and its leather and paper artisans; Venice, the sinking city of canals; and Milan, Italy’s fashion capital.",
		"url" : "https://www.youtube.com/embed/TeVs1FeRDAw"
	},
	{
		"name" : "United States of America",
		"description" : "The U.S. is a country of 50 states covering a vast swath of North America, with Alaska in the extreme Northwest and Hawaii extending the nation’s presence into the Pacific Ocean. Major cities include New York, a global finance and culture center, and Washington, DC, the capital, both on the Atlantic Coast; Los Angeles, famed for filmmaking, on the Pacific Coast; and the Midwestern metropolis Chicago.",
		"url" : "https://www.youtube.com/embed/t-truXWH6gA"
	},
	{
		"name" : "Sweden",
		"description" : "Sweden is a Scandinavian nation of thousands of coastal islands, inland lakes, forests and mountains. Its principal cities, eastern capital Stockholm and southwestern Gothenburg and Malmö, are all on the sea. Stockholm is home to royal palaces, parkland and museums such as open-air Skansen. Its 13th-century old town, Gamla Stan, is set on islands joined by bridges and ferries.",
		"url" : "https://www.youtube.com/embed/g9QpC7QwnRU"
	},
	{
		"name" : "Canada",
		"description" : "Canada, stretching from the U.S. in the south to the Arctic Circle in the north, is filled with vibrant cities including massive, multicultural Toronto; predominantly French-speaking Montréal and Québec City; Vancouver and Halifax on the Pacific and Atlantic coasts, respectively; and Ottawa, the capital. It’s also crossed by the Rocky Mountains and home to vast swaths of protected wilderness",
		"url" : "https://www.youtube.com/embed/YQrTIC7T-mw"
	},
	{
		"name" : "Switzerland",
		"description" : "Switzerland is a mountainous Central European country, home to numerous lakes, villages and the high peaks of the Alps. Old Towns within its cities contain medieval landmarks like capital Bern’s Zytglogge clock tower and Cathedral of Bern. The country is also a destination for its ski resorts and hiking trails. Banking and finance are key industries, and Swiss watches and chocolate are renowned.",
		"url" : "https://www.youtube.com/embed/HoxTf-6pKA0"
	},
	{
		"name" : "Basel",
		"description" : "Basel is a city on the Rhine River in northwest Switzerland, close to the country’s borders with France and Germany. Its medieval old town centers around Marktplatz, dominated by the 16th-century, red-sandstone Town Hall. Its 12th-century Gothic cathedral has city views, and contains the tomb of the 16th-century Dutch scholar, Erasmus. The city’s university houses some of Erasmus’ works.",
		"url" : "https://www.youtube.com/embed/5chqC8eXRU4"
	},
	{
		"name" : "Australia",
		"description" : "Australia is a country, and continent, surrounded by the Indian and Pacific oceans. Its major cities – Sydney, Brisbane, Melbourne, Perth, Adelaide – are coastal, but its capital, Canberra, is inland and nicknamed the ’Bush Capital.’ The country is known for its Sydney Opera House, Great Barrier Reef, the vast Outback (interior desert wilderness) and unique animal species including kangaroos and duck-billed platypuses.",
		"url" : "https://www.youtube.com/embed/plao18z9JTU"
	},
	{
		"name" : "Melbourne",
		"description" : "Melbourne, Victoria’s coastal capital, is a city of stately 19th-century buildings and tree-lined boulevards. Yet at its centre is the strikingly modern Federation Square development, with plazas, bars, restaurants and cultural events along the Yarra River. In Southbank, the Melbourne Arts Precinct is site of Arts Centre Melbourne – a performing arts complex – and National Gallery of Victoria, displaying Australian and Indigenous art.",
		"url" : "https://www.youtube.com/embed/RLOsQViPLhw"
	},
	{
		"name" : "Belgium",
		"description" : "Belgium, a country in Western Europe, is known for its medieval old towns, Flemish Renaissance architecture and international headquarters of the European Union and NATO. The country is divided into 2 distinctive multilingual regions: Dutch-speaking Flanders to the north and French-speaking Wallonia to the south. The bilingual capital, Brussels, is home to ornate guildhalls at Grand-Place and an art nouveau-influenced European Quarter.",
		"url" : "https://www.youtube.com/embed/gZuI-dR9pfU"
	},
	{
		"name" : "Brussels",
		"description" : "Brussels, officially the Brussels-Capital Region, is a region of Belgium comprising 19 municipalities, including the City of Brussels which is the capital of Belgium, the French Community of Belgium, and the Flemish Community.",
		"url" : "https://www.youtube.com/embed/gZuI-dR9pfU"
	},
	{
		"name" : "Brazil",
		"description" : "Brazil, a vast South American country, stretches from the Amazon Basin in the north to vineyards and massive Iguaçu Falls in the south. Rio de Janeiro, symbolized by its 38m Christ the Redeemer statue atop Mt. Corcovado, is famed for its busy Copacabana and Ipanema beaches as well as its enormous, raucous Carnival festival, featuring parade floats, flamboyant costumes and samba.",
		"url" : "https://www.youtube.com/embed/aBWA1udhv1E"
	},
	{
		"name" : "Rio de Janeiro",
		"description" : "Rio de Janeiro is a huge seaside city in Brazil, famed for its Copacabana and Ipanema beaches, 38m Christ the Redeemer statue atop Mt. Corcovado and Sugarloaf, a granite monolith with cable cars to its summit. The city is also known for its sprawling favelas (shanty towns). Its raucous Carnival festival, featuring parade floats, flamboyant costumes and samba, is considered the world’s largest.",
		"url" : "https://www.youtube.com/embed/ieWNzZPfZzk"
	},
	{
		"name" : "Liverpool",
		"description" : "Liverpool is a historic maritime city in northwest England, where the River Mersey meets the Irish Sea. A key trade and migration port from the 18th to the early 20th centuries, it’s also, famously, the hometown of The Beatles. Ferries cruise the waterfront, where the iconic shipping and mercantile buildings known as the ’Three Graces’ stand on the Pier Head.",
		"url" : "https://www.youtube.com/embed/MgXoYNjxkK0"
	},
	{
		"name" : "Leicester",
		"description" : "Leicester is a city and unitary authority area in the East Midlands of England, and the county town of Leicestershire. The city lies on the River Soar and at the edge of the National Forest",
		"url" : "https://www.youtube.com/embed/fs-I-opGSQk"
	},
	{
		"name" : "​Southampton",
		"description" : "Southampton is the largest city in the ceremonial county of Hampshire on the south coast of England, and is situated 75 miles south-west of London and 19 miles north-west of Portsmouth",
		"url" : "https://www.youtube.com/embed/1L-iuM6yfAs"
	},
	{
		"name" : "Sunderland",
		"description" : "Sunderland is a city at the centre of the City of Sunderland metropolitan borough, in Tyne and Wear, North East England. It is situated at the mouth of the River Wear",
		"url" : "https://www.youtube.com/embed/Dye1THEv7R0"
	},
	{
		"name" : "Tottenham",
		"description" : "Tottenham is an area in the London Borough of Haringey, in north London, England. It is situated 8.2 miles north-north-east of Charing Cross.",
		"url" : ""
	},
	{
		"name" : "West Bromwich",
		"description" : "West Bromwich /wɛst ˈbrɒmɪtʃ/ or /wɛst ˈbrɒmwɪtʃ/ is a town in Sandwell, West Midlands, England. Historically in Staffordshire, it is in the Black Country, 5 miles northwest of Birmingham",
		"url" : "https://www.youtube.com/embed/X5nkQJTeQD0?list=PLtLiYkSrGfJoiOUgsf09Oc1MBlqgkcCX4"
	},
	{
		"name" : "Holloway",
		"description" : "Holloway is an inner-city district in the London Borough of Islington located 3.3 miles north of Charing Cross and follows for the most part, the line of the Holloway Road. At the centre of Holloway is the Nag’s Head area.",
		"url" : "https://www.youtube.com/watch?v=PtWeqZsuzpE"
	},
	{
		"name" : "Lens",
		"description" : "Lens is a commune in the Pas-de-Calais department in northern France. It is one of France’s large Picarde cities along with Lille, Valenciennes, Amiens, Roubaix, Tourcoing, Arras, and Douai. The inhabitants are called Lensois",
		"url" : "https://www.youtube.com/watch?v=at7qsNUY56w"
	},

	{
		"name" : "Seattle",
		"description" : "Seattle, on Puget Sound in the Pacific Northwest, is surrounded by water, mountains and evergreen forests, and encompasses thousands of acres of parkland (hence its nickname, ’Emerald City’). It’s home to a thriving tech industry, with Microsoft and Amazon.com headquartered in its metropolitan area. The futuristic Space Needle, a legacy of the 1962 World’s Fair, is its most recognizable landmark.",
		"url" : "https://www.youtube.com/embed/GvDMIxQIvi8"
	}
]');

