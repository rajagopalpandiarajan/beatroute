-- ALTER TABLE users DROP INDEX email_UNIQUE ;
ALTER TABLE users ADD CONSTRAINT unique_user_enterprise UNIQUE (email,enterprise_id);
ALTER TABLE subscribe DROP INDEX email ;

ALTER TABLE subscribe ADD COLUMN enterprise_id BIGINT NOT NULL DEFAULT 1;
ALTER TABLE subscribe ADD CONSTRAINT fk_subscribe_enterprise FOREIGN KEY (enterprise_id) REFERENCES enterprise(id);
ALTER TABLE subscribe ADD CONSTRAINT unique_user_subscribe UNIQUE (email,enterprise_id);




