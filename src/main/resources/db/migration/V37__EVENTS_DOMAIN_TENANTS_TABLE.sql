DROP TABLE IF EXISTS events_enterprise_tenants;
CREATE TABLE events_enterprise_tenants (
  event_id BIGINT NOT NULL,
  enterprise_id BIGINT NOT NULL,
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW(),
  FOREIGN KEY events_enterprise_tenants_event(event_id) REFERENCES events(id),
  FOREIGN KEY events_enterprise_tenants_enterprise_id(enterprise_id) REFERENCES enterprise(id)
);