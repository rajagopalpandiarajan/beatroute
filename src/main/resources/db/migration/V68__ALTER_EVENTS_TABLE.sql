ALTER TABLE events
CHANGE COLUMN image_gallery image_gallery LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN video_gallery video_gallery TEXT NULL DEFAULT NULL ,
CHANGE COLUMN event_terms_and_conditions event_terms_and_conditions LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN notes notes LONGTEXT NULL DEFAULT NULL ;