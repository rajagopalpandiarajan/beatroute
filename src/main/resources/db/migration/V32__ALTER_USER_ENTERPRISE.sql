CREATE TABLE user_enterprise (
  user_id BIGINT NOT NULL,
  enterprise_id BIGINT NOT NULL,
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW(),
  FOREIGN KEY user_enterprise_user(user_id) REFERENCES users(id),
  FOREIGN KEY user_enterprise_enterprise(enterprise_id) REFERENCES enterprise(id)
);