
ALTER TABLE enterprise_experiences DROP FOREIGN KEY enterprise_experiences_ibfk_2;
ALTER TABLE enterprise_experiences DROP COLUMN experiences_id;
ALTER TABLE enterprise_experiences ADD COLUMN country VARCHAR(256) NULL;
ALTER TABLE enterprise_experiences ADD COLUMN experience_type VARCHAR(256) NULL;

CREATE TABLE enterprise_events(
id BIGINT AUTO_INCREMENT PRIMARY KEY,
event_reference_id VARCHAR(256) NOT NULL,
enterprise_id BIGINT NOT NULL ,
FOREIGN KEY  enterprise_events_enterprise(enterprise_id) REFERENCES enterprise(id)
);




