DROP TABLE IF EXISTS tenant_category;

DROP TABLE IF EXISTS navigator;

DROP TABLE IF EXISTS home_event;

CREATE TABLE tenant_category (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  primary_category_id bigint(20) DEFAULT NULL,
  secondary_category_id bigint(20) DEFAULT NULL,
  sub_category_id bigint(20) DEFAULT NULL,
  tenant_id bigint(20) DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (sub_category_id) REFERENCES categories (id),
  FOREIGN KEY (tenant_id) REFERENCES tenants (id),
  FOREIGN KEY (secondary_category_id) REFERENCES categories (id),
  FOREIGN KEY (primary_category_id) REFERENCES categories (id)
);

CREATE TABLE navigator (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  description varchar(255) DEFAULT NULL,
  event_priority int(11) DEFAULT NULL,
  secondary_category_priority int(11) DEFAULT NULL,
  video_link varchar(255) DEFAULT NULL,
  event_id bigint(20) DEFAULT NULL,
  primary_category_id bigint(20) DEFAULT NULL,
  secondary_category_id bigint(20) DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (secondary_category_id) REFERENCES categories (id),
  FOREIGN KEY (event_id) REFERENCES events (id),
  FOREIGN KEY (primary_category_id) REFERENCES categories (id)
);

CREATE TABLE home_event(
  id bigint(20) NOT NULL AUTO_INCREMENT,
  priority int(11) DEFAULT 1,
  event_name VARCHAR(255) NOT NULL,
  event_reference_id VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
)