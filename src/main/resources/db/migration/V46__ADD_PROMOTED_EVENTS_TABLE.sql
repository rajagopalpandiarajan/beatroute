drop table if exists promoted_events;

CREATE TABLE promoted_events (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  event_name varchar(200) NOT NULL,
  event_url varchar(256) DEFAULT NULL,
  image varchar(256) DEFAULT NULL,
  video varchar(256) DEFAULT NULL,
  description longtext,
  created_date datetime DEFAULT NULL,
  updated_date datetime DEFAULT NULL,
  PRIMARY KEY (id)
)