ALTER TABLE enterprise CHANGE COLUMN theme_data theme_data LONGTEXT NULL;

ALTER TABLE sessions ADD COLUMN available BIT(1) NOT NULL DEFAULT 1;

