-- add the super admin user for beatroute with password Beatroute2016
INSERT INTO users(name, email, password, country, user_type, authorised,created_date ,updated_date) VALUES ('Support', 'support@beatroute.com', '$2a$05$DaUhhiE6EkqIUu0VyuzDzOSB18An0wztqOei1PJbn2uAhmh.DS8da', 'India', 4, 1, now(), now());
