drop table if exists user_destination_track;
drop table if exists user_event_track;
drop table if exists user_tag_track;
drop table if exists global_destination_track;

CREATE TABLE user_destination_track (
  id                 BIGINT AUTO_INCREMENT PRIMARY KEY,
  user_id            VARCHAR(200) NOT NULL,
  search_destination VARCHAR(300) NULL,
  search_count       BIGINT,
  last_searched_date DATETIME NOT NULL DEFAULT NOW()
);

CREATE TABLE user_event_track (
  id                 BIGINT AUTO_INCREMENT PRIMARY KEY,
  user_id            VARCHAR(200) NOT NULL,
  event_name         VARCHAR(300),
  event_reference_id VARCHAR(300),
  visit_count        BIGINT,
  last_visited_date  DATETIME NOT NULL DEFAULT NOW()
);

CREATE TABLE user_tag_track (
  id                 BIGINT AUTO_INCREMENT PRIMARY KEY,
  user_id            VARCHAR(200) NOT NULL,
  search_tag         VARCHAR(300),
  search_count        BIGINT,
  last_searched_date  DATETIME NOT NULL DEFAULT NOW()
);

CREATE TABLE global_destination_track (
  id                  BIGINT AUTO_INCREMENT PRIMARY KEY,
  source_country      VARCHAR(200) NOT NULL,
  destination_country VARCHAR(300),
  visit_count        BIGINT
);
