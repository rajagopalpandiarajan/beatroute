ALTER TABLE destination_display
ADD COLUMN destination_type VARCHAR(200) NULL AFTER user_interest_destinations,
ADD COLUMN title VARCHAR(256) NULL AFTER updated_date,
ADD COLUMN meta_name VARCHAR(256) NULL AFTER title,
ADD COLUMN meta_tags LONGTEXT NULL AFTER meta_name,
ADD COLUMN meta_description LONGTEXT NULL AFTER meta_tags;