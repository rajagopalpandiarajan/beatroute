DROP TABLE IF EXISTS faq;

CREATE TABLE faq (
  id       BIGINT AUTO_INCREMENT PRIMARY KEY,
  question LONGTEXT NULL,
  answer  LONGTEXT NULL,
  event_id   BIGINT NOT NULL,
  FOREIGN KEY faq(event_id) REFERENCES events(id)
);
