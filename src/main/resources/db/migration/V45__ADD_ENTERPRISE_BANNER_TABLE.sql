ALTER TABLE enterprise DROP COLUMN banner_data;

drop table if exists enterprise_banner;

CREATE TABLE enterprise_banner (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(200) NOT NULL,
    description VARCHAR(256) NOT NULL,
    image VARCHAR(200) NOT NULL,
    button_text VARCHAR(200) DEFAULT NULL,
    link LONGTEXT DEFAULT NULL,
    priority int(11) DEFAULT 1,
    created_date DATETIME NOT NULL,
    updated_date DATETIME NOT NULL,
    enterprise_id   BIGINT NOT NULL,
    FOREIGN KEY enterprise_banner(enterprise_id) REFERENCES enterprise(id)
) ;