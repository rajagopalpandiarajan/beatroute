ALTER TABLE events ADD COLUMN view_count BIGINT(20) NULL DEFAULT 0 AFTER generic_detail_image;
