drop table if exists user_enterprise;

ALTER TABLE users ADD COLUMN enterprise_id BIGINT NOT NULL DEFAULT 1;
ALTER TABLE users ADD CONSTRAINT users_enterprise FOREIGN KEY (enterprise_id) REFERENCES enterprise(id);
