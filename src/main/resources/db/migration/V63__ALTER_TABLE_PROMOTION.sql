ALTER TABLE promoted_events ADD COLUMN enterprise_id BIGINT(20) NOT NULL DEFAULT 1 AFTER priority;
ALTER TABLE promoted_events ADD FOREIGN KEY (enterprise_id) REFERENCES enterprise (id);

ALTER TABLE adviced_events ADD COLUMN enterprise_id  BIGINT(20) NOT NULL DEFAULT 1 AFTER priority;
ALTER TABLE adviced_events ADD FOREIGN KEY (enterprise_id) REFERENCES enterprise (id);