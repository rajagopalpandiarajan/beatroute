
ALTER TABLE adviced_events
CHANGE COLUMN event_name title VARCHAR(200) NOT NULL ,
CHANGE COLUMN event_url link VARCHAR(256) NULL DEFAULT NULL ,
ADD COLUMN button_text VARCHAR(256) NULL AFTER updated_date,
ADD COLUMN priority INT(10) NULL AFTER button_text;
