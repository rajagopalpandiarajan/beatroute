ALTER TABLE destination_mapping
ADD COLUMN enterprise_id BIGINT(20) NOT NULL DEFAULT 1 AFTER updated_date,
ADD INDEX destination_mapping_ibfk_3_idx (enterprise_id ASC);
ALTER TABLE destination_mapping
ADD CONSTRAINT destination_mapping_ibfk_3
  FOREIGN KEY (enterprise_id)
  REFERENCES enterprise (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

