ALTER TABLE tenant_category ADD COLUMN enterprise_id BIGINT NOT NULL DEFAULT 1,
ADD FOREIGN KEY tenant_category_enterprise(enterprise_id) REFERENCES enterprise(id);