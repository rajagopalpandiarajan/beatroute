DROP TABLE IF EXISTS destination_mapping;

CREATE TABLE destination_mapping (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  country_id bigint(20) NOT NULL,
  city_id bigint(20) NOT NULL,
  priority int(10) DEFAULT NULL,
  created_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  KEY key1 (priority),
  KEY country_id (country_id),
  KEY city_id (city_id),
  CONSTRAINT destination_mapping_ibfk_1 FOREIGN KEY (country_id) REFERENCES destination_display (id) ON DELETE CASCADE,
  CONSTRAINT destination_mapping_ibfk_2 FOREIGN KEY (city_id) REFERENCES destination_display (id) ON DELETE CASCADE
);
