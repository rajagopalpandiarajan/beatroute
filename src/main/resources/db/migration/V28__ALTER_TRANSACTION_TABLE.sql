ALTER TABLE transactions ADD COLUMN supplier_paid_date DATETIME NULL;
ALTER TABLE transactions ADD COLUMN supplier_paid BIT(1) NOT NULL DEFAULT 0;
ALTER TABLE transactions ADD COLUMN supplier_amount DECIMAL(10,4) DEFAULT 0.0;