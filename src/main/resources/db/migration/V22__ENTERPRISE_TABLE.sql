drop table if exists products_api;
drop table if exists commercials;
drop table if exists enterprise_commercials;
drop table if exists enterprise_products;
drop table if exists products;
drop table if exists enterprise_payment_gateway;
drop table if exists enterprise_experiences;
drop table if exists enterprise_countries;
drop table if exists enterprise_suppliers;
drop table if exists enterprise_contacts;
drop table if exists domain_tenant;

drop table if exists payment_gateways;
drop table if exists payment_gateway;
create table payment_gateway(
id BIGINT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(100) NOT NULL
);
INSERT INTO payment_gateway (name) VALUES ('VISA');
INSERT INTO payment_gateway (name) VALUES ('MASTERCARD');
INSERT INTO payment_gateway (name) VALUES ('AMEX');

drop table if exists enterprise;
create table enterprise(
id BIGINT AUTO_INCREMENT PRIMARY KEY,
client_servicing_personnel VARCHAR(128) NOT NULL,
signed_agreement BIT(1) NOT NULL,
company_name VARCHAR(256) NOT NULL,
display_name VARCHAR(256) NOT NULL,
contract_termination_date DATETIME NOT NULL DEFAULT NOW(),
userid VARCHAR(200) NOT NULL,
password VARCHAR(200),
data_policy TINYINT(1),
setup_complete BIT(1) NOT NULL
);

create table enterprise_commercials(
id BIGINT AUTO_INCREMENT PRIMARY KEY,
revenue_model TINYINT(1) NOT NULL,
revenue_percent FLOAT,
billing_slabs LONGTEXT NULL,
enterprise_id   BIGINT NOT NULL,
FOREIGN KEY commercials_enterprise(enterprise_id) REFERENCES enterprise(id)
);

create table enterprise_products(
id BIGINT AUTO_INCREMENT PRIMARY KEY,
enterprise_id   BIGINT NOT NULL,
product TINYINT(1) NOT NULL ,
FOREIGN KEY products_enterprise(enterprise_id) REFERENCES enterprise(id)
);

create table enterprise_payment_gateway(
id BIGINT AUTO_INCREMENT PRIMARY KEY,
payment_gateway_id BIGINT NOT NULL,
enterprise_id  BIGINT NOT NULL,
FOREIGN KEY  payment_gateway_enterprise(enterprise_id) REFERENCES enterprise(id),
FOREIGN KEY  payment_gateway_ID(payment_gateway_id) REFERENCES payment_gateway(id)
);

create table enterprise_suppliers(
id BIGINT AUTO_INCREMENT PRIMARY KEY,
enterprise_id   BIGINT NOT NULL,
suppliers_id BIGINT NOT NULL ,
FOREIGN KEY  enterprise_suppliers_enterprise(enterprise_id) REFERENCES enterprise(id),
FOREIGN KEY  enterprise_suppliers_suppliers_id(suppliers_id) REFERENCES tenants(id)
);

create table enterprise_experiences(
id BIGINT AUTO_INCREMENT PRIMARY KEY,
enterprise_id   BIGINT NOT NULL,
experiences_id BIGINT NOT NULL ,
FOREIGN KEY  enterprise_experiences_enterprise(enterprise_id) REFERENCES enterprise(id),
FOREIGN KEY  enterprise_experiences_experiences_id(experiences_id) REFERENCES categories(id)
);

create table enterprise_countries(
id BIGINT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(128) NOT NULL,
enterprise_id   BIGINT NOT NULL,
FOREIGN KEY enterprise_countries_enterprise(enterprise_id) REFERENCES enterprise(id)
);

create table enterprise_contacts(
id BIGINT AUTO_INCREMENT PRIMARY KEY,
contact_name VARCHAR(200) NOT NULL,
contact_no VARCHAR(15) NOT NULL,
email_id VARCHAR(200) NOT NULL,
contact_type TINYINT(1) NOT NULL,
enterprise_id BIGINT NOT NULL,
FOREIGN KEY  enterprise_contacts_enterprise(enterprise_id) REFERENCES enterprise(id)
);

create table domain_tenant(
id BIGINT AUTO_INCREMENT PRIMARY KEY,
sub_domain_name VARCHAR(200) NOT NULL,
enterprise_id BIGINT NOT NULL,
FOREIGN KEY  enterprise_contacts_enterprise(enterprise_id) REFERENCES enterprise(id)
);