INSERT INTO enterprise (client_servicing_personnel, signed_agreement, company_name, display_name, contract_termination_date, userid, password, data_policy, setup_complete) VALUES ('Afraaz', 1, 'Beatroute Pte. Ltd., Singapore', 'Beatroute', '2030-12-31 00:00:00', 'support@beatroute.com', '$2a$05$kES3FtdkjXBzh3ga0m/OFuD4WyPq75bbp72x6V2JGmkz9I0Yk5sCK', 1, 1);

INSERT INTO enterprise_products (enterprise_id, product) VALUES (1, 1);
INSERT INTO enterprise_products (enterprise_id, product) VALUES (1, 2);
INSERT INTO enterprise_products (enterprise_id, product) VALUES (1, 3);

INSERT INTO enterprise_payment_gateway (enterprise_id, payment_gateway_id) VALUES (1, 1);
INSERT INTO enterprise_payment_gateway (enterprise_id, payment_gateway_id) VALUES (1, 2);
INSERT INTO enterprise_payment_gateway (enterprise_id, payment_gateway_id) VALUES (1, 3);

INSERT INTO enterprise_contacts (contact_name,contact_no,email_id,contact_type,enterprise_id) VALUES ('Support', '+91 4445671221', 'support@beatroute.com', 1, 1);
INSERT INTO enterprise_contacts (contact_name,contact_no,email_id,contact_type,enterprise_id) VALUES ('Accounts', '+91 4747372429', 'accounts@beatroute.com', 2, 1);

INSERT INTO domain_tenant (sub_domain_name, enterprise_id) VALUES ('www', 1);
