ALTER TABLE events
CHANGE COLUMN venue_map venue_map TEXT NULL DEFAULT NULL ,
CHANGE COLUMN generic_detail_image generic_detail_image TEXT NULL DEFAULT NULL ;

ALTER TABLE display_categories ADD COLUMN enterprise_id BIGINT(20) NOT NULL DEFAULT 1 AFTER updated_date;
ALTER TABLE display_categories ADD FOREIGN KEY (enterprise_id) REFERENCES enterprise (id);
ALTER TABLE display_categories DROP INDEX name_UNIQUE ;