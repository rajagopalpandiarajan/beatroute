drop table if exists display_categories;

create table display_categories(
id BIGINT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(128) NOT NULL,
icon VARCHAR(256) NULL DEFAULT NULL,
mapped_categories LONGTEXT NULL DEFAULT NULL,
mapped_events LONGTEXT NULL DEFAULT NULL,
created_date DATETIME NOT NULL DEFAULT NOW(),
updated_date DATETIME NOT NULL DEFAULT NOW()
);

ALTER TABLE display_categories ADD UNIQUE INDEX name_UNIQUE (name ASC);
