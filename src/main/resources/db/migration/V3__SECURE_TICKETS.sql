CREATE TABLE secure_tickets (
  id   BIGINT AUTO_INCREMENT PRIMARY KEY,
  user_id  BIGINT NOT NULL,
  event_id BIGINT NOT NULL,
  ticket_booking_price DECIMAL(10,4) NOT NULL,
  ticket_group_id VARCHAR(200) NOT NULL,
  no_of_tickets INTEGER DEFAULT 0,
  payment_reference_id VARCHAR(200) NOT NULL,
  session_reference_id VARCHAR(200) NOT NULL,
  lock_request_id VARCHAR(200) NOT NULL,
  lock_status INTEGER NOT NULL,
  lock_response LONGTEXT,
  created_date DATETIME NOT NULL DEFAULT NOW(),
  updated_date DATETIME NOT NULL DEFAULT NOW(),
  expiry_date_time DATETIME NOT NULL DEFAULT NOW(),
  FOREIGN KEY secure_tickets_users(user_id) REFERENCES users(id),
  FOREIGN KEY secure_tickets_events(event_id) REFERENCES events(id)
);
