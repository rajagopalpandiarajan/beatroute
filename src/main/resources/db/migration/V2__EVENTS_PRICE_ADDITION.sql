alter table events add column high_price DECIMAL(10,4) DEFAULT 0.0;
alter table events add column avg_price DECIMAL(10,4) DEFAULT 0.0;
alter table events add column low_price DECIMAL(10,4) DEFAULT 0.0;
