package com.beatroute.api.rest;

import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.enumeration.TransactionStatus;
import com.beatroute.api.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Future;

@Controller
@RequestMapping("/api/payment")
@Slf4j
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private EmailHelperService emailHelperService;

    @Autowired
    private SmsHelperService smsHelperService;

    @Autowired
    private TicketService ticketService;

    @Value("${api.payment.success.redirect.url}")
    private String successRedirect;

    @Value("${api.payment.failure.redirect.url}")
    private String failureRedirect;

    @Value("${api.payment.cancel.redirect.url}")
    private String cancelRedirect;

    // on success, you have to show him a success page, clear his shopping cart, mark transaction as successful
    @RequestMapping(value = "/success", method = RequestMethod.GET)
    public String paymentSuccess(@RequestParam("Ref") String ref, @RequestParam("sourceHost") String sourceHost) throws Exception {
        Collection<Future<String>> futures = new ArrayList<Future<String>>();
        log.info("paymentSuccess: SourceHost - "+sourceHost+" for the reference - "+ref);
        try {
            //update payment status success
            paymentService.updatePaymentStatus(ref, TransactionStatus.SUCCESS);

            /*//place order and update transaction status
            paymentService.processSuccessfulPayment(ref);*/

            //place order and update transaction status
            futures = ticketService.placeOrderForSupplier(ref);

            //get order response
            ticketService.getOrderResponse(ref, futures);

            //update transactions and delete from bagpack
            List<SecureTicket> secureTicketList = paymentService.updateTransactionStatus(ref);

            //update transaction ticket url
            paymentService.updateTransactionUrl(ref, futures);

            //send sms
            smsHelperService.sendSmsToUser(ref, sourceHost);

            //send email
            emailHelperService.sendMailToUser(ref, secureTicketList, sourceHost);

        }catch (Exception e) {
            log.error(e.getMessage(),e);
        }

        return "redirect:"+sourceHost+successRedirect+ref;
    }


    // on failure, you have to show him a failure page, mark transaction as failure, provide option to retry
    @RequestMapping(value = "/failure", method = RequestMethod.GET)
    public String paymentFailure(@RequestParam("Ref") String ref, @RequestParam("sourceHost") String sourceHost) throws Exception {
        log.info("paymentFailure: SourceHost - "+sourceHost+" for the reference - "+ref);
        return "redirect:"+sourceHost+failureRedirect+ref;
    }

    // on cancel, you have to show him a cancel message, mark transaction as failure, provide option to retry
    @RequestMapping(value = "/cancel", method = RequestMethod.GET)
    public String paymentCancelled(@RequestParam("Ref") String ref, @RequestParam("sourceHost") String sourceHost) throws Exception {
        log.info("paymentCancelled: SourceHost - "+sourceHost+" for the reference - "+ref);
        return "redirect:"+sourceHost+cancelRedirect;
    }
}
