package com.beatroute.api.rest;

import com.beatroute.api.dto.Enquiry;
import com.beatroute.api.service.EmailHelperService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/enquiry/")
@Slf4j
public class EnquiryController {

    @Autowired
    private EmailHelperService emailHelperService;

    @RequestMapping(value = "send", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> sendEnquiry(@RequestBody String enquiryJson) {
        try{
            Enquiry enquiry=getEnquiry(enquiryJson);
            return  new ResponseEntity<Object>(this.emailHelperService.sendEnquiryEmail(enquiry), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    private Enquiry getEnquiry(String requestJson) {
        Enquiry enquiry=new Enquiry();
        JSONObject jsonObject=new JSONObject(requestJson);
        enquiry.setFirstName(jsonObject.getString("firstName"));
        enquiry.setLastName(jsonObject.getString("lastName"));
        enquiry.setEmail(jsonObject.getString("email"));
        enquiry.setContactNumber(jsonObject.getString("contactNumber"));
        enquiry.setDesignation(jsonObject.getString("designation"));
        enquiry.setCompanyName(jsonObject.getString("companyName"));
        enquiry.setAddress(jsonObject.getString("address"));
        enquiry.setRegId(jsonObject.getString("regId"));
        enquiry.setTypeOfSolution(jsonObject.getString("typeOfSolution"));
        return enquiry;
    }

}
