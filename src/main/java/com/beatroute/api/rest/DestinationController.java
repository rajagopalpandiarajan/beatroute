package com.beatroute.api.rest;

import com.beatroute.api.service.DestinationService;
import com.beatroute.api.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Controller
@RestController
@RequestMapping("/api/destination")
public class DestinationController {

    @Autowired
    private DestinationService destinationService;

/**
 * API provides the Countries in landing page,
 *
 * @return
 * Success -
        {
        "data": {
            "destinationDisplay": [
                {
                "image": "\\dev\\Destination\\.png",
                "destination": "india",
                "destinationId": 8,
                "eventCount": 112
                }
             ]
        },
    "status": "success"
    }

    * Failure
    * {
        "status": "failure",
        "errors": {
        "INVALIDINPUTS": "Invalid values for products, email"
        }
    }
 */
    @RequestMapping(value = "/list/destinations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getDestinationList(){
        try {
            return new ResponseEntity<Object>(this.destinationService.listDestinations(),HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * API provides the Countries in landing page,
     *
     * @return
     * Success -
        {
        "data": {
            "destinationDisplay": [
            {
                "image": "\\dev\\Destination\\.png",
                "destination": "india",
                "destinationId": 8,
                "priority":"1"
            }
            ]
        },
        "status": "success"
        }

     * Failure
     * {
            "status": "failure",
            "errors": {
                "INVALIDINPUTS": "Invalid values for products, email"
            }
        }
     */
    @RequestMapping(value = "/list/destinations/priority", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPrioritizedDestinations(@RequestParam("country") String country){
        try {
            return new ResponseEntity<Object>(this.destinationService.getPrioritizedDestinations(country),HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "sidebar/countries", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCountries(){
        try {
            return new ResponseEntity<Object>(this.destinationService.getCountries(), HttpStatus.OK);
        }catch(Exception ex){
            log.error(ex.getMessage(), ex);
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @param country - ex: India
     * @return
     * Success -
     *  {
            "data":[
                {
                    "city": "Delhi and NCR",
                    "eventCount": 43
                },
                {
                    "city": "Bangalore",
                    "eventCount": 4
                },
                {
                    "city": "Goa",
                    "eventCount": 4
                }
            ],
            "status": "success"
        }
     * Failure -
     *  {
            "status": "failure",
            "errors": {
                "INVALIDINPUTS": "Invalid values for products, email"
            }
        }
     */
    @RequestMapping(value = "sidebar/cities", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCities(@RequestParam("country") String country){
        try {
            return new ResponseEntity<Object>(this.destinationService.getCities(country), HttpStatus.OK);
        }catch(Exception ex){
            log.error(ex.getMessage(), ex);
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     *
     * @param country - ex: India
     * @return
     * Success -
     *  {
            "data": [
                {
                    "displayCategory": "Excursions",
                    "eventCount": 98
                }
            ],
            "status": "success"
        }
     * Failure -
     *  {
            "status": "failure",
            "errors": {
                "INVALIDINPUTS": "Invalid values for products, email"
            }
        }
     */
    @RequestMapping(value = "sidebar/experiences", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getExperiences(@RequestParam(name="country", required=false) String country,@RequestParam(name="city", required=false) String city){
        try {
            return new ResponseEntity<Object>(this.destinationService.getExperiences(country,city), HttpStatus.OK);
        }catch(Exception ex){
            log.error(ex.getMessage(), ex);
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}