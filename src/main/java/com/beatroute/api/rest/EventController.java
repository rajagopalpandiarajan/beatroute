package com.beatroute.api.rest;

import com.beatroute.api.model.Event;
import com.beatroute.api.model.Tag;
import com.beatroute.api.repository.TagRepository;
import com.beatroute.api.rest.request.EventUpdateRequest;
import com.beatroute.api.rest.template.OrderDecisionRequest;
import com.beatroute.api.rest.template.OrderRequest;
import com.beatroute.api.rest.template.TempLockRequest;
import com.beatroute.api.rest.template.TicketNetworkEvent;
import com.beatroute.api.service.AmazonS3Service;
import com.beatroute.api.service.EventService;
import com.beatroute.api.service.NavigationService;
import com.beatroute.api.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/event/")
@Slf4j
public class EventController {

    @Value("${app.ticketnetwork.getTicket}")
    private String ticketUrl;

    @Value("${app.ticketnetwork.getTicketLock}")
    private String ticketLockUrl;

    @Value("${app.ticketnetwork.createOrder}")
    private String createOrderUrl;

    @Value("${app.ticketnetwork.download.ticket}")
    private String downloadUrl;

    @Value("${app.ticketnetwork.createOrderDecision}")
    private String createOrderDecisionUrl;

    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private AmazonS3Service amazonS3Service;

    @Autowired
    private EventService eventService;

    @Autowired
    private NavigationService navigationService;

    @RequestMapping(value = "get", method = RequestMethod.GET)
    public ResponseEntity<?> getEvent(@RequestParam("id") String id) {
        try {
            return new ResponseEntity<Object>(eventService.get(id), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveEvent(@RequestBody Event event) {
        try {
            eventService.save(event);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateEvent(@RequestBody Event event) {
        try {
            eventService.update(event);
            return new ResponseEntity<Object>(HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "delete", method = RequestMethod.DELETE)
    public ResponseEntity deleteEvent(@RequestParam("eventId") String eventId, @RequestParam("eventReferenceId") String eventReferenceId) {
        try {
            eventService.delete(Long.parseLong(eventId), eventReferenceId);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "tickets/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTickets(@RequestBody @PathVariable("id") String id) {
        try {
            String url = ticketUrl + id;
            return new ResponseEntity<Object>(this.restTemplate.getForEntity(url, String.class).getBody(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            Map error = new HashMap<>();
            error.put("error", "Ticketnetwork not responding");
            return new ResponseEntity<Object>(error, HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
        }
    }

    @RequestMapping(value = "ticket/lock", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTicketLock(@RequestBody TempLockRequest tempLockRequest) {
        try {
            return new ResponseEntity<Object>(this.restTemplate.postForEntity(ticketLockUrl, tempLockRequest, String.class).getBody(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            Map error = new HashMap<>();
            error.put("error", "lock can not be created");
            return new ResponseEntity<Object>(error, HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
        }
    }

    @RequestMapping(value = "order", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createOrder(@RequestBody OrderRequest orderRequest) {
        try {
            return new ResponseEntity<Object>(this.restTemplate.postForEntity(createOrderUrl, orderRequest, String.class).getBody(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            Map error = new HashMap<>();
            error.put("error", "order cannot be created");
            return new ResponseEntity<Object>(error, HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
        }
    }

    @RequestMapping(value = "ticketNetworkEvent", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveApiEvent(@RequestBody TicketNetworkEvent ticketNetworkEvent) {
        eventService.saveApiEvent(ticketNetworkEvent);
        return new ResponseEntity<Object>(ticketNetworkEvent, HttpStatus.OK);
    }

    @RequestMapping(value = "bulk/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String bulkUpdateEvents(@RequestBody List<EventUpdateRequest> eventUpdateRequests) {
        log.info("Started processing the update event request");
        String status = "{\"message\":\"success\"}";
        try {
            for (EventUpdateRequest eventUpdateRequest : eventUpdateRequests) {
            	try {
	                Event event = eventService.getByName(eventUpdateRequest.getName());
	                if (event != null) {
	                    log.info("Updating the event with name " + event.getName());
	                    event.setDescription(eventUpdateRequest.getDescription());
	                    if (event.getImageGallery() == null) {
	                        String s3Url = amazonS3Service.upload(eventUpdateRequest.getLinkToPhoto());
	                        log.info("URL for image is " + s3Url);
	                        event.setImageGallery(s3Url);
	                    }
	                    event.setVideoGallery(eventUpdateRequest.getEmbedLink());
	
	                    if(eventUpdateRequest.getTags() != null) {
				            Tag t = null;
				            String[] tagsInEvent = eventUpdateRequest.getTags().split(",");
				            for (String s: tagsInEvent) {
				                t = this.tagRepository.findOneByName(s);
				                if(t == null){
				                    t = new Tag();
				                    t.setName(s);
				                    t.setDescription(s);
				                    t = this.tagRepository.save(t);
				                    event.getTags().add(t);
				                }else{
				                    event.getTags().add(t);
				                }
				            }
	                    }
	                    if (event.getImageGallery() != null) {
	                        event.setStatus(true);
	                    }
	                    eventService.update(event);
	                }
	            } catch (Exception e) {
	                log.error("Failed for event: '"+eventUpdateRequest.getName()+"'", e);
	                status = "{\"message\":\"Processed with errors.\"}";
	            }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            status = "{\"message\":\"Failed to process\"}";
        }
        log.info("Completed processing the update event request");
        return status;
    }

    @RequestMapping(value = "createOrderDecision", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createOrderDecision(@RequestBody OrderDecisionRequest orderDecisionRequest) {
        try {
            return new ResponseEntity<Object>(this.restTemplate.postForEntity(createOrderDecisionUrl, orderDecisionRequest, String.class).getBody(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            Map error = new HashMap<>();
            error.put("error", "order decision cannot be created");
            return new ResponseEntity<Object>(error, HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
        }
    }

    @RequestMapping(value = "allCategories", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPrimaryCategory() {
        return new ResponseEntity<Object>(this.eventService.getPrimaryCategory(), HttpStatus.OK);
    }

    @RequestMapping(value = "getHomePageEvents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getHomePageEvents() {
        try {
            return new ResponseEntity<Object>(this.navigationService.getHomePageEvents(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "sessions", method = RequestMethod.GET)
    public ResponseEntity<?> getEventSessions(@RequestParam("id") String id, @RequestParam("date") String date) {
        try {
            return new ResponseEntity<Object>(eventService.getSessions(id, date, false), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "indexEventsForEnterprise/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> indexEventsForEnterprise(@PathVariable("id") String id, @RequestBody Object userInfo) {
        try {
            eventService.indexEnterpriseEvents(Long.parseLong(id));
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "indexEventsForAllEnterprises", method = RequestMethod.POST)
    public ResponseEntity<?> reIndexEventsForAllEnterprises() {
        try {
            eventService.reIndexEventsForAllEnterprises();
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "populateSessions", method = RequestMethod.POST)
    public ResponseEntity<?> populateSessions() {
        try {
            eventService.populateSessions();
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @return
     * Success json :
     *  {
            "data": [{
                whole event data
            }],
            "status": "success"
        }
     *
     */
    @RequestMapping(value = "newEvents", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getNewEvents() {
        try {
            return new ResponseEntity<Object>(eventService.getNewEvents(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "recommendation/get/{recommendationName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> getRecommendationPageEvents(@PathVariable("recommendationName") String recommendationName){
        try {
            return new ResponseEntity<Object>(this.eventService.getRecommendationPageEvents(recommendationName), HttpStatus.OK);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
