package com.beatroute.api.rest;

import com.beatroute.api.service.SessionOptimizeService;
import com.beatroute.api.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

@Controller
@RequestMapping("/api/session")
@Slf4j
public class SessionOptimizeController {

    @Autowired
    SessionOptimizeService sessionOptimizeService;

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity<?> sessionOptimization() {
        log.info("Started Session Optimization scheduler at "+new Date());
        try {
            this.sessionOptimizeService.optimizeSession();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
    }
}
