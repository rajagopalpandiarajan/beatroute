package com.beatroute.api.rest;

import com.beatroute.api.model.DestinationDisplay;
import com.beatroute.api.repository.DestinationRepository;
import com.beatroute.api.rest.template.*;
import com.beatroute.api.service.EventSearchService;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/event/search/")
@Slf4j
public class EventSearchController {

    @Value("${elastic.search.url}")
    private String elasticSearchServerUrl;

    @Autowired
    private EventSearchService eventSearchService;

    @Autowired
    private DestinationRepository destinationRepository;

    RestTemplate restTemplate = new RestTemplate();

    /*
        INPUT: {"query_string": "competetive"}
        todo: on hold - check not used delete
     */
//    @RequestMapping(value = "findall", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> findAll(@RequestBody String query) {
//        try {
//            String response = restTemplate.postForEntity(this.elasticSearchServerUrl, query, String.class).getBody();
//            return new ResponseEntity<Object>(response, HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

    /*
       INPUT: {"term": "brick Cowardly manager", "from": 0, "size": 9}
    */
    @RequestMapping(value = "freeTextSearch", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponse(code = 201, message = "{'hits':{'total':172,'max_score':5.791814,'hits':[]}}")
    public ResponseEntity<?> freeTextSearch(@RequestBody @ApiParam(name = "queryString", 
			value="Search criteria in json format: {'term': 'brick Cowardly manager', 'from': 0, 'size': 9}", required = true) String query_string) {
        try {
            ElasticSearchQuery query = new ElasticSearchQuery();
            AdvanceSearchFilter advanceSearchFilter = new AdvanceSearchFilter();
            List mustList = new ArrayList<>();

            JSONParser parser = new JSONParser();
            JSONObject o = (JSONObject) parser.parse(query_string);

            mustList.add(advanceSearchFilter.getTermQuery());
            mustList.add(advanceSearchFilter.getFreeTextQuery(o.get("term").toString()));

            query.setFrom(o.get("from").toString());
            query.setSize(o.get("size").toString());
            query.addToMust(mustList);

            JSONObject masterQuery = query.getMasterQuery();
            log.info("Master Query - "+masterQuery);

            String response = restTemplate.postForEntity(this.elasticSearchServerUrl, masterQuery, String.class).getBody();
            return new ResponseEntity<Object>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
      INPUT: {"filters": { "title": "", "destination": {"country": "", "city": ""}, "date": {"from": "", "to": "", "time_zone": ""}, "price": {"high": "", "low" : ""}, "experienceType": "", "tags": []}, "from": 0, "size": 9}
   */
    @RequestMapping(value = "filters", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> filters(@RequestBody String filter) {
        try {
            JSONParser parser = new JSONParser();
            JSONObject o = (JSONObject) parser.parse(filter);
            log.info("filter input - "+o);

            //To include alias in the filters for country
            Map filters=(Map) o.get("filters");
            JSONObject destination = (JSONObject) filters.get("destination");
            String  country= destination.get("country").toString();
            String city=destination.get("city").toString();

            List mustList = new ArrayList<>();
            AdvanceSearchFilter advanceSearchFilter = new AdvanceSearchFilter(filters);

            DestinationDisplay destinationDisplay=this.destinationRepository.findOneByCountryName(country);
            if(StringUtils.isNotEmpty(country)) {
                String aliasCountry = country;
                ((JSONObject) filters.get("destination")).remove("country");
                ((JSONObject) filters.get("destination")).put("country", "");
                if (destinationDisplay != null && StringUtils.isNotEmpty(destinationDisplay.getCountryAlias())) {
                    aliasCountry = (destinationDisplay.getCountryAlias().length() >= 1 ? destinationDisplay.getCountryAlias() + "," + country : country);
                }

                String countryAliasSq=this.getQuerySequence(aliasCountry);
                String aliasCountryQueryString = countryAliasSq.replace(",", " OR ");
                mustList.add(advanceSearchFilter.getInQuery(aliasCountryQueryString, "country"));
            }
            if(StringUtils.isNotEmpty(city)) {
                DestinationDisplay destinationDisplayCity = this.destinationRepository.findOneByCityName(city);
                ((JSONObject) filters.get("destination")).remove("city");
                ((JSONObject) filters.get("destination")).put("city", "");
                String cityAlias = city;
                if (destinationDisplayCity != null && StringUtils.isNotEmpty(destinationDisplayCity.getCityAlias())) {

                    cityAlias = (destinationDisplayCity.getCityAlias().length() >= 1 ? destinationDisplayCity.getCityAlias() + "," + city : city);
                }
                log.info("cityAlias - " + cityAlias);
                String cityAliasSq=this.getQuerySequence(cityAlias);

                String aliasCityQueryString = cityAliasSq.replace(",", " OR ");
                log.info("aliasCityQueryString - " + aliasCityQueryString);
                mustList.add(advanceSearchFilter.getInQuery(aliasCityQueryString, "city"));
            }
            List experienceList=advanceSearchFilter.getExperienceList();
            if(experienceList.size() > 0) {
                mustList.add(advanceSearchFilter.getTermsQueryForIndexEvents("primaryCategory.name", experienceList));
            }
            ElasticSearchQuery query = new ElasticSearchQuery();
            query.setFrom(o.get("from").toString());
            query.setSize(o.get("size").toString());


                mustList.add(advanceSearchFilter.getTermQuery());
                query.addToMust(mustList);


            JSONObject masterQuery = query.getMasterQuery();
            log.info("Master Query - "+masterQuery);

            JSONObject response = restTemplate.postForEntity(this.elasticSearchServerUrl, masterQuery, JSONObject.class).getBody();
             return new ResponseEntity<Object>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
        INPUT: {"tags": ["brick", "Cowardly"], "from": 0, "size": 9}
     */
    @RequestMapping(value = "tags", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> searchByTags(@RequestBody String tags) {
        try {
            //parse json
            JSONParser parser = new JSONParser();
            JSONObject o = (JSONObject) parser.parse(tags);
            JSONArray tagsArray = (JSONArray) o.get("tags");

            //convert search_string to elastic_query
            ElasticSearchQuery query = new ElasticSearchQuery();
            query.setFrom(o.get("from").toString());
            query.setSize(o.get("size").toString());
            query.addToMust(new TagSearch().getTagSearchList(tagsArray));

            JSONObject masterQuery = query.getMasterQuery();
            log.info("Master Query - "+masterQuery);

            //fire query
            JSONObject response = restTemplate.postForEntity(this.elasticSearchServerUrl, masterQuery, JSONObject.class).getBody();

            return new ResponseEntity<Object>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
        INPUT: {"name": ["comp", "swee"]}
     */
    @RequestMapping(value = "autocomplete/name", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> autocompleteName(@RequestBody String name) {
        try {
            JSONParser parser = new JSONParser();
            JSONObject object = (JSONObject) parser.parse(name);
            JSONArray array = (JSONArray) object.get("name");

            ElasticSearchQuery query = new ElasticSearchQuery();
            AutoCompleteName autoCompleteName = new AutoCompleteName();
            query.addToMust(autoCompleteName.getSearchList(array));
            query.setFields(autoCompleteName.getFields());
            query.setSize("5");
            query.setHighLights(autoCompleteName.getHighLights());

            JSONObject masterQuery = query.getMasterQuery();
            log.info("Master Query - "+masterQuery);

            JSONObject response = restTemplate.postForEntity(this.elasticSearchServerUrl, masterQuery, JSONObject.class).getBody();

            return new ResponseEntity<Object>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
        INPUT: {"tag": ["comp", "swee"]}
        todo : do tag highlight
     */
    @RequestMapping(value = "autocomplete/tags", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> autoCompleteTags(@RequestBody String tag) {
        try {
            JSONParser parser = new JSONParser();
            JSONObject object = (JSONObject) parser.parse(tag);
            JSONArray array = (JSONArray) object.get("tag");

            ElasticSearchQuery query = new ElasticSearchQuery();
            AutoCompleteTag autoCompleteTag = new AutoCompleteTag();
            query.addToMust(autoCompleteTag.getSearchList(array));
            query.setHighLights(autoCompleteTag.getHighLights());
            query.setTagField();
            query.setSize("5");

            JSONObject masterQuery = query.getMasterQuery();
            log.info("Master Query - "+masterQuery);

            JSONObject response = restTemplate.postForEntity(this.elasticSearchServerUrl, masterQuery, JSONObject.class).getBody();

            return new ResponseEntity<Object>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
        INPUT: {"tags": ["brick", "Cowardly"], "from": 0, "size": 9}
     */
    @RequestMapping(value = "allTags", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> tagsShould(@RequestBody String tags) {
        try {
            JSONParser parser = new JSONParser();
            JSONObject o = (JSONObject) parser.parse(tags);
            JSONArray tagsArray = (JSONArray) o.get("tags");

            ElasticSearchQuery query = new ElasticSearchQuery();
            TagSearch tagSearch = new TagSearch();
            query.setFrom(o.get("from").toString());
            query.setSize(o.get("size").toString());

            List mustList = new ArrayList<>();
            mustList.add(tagSearch.getTermQuery());
            query.addToMust(mustList);
            query.addToShould(tagSearch.getTagSearchList(tagsArray));

            JSONObject masterQuery = query.getMasterQuery();
            log.info("Master Query - "+masterQuery);

            JSONObject response = restTemplate.postForEntity(this.elasticSearchServerUrl, masterQuery, JSONObject.class).getBody();

            return new ResponseEntity<Object>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "destination/{destination}/{from}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getEventsFromDestination(@RequestBody @PathVariable("destination") String destination, @PathVariable("from") String from){
        try{
            ElasticSearchQuery query = new ElasticSearchQuery();
            AdvanceSearchFilter advanceSearchFilter = new AdvanceSearchFilter();
            List mustList = new ArrayList<>();
            mustList.add(advanceSearchFilter.getQueryStringBean("sessions.venues.country", destination));
            mustList.add(advanceSearchFilter.getTermQuery());
            query.addToMust(mustList);
            query.setSize("9");
            query.setFrom(from);

            JSONObject masterQuery = query.getMasterQuery();
            log.info("Master Query - "+masterQuery);

            JSONObject response = restTemplate.postForEntity(this.elasticSearchServerUrl, masterQuery, JSONObject.class).getBody();

            return new ResponseEntity<Object>(response, HttpStatus.OK);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     *
     * @param startsWith : ger
     * @return
     * Success json:
     *  {
            "destination": [
                {
                    "country": "New Zealand",
                    "name": "<em>new</em> zealand",
                    "type": "country"
                },{
                    "city": "New York",
                    "name": "<em>new</em> york",
                    "type": "city"
                },{
                    "city": "New Orleans",
                    "name": "<em>new</em> orleans",
                    "type": "city"
                },{
                    "city": "Newcastle-Upon-Tyne",
                    "name": "<em>newcastle-upon-tyne</em>",
                    "type": "city"
                },{
                    "city": "New York Area - NY",
                    "name": "<em>new</em> york area - ny",
                    "type": "city"
                },{
                    "city": "New Orleans - LA",
                    "name": "<em>new</em> orleans - la",
                    "type": "city"
                }
            ],
            "event": [
                {
                    "name": "alcudia market and formentor - in <em>german</em> - alcudia market and formentor - in german",
                    "id": "17729-39085~E-E10-000200056",
                    "title": "Alcudia Market and Formentor - in German - Alcudia Market and Formentor - in German",
                    "type": "title"
                }, {
                    "name": "amazing los angeles race: small group tour and scaven<em>ger</em> hunt .",
                    "id": "33Q9",
                    "title": "Amazing Los Angeles Race: Small Group Tour and Scavenger Hunt .",
                    "type": "title"
                }
            ]
        }
     * @throws Exception
     */
    @RequestMapping(value = "/autocomplete/{startsWith}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTags(@PathVariable String startsWith) throws Exception {
        try{
            return new ResponseEntity<Object>(eventSearchService.getAutoCompleteEvents(startsWith), HttpStatus.OK);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
      INPUT: {"country": "", "city": ""}
   */
    @RequestMapping(value = "count/filters", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> countFilters(@RequestBody String filter) {
        try {
            Long count = eventSearchService.getEventsCount(filter);
            return new ResponseEntity<Object>(count, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /*
      INPUT: {"filters": { "title": "", "destination": {"country": "", "city": ""}, "date": {"from": "", "to": "", "time_zone": ""}, "price": {"high": "", "low" : ""}, "experienceType": "", "tags": []}, "from": 0, "size": 9}
   */
    @RequestMapping(value = "destination/date/filters", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> dateFilters(@RequestBody String filter) {
        try {
            return new ResponseEntity<Object>(this.eventSearchService.dateFilters(filter), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String getQuerySequence(String queryString){
        String splitString[]=queryString.split(",");
        String querySequence="";
        for(int i=0;i<splitString.length;i++){
            querySequence+="\""+splitString[i]+"\"";
            if(i!=splitString.length-1){
                querySequence+=",";
            }
        }

        return querySequence;
    }
}
