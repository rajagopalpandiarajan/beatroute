package com.beatroute.api.rest.request;

import lombok.Data;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Data
public class EventUpdateRequest {

    private String name;

    private String description;

    private String embedLink;

    private String linkToPhoto;

    private String tags;
}
