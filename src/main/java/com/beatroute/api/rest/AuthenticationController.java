package com.beatroute.api.rest;

import com.beatroute.api.model.User;
import com.beatroute.api.service.AuthenticationService;
import com.beatroute.api.service.UserService;
import com.beatroute.api.service.exception.EntityAlreadyExistsException;
import com.beatroute.api.utils.ResponseJsonUtil;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@RestController
public class AuthenticationController {

    @Value("${app.signin.error}")
    private String signInError;

    private final Logger log = LoggerFactory.getLogger(AuthenticationController.class);

    private final AuthenticationService authenticationService;

    private final UserService userService;

    @Autowired
    AuthenticationController(AuthenticationService authenticationService, UserService userService){
        this.authenticationService = authenticationService;
        this.userService = userService;
    }

    /*
        FUNCTION : Authorise user when he comes from auto-generated-mail link
        INPUT : {"email" : "" , "password" : ""}
        OUTPUT : {"id": "bcce3e66-0048-426d-be86-daa697c0890a", "name": "Maddy", "email": "madhu@technome.co", "password": "123456", "country": "India", "userType": "APP_USER", "authorised": false, "mobile": null, "address": null, "city": null, "ratings": null, "interests": null, "reviews": null, "transactions": null}
     */
    @RequestMapping(value = "/signIn", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> login(@RequestBody User loginUser){
        try{
            User authenticatedUser = this.userService.login(loginUser);
            return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(authenticatedUser), HttpStatus.OK);
        } catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/isAuthenticated")
    public boolean isAuthenticated(Principal user) {
        if (user != null){
            return true;
        }else {
            return false;
        }
    }

    @RequestMapping(value = "/isAuthorised")
    public boolean isAuthorised(User user){
        if(user != null) {
            return this.userService.isUserAuthorised(user);
        } else {
            return false;
        }
    }

    /*
        FUNCTION : Authorise user when he comes from auto-generated-mail link
        INPUT : id
        OUTPUT : {"id": "bcce3e66-0048-426d-be86-daa697c0890a", "name": "Maddy", "email": "madhu@technome.co", "password": "123456", "country": "India", "userType": "APP_USER", "authorised": false, "mobile": null, "address": null, "city": null, "ratings": null, "interests": null, "reviews": null, "transactions": null}
     */
    @RequestMapping(value = "/authorise", method = RequestMethod.GET, consumes = MediaType.ALL_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> authoriseUser(@RequestParam("id") long id){
        try{
            User authorizedUser = this.userService.authorizeUser(id);
            return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(authorizedUser), HttpStatus.OK);
        } catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
