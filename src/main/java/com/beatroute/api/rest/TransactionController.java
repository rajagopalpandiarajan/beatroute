package com.beatroute.api.rest;

import com.beatroute.api.model.Transaction;
import com.beatroute.api.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/transaction/")
public class TransactionController {

    @Autowired
    private TransactionRepository transactionRepository;

    @RequestMapping(value = "getTransactions/{ref}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTransactions(@PathVariable String ref){
        List<Transaction> transactions = transactionRepository.findAllByPaymentReferenceId(ref);
        return new ResponseEntity<>(transactions, HttpStatus.OK);
    }

}
