package com.beatroute.api.rest.template;


import org.json.simple.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AutoCompleteTag {

    List<Map> tagArray = null;

    public AutoCompleteTag(){
        this.tagArray = new ArrayList<>();
    }

    public Map getTermQuery(){
        Map termQuery = new HashMap<>();
        Map fieldMap = new HashMap<>();
        fieldMap.put("status", true);
        termQuery.put("term", fieldMap);
        return termQuery;
    }

    public List getSearchList(JSONArray array){
        for(Object o: array){
            Map prefixMap = new HashMap<>();
            Map fieldMap = new HashMap<>();
            fieldMap.put("tags.name", o);

            prefixMap.put("prefix", fieldMap);
            this.tagArray.add(prefixMap);
        }

        this.tagArray.add(getTermQuery());
        return this.tagArray;
    }

    public Map getHighLights(){
        Map fieldMap = new HashMap<>();
        fieldMap.put("post_tags", new ArrayList<>());
        fieldMap.put("pre_tags", new ArrayList<>());
        return fieldMap;
    }
}
