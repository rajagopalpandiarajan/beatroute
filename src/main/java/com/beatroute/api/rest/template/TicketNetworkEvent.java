package com.beatroute.api.rest.template;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@NoArgsConstructor
public class TicketNetworkEvent {

    @Getter
    @Setter
    String name = "";

    @Getter
    @Setter
    private String highPrice = "0.0";

    @Getter
    @Setter
    private String lowPrice = "0.0";

    @Getter
    @Setter
    private String avgPrice = "0.0";

    @Getter
    @Setter
    String supplierReferenceId = "";

    @Getter
    @Setter
    String supplier = "";

    @Getter
    @Setter
    String description = "";

    @Getter
    @Setter
    String genericDetail = "";

    @Getter
    @Setter
    String eventReferenceId = "";

    @Getter
    @Setter
    String imageGallery = "";

    @Getter
    @Setter
    String videoGallery = "";

    @Getter
    @Setter
    String venueMap = "";

    @Getter
    @Setter
    String eventTermsAndConditions = "";

    @Getter
    @Setter
    String notes = "";

    @Getter
    @Setter
    PrimaryCategory primaryCategory;

    @Getter
    @Setter
    SecondaryCategory secondaryCategory;

    @Getter
    @Setter
    List<SessionTemplate> sessions;

    @Getter
    @Setter
    List<TagTemplate> tags;

    @Getter
    @Setter
    List<TenantTemplate> tenants;

    @Getter
    @Setter
    private boolean status = false;

    @Getter
    @Setter
    private Date createdDate = new Date();

}
