package com.beatroute.api.rest.template;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OrderDecisionRequest {

    private String webConfigId;

    private String ticketGroupId;

    private String stage1Guid;

    private String stage2Guid;

    private static String soapRequestData;

    static {
        soapRequestData = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:web=\"http://webservices.ticketnetwork.com\">" +
                "            <soap:Header xmlns:wsa='http://www.w3.org/2005/08/addressing'><wsa:Action>http://webservices.ticketnetwork.com/IMercuryService/GetOrderRequestDecision</wsa:Action></soap:Header>" +
                "   <soap:Body>" +
                "      <web:GetOrderRequestDecision>" +
                "         <web:webConfigID>:webConfigIDData</web:webConfigID>" +
                "         <web:ticketGroupID>:ticketGroupIdData</web:ticketGroupID>" +
                "         <web:stage1Guid>:stage1GuidData</web:stage1Guid>" +
                "         <web:stage2Guid>:stage2GuidData</web:stage2Guid>" +
                "      </web:GetOrderRequestDecision>" +
                "   </soap:Body>" +
                "</soap:Envelope>";
    }

    public String toSoapRequest() {
        return soapRequestData
                .replace(":webConfigIDData", webConfigId)
                .replace(":ticketGroupIdData", String.valueOf(ticketGroupId))
                .replace(":stage1GuidData", stage1Guid)
                .replace(":stage2GuidData", stage2Guid);
    }

}
