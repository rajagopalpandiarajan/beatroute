package com.beatroute.api.rest.template;

import com.beatroute.api.model.Category;
import com.beatroute.api.model.Session;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
public class EventDto {

    private Long id;

    private String name;

    private Category primaryCategory;

    private Category secondaryCategory;

    private String supplierReferenceId;

    private String supplier;

    private String description;

    private String genericDetailTitle;

    private String genericDetailDescription;

    private String genericDetailImage;

    private Date createdDate ;

    private Date updatedDate;

    private String eventReferenceId;

    private String imageGallery;

    private String venueMap;

    private String videoGallery;

    private String eventTermsAndConditions;

    private String notes;

    private boolean status ;

    private BigDecimal lowPrice;

    private BigDecimal highPrice;

    private BigDecimal avgPrice;

    private Set<Session> sessions = new HashSet<>();

    private Long viewCount;

}


