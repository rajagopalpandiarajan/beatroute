package com.beatroute.api.rest.template;

import lombok.Data;

@Data
public class PaxDto {

	private String paxType;
	private int ageFrom;
	private int ageTo;
	private double amount;
	private double boxOfficeAmount;
	
}
