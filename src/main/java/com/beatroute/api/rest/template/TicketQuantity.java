package com.beatroute.api.rest.template;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class TicketQuantity {
    @Getter
    @Setter
	private int quantity;
	
    @Getter
    @Setter
	private int numAdults;
	
    @Getter
    @Setter
	private int numChildren;
	
    @Getter
    @Setter
	private List<Integer> childrenAges;
	
    @Getter
    @Setter
	private List<Integer> adultAges;
}
