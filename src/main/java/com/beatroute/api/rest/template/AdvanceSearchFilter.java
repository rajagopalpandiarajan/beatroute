package com.beatroute.api.rest.template;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@SuppressWarnings({ "rawtypes", "unchecked" })
@Slf4j
public class AdvanceSearchFilter {

    Map filters = new HashMap<>();
    String title;
    String country;
    String city;
    String fromDate;
    String toDate;
    String timeZone;
    String experienceType="";
    String lowPrice;
    String highPrice;
    List tags;
    List addToMust = new ArrayList<>();

    List<String> countries;
    List<String> experiences;
    List<String> events;
    List<String> _id;
    String createdDate;
    String createdFromDate;
    String createdToDate;
    List cities=null;

    public AdvanceSearchFilter(){
    }

    public AdvanceSearchFilter(Map filters){
        this.filters = filters;
        this.title = this.filters.get("title").toString();
        if (this.filters.containsKey("experienceType")) {
        	this.experienceType = this.filters.get("experienceType").toString();
        }
        this.tags = (List) this.filters.get("tags");
        if(StringUtils.isNotEmpty(String.valueOf(this.filters.get("experiences")))){ // != null) {
            this.experiences = (List) this.filters.get("experiences");
        }else{
            this.experiences = null;
        }
        if(StringUtils.isNotEmpty(String.valueOf(this.filters.get("countries")))){ // != null) {
            this.countries = (List) this.filters.get("countries");
        }else{
            this.countries = null;
        }

        if(StringUtils.isNotEmpty(String.valueOf(this.filters.get("cities")))){ // != null) {
            this.cities = (List) this.filters.get("cities");
        }else{
            this.cities = null;
        }
        JSONObject destination = (JSONObject) this.filters.get("destination");
        this.country = destination.get("country").toString();
        this.city = destination.get("city").toString();



        JSONObject date = (JSONObject) this.filters.get("date");
        this.fromDate = date.get("from").toString();
        this.toDate = date.get("to").toString();
        this.timeZone = date.get("time_zone").toString();

        JSONObject price = (JSONObject) this.filters.get("price");
        this.lowPrice = price.get("low").toString();
        this.highPrice = price.get("high").toString();
    }

    public Map getQueryStringBean(String doc, String value){
        Map queryString = new HashMap<>();
        Map fieldMap = new HashMap<>();
        fieldMap.put("default_field", doc);
        fieldMap.put("query", value);
        queryString.put("query_string", fieldMap);
        return queryString;
    }

    public Map getDateRangeQueryFrom(){
        Map rangeMap = new HashMap<>();
        Map dateMap = new HashMap<>();
        Map fieldMap = new HashMap<>();

        try{
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            format.setTimeZone(TimeZone.getTimeZone(this.timeZone));
            Date date = format.parse(this.fromDate);

            fieldMap.put("gte", date.getTime());
            dateMap.put("sessions.beginDateTime", fieldMap);
            rangeMap.put("range", dateMap);
        }catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return rangeMap;
    }

    public Map getDateRangeQueryTo(){
        Map rangeMap = new HashMap<>();
        Map dateMap = new HashMap<>();
        Map fieldMap = new HashMap<>();

        try{
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            format.setTimeZone(TimeZone.getTimeZone(this.timeZone));
            Date date = format.parse(this.toDate);

            fieldMap.put("lte", date.getTime());
            dateMap.put("sessions.beginDateTime", fieldMap);
            rangeMap.put("range", dateMap);
        }catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return rangeMap;
    }

    public Map getDateRangeQueryFromTo(){
        Map rangeMap = new HashMap<>();
        Map dateMap = new HashMap<>();
        Map fieldMap = new HashMap<>();

        try{
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            format.setTimeZone(TimeZone.getTimeZone(this.timeZone));
            Date fromDate = format.parse(this.fromDate);
            Date toDate = format.parse(this.toDate);

            fieldMap.put("gte", fromDate.getTime());
            fieldMap.put("lte", toDate.getTime());
            dateMap.put("sessions.beginDateTime", fieldMap);
            rangeMap.put("range", dateMap);
        }catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return rangeMap;
    }

    public Map getLowPriceRange(){
        Map rangeMap = new HashMap<>();
        Map priceMap = new HashMap<>();
        Map fieldMap = new HashMap<>();

        fieldMap.put("gte", this.lowPrice);
        priceMap.put("lowPrice", fieldMap);
        rangeMap.put("range", priceMap);

        return rangeMap;
    }

    public Map getHighPriceRange(){
        Map rangeMap = new HashMap<>();
        Map priceMap = new HashMap<>();
        Map fieldMap = new HashMap<>();

        fieldMap.put("lte", this.highPrice);
        priceMap.put("lowPrice", fieldMap);
        rangeMap.put("range", priceMap);

        return rangeMap;
    }

    public Map getMatchPhraseQuery(String doc,String value){
        Map matchPhrase = new HashMap<>();
        Map fieldMap = new HashMap<>();
        fieldMap.put(doc, value);
        matchPhrase.put("match_phrase", fieldMap);
        return matchPhrase;
    }

    public Map getTermQuery(){
        Map termQuery = new HashMap<>();
        Map fieldMap = new HashMap<>();
        fieldMap.put("status", true);
        termQuery.put("term", fieldMap);
        return termQuery;
    }

    public List getQuery(){
        addToMust.add(getTermQuery());

        if(this.title.length() > 0){
            addToMust.add(getQueryStringBean("name", this.title.toLowerCase()));
        }

        if(this.country.length() > 0){
            addToMust.add(getMatchPhraseQuery("sessions.venues.country", this.country.toLowerCase()));
        }

        if(this.city.length() > 0){
            addToMust.add(getMatchPhraseQuery("sessions.venues.city", this.city.toLowerCase()));
        }

        if(this.experienceType.length() > 0){
            addToMust.add(getMatchPhraseQuery("primaryCategory.name", this.experienceType.toLowerCase()));
        }
        
        if(this.experiences!=null && this.experiences.size() > 0) {
            List experienceList = new ArrayList();
            experiences.forEach(e -> {
                if(StringUtils.isNotEmpty(e)) {
                    experienceList.add(String.valueOf(e).toLowerCase());
                }
            });
            if(experienceList.size() > 0) {
                addToMust.add(getTermsQueryForIndexEvents("primaryCategory.name", experienceList));
            }
        }

        /*if(this.countries!=null && this.countries.size()>0 && !this.countries.isEmpty()  ) {
            List countryList = new ArrayList();
            countries.forEach(e -> {
                countryList.add(String.valueOf(e).toLowerCase());
            });
            if(countryList.size() == 1) {
                addToMust.add(getMatchPhraseQuery("sessions.venues.country", countryList.get(0).toString().toLowerCase()));
            }else{
                addToMust.add(getTermsQueryForIndexEvents("sessions.venues.country", countryList));
            }
        }

        if(this.cities!=null && this.cities.size() > 0 && !this.cities.isEmpty()) {
            List cityList = new ArrayList();
            cities.forEach(e -> {
                cityList.add(String.valueOf(e).toLowerCase());
            });
            if(cityList.size() == 1) {
                addToMust.add(getMatchPhraseQuery("sessions.venues.city", cityList.get(0).toString().toLowerCase()));
            }else{
                addToMust.add(getTermsQueryForIndexEvents("sessions.venues.city", cityList));
            }
        }*/

        if ((this.fromDate.toString().length() > 0) && (this.toDate.toString().length() > 0)) {
            addToMust.add(getDateRangeQueryFromTo());
        } else {

            if(this.fromDate.toString().length() > 0){
                addToMust.add(getDateRangeQueryFrom());
            }

            if(this.toDate.toString().length() > 0){
                addToMust.add(getDateRangeQueryTo());
            }
        }

        if(this.tags.size() > 0){
            for(Object i: this.tags){
                addToMust.add(getMatchPhraseQuery("tags.name", i.toString().toLowerCase()));
            }
        }

        if(this.lowPrice.toString().length() > 0){
            addToMust.add(getLowPriceRange());
        }

        if(this.highPrice.toString().length() > 0){
            addToMust.add(getHighPriceRange());
        }

        return addToMust;
    }

    public Map getFreeTextQuery(String query){
        Map queryMap = new HashMap<>();
        Map fieldMap = new HashMap<>();
        List fieldList = new ArrayList<>();
        fieldList.add("name");
        fieldList.add("primaryCategory.name");
        fieldList.add("tags.name");
        fieldList.add("description");

        fieldMap.put("fields", fieldList);
        fieldMap.put("query", query);
        queryMap.put("query_string", fieldMap);
        return queryMap;
    }

    public void getEventsOnEnterpriseCriteria(Map filters){
        this.filters = filters;
        this.countries = (List) this.filters.get("countries");
        this.experiences = (List) this.filters.get("experiences");
        this.events = (List) this.filters.get("events");
        this._id = (List) this.filters.get("_id");
        this.createdDate = (String) this.filters.get("createdDate");
    }

    public List getIndexEventsQuery(){
        //addToMust.add(getTermQuery());

        /*if(this.countries.size() > 0){
            for(Object i: this.countries){
                addToMust.add(getTermQueryForIndexEvents("sessions.venues.country", i.toString().toLowerCase()));
            }
        }
        if(this.experiences.size() > 0){
            for(Object i: this.experiences){
                addToMust.add(getTermQueryForIndexEvents("primaryCategory.name", i.toString().toLowerCase()));
            }
        }
        if(this.events.size() > 0){
            for(Object i: this.events){
                addToMust.add(getTermQueryForIndexEvents("eventReferenceId", i.toString().toLowerCase()));
            }
        }
        if(this._id.size() > 0){
            for(Object i: this._id){
                addToMust.add(getTermQueryForIndexEvents("_id", i.toString().toUpperCase()));
            }
        }*/

        if(this.countries.size() > 0){
            List countriesList = new ArrayList();
            countries.forEach(c -> {
                if(StringUtils.isNotEmpty(c)) {
                    countriesList.add(String.valueOf(c).toLowerCase());
                }
            });
            if(countriesList.size() > 0) {
                addToMust.add(getTermsQueryForIndexEvents("sessions.venues.country", countriesList));
            }
        }

        if(this.experiences.size() > 0){
            List experiencesList = new ArrayList();
            experiences.forEach(e -> {
                if(StringUtils.isNotEmpty(e)) {
                    experiencesList.add(String.valueOf(e).toLowerCase());
                }
            });
            if(experiencesList.size() > 0) {
                addToMust.add(getTermsQueryForIndexEvents("primaryCategory.name", experiencesList));
            }
        }

        if(this.events.size() > 0){
            List eventsList = new ArrayList();
            events.forEach(e -> {
                if(StringUtils.isNotEmpty(e)) {
                    eventsList.add(String.valueOf(e).toLowerCase());
                }
            });
            if(eventsList.size() > 0) {
                addToMust.add(getTermsQueryForIndexEvents("eventReferenceId", eventsList));
            }
        }

        if(this._id.size() > 0){
            List _idList = new ArrayList();
            _id.forEach(c -> {
                if(StringUtils.isNotEmpty(c)) {
                    _idList.add(String.valueOf(c).toUpperCase());
                }
            });
            if(_idList.size() > 0) {
                addToMust.add(getTermsQueryForIndexEvents("_id", _idList));
            }
        }

        return addToMust;
    }

    public List getIndexAllEventsQuery() {
        if(this.createdDate.toString().length() > 0){
            addToMust.add(getCreatedDateQueryFrom(createdDate));
        }

        return addToMust;
    }

    public Map getTermsQueryForIndexEvents(String key, List value) {
        Map termQuery = new HashMap<>();
        Map fieldMap = new HashMap<>();
        fieldMap.put(key, value);
        termQuery.put("terms", fieldMap);
        return termQuery;
    }

    public Map getTermQueryForIndexEvents(String key, String value){
        Map termQuery = new HashMap<>();
        Map fieldMap = new HashMap<>();
        fieldMap.put(key, value);
        termQuery.put("term", fieldMap);
        return termQuery;
    }

    public Map getCreatedDateQueryFrom(String createdDate){
        Map rangeMap = new HashMap<>();
        Map dateMap = new HashMap<>();
        Map fieldMap = new HashMap<>();

        try{
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date date = format.parse(createdDate);

            fieldMap.put("lt", date.getTime());
            dateMap.put("createdDate", fieldMap);
            rangeMap.put("range", dateMap);
        }catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return rangeMap;
    }

    public void getEventsOnCreatedDateCriteria(Map filters){
        this.filters = filters;
        this.createdFromDate = this.filters.get("createdFromDate").toString();
        this.createdToDate = this.filters.get("createdToDate").toString();
        this.timeZone = this.filters.get("time_zone").toString();
    }

    public List getCreatedDateRangeQuery() {
        addToMust.add(getTermQuery());
        if ((this.createdFromDate.toString().length() > 0) && (this.createdToDate.toString().length() > 0)) {
            addToMust.add(getCreatedDateRangeQueryFromTo());
        }

        return addToMust;
    }

    public Map getCreatedDateRangeQueryFromTo(){
        Map rangeMap = new HashMap<>();
        Map dateMap = new HashMap<>();
        Map fieldMap = new HashMap<>();

        try{
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            format.setTimeZone(TimeZone.getTimeZone(this.timeZone));
            Date fromDate = format.parse(this.createdFromDate);
            Date toDate = format.parse(this.createdToDate);

            fieldMap.put("gte", fromDate.getTime());
            fieldMap.put("lte", toDate.getTime());
            dateMap.put("createdDate", fieldMap);
            rangeMap.put("range", dateMap);
        }catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return rangeMap;
    }

    //count
    public void setEventsCount(String value, String key){
        if(key.equals("country")){
            this.country = value;
        }else if(key.equals("city")){
            this.city = value;
        }
    }

    public List getEventsCountQuery() {
        addToMust.add(getTermQuery());

        if(StringUtils.isNotEmpty(this.country) && this.country.length() > 0){
            addToMust.add(getMatchPhraseQuery("sessions.venues.country", this.country.toLowerCase()));
        }

        if(StringUtils.isNotEmpty(this.city) && this.city.length() > 0){
            addToMust.add(getMatchPhraseQuery("sessions.venues.city", this.city.toLowerCase()));
        }

        return addToMust;
    }

    //count
    public void setExperiencesCount(String value, String key){
        if(key.equals("country")){
            this.country = value;
        }else if(key.equals("primaryCategory")){
            this.experienceType = value;
        }else if(key.equals("city")){
            this.city = value;
        }
    }

    public List setExperiencesCountQuery() {
        addToMust.add(getTermQuery());

        if(StringUtils.isNotEmpty(this.country) && this.country.length() > 0){
            addToMust.add(getMatchPhraseQuery("sessions.venues.country", this.country.toLowerCase()));
        }

        if(StringUtils.isNotEmpty(this.experienceType) && this.experienceType.length() > 0){
            addToMust.add(getMatchPhraseQuery("primaryCategory.name", this.experienceType.toLowerCase()));
        }

        if(StringUtils.isNotEmpty(this.city) && this.city.length() > 0){
            addToMust.add(getMatchPhraseQuery("sessions.venues.city", this.city.toLowerCase()));
        }

        return addToMust;
    }

    public Map getInQuery(String query,String type){
        Map queryMap = new HashMap<>();
        Map fieldMap = new HashMap<>();
        List fieldList = new ArrayList<>();
        boolean a=(type.equals("country"))?fieldList.add("sessions.venues.country"):fieldList.add("sessions.venues.city");


        fieldMap.put("default_field", fieldList.get(0).toString());
        fieldMap.put("query", query);
        queryMap.put("query_string", fieldMap);

        log.info("getInQuery called for the query - "+query+", and for the type - "+type+". MAP is "+queryMap.toString());
        return queryMap;
    }

    public List getExperienceList() {
        List experienceList = new ArrayList();

        if (this.experiences != null && this.experiences.size() > 0) {
            experiences.forEach(e -> {
                if (StringUtils.isNotEmpty(e)) {
                    experienceList.add(String.valueOf(e).toLowerCase());
                }
            });
        }
        return experienceList;
    }


    }