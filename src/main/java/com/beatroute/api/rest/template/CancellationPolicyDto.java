package com.beatroute.api.rest.template;

import java.util.Date;

import lombok.Data;

@Data
public class CancellationPolicyDto {
	private Date dateFrom;
	private double amount;
}
