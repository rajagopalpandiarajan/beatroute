package com.beatroute.api.rest.template;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class LockedTicketTemplate {

    private String supplierRefId;

    private String sessionId;

    private BigDecimal price;

    private Integer quantity;

    private String ticketGroupId;

    private String lockRequestId;

    private String row;

    private String section;
}
