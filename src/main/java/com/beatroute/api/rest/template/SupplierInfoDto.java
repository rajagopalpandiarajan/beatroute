package com.beatroute.api.rest.template;

import java.util.List;
import lombok.Data;

@Data
public class SupplierInfoDto {

	private List<QuestionDto> additionalInfo;
	
	private String importantNotes;
	
	private String additionalNotes;
	
	private List<CancellationPolicyDto> cancellationPolicies;
	
	private List<PaxDto> paxAmounts;
	
	private String travellerName;
}
