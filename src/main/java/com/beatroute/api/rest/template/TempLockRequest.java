package com.beatroute.api.rest.template;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class TempLockRequest {

    private Integer quantity;

    private String ticketGroupId;

    private BigDecimal wholeSalePrice;

    private String lockRequestId;

}
