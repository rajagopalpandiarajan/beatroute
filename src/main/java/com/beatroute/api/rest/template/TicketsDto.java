package com.beatroute.api.rest.template;

import java.util.List;

import lombok.Data;

@Data
public class TicketsDto {

	private List<TicketDto> tickets;
}
