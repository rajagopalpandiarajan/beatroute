package com.beatroute.api.rest.template;

import org.json.simple.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TagSearch {

    List<Map> tagArray = null;

    public TagSearch(){
        this.tagArray = new ArrayList<>();
    }

    public Map getTermQuery(){
        Map termQuery = new HashMap<>();
        Map fieldMap = new HashMap<>();
        fieldMap.put("status", true);
        termQuery.put("term", fieldMap);
        return termQuery;
    }

    public List getTagSearchList(JSONArray array){
        for(Object o: array){
            Map queryStringMap = new HashMap<>();
            Map fieldMap = new HashMap<>();
            fieldMap.put("tags.name", o);

            queryStringMap.put("match_phrase", fieldMap);
            this.tagArray.add(queryStringMap);
        }

        this.tagArray.add(getTermQuery());

        return this.tagArray;
    }

    public List getTagSearchListForAdmin(JSONArray array){
        for(Object o: array){
            Map queryStringMap = new HashMap<>();
            Map fieldMap = new HashMap<>();
            fieldMap.put("tags.name", o);

            queryStringMap.put("match_phrase", fieldMap);
            this.tagArray.add(queryStringMap);
        }

        return this.tagArray;
    }

    public List searchAllTags(JSONArray array){
        for(Object o: array){
            Map queryStringMap = new HashMap<>();
            Map fieldMap = new HashMap<>();
            fieldMap.put("default_field", "tags.name");
            fieldMap.put("query", o);

            queryStringMap.put("query_string", fieldMap);
            this.tagArray.add(queryStringMap);
        }
        return new ArrayList<>();
    }
}
