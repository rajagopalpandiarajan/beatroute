package com.beatroute.api.rest.template;

import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ElasticSearchQuery {

    Map result = new HashMap<>();
    Map queryMap = new HashMap<>();
    Map boolMap = new HashMap<>();
    Map highLight = new HashMap<>();
    List<Map> must = new ArrayList<>();
    List<Map> mustNot = new ArrayList<>();
    List<Map> should = new ArrayList<>();
    List<String> fields = new ArrayList<>();
    List<Map> sort = new ArrayList<>();
    int size;
    int from;
    long enterpriseId;  //eventsEnterpriseTenants.id

    //TODO: result.put must contain current enterprise id (done)
    public ElasticSearchQuery(){
        this.boolMap.put("must", this.must);
        this.boolMap.put("must_not", this.mustNot);
        this.boolMap.put("should", this.should);

        this.queryMap.put("bool", boolMap);

        this.result.put("query", this.queryMap);
        this.setSort(this.sort);
        this.result.put("sort", this.sort);
        this.result.put("aggs", new HashMap<>());
        this.result.put("highlight", this.highLight);
    }

    /**
     *  "sort": [{
            "viewCount": "desc"
        }]
     */
    public void setSort(List<Map> responseSort){
        Map mapObj = new HashMap();
        mapObj.put("viewCount", "desc");

        responseSort.add(mapObj);
    }

    public void setFrom(String from){
        this.result.put("from",  Integer.valueOf(from));
    }

    public void setSize(String size){
        this.result.put("size", Integer.valueOf(size));
    }

    /*public Map getEnterpriseId(){
        Map termQuery = new HashMap<>();
        Map fieldMap = new HashMap<>();
        ContextData cd = ContextStorage.get();
        long enterpriseId = cd.getEnterpriseId();
        fieldMap.put("eventsEnterpriseTenants.id", enterpriseId);
        termQuery.put("term", fieldMap);
        return termQuery;
        //this.result.put("eventsEnterpriseTenants.id", enterpriseId);
    }*/

    public void setNameField(){
        this.fields.add("name");
        this.fields.add("eventReferenceId");
        this.result.put("fields", this.fields);
    }

    public void setTagField(){
        this.fields.add("tags.name");
        this.result.put("fields", this.fields);
    }

    public void setFields(List fields){
        this.result.put("fields", fields);
    }

    public void setHighLights(Map highLights){
        this.result.put("highlight", highLights);
    }

    public void addToMust(List must){
        this.must = must;
        this.boolMap.put("must", must);
    }

    public void addToMustNot(List mustNot){
        this.boolMap.put("must_not", mustNot);
    }

    public void addToShould(List should){
        this.boolMap.put("should", should);
    }

    public JSONObject getMasterQuery(){
        setSourceExclude();
        addEnterpriseId(this.must);
        return new JSONObject(this.result);
    }

    public JSONObject getAllMasterQuery(){
        setSourceExclude();
        return new JSONObject(this.result);
    }

    public void addEnterpriseId(List<Map> must){
        //[{"term":{"status":true}},{"term":{"eventsEnterpriseTenants.id":1}}}]
        boolean canAdd = true;
        for(int i=0; i< must.size(); i++){
            Map term = (Map) must.get(i).get("term");
            if(term != null && term.containsKey("eventsEnterpriseTenants.id")){
                canAdd = false;
            }
        }

        if(canAdd) {
            Map termQuery = new HashMap<>();
            Map fieldMap = new HashMap<>();
            ContextData cd = ContextStorage.get();
            long enterpriseId = cd.getEnterpriseId();
            fieldMap.put("eventsEnterpriseTenants.id", enterpriseId);
            termQuery.put("term", fieldMap);
            this.must.add(termQuery);
        }
    }

    /*
        "_source": {
            "exclude": ["eventsEnterpriseTenants", "tenants"]
        }
    */
    private void setSourceExclude() {
        Map termQuery = new HashMap<>();
        List<String> excludeList = new ArrayList<>();
        excludeList.add("eventsEnterpriseTenants");
        excludeList.add("tenants");
        termQuery.put("exclude", excludeList);

        this.result.put("_source",  termQuery);
    }
}
