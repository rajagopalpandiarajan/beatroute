package com.beatroute.api.rest.template;

import lombok.Data;

@Data
public class QuestionDto {
	private String code;
	private String text;
	private String answer;
	private boolean required;
}
