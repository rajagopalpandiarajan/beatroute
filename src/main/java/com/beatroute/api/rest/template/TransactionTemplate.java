package com.beatroute.api.rest.template;

import com.beatroute.api.model.Transaction;
import com.beatroute.api.model.enumeration.TransactionStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class TransactionTemplate {

    @Getter
    @Setter
    private double amount;

    @Getter
    @Setter
    private String currency;

    @Getter
    @Setter
    private String paymentReferenceId;

    @Getter
    @Setter
    private String sessionReferenceId;

    @Getter
    @Setter
    private int numberOfTickets;

    @Getter
    @Setter
    private double ticketBookingPrice;

    @Getter
    @Setter
    private TransactionStatus transactionStatus;

    @Getter
    @Setter
    private long userId;

    @Getter
    @Setter
    private long eventId;
}
