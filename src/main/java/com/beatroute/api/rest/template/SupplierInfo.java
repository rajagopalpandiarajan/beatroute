package com.beatroute.api.rest.template;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
public class SupplierInfo {

    @Getter
    @Setter
    private String importantNotes;

    @Getter
    @Setter
    private String additionalNotes;

    @Getter
    @Setter
    private List<AdditionalInfo> additionalInfo;

    @Getter
    @Setter
    private TicketQuantity tickets;

    @Getter
    @Setter
	private List<CancellationPolicyDto> cancellationPolicies;
	
    @Getter
    @Setter
	private List<PaxDto> paxAmounts;

    @Getter
    @Setter
    private String travellerName;
}
