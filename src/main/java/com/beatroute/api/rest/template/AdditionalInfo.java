package com.beatroute.api.rest.template;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class AdditionalInfo {

    @Getter
    @Setter
    private String code;

    @Getter
    @Setter
    private String text;

    @Getter
    @Setter
    private String answer;

    @Getter
    @Setter
    private Boolean required;

}
