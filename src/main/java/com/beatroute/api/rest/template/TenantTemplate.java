package com.beatroute.api.rest.template;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class TenantTemplate {

    @Getter
    @Setter
    String name = "";

    @Getter
    @Setter
    String apiKey = "";
}