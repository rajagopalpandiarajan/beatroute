package com.beatroute.api.rest.template;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@NoArgsConstructor
public class UserBagPackTemplate {

    @Getter
    @Setter
    private String eventId;

    @Getter
    @Setter
    private String eventName;

    @Getter
    @Setter
    private String currency;

    @Getter
    @Setter
    private String numberOfTickets;

    @Getter
    @Setter
    private String row;

    @Getter
    @Setter
    private String seatNo;

    @Getter
    @Setter
    private String section;

    @Getter
    @Setter
    private String sessionReferenceId;

    @Getter
    @Setter
    private String supplierName;

    @Getter
    @Setter
    private String ticketBookingPrice;

    @Getter
    @Setter
    private String ticketGroupId;

    @Getter
    @Setter
    private String address;

    @Getter
    @Setter
    private String sessionDate;

    @Getter
    @Setter
    private String city;

    @Getter
    @Setter
    private String country;

    @Getter
    @Setter
    private String imageGallery;

    @Getter
    @Setter
    private String latestTicketBookingPrice;

    @Getter
    @Setter
    private boolean expired;

    @Getter
    @Setter
    private SupplierInfo supplierInfo;

    @Getter
    @Setter
    private BigDecimal supplierAmount;

    @Getter
    @Setter
    private List<String> shippingMethods;

}
