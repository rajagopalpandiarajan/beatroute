package com.beatroute.api.rest.template;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class PrimaryCategory {

    @Getter
    @Setter
    String name = "";

    @Getter
    @Setter
    String description = "";
}