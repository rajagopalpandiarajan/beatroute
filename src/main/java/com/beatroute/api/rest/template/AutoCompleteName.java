package com.beatroute.api.rest.template;

import org.json.simple.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AutoCompleteName {

    List<Map> nameArray = null;

    public AutoCompleteName(){
        this.nameArray = new ArrayList<>();
    }

    public Map getTermQuery(){
        Map termQuery = new HashMap<>();
        Map fieldMap = new HashMap<>();
        fieldMap.put("status", true);
        termQuery.put("term", fieldMap);
        return termQuery;
    }

    public List getSearchList(JSONArray array){
        for(Object o: array){
            Map prefixMap = new HashMap<>();
            Map fieldMap = new HashMap<>();
            fieldMap.put("name", o);

            prefixMap.put("prefix", fieldMap);
            this.nameArray.add(prefixMap);
        }

        this.nameArray.add(getTermQuery());
        return this.nameArray;
    }


    public Map getHighLights(){
        Map highlights = new HashMap<>();
        Map fieldMap = new HashMap<>();
        fieldMap.put("name", new HashMap<>());
        highlights.put("fields", fieldMap);
        return highlights;
    }

    public List getFields(){
        List fields = new ArrayList<>();
        fields.add("name");
        fields.add("eventReferenceId");
        return fields;
    }
}
