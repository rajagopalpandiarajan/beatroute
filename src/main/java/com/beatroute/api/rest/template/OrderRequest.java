package com.beatroute.api.rest.template;

import lombok.Data;

@Data
public class OrderRequest {

    private String webConfigId;

    private String externalPONumber;

    private String shippingMethodID = "0";

    private String shippingMethodDesc;

    private String shippingPrice = "0";

    private String shippingInstructions;

    private String retailCustomerName;

    private String retailCustomerPhone;

    private String retailCustomerStreet1;

    private String retailCustomerStreet2;

    private String retailCustomerCity;

    private String retailCustomerStateProvince;

    private String retailCustomerCountry;

    private String retailCustomerEmail;

    private String retailCustomerZipcode;

    private String generalPurchaseNotes;

    private String stage1Guid;

    private String stage2Guid;



}
