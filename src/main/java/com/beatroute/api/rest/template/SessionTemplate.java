package com.beatroute.api.rest.template;

import com.beatroute.api.model.Venue;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@NoArgsConstructor
public class SessionTemplate {

    @Getter
    @Setter
    String name = "";

    @Getter
    @Setter
    String description = "";

    @Getter
    @Setter
    int duration = 0;

    @Getter
    @Setter
    String usp = "";

    @Getter
    @Setter
    String rating = "";

    @Getter
    @Setter
    String sessionReferenceId = "";

    @Getter
    @Setter
    Date beginDateTime;

    @Getter
    @Setter
    Date endDateTime;

    @Getter
    @Setter
    Set<Venue> venues;

    @Getter
    @Setter
    private boolean available = true;
}