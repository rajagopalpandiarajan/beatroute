package com.beatroute.api.rest.template;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class TicketDto {

    private Integer quantity;

    private List<Integer> splits;

    private BigDecimal actualPrice;

    private BigDecimal supplierAmount;

    private String rating;

    private String description;

    private String section;

    private BigDecimal wholeSalePrice;

    private BigDecimal retailPrice;

    private Date expectedArrivalDate;

    private String duration;

    private String usp;

    private List<String> shippingMethods;

    private String currency;

    private BigDecimal convertedPrice;

    private String row;

    private String category;

    private String vendorTicketRefId;

    private String supplierName;
    
    private SupplierInfoDto supplierInfo;

}
