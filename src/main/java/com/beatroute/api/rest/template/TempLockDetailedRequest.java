package com.beatroute.api.rest.template;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class TempLockDetailedRequest {

    private Integer quantity;

    private String ticketGroupId;

    private BigDecimal wholeSalePrice;

    private String lockRequestId;

    private String name;
    private String email;
    private String mobile;
    private String sessionDate;
    
    private SupplierInfo supplierInfo;
}
