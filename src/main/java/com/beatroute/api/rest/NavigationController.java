package com.beatroute.api.rest;

import com.beatroute.api.model.*;
import com.beatroute.api.repository.*;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import com.beatroute.api.service.CategoryService;
import com.beatroute.api.service.NavigationService;
import com.beatroute.api.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/api/navigation/")
public class NavigationController {

    @Autowired
    private NavigatorRepository navigator_newRepository;

    @Autowired
    private NavigationService navigationService;

    /**
     *
     * @return
     *  {
            "data": {
                "apiKey": "ab120bs0-42dc-4g76-a6fb-3f2aa12349ed"
            },
            "status": "success"
        }
     */
    @RequestMapping(value = "data/apiKey", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map getTenantApiKey() {
        try {
            return this.navigationService.getTenantApiKey();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage());
        }
    }

    /**
     *
     * @param apiKey
     * @return
     * Success json format:
     *  {
            "Cricket": {
                "primaryCategory": ["theatre", "music", "Excursions", "attractions"],
                "name": "Cricket",
                "icon": "/dev/category/cricket.png",
                "id": 1
            },
            "Sports": {
                "primaryCategory": ["comedy", "dance", "food"],
                "name": "Sports",
                "icon": "/dev/category/sports.png",
                "id": 2
            }
        }
     * Failure json:
     *  {
            "status": "failure",
            "errors": {
                "internalError": "internal Error"
            }
        }
     */
    @RequestMapping(value = "data/{apiKey}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map getTenantCategory(@PathVariable("apiKey") String apiKey) {
        try {
            return this.navigationService.getTenantCategory(apiKey);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage());
        }
    }

    @RequestMapping(value = "description/{primaryCategoryName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getNavigationDescription(@PathVariable("primaryCategoryName") String primaryCategoryName){
        try{
            return new ResponseEntity<>(navigator_newRepository.findOneByPrimaryCategoryNameAndEventIdNull(primaryCategoryName), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "destinationDescription/{secondaryCategory}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> destinationDescription(@PathVariable("secondaryCategory") String secondaryCategory) {
        try{
            return new ResponseEntity<>(this.navigator_newRepository.findOneBySecondaryCategoryName(secondaryCategory), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
