package com.beatroute.api.rest;

import com.beatroute.api.service.UserBagPackService;
import com.beatroute.api.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/api/userBagPack")
public class UserBagPackController {

    @Autowired
    private UserBagPackService userBagPackService;

    /*@RequestMapping(value = "/checkoutBagPack/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> checkoutBagPack(@PathVariable("id") String id){
        try{
            //get shopping cart for user
            List<UserBagPack> userBagPacks = userBagPackService.fetchBagPack(Long.valueOf(id));

            //for each event in user bag pack, check for availability of tickets, update prices, calculate total shopping cart amount
            Map<Object, Object> updatedUserBagPack = userBagPackService.checkAvailabilityOfTickets(userBagPacks);

            return new ResponseEntity<Object>(updatedUserBagPack, HttpStatus.OK);
        }catch (EntityNotFoundException e){
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }*/

    @RequestMapping(value = "/addToUserBagPack", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addToBagPack(@RequestBody String jsonData){
        try{
            return new ResponseEntity<Object>(this.userBagPackService.addToUserBagPack(jsonData), HttpStatus.OK);
        }catch (EntityNotFoundException e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/deleteUserBagPack/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteBagPack(@PathVariable("id") String userId){
        try{
            this.userBagPackService.deleteUserBagPack(Long.parseLong(userId));
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
        }catch (EntityNotFoundException e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/deleteEventFromUserBagPack/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteEventFromBagPack(@PathVariable("id") String bagPackId){
        try{
            this.userBagPackService.deleteEventFromUserBagPack(Long.parseLong(bagPackId));
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
        }catch (EntityNotFoundException e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/getUserBagPack/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getBagPack(@PathVariable("id") String id){
        try{
            //get shopping cart for user
            Map<Object, Object> userBagPacks = userBagPackService.getUserBagPack(Long.valueOf(id));
            return new ResponseEntity<Object>(userBagPacks, HttpStatus.OK);
        }catch (EntityNotFoundException e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
