package com.beatroute.api.rest;

import com.beatroute.api.dto.EmailContent;
import com.beatroute.api.dto.PaymentResponse;
import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.Transaction;
import com.beatroute.api.model.enumeration.TransactionStatus;
import com.beatroute.api.repository.TransactionRepository;
import com.beatroute.api.service.*;
import com.beatroute.api.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;

@Controller
@RestController
@RequestMapping("/api/ticket")
@Slf4j
public class TicketBookingController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private SecureTicketService secureTicketService;

    @Value("${payment.bump.up.percentage}")
    private String bumpUpValue;

    @Autowired
    private EmailHelperService emailHelperService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private RestTemplate networkTemplate;

    @Autowired
    private EnterpriseService enterpriseService;

    @Value("${app.isango.cancel.simulation}")
    private String cancelSimulationUrl;

    @Value("${app.isango.cancel.ticket}")
    private String cancelUrl;

    @RequestMapping(value = "/markupPercent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> bumpUpPercent(@RequestParam String value) throws Exception {
        BigDecimal totalValue = new BigDecimal(value);
        BigDecimal markUpValue = totalValue.multiply(new BigDecimal(bumpUpValue));
        totalValue = totalValue.add(markUpValue);
        totalValue = totalValue.setScale(2, BigDecimal.ROUND_UP);
        return new ResponseEntity<>(totalValue, HttpStatus.OK);
    }

    @RequestMapping(value = "/makePaymentForTickets", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> makePaymentForTickets(@RequestBody String requestJson) throws Exception {
        PaymentResponse paymentResponse = new PaymentResponse();
        try {
            JSONObject jsonObj = new JSONObject(requestJson);
            Long id = jsonObj.getLong("id");
            // get secure tickets for user
            List<SecureTicket> secureTickets = secureTicketService.fetchSecureTicketsWithLockTrue(id);

             // if all tickets are locked successfully, proceed to payment, otherwise return back to the user
            // and provide a message to remove the cart items, which are not successful
            paymentResponse = paymentService.processPayment(secureTickets);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return new ResponseEntity<>(paymentResponse, HttpStatus.OK);

    }

    @RequestMapping(value = "/cancel/booking/{transactionId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> cancelBooking(@PathVariable("transactionId") Long transactionId) throws Exception {
        ResponseEntity<?> response = null;
        Transaction transaction = this.transactionRepository.findOne(transactionId);
        if(transaction != null && transaction.getId() > 0) {
            EmailContent emailContent = this.enterpriseService.getEnterpriseContent();
            log.info("Display Name - "+emailContent.getDisplayName());
            String bookingReferenceId = transaction.getMercuryOrderId();
            BigDecimal totalAmount = transaction.getAmount();
            try {
                response = networkTemplate.exchange(cancelUrl + bookingReferenceId + "/" + totalAmount, HttpMethod.DELETE, null, String.class);
                JSONObject jsonObject = new JSONObject(response);
                String status = jsonObject.getString("body");
                jsonObject = new JSONObject(status);
                if (jsonObject.getString("status").equals("success")) {
                    transaction.setTransactionStatus(TransactionStatus.CANCELLED);
                    this.transactionRepository.save(transaction);
                    //send SMS to user.
                    // this.smsHelperService.sendSmsToUserForCancelBooking(transaction);
                    this.emailHelperService.sendMailForCancelBooking(transaction, emailContent);
                    log.info("complete booking cancel and success message");
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }else{
            log.error("Transaction ID not found");
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("notFound", transactionId.toString()), HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @RequestMapping(value = "/cancel/simulation/{transactionId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> cancelBookingSimulation(@PathVariable("transactionId") Long transactionId) throws Exception {
        ResponseEntity<?> response = null;
        Transaction transaction = this.transactionRepository.findOne(transactionId);
        if(transaction != null && transaction.getId() > 0) {
            String bookingReferenceId = transaction.getMercuryOrderId();
            String totalAmount = String.valueOf(transaction.getAmount()).replace(".","%2E");
            try {
                response = networkTemplate.exchange(cancelSimulationUrl + bookingReferenceId + "/" + totalAmount, HttpMethod.DELETE, null, String.class);
                JSONObject jsonObject = new JSONObject(response);
                String status = jsonObject.getString("body");
                jsonObject = new JSONObject(status);
                if (jsonObject.getString("status").equals("success")) {
                    transaction.setRefundAmount(jsonObject.getBigDecimal("refundAmount"));
                    this.transactionRepository.save(transaction);
                    log.info("Complete saving refund amount of " + jsonObject.getBigDecimal("refundAmount") + " for the transaction id " + transaction.getId());
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                response = new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }else{
            log.error("Transaction ID not found");
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("notFound", transactionId.toString()), HttpStatus.NOT_FOUND);
        }
        return response;
    }

    /**
     * fetch the secure tickets , builds the payment response object
     * post's the payment ref object to pay doller
     * makes the payment
     *
     * @param : {
                    "userId":"23",
                    "pMethod": "VISA",
                    "epMonth": "01",
                    "epYear": "2002",
                    "cardNo": "4123412341234123",
                    "cardholder": "Edward",
                    "remark": "test"
                }
    */
    @RequestMapping(value = "/initiatePaymentForTickets", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> initiatePaymentForTickets(@RequestBody String paymentRequestJson) throws Exception {
        try {
            //TODO: ADD validate func TO CHECK ALL PARAMETERS ARE THERE
            String response= paymentService.initiatePayment(paymentRequestJson);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("Error Not able to make payment ", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    * This method is dummy method. to test the payment with paydollar site added this method*/
    @RequestMapping(value = "/makePaymentTesting", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> makePaymentTesting() throws Exception {
        try {
            //TODO: ADD validate func TO CHECK ALL PARAMETERS ARE THERE
            String response= paymentService.initiatePaymentTesting();
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("Error Not able to make payment ", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
