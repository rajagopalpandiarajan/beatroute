package com.beatroute.api.rest;

import com.beatroute.api.model.User;
import com.beatroute.api.repository.VenueRepository;
import com.beatroute.api.service.RecommendationService;
import com.beatroute.api.service.UserService;
import com.beatroute.api.service.exception.EntityAlreadyExistsException;
import com.beatroute.api.service.exception.EntityNotFoundException;
import com.beatroute.api.utils.ResponseJsonUtil;
import in.ankushs.dbip.api.DbIpClient;
import in.ankushs.dbip.api.GeoEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@RestController
@RequestMapping("/user/")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private VenueRepository venueRepository;

    @Autowired
    private ResourceLoader resourceLoader;

    @Value("${oauth.token.fb}")
    private String facebookOauthToken;

    @Value("${oauth.token.google}")
    private String googleOauthToken;

    @Autowired
    private RecommendationService recommendationService;

    /**
     *
     *
     * @RequestBody usageData - JSON of format:
     * {
    "userId": "1",
    "currentLocation": "india",
    "events": [{
    "name": "Trekking Tenerife - Trekking Tenerife",
    "referenceId": "33996-10888-X-E10-002100055",
    "visits": 3
    }, {
    "name": "Masca - Masca",
    "referenceId": "33993-10899-X-E10-002100006",
    "visits": 4
    },{
    "name": "Masca - Masca-Garachico-Icod",
    "referenceId": "33993-10899-X-E20-011100006",
    "visits": 3
    }, {
    "name": "Siam Park transfer - Transfer to Siam Park",
    "referenceId": "33991-10902-X-E10-002100177",
    "visits": 4
    }],
    "destinations": [{
    "name": "Australia",
    "count": 5
    }, {
    "name": "India",
    "count": 2
    }],
    "tags": [{
    "name": "Key3",
    "count": 5
    }, {
    "name": "Trekking Tenerife",
    "count": 2
    },{
    "name": "Excursions",
    "count": 3
    }, {
    "name": "Sightseeing",
    "count": 2
    }]
    }
     *
     * @return - success
     */
    @RequestMapping(value = "/usage/set", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> setUsageInfo(@RequestBody String usageData) {
        try {
            this.recommendationService.setUsageInfo(usageData);
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //TODO: set java comments with json format

    /**
     *
     * @PathVariable id - user Id
     * @return
     *  {
     *      "status:"success,
     *      "data":{
                "destinations": [
                    "Australia",
                    "India"
                ],
                "eventNames": [
                    event 1,
                    event 2,
                ],
                "tags": [
                    "Rock",
                    "Music"
                ]
            }
        }
     */
    @RequestMapping(value = "/recommendations/get/{userID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getRecommendations(@PathVariable("userID") String id) {
        try {
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(this.recommendationService.getUserRecommendations(id)), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @return
     * {
     *     "eventNames": ["Riverdance", "Wicked"]
     * }
     */
    @RequestMapping(value = "/recommendations/globalEvents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getGlobalEvents() {
        try {
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(this.recommendationService.getGlobalEvents()), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @PathVariable sourceCountry - user source country
     * @return
     * {
     *     destinations: ["india", "spain"]
     * }
     */
    @RequestMapping(value = "/recommendations/globalDestinations/{sourceCountry}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getGlobalDestinations(@PathVariable("sourceCountry") String sourceCountry) {
        try {
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(this.recommendationService.getGlobalDestinations(sourceCountry)), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "geolocation", method = RequestMethod.GET)
    public ResponseEntity<?> getDefaultLocation(HttpRequest httpRequest){
        try{
            URL url = new URL("http://bot.whatismyipaddress.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String ipAddress = new String();
            ipAddress = (in.readLine()).trim();

            log.info("ip : "+ipAddress);

            File gzip = resourceLoader.getResource("classpath:dbip-city-2016-10.csv.gz").getFile();
            DbIpClient client = new DbIpClient(gzip);
            GeoEntity geoEntity = client.lookup(ipAddress);

            String countryCode = convertCountryNameToIsoCode(geoEntity.getCountry());
            String currency = Currency.getInstance(new Locale("",countryCode)).toString();

            Map data = new HashMap<>();
            data.put("country", geoEntity.getCountry());
            data.put("code",countryCode );
            data.put("currency", currency);
            return new ResponseEntity<Object>(data, HttpStatus.OK);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String convertCountryNameToIsoCode(String countryName){
        for(Locale l : Locale.getAvailableLocales()) {
            if (l.getDisplayCountry().equals(countryName)) {
                return l.toString().substring(3);
            }
        }
        return null;
    }

    @RequestMapping(value = "oauth/getTokens", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getOauthTokens(){
        try{
            Map oauthTokens = new HashMap<>();
            oauthTokens.put("facebook", facebookOauthToken);
            oauthTokens.put("google", googleOauthToken);
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(oauthTokens), HttpStatus.OK);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> registerUser(@RequestBody User user){
        try{
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(this.userService.register(user)), HttpStatus.OK);
        }catch (InterruptedException e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("error", "unable to send email"), HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "details", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUserDetails(@RequestParam("userId") long id){
        try {
            log.info("get User details for the user id - "+id);
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(this.userService.get(id)), HttpStatus.OK);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "validateEmail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> validateEmail(@RequestParam("email") String email){
        log.info("validateEmail for the id - "+email);
        Map<String, Boolean> data = new HashMap<>();
        data.put("canUseEmail", this.userService.validateEmail(email));
        return new ResponseEntity<Object>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "validateEmailGetUserType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> validateEmailGetUserType(@RequestParam("email") String email){
        log.info("validateEmailGetUserType for the id - "+email);
        Map<String, Boolean> data = new HashMap<>();
        data.put("canUseEmail", this.userService.validateEmail(email));
       // data = this.userService.getUserType(email);
        return new ResponseEntity<Object>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "oauth/login",  method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> registerOauthUser(@RequestBody User user){
        try {
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(this.userService.oauthLogin(user)), HttpStatus.OK);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "forgotPassword", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> forgotPassword(@RequestParam("email") String email){
        try{
            log.info("started forgot password");
            this.userService.forgotPassword(email);
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
        }catch (EntityNotFoundException e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("error", "Invalid Email"), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "resetPassword", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> resetPassword(@RequestParam("password") String password, @RequestParam("id") long id){
        try{
            log.info("resetPassword for the id - "+id);
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(this.userService.resetPassword(password, id)), HttpStatus.OK);
        }catch (EntityNotFoundException e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findUser(@RequestBody User user){
        try{
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(this.userService.update(user)), HttpStatus.OK);
        }catch (EntityNotFoundException e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "sidebar/transaction/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSideBarDetails(@PathVariable("userId") long id){
        try{
            log.info("get transaction for the user id - "+id);
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(this.userService.getTransactions(id)), HttpStatus.OK);
        }catch (EntityNotFoundException e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "sidebar/wishlist/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSideBarWishList(@PathVariable("userId") long id){
        try{
            log.info("Get Wishlist for the user id - "+id);
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(this.userService.getWishList(id)), HttpStatus.OK);
        }catch (EntityNotFoundException e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @param userId - 1
     * @param eventId - 11
     * @return
     * whole user data
     */
    @RequestMapping(value = "wishlist/add", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addToWishlist(@RequestParam("userId") long userId, @RequestParam("eventId") long eventId) {
        try{
            log.info("add Wishlist for the user id - "+userId);
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(this.userService.addWishList(userId, eventId)), HttpStatus.OK);
        }catch (EntityAlreadyExistsException e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("error", "event already in wishlist"), HttpStatus.ALREADY_REPORTED);
        }catch (EntityNotFoundException e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @param userId - 1
     * @param eventId - 11
     * @return
     * whole user data
     */
    @RequestMapping(value = "wishlist/remove", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> removeFromWishList(@RequestParam("userId") long userId, @RequestParam("eventId") long eventId) {
        try {
            return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(this.userService.removeFromWishList(userId, eventId)), HttpStatus.OK);
        }catch (EntityNotFoundException e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @param userJson
     * input:
     * {
            "userName":"some@gmail.com",
            "oldPassword":"S234",
            "newPassword":"S321"
        }
     * @return
     * Success json -
     *  {
            "status": "success"
        }
     * Failure json -
     * {
            "status": "failure",
            "errors": {
                "error": "Invalid values for products, email"
            }
        }
     */
    @RequestMapping(value = "changePassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> changePassword(@RequestBody String userJson){
        try{
            User authenticatedUser = this.userService.changePassword(userJson);
            return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(authenticatedUser), HttpStatus.OK);
        } catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("error", e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

}
