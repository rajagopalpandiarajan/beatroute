package com.beatroute.api.rest;

import com.beatroute.api.model.Event;
import com.beatroute.api.model.Transaction;
import com.beatroute.api.model.Venue;
import com.beatroute.api.repository.EventRepository;
import com.beatroute.api.repository.TransactionRepository;
import com.beatroute.api.repository.VenueRepository;
import com.beatroute.api.rest.template.UserBagPackTemplate;
import com.beatroute.api.service.AmazonS3Service;
import com.beatroute.api.service.TicketService;
import com.beatroute.api.service.TransactionService;
import com.beatroute.api.utils.ResponseJsonUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/")
public class TicketDownload {

    public static final String CONTENT_DISPOSITION = "Content-Disposition";

    public static final String FORMAT = "inline; filename=\"" + "ticket.pdf" + "\"";

    public static final String APPLICATION_OCTET_STREAM = "application/octet-stream";

    @Value("${app.ticketnetwork.download.ticket}")
    private String downloadUrl;

    @Value("${facebook.shareUrl}")
    private String facebookShareUrl;

    public static final String APPLICATION_TEXT = "text/plain";

    public static final String TXT = "txt";

    @Value("${aws.s3.bucket}")
    private String bucketName;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private VenueRepository venueRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AmazonS3Service amazonS3Service;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TicketService ticketService;

    RestTemplate restTemplate = new RestTemplate();

//    @RequestMapping(value = "ticket/download/{id}", method = RequestMethod.GET, produces = "application/pdf")
//    public String downloadTicket(@PathVariable("id") String id, HttpServletResponse response) throws Exception {
//        try{
//            JSONObject jsonObject = restTemplate.getForEntity(downloadUrl+id, JSONObject.class).getBody();
//            log.info("data object "+jsonObject.get("downloadStream").toString());
//            byte[] downloadStream = Base64.decodeBase64(jsonObject.get("downloadStream").toString());
//
//            response.setContentType(APPLICATION_OCTET_STREAM);
//            response.setHeader(CONTENT_DISPOSITION, String.format(FORMAT));
//            FileCopyUtils.copy(downloadStream, response.getOutputStream());
//            return "";
//        }catch (Exception e){
//            return "redirect:/#/user/booking-history?error=ticket_not_available";
//        }
//    }

    @RequestMapping(value = "facebook/share/{id}", method = RequestMethod.GET)
    public String facebookShare(@PathVariable("id") String id, Model model) {
        try {
            Event event = this.eventRepository.findOneByEventReferenceId(id);
            model.addAttribute("url", facebookShareUrl + "facebook/share/" + id);
            model.addAttribute("redirectUrl", facebookShareUrl + "/event/detail/" + id);
            model.addAttribute("title", event.getName());
            model.addAttribute("description", event.getDescription());
            model.addAttribute("image", event.getImageGallery());
            return "share";
        } catch (Exception e) {
            return "redirect:/";
        }
    }

    @RequestMapping(value = "ticket/download/{id}", method = RequestMethod.GET, produces = "application/pdf")
    public String testDownloadTicketNetwork(@PathVariable("id") long id, HttpServletResponse response) {
        log.info("Started downloading Ticket for the id - " + id);
        try {
            Transaction transaction = this.transactionRepository.findOne(id);
            URL url = new URL(transaction.getUrl());
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String inputLine;
            StringBuilder sb = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);
            }

            byte[] downloadStream = Base64.decodeBase64(sb.toString());
            response.setContentType(APPLICATION_OCTET_STREAM);
            response.setHeader(CONTENT_DISPOSITION, String.format(FORMAT));
            FileCopyUtils.copy(downloadStream, response.getOutputStream());

            return "";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return "redirect:/user/booking-history?error=ticket_not_available";
        }
    }

    /**
     *
     * @param id
     * @param response
     * @return
     * For Dynamic URL -
     *  {
            "status":"success",
            "data":{
                "url":"https://beatroute-dev.tsd-aff.com/tickets/web_self_print.buy/beatroute-dev?crypto_block=60--d4AHNFQamZZawEYFRUV_pGJKHo6ULov2n8SowpIVnWSvSA5EYGc9-4pxx4V5D4YDKDaoXvHhBFiOkwjuEx9BE2ylStgxGuUcgjO_mGeQ6K7tayAsfYabSZk-sNfASCt4vgXG7c46w-IePsjiRIKPIHvJK7x1HqrG3u1f0GAGmIDhYgAda-9Ou0--Z"
            }
        }
       For Manual URL -
     *  {
            "status":"success",
            "data":{
                "date":"Sat 10 Dec 2016, 02:30 AM",
                "transaction_id":"1531506914983",
                "supplierName":"Ingresso",
                "venue":"Belfast Tours Ltd, United Kingdom, United Kingdom",
                "amount":12.0000,
                "numAdults":0,
                "ticketBookingPrice":12.0000,
                "travellerName":"Rakshith D",
                "cancellationPolicies":null,
                "childrenAges":[],
                "paxAmounts":"",
                "adultsAges":[],
                "numberOfTickets":1,
                "importantNotes":null,
                "numChildren":0,
                "additionalInfo":"",
                "eventName":"Test Event - Type 14 (e) (Ingresso)",
                "additionalNotes":null,
                "order_id":"T000-0000-87KC-1767"
            }
        }
     */
    @RequestMapping(value = "ticket/downloadTicket/{id}", method = RequestMethod.GET, consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object testDownload(@PathVariable("id") long id, HttpServletResponse response) {
        log.info("Started downloading Ingresso Ticket for the id - " + id);
        try {
            Transaction transaction = this.transactionRepository.findOne(id);

            if ("Ingresso".equals(transaction.getEvent().getSupplier()) || "Isango".equals(transaction.getEvent().getSupplier())) {
                if (StringUtils.isEmpty(transaction.getUrl())) {
                    // manual html link
                    return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(getManualJsonObject(transaction)), HttpStatus.OK);
                } else {
                    //send redirect as link received
                    return new ResponseEntity<Object>(ResponseJsonUtil.getSuccessResponseJson(getDynamicUrlObject(transaction)), HttpStatus.OK);
                }
            }
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("invalid Input", "Wrong Supplier called, this supplier wont have manual template"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            log.info("Exception caught -> " + e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private JSONObject getDynamicUrlObject(Transaction transaction) {
        JSONObject obj = new JSONObject();
        obj.put("url", transaction.getUrl());

        log.info("Json Object " + obj);
        return obj;
    }

    private JSONObject getManualJsonObject(Transaction transaction) throws Exception {
        JSONObject obj = new JSONObject();

        List<Venue> venueList = this.venueRepository.findAllBySessionId(transaction.getSession().getId());
        String venue = venueList.get(0).getAddress() + ", " + venueList.get(0).getCity() + ", " + venueList.get(0).getCountry();
        SimpleDateFormat formatter = new SimpleDateFormat("EEE dd MMM yyyy, hh:mm aaa");

        ObjectMapper objectMapper = new ObjectMapper();
        UserBagPackTemplate userBagPackTemplate = objectMapper.readValue(transaction.getUserBagPackJson(), UserBagPackTemplate.class);

        /*Calendar cal = Calendar.getInstance();
        cal.setTime(transaction.getSession().getBeginDateTime());
        cal.add(Calendar.HOUR, -5);
        cal.add(Calendar.MINUTE, -30);
        Date date = cal.getTime(); */
        Date date = transaction.getSession().getBeginDateTime();
        log.info("date - " + date);

        String cancellationPolicies = transactionService.getCancellationPolicies(userBagPackTemplate.getSupplierInfo().getCancellationPolicies());
        log.info("cancellationPolicies " + cancellationPolicies);

        String paxAmount = transactionService.getPaxAmounts(userBagPackTemplate.getSupplierInfo().getPaxAmounts());
        log.info("paxAmounts " + paxAmount);

        String additionalInfo = transactionService.getAdditionalInfo(userBagPackTemplate.getSupplierInfo().getAdditionalInfo());
        log.info("additionalInfo " + additionalInfo);

        String travellerName = userBagPackTemplate.getSupplierInfo().getTravellerName();
        if(StringUtils.isEmpty(travellerName)){
            travellerName = transaction.getUser().getName();
        }
        if(userBagPackTemplate.getSupplierName().equals("Isango")){
            obj.put("eventName", transaction.getEvent().getName() +" - "+ userBagPackTemplate.getRow());
        } else {
            obj.put("eventName", transaction.getEvent().getName());
        }

        obj.put("date", formatter.format(date));
        obj.put("transaction_id", transaction.getPaymentReferenceId());
        obj.put("order_id", transaction.getMercuryOrderId());
        obj.put("venue", venue);
        obj.put("numAdults", userBagPackTemplate.getSupplierInfo().getTickets().getNumAdults());
        obj.put("numChildren", userBagPackTemplate.getSupplierInfo().getTickets().getNumChildren());
        obj.put("adultsAges", userBagPackTemplate.getSupplierInfo().getTickets().getAdultAges());
        obj.put("childrenAges", userBagPackTemplate.getSupplierInfo().getTickets().getChildrenAges());
        obj.put("travellerName", travellerName);
        obj.put("cancellationPolicies", cancellationPolicies);
        obj.put("ticketBookingPrice", transaction.getTicketBookingPrice());
        obj.put("numberOfTickets", transaction.getNumberOfTickets());
        obj.put("amount", transaction.getAmount());
        obj.put("supplierName", userBagPackTemplate.getSupplierName());
        obj.put("importantNotes", userBagPackTemplate.getSupplierInfo().getImportantNotes());
        obj.put("additionalNotes", userBagPackTemplate.getSupplierInfo().getAdditionalNotes());
        obj.put("additionalInfo", additionalInfo);
        obj.put("paxAmounts", paxAmount);

        log.info("Json Object " + obj);

        return obj;
    }
    /*
        // Dynamic Json Object URL Present
        {
            "url":"https://www.google.com"
        }

        // Manual Json Object URL NOT Present
        {
            "date":"Sat 10 Dec 2016, 02:30 AM",
            "transaction_id":"1531506914983",
            "supplierName":"Ingresso",
            "venue":"Belfast Tours Ltd, United Kingdom, United Kingdom",
            "amount":12.0000,
            "numAdults":0,
            "ticketBookingPrice":12.0000,
            "travellerName":"Rakshith D",
            "cancellationPolicies":null,
            "childrenAges":[],
            "paxAmounts":"",
            "adultsAges":[],
            "numberOfTickets":1,
            "importantNotes":null,
            "numChildren":0,
            "additionalInfo":"",
            "eventName":"Test Event - Type 14 (e) (Ingresso)",
            "additionalNotes":null,
            "order_id":"T000-0000-87KC-1767"
        }
    */
}
