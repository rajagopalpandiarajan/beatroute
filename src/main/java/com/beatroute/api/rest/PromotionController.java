package com.beatroute.api.rest;

import com.beatroute.api.model.AdvicedEvent;
import com.beatroute.api.model.PromotedEvent;
import com.beatroute.api.repository.AdvicedEventRepository;
import com.beatroute.api.repository.PromotedEventRepository;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import com.beatroute.api.utils.ResponseJsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/promotion/")
public class PromotionController {

    @Autowired
    private PromotedEventRepository promotedEventRepository;

    @Autowired
    private AdvicedEventRepository advicedEventRepository;

    @RequestMapping(value = "promoted/events", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPromotedEvents(){
        try {
            ContextData cd = ContextStorage.get();
            long enterpriseId = cd.getEnterpriseId();
            List<PromotedEvent> promotedEvents = this.promotedEventRepository.findAllByEnterpriseIdOrderByPriorityAsc(enterpriseId);
            return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(promotedEvents), HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("Internal Server Error",e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "adviced/events", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAdvicedEvents(){
        try {
            ContextData cd = ContextStorage.get();
            long enterpriseId = cd.getEnterpriseId();
            List<AdvicedEvent> advicedEvents = this.advicedEventRepository.findAllByEnterpriseIdOrderByPriorityAsc(enterpriseId);
            return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(advicedEvents), HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("Internal Server Error",e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
