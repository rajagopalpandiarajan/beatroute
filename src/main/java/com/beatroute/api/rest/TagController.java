package com.beatroute.api.rest;

import com.beatroute.api.repository.TagRepository;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/tags")
public class TagController {

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @RequestMapping(value = "/autocomplete/{startsWith}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTags(@PathVariable String startsWith) throws Exception {
        ContextData cd = ContextStorage.get();
        long enterpriseId = cd.getEnterpriseId();
        List<String> names = tagRepository.findByNameThatStartsWith(startsWith, enterpriseId);
        List<Map> data = new ArrayList<>();
        for(String n : names){
            Map temp = new HashMap<>();
            temp.put("name", capitalize(n.toLowerCase()));
            temp.put("title", n);
            temp.put("type", "tag");
            data.add(temp);
        }
        return new ResponseEntity(data, HttpStatus.OK);
    }

    private String capitalize(final String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }
}
