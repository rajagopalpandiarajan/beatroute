package com.beatroute.api.rest;

import com.beatroute.api.service.EnterpriseService;
import com.beatroute.api.utils.ResponseJsonUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/enterprise/")
public class EnterpriseController {

    @Autowired
    private EnterpriseService enterpriseService;

    /**
     * Fetches the enterprise info for domain from request Origin
     *
     * @return JSON with enterprise details. Example:
     * {
            "domainTenantId": 1,
            "name": "Infosys Technologies Pvt. Ltd.",
            "displayName" : "Infosys",
            "contact": {
	            "email" : "support@Infosys.com",
	            "contactNo" : "+65 84809775"
            }
        }
     */
    @RequestMapping(value = "info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getEnterpriseInfo() {
        try {
            return new ResponseEntity<Object>(this.enterpriseService.getEnterpriseInfo(), HttpStatus.OK);
        } catch (Exception e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<Object>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get the selected Generic Pages Files List for enterprise.
     *
     * @return  Success json:
     * {
     * "status": "success",
     * "data": {
     *          "files":["file1.html","file2.html"]
     * }
     * }
     * <p>
     * Failure json:
     * {
     * "status": "failure",
     * "errors": {
     * "UNKNOWN": "Internal error"     // errorCode: Error message
     * }
     * }
     */
    @RequestMapping(value = "pages/generic/list", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> listGenericPages() {
        try {
            return new ResponseEntity<>(this.enterpriseService.listGenericPages(),HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     *
     * @return
     * Success json:
     *  {
            "data": [
                {
                    "id": 2,
                    "image": "/dev/images/banner/Screenshot from 2017-04-11 11-01-40.png",
                    "title": "Sports For All",
                    "description": "sub text for sports",
                    "buttonText": "",
                    "link": "",
                    "priority": 2,
                    "enterprise": null,
                    "createdDate": 1492753994000,
                    "updatedDate": 1492753994000
                }, {
                    "id": 4,
                    "image": "/dev/images/banner/Signed Doc Upload save.png",
                    "title": "Sports For All",
                    "description": "",
                    "buttonText": "Explore",
                    "link": "",
                    "priority": 3,
                    "enterprise": null,
                    "createdDate": 1492756486000,
                    "updatedDate": 1492760884000
                }
            ],
            "status": "success"
        }
     * Failure json:
     *  {
            "status": "failure",
            "errors": {
                "UNKNOWN": "Failed to fetch enterprises"
            }
        }
     */
    @RequestMapping(value = "banner/list", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> enterpriseBannerList() {
        try {
            return new ResponseEntity<Object>(this.enterpriseService.enterpriseBannerList(),HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
