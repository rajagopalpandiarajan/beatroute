package com.beatroute.api.rest;

import com.beatroute.api.model.Event;
import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.Tenant;
import com.beatroute.api.model.UserBagPack;
import com.beatroute.api.repository.EventRepository;
import com.beatroute.api.repository.TenantRepository;
import com.beatroute.api.service.*;
import com.beatroute.api.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;

import org.jboss.logging.Param;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Slf4j
@Controller
@RestController
@RequestMapping("/api/supplier")
public class SupplierController {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private SupplierFactory supplierFactory;

    @Autowired
    private UserBagPackService userBagPackService;

    @Autowired
    private SecureTicketService secureTicketService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private TenantRepository tenantRepository;

    @RequestMapping(value = "/tickets/{eventId}/{sessionId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTickets(@PathVariable("eventId") String eventId, @PathVariable("sessionId") String sessionId) throws ExecutionException, InterruptedException {
        Future<String> getTicketsResponse = null;
        try {
            // identify supplier
            Event event = eventRepository.findOneByEventReferenceId(eventId);

            //based on supplier call get tickets
            Supplier supplier = supplierFactory.getSupplier(event.getSupplier());

            //get tickets
            getTicketsResponse = supplier.getTickets(sessionId);

            boolean responseNotReceived = true;
            while (responseNotReceived) {
                responseNotReceived = !getTicketsResponse.isDone();
            }

        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(getTicketsResponse.get(), HttpStatus.OK);
    }

    //London Theatre Direct
    @RequestMapping(value = "/tickets/{eventId}/{sessionId}/{requiredTicketsCount}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTicketsWithCount(@PathVariable("eventId") String eventId, @PathVariable("sessionId") String sessionId, @PathVariable("requiredTicketsCount") String requiredTicketsCount) throws ExecutionException, InterruptedException {
        Future<String> getTicketsResponse = null;
        try {
            // identify supplier
            Event event = eventRepository.findOneByEventReferenceId(eventId);

            //based on supplier call get tickets
            Supplier supplier = supplierFactory.getSupplier(event.getSupplier());

            //get tickets
            getTicketsResponse = supplier.getTickets(sessionId, requiredTicketsCount);

            boolean responseNotReceived = true;
            while (responseNotReceived) {
                responseNotReceived = !getTicketsResponse.isDone();
            }

        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(getTicketsResponse.get(), HttpStatus.OK);
    }

    @RequestMapping(value = "/tickets", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTickets(@RequestBody String requestJson) throws ExecutionException, InterruptedException {
        Future<String> getTicketsResponse = null;
        try {
            JSONObject jsonObj = new JSONObject(requestJson);

            // identify supplier
            Event event = eventRepository.findOneByEventReferenceId(jsonObj.getString("eventId"));

            //based on supplier call get tickets
            Supplier supplier = supplierFactory.getSupplier(event.getSupplier());

            //get tickets
            getTicketsResponse = supplier.getTickets(requestJson);

            boolean responseNotReceived = true;
            while (responseNotReceived) {
                responseNotReceived = !getTicketsResponse.isDone();
            }

        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(getTicketsResponse.get(), HttpStatus.OK);
    }

    @RequestMapping(value = "/sessionInfo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSessionInfo(@RequestBody String requestJson) throws ExecutionException, InterruptedException {
        Future<String> getTicketsResponse = null;
        try {
            JSONObject jsonObj = new JSONObject(requestJson);
            // identify supplier
            Event event = eventRepository.findOneByEventReferenceId(jsonObj.getString("eventId"));
            //based on supplier call get tickets
            Supplier supplier = supplierFactory.getSupplier(event.getSupplier());

            //get session info
            getTicketsResponse = supplier.getSessionInfo(requestJson);

            boolean responseNotReceived = true;
            while (responseNotReceived) {
                responseNotReceived = !getTicketsResponse.isDone();
            }

        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(getTicketsResponse.get(), HttpStatus.OK);
    }

    /**
     *
     * @param requestJson : {"id": 100 }
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/booking", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> bookTickets(@RequestBody String requestJson) throws Exception {
        List<SecureTicket> secureTickets = new ArrayList<>();
        Collection<Future<String>> futures = new ArrayList<Future<String>>();
        try {
            JSONObject jsonObj = new JSONObject(requestJson);
            Long id = jsonObj.getLong("id");
            // get shopping cart for the user
            List<UserBagPack> bagPackList = userBagPackService.fetchBagPack(id);

            //delete tickets that were locked but never proceeded for payment
            secureTicketService.deleteSecureTickets(id);

            // if not items, do not proceed, otherwise, get the list of secure tickets (locking the tickets)
            // for each event in bagpack identify supplier and lock tickets
            futures = ticketService.lockTicketsForSupplier(bagPackList);

            // for each event in bagpack get secure ticket(created after lock is success)
            secureTickets = ticketService.getSecureTickets(bagPackList, futures);

        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(secureTickets, HttpStatus.OK);
    }

    @RequestMapping(value = "/checkoutBagPack/{id}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> checkoutBagPack(@PathVariable("id") String id) throws Exception {
        Map<Object, Object> updatedUserBagPack = null;
        Map<Long, Future<String>> futures = new HashMap<Long, Future<String>>();
        try {
            //get shopping cart for user
            List<UserBagPack> userBagPacks = userBagPackService.fetchBagPack(Long.valueOf(id));

            //for each event in user bag pack, check for availability of tickets
            futures = userBagPackService.checkAvailabilityOfSupplierTickets(userBagPacks);

            //for each event in user bag pack, update prices and calculate the shopping cart total
            updatedUserBagPack = userBagPackService.calculateUserBagPack(userBagPacks, futures);
        }catch (Exception e) {
            log.error(e.getMessage(),e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError",e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<Object>(updatedUserBagPack, HttpStatus.OK);
    }

    /**
     *
     * @param apiKey - g7r4k7ag-9e1w-4via-ioed-c1o0d8lk46rd
     * @return
     *  {
            "markUpPercent": 0.2,
            "status": "success"
        }
     */
    @RequestMapping(value = "tenants/{apiKey}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getMarkUpPercent(@PathVariable("apiKey") String apiKey){
        try{
            Tenant tenant = this.tenantRepository.findByApiKey(apiKey);
            log.info("Get Tenants markUpPercent by apiKey called for the tenant - "+tenant.getName());
            Map response = new HashMap();
            response.put("status", "success");
            response.put("markUpPercent", tenant.getMarkUpPercent());
            return new ResponseEntity<Object>(response, HttpStatus.OK);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}