package com.beatroute.api.rest;

import com.beatroute.api.model.Event;
import com.beatroute.api.service.EventSyncService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
public class EventSyncController {

    @Autowired
    private EventSyncService eventSyncService;

    @RequestMapping(value = "/events/sync", method = RequestMethod.GET)
    public ResponseEntity<?> syncEvents(){
        try{
            Map map = new HashMap<>();
            map.put("updated", eventSyncService.syncEvents());
            return new ResponseEntity<Object>(map, HttpStatus.OK);
        }catch (Exception e){
            log.error(e +" : "+ e.getMessage());
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
