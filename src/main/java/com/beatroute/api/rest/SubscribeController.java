package com.beatroute.api.rest;

import com.beatroute.api.service.SubscribeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/subscribe/")
@Slf4j
public class SubscribeController {

    @Autowired
    private SubscribeService subscribeService;

    /**
     *
     * @param email
     * @return
     */
    @RequestMapping(value = "emailId", method = RequestMethod.GET, consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> validateEmail(@RequestParam("email") String email){
        log.info("Started saving from controller for Subscription email to DB");
        try{
            return new ResponseEntity<Object>(this.subscribeService.save(email), HttpStatus.OK);
        }catch (Exception e){
            log.info(e.getMessage(), e);
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
