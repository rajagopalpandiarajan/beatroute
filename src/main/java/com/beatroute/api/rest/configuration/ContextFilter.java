package com.beatroute.api.rest.configuration;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.beatroute.api.model.DomainTenant;
import com.beatroute.api.repository.DomainTenantRepository;
import com.beatroute.api.utils.HttpUtil;

import lombok.extern.slf4j.Slf4j;

@Component
@Configuration
@Slf4j
public class ContextFilter implements Filter {
	
	@Autowired
	private DomainTenantRepository domainTenantRepository;
	 
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;

        //fetch the origin domain and set in context object
        String origin=request.getHeader("Origin");
        if (origin !=null && !origin.isEmpty()) {
            ContextData cd=new ContextData();
        	cd.setOriginHost(origin);
	
	        String subDomain=HttpUtil.getSubdomain(origin);
	        cd.setOriginSubDomain(subDomain);
	        
	        try {
		        DomainTenant domainTenant = domainTenantRepository.findBySubDomainName(subDomain);
		        cd.setEnterpriseId(domainTenant.getEnterprise().getId());
	        } catch(Exception e) {
	        	log.error("Failed to get enterpriseId for: "+origin);
                log.error(e.getMessage(), e);
	        }
	        
//	        log.info("Setting contextData: "+cd.toString());
	        ContextStorage.set(cd);
        }
        
        chain.doFilter(req, res);
    }

    public void init(FilterConfig filterConfig) {
    }

    public void destroy() {
    }

}
