package com.beatroute.api.rest.configuration;


import com.beatroute.api.model.enumeration.UserType;
import com.beatroute.api.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("admin").password("password").roles("USER", "ADMIN");
        auth.userDetailsService(authenticationService);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests().antMatchers("/isAuthenticated", "/isAuthorised").permitAll();

        http.csrf().ignoringAntMatchers("/login");

        http
                .authorizeRequests()
                .antMatchers("/", "/index.html", "/#/**", "/activate/**", "/error", "/api/**", "/error.html")
                .permitAll()
                .and()
                .formLogin().loginPage("/").failureUrl("/?error=invalid_credentials").permitAll()
                .and()
                .logout().logoutUrl("/logout")
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/")
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/error?error=access_denied")
                .and()
                .authorizeRequests()
                .antMatchers("#/user/**")
                .hasRole(UserType.APP_USER.toString());
//                .and()
//                .addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class);

        http.csrf().disable();
    }
}
