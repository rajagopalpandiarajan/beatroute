package com.beatroute.api.rest.configuration;

public class ContextStorage {
	static private ThreadLocal<ContextData> threadLocal = new ThreadLocal<ContextData>();

	public static void set(ContextData cd) {
		threadLocal.set(cd);
	}

	public static void unset() {
		threadLocal.remove();
	}

	public static ContextData get() {
		return threadLocal.get();
//		if(threadLocal.get() != null) {
//			return threadLocal.get();
//		}else{
//			return new ContextData();
//		}
	}
}
