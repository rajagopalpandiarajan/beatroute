package com.beatroute.api.rest.configuration;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContextData {

	@Getter
    @Setter
	private String originHost;
	
	@Getter
    @Setter
	private String originSubDomain;

	private Long enterpriseId;

	public Long getEnterpriseId() {

		return enterpriseId;
//		if(enterpriseId != null && enterpriseId > 0) {
//			return enterpriseId;
//		}else{
//			return Long.valueOf(1);
//		}
	}

	public void setEnterpriseId(Long enterpriseId) {
		this.enterpriseId = enterpriseId;
	}
	
	public ContextData copy(ContextData src) {
		ContextData target=new ContextData();
		target.setOriginHost(src.getOriginHost());
		target.setOriginSubDomain(src.getOriginSubDomain());
		target.setEnterpriseId(src.getEnterpriseId());
		
		return target;
	}
	
	@Override
	public String toString(){
		return "originHost: "+originHost+" ;originSubDomain: "+originSubDomain+" ;enterpriseId: "+enterpriseId;
	}
}
