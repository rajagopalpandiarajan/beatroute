package com.beatroute.api.rest;

import com.beatroute.api.service.CategoryService;
import com.beatroute.api.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/category/")
@Slf4j
@CrossOrigin

public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     *
     *
     * @return
     * Success json:
     * {
            "data": {
                "displayCategory": [{
                    "id": 1,
                    "name": "Sports",
                    "icon": "/beta/category/sports.png",
                    "mappedCategories": "1, 2, 3",
                    "mappedEvents": "7",
                    "createdDate": 1491972969000,
                    "updatedDate": 1491972969000
                }, {
                    "id": 2,
                    "name": "Food",
                    "icon": "/beta-jet/category/food.png",
                    "mappedCategories": "8, 9, 13",
                    "mappedEvents": "17,112",
                    "createdDate": 1491973657000,
                    "updatedDate": 1491973657000
                }]
                },
            "status": "success"
        }
     * Failure json:
     *  {
            "status": "failure",
            "errors": {
                "UNKNOWN": "Failed to fetch enterprises"
            }
        }
     */
    @RequestMapping(value = "display/list", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> listCategories() {
        try {
            return new ResponseEntity<Object>(this.categoryService.listCategories(),HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson("internalError", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
