package com.beatroute.api.job;

import com.beatroute.api.service.AmazonS3Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import java.util.Date;
@Slf4j
@Configuration
public class TicketDownloadScheduler {

    @Autowired
    AmazonS3Service amazonS3Service;

    @Scheduled(cron = "${ticket.upload.time}")
    public void reportCurrentTime() {

        amazonS3Service.uploadTicket();
        log.info("scheduler running at "+new Date());
    }
}
