package com.beatroute.api.job;

import com.beatroute.api.service.EventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;

@Slf4j
@Configuration
public class ReIndexEventsForAllEnterprisesScheduler {

   @Autowired
   private EventService eventService;

    //@Scheduled(cron = "${reindex.events.scheduler.time}")
    public void reIndexEventsForAllEnterprisesScheduler() {
        log.info("Started ReIndex Events For All Enterprises Scheduler at "+new Date());
        /*try {
            this.eventService.reIndexEventsForAllEnterprises();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }*/
        log.info("Completed ReIndex Events For All Enterprises Scheduler at "+new Date());
    }

}
