package com.beatroute.api.dto;

import lombok.Data;

@Data
public class EventElasticDto {

    private Integer took;
    private Boolean timed_out;
    private ShardsDto _shards;
    private HitsDto hits;

}
