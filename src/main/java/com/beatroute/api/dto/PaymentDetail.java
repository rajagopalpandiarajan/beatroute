package com.beatroute.api.dto;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class PaymentDetail {

    @Value("${asiapay.credentials.merchant.id}")
    private String merchantID;

    @Value("${asiapay.secure.hash.secret}")
    private String secureHashSecret;

    @Value("${asiapay.payment.type}")
    private String payType;

    @Value("${asiapay.currency.code}")
    private String currCode;

    @Value("${payment.bump.up.percentage}")
    private String paymentBumpUpPercent;

    @Value("${asiapay.payment.url}")
    private String paymentUrl;

    @Value("${asiapay.mps.mode}")
    private String mpsMode;

    @Value("${asiapay.mps.payMethod}")
    private String payMethod;

    @Value("${asiapay.success.url}")
    private String successUrl;

    @Value("${asiapay.failure.url}")
    private String failureUrl;

    @Value("${asiapay.cancel.url}")
    private String cancelUrl;

    @Value("${asiapay.language}")
    private String language;

    @Value("${ticketing.network.template.id}")
    private String templateId;

    @Value("${ticketing.network.redirect}")
    private String redirect;

}
