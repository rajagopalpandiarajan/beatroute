package com.beatroute.api.dto;

import com.beatroute.api.rest.template.EventDto;
import lombok.Data;

@Data
public class EventHitsDto {

    private String _index;
    private String _type;
    private String _id;
    private Double _score;
    private EventDto _source;

}
