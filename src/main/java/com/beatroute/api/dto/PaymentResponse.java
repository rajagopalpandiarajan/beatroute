package com.beatroute.api.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PaymentResponse {

    private String url;

    private String merchantId;

    private String orderRef;

    private String currCode;

    private String mpsMode;

    private String successUrl;

    private String cancelUrl;

    private String failUrl;

    private String payType;

    private String lang;

    private String payMethod;

    private String secureHash;

    private String templateId;

    private String redirect;

    private String amount;

}
