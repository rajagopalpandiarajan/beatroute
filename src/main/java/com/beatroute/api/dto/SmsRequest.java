package com.beatroute.api.dto;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;

@Component
@Data
public class SmsRequest {

    @Value("${app.payment.sms.url}")
    private String smsUrl;

    @Value("${app.payment.sms.username}")
    private String smsUserName;

    @Value("${app.payment.sms.password}")
    private String smsPassword;

    @Value("${app.payment.sms.from}")
    private String smsFrom;

    //@Value("${app.payment.sms.returnUrl}")
    //private String smsReturnUrl;

    private String sendSmsTo;

    private String eventName;

    private LocalDate eventSessionDate;

    private LocalTime eventSessionTime;

    private String text;

    private String smsReturnUrl;

    private static String restData;

    static {
        restData = ":smsUrl&username=:smsUserName&password=:smsPassword&to=:sendSmsTo&from=:smsFrom&text=:text&dlr-url=:smsReturnUrl";
    }

    public String toRestRequest(){
        return  restData.replace(":smsUrl", smsUrl)
                .replace(":smsUserName", smsUserName)
                .replace(":smsPassword", smsPassword)
                .replace(":sendSmsTo", sendSmsTo)
                .replace(":smsFrom", smsFrom)
                .replace(":text", text)
                .replace(":eventName", eventName)
                .replace(":eventSessionDate", eventSessionDate.toString())
                .replace(":eventSessionTime", eventSessionTime.toString())
                .replace(":smsReturnUrl", smsReturnUrl);
    }

}