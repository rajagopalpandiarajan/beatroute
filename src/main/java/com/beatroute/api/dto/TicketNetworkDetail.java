package com.beatroute.api.dto;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class TicketNetworkDetail {

    @Value("${ticketing.network.template.id}")
    private String templateId;

    @Value("${ticketing.network.external.po.number}")
    private String poNumber;

    @Value("${ticketing.network.shipping.method.id}")
    private String shippingMethod;

    @Value("${ticketing.network.shipping.method.desc}")
    private String shippingMethodDesc;

    @Value("${ticketing.network.street.one}")
    private String streetOne;

    @Value("${ticketing.network.street.two}")
    private String streetTwo;

    @Value("${ticketing.network.city}")
    private String city;

    @Value("${ticketing.network.state.province}")
    private String state;

    @Value("${ticketing.network.country}")
    private String country;

    @Value("${ticketing.network.zipcode}")
    private String zipCode;

    @Value("${ticketing.network.purchase.notes}")
    private String notes;
}
