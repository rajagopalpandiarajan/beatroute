package com.beatroute.api.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class EmailContent {

    @Getter
    @Setter
    private String companyName;

    @Getter
    @Setter
    private String displayName;

    @Getter
    @Setter
    private String supportContactName;

    @Getter
    @Setter
    private String supportContactNo;

    @Getter
    @Setter
    private String supportEmailId;

    @Getter
    @Setter
    private String host;

    @Getter
    @Setter
    private String subDomain;

    @Getter
    @Setter
    private String imageUrl;

}
