package com.beatroute.api.dto;

import lombok.Data;

@Data
public class ShardsDto {

    private Integer total;
    private Integer successful;
    private Integer failed;

}
