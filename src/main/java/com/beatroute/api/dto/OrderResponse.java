package com.beatroute.api.dto;

import lombok.Data;

import java.util.Date;

@Data
public class OrderResponse {

    private int highSeat;

    private String invoiceID;

    private int lowSeat;

    private String mercuryOrderID;

    private String message;

    private String row;

    private String section;

    private String sellerEmail;

    private String sellerName;

    private String sellerPhone;

    private Date shipByDate;

    private boolean shippingTrackingCreatedStatus;

    private String shippingTrackingCreatedStatusDesc;

    private String shippingTrackingID;

    private String shippingTrackingLocation;

    private boolean success;
}
