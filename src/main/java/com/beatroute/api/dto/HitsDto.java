package com.beatroute.api.dto;

import lombok.Data;
import java.util.List;

@Data
public class HitsDto {

    private Integer total;
    private Double max_score;
    private List<EventHitsDto> hits;

}
