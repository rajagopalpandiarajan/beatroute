package com.beatroute.api.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class Enquiry {

    @Getter
    @Setter
    private String firstName;

    @Getter
    @Setter
    private String lastName;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String contactNumber;

    @Getter
    @Setter
    private String designation;

    @Getter
    @Setter
    private String companyName;

    @Getter
    @Setter
    private String address;

    @Getter
    @Setter
    private String regId;

    @Getter
    @Setter
    private String typeOfSolution;

}
