package com.beatroute.api.repository;

import com.beatroute.api.model.Category;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.HashSet;
import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    Category findOneByName(String name);

    List<Category> findAllByNameIn(List categoryNamesArray);

    @Query("select distinct c.name from Category c, Event e where c = e.primaryCategory and e.id IN (:eventIdList) order by c.name")
    List<String> findAllPrimaryCategoryForEnterprise(@Param(value = "eventIdList") List<Long> eventIdList);

    @Query("Select name, description from Category c where c.name like :startsWith% order by c.name asc")
    @Cacheable(value = "category")
    List<Category> findByNameThatStartsWith(@Param(value = "startsWith") String startsWith, Pageable pageable);

    Category findById(Long id);

    @Query("Select name from Category c where c.id in :ids")
    List<String> getNameByIdIn(@Param(value="ids") HashSet<String> ids);

    @Query(value = "Select * from categories c where c.name like %:name% limit 1", nativeQuery = true)
    Category findByNameThatStartsWith(@Param(value = "name") String name);

}
