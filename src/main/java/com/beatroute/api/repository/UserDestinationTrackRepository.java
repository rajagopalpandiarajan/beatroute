package com.beatroute.api.repository;

import com.beatroute.api.model.UserDestinationTrack;
import com.beatroute.api.model.UserEventTrack;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserDestinationTrackRepository extends JpaRepository<UserDestinationTrack, Long>{

    UserDestinationTrack findByUserIdAndSearchDestination(String userId, String searchDestination);

    /*@Query("SELECT DISTINCT(u.searchDestination) from UserDestinationTrack u where u.userId = :userId order by u.searchCount, u.lastSearchedDate desc")
    List<String> findTop5ByUserId(@Param(value = "userId") String userId);*/

    @Query("SELECT u.searchDestination from UserDestinationTrack u where u.userId = :userId")
    List<String> findByUserId(@Param(value = "userId") String userId, Pageable pageable);
}
