package com.beatroute.api.repository;

import com.beatroute.api.model.DomainTenant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DomainTenantRepository extends JpaRepository<DomainTenant, Long> {

    DomainTenant findBySubDomainName(String name);

}

