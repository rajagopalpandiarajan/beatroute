package com.beatroute.api.repository;

import com.beatroute.api.model.EnterpriseCommercial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EnterpriseCommercialsRepository extends JpaRepository<EnterpriseCommercial, Long> {

    @Query(value = "select * from enterprise_commercials where enterprise_id = :id", nativeQuery = true)
    EnterpriseCommercial findOneByEnterpriseId(@Param(value = "id") Long id);

}
