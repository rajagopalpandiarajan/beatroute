package com.beatroute.api.repository;

import com.beatroute.api.model.DestinationDisplay;
import com.beatroute.api.model.enumeration.DestinationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DestinationRepository extends JpaRepository<DestinationDisplay, Long> {

    List<DestinationDisplay> findByCountry(String country);

    @Query("SELECT d.id,d.city,d.image,d.priority from DestinationDisplay d where  d.destinationType=:destinationType and d.priority is not null order by d.priority asc")
    List<Object[]> getDestinationByTypeOrderByPriorityAsc(@Param("destinationType")DestinationType destinationType);

    List<DestinationDisplay> findByDestinationType(DestinationType country);

    @Query(value="SELECT * from destination_display d where d.country = :country  and country!='' limit 1", nativeQuery = true)
    DestinationDisplay findOneByCountryName(@Param("country")String country);

    @Query(value="SELECT * from destination_display d where d.city = :city and city!='' limit 1", nativeQuery = true)
    DestinationDisplay findOneByCityName(@Param("city")String city);

    @Query(value = "select distinct d.country as country,d.country_alias as alias from destination_display d where d.country_alias like :startsWith limit :count", nativeQuery = true)
    List<Object[]> findAllDistinctCountriesAliasStartsWith(@Param(value = "startsWith") String startsWith, @Param(value = "count") int count);

    @Query(value = "select distinct d.city as city,d.city_alias as alias from destination_display d  where d.city_alias like :startsWith limit :count", nativeQuery = true)
    List<Object[]> findAllDistinctCitiesAliasStartsWith(@Param(value = "startsWith") String startsWith,@Param(value = "count") int count);

}