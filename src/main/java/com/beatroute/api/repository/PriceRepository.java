package com.beatroute.api.repository;

import com.beatroute.api.model.Price;
import com.beatroute.api.model.Session;
import com.beatroute.api.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PriceRepository extends JpaRepository<Price, Long> {

    List<Price> findAllBySession(Session session);

    Price findByTicket(Ticket ticket);
}
