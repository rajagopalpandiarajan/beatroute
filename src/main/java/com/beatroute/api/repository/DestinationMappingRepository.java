package com.beatroute.api.repository;

import com.beatroute.api.model.DestinationDisplay;
import com.beatroute.api.model.DestinationMapping;
import com.beatroute.api.model.Enterprise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DestinationMappingRepository extends JpaRepository<DestinationMapping, Long> {

    @Query("SELECT d from DestinationMapping d where country = :destinationDisplay AND (d.priority is not null or d.priority!='') AND enterprise=:enterprise order by d.priority asc")
    List<DestinationMapping> getCityMappingOrderByPriorityAsc(@Param("destinationDisplay")DestinationDisplay destinationDisplay, @Param("enterprise")Enterprise enterprise);

}