package com.beatroute.api.repository;

import com.beatroute.api.model.Category;
import com.beatroute.api.model.Enterprise;
import com.beatroute.api.model.Event;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Set;

public interface EventRepository extends JpaRepository<Event, Long> {

    Event findOneByName(String name);

    @SuppressWarnings("rawtypes")
	Page<Event> findAllBySupplierInAndPrimaryCategoryNotInAndStatusTrue(List supplier, List primaryCatrgory, Pageable pageable);

    @Query("Select e from Event e where e.name like :startsWith and e.status = true order by e.name asc")
    @Cacheable(value = "events")
    List<Event> findByNameThatStartsWith(@Param(value = "startsWith") String startsWith, Pageable pageable);

    @Query(value="Select name, event_reference_id from events e join events_enterprise_tenants eet ON eet.event_id=e.id "
            + "where eet.enterprise_id = :enterpriseId AND e.name like :startsWith and e.status = true order by e.name asc limit :count", nativeQuery = true)
    List<Object> findByNameThatStartsWithForEnterprise(@Param(value = "enterpriseId") Long enterpriseId,
                                                       @Param(value = "startsWith") String startsWith, @Param(value = "count") int count);

    @Query(value = "Select * from events where event_reference_id = :eventReferenceId and status = true limit 1", nativeQuery = true)
    Event findOneByEventReferenceId(@Param(value = "eventReferenceId") String eventReferenceId);

    @Query(value = "Select * from events where event_reference_id = :eventReferenceId limit 1", nativeQuery = true)
    Event findOneByEventReferenceIdStatusFalseOrTrue(@Param(value = "eventReferenceId") String eventReferenceId);

    @Query("select distinct(e.primaryCategory) from Event e")
    List<Event> findAllPrimaryCategory();

    @Query("select count(e) from Event e")
    long findCountOfEvents();

    @Modifying
    @Transactional
    @Query(value = "delete from events_enterprise_tenants where enterprise_id = :enterpriseId", nativeQuery = true)
    int deleteAllEnterpriseTenantsByEnterprise(@Param(value = "enterpriseId") Long enterpriseId);

    @Query(value = "select event_id from events_enterprise_tenants where enterprise_id = :enterpriseId", nativeQuery = true)
    List<BigInteger> findAllEnterpriseTenantsByEnterprise(@Param(value = "enterpriseId") Long enterpriseId);


    Page<Event> findAllByEventsEnterpriseTenants(Enterprise enterprise, Pageable pageable);

    /*Event findOneById(long eId);

    @Query("SELECT e FROM Event e WHERE e.id IN :id")
    Page<Event> findAllById(List<Long> id, Pageable pageable);*/

    @Query("SELECT e from Event e WHERE e.createdDate >= :createdDate")
    Page<Event> findAll(@Param(value = "createdDate") Date createdDate, Pageable var1);

    @Query("SELECT e from Event e WHERE e.eventReferenceId IN (:eventReferenceIdSet) AND (e.primaryCategory IN (:categoryList) OR e.supplier IN (:supplier))")
    List<Event> findAllByEventReferenceId(@Param(value = "eventReferenceIdSet") Set<String> eventReferenceIdSet, @Param(value = "categoryList") List<Category> categoryList, @Param(value = "supplier") List<String> supplier);

    @Query("SELECT e from Event e WHERE e.eventReferenceId IN (:eventReferenceIdSet)")
    List<Event> findAllByEventReferenceIdIn(@Param(value = "eventReferenceIdSet") Set<String> eventReferenceIdSet);

    @Query("SELECT e from Event e WHERE e.eventReferenceId = :eventReferenceId")
    Event findOneByEventReferenceIdWithStatusTrueOrFalse(@Param(value = "eventReferenceId") String eventReferenceId);

    @Query("SELECT e from Event e where (e.createdDate between :startDate AND :endDate) AND e.status = true order by e.createdDate")
    List<Event> findAllInBetweenCreatedDate(@Param(value = "startDate") Date dateStartDate, @Param(value = "endDate") Date dateEndDate);

    //@Query(value = "select distinct c.name from categories c where c.id IN (select distinct e.primary_category_id from events e where e.id IN (select s.event_id from sessions s where s.id IN (select v.session_id from venues v where v.country=:country)));", nativeQuery = true)
    @Query(value = "select distinct c.name,c.id from categories c JOIN events e on c.id = e.primary_category_id JOIN sessions s ON e.id = s.event_id JOIN venues v ON s.id = v.session_id where v.country=:country", nativeQuery = true)
    List<Object[]> findAllPrimaryCategoryByCountry(@Param(value = "country") String country);

    @Query(value = "select distinct c.name,c.id from categories c JOIN events e on c.id = e.primary_category_id JOIN sessions s ON e.id = s.event_id JOIN venues v ON s.id = v.session_id where v.city=:city", nativeQuery = true)
    List<Object[]> findAllPrimaryCategoryByCity(@Param(value = "city") String city);
}
