package com.beatroute.api.repository;

import com.beatroute.api.model.Enterprise;
import com.beatroute.api.model.TenantCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TenantCategoryRepository extends JpaRepository<TenantCategory, Long> {

    List<TenantCategory> findAllByTenantIdAndPrimaryCategoryId(Long tenantId, Long primaryCategoryId);

    List<TenantCategory> findAllByTenantId(Long tenantId);

    List<TenantCategory> findAllByEnterprise(Enterprise enterprise);

    /*@Query(value = "select tc.id, tc.primary_category_id, tc.secondary_category_id, tc.sub_category_id, tc.tenant_id from tenant_category tc where tc.tenant_id=?1 group by tc.id, tc.primary_category_id, tc.secondary_category_id, tc.sub_category_id, tc.tenant_id", nativeQuery = true)
    List<TenantCategory> findAllByTenantIdAndGroupByPrimaryCategoryId(Long tenantId);*/
}