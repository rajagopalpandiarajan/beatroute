package com.beatroute.api.repository;


import com.beatroute.api.model.Category;
import com.beatroute.api.model.DisplayCategory;
import com.beatroute.api.model.Navigator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NavigatorRepository extends JpaRepository<Navigator, Long> {

    @Query("Select n from Navigator n where n.primaryCategory IN (:category)")
    List<Navigator> findAllByPrimaryCategory(@Param("category") List<Category> category);

    Navigator findOneByPrimaryCategoryNameAndEventIdNull(String name);

    Navigator findOneBySecondaryCategoryName(String secondaryCategory);

}
