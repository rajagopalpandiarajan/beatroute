package com.beatroute.api.repository;

import com.beatroute.api.model.HomeEvent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HomeRepository extends JpaRepository<HomeEvent, Long> {

    List<HomeEvent> findAllByEnterpriseIdOrderByPriorityAsc(Long enterpriseId);
    //List<HomeEvent> findAllByOrderByPriorityAsc();
}
