package com.beatroute.api.repository;

import com.beatroute.api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Long> {

    User findOneById(Long id);

    @Query(value="SELECT name from users WHERE id=2" ,nativeQuery = true)
    User getUser();

    User findOneByEmailAndEnterpriseId(String email, Long enterpriseId);

    User findOneByOauthIdAndEnterpriseId(String oauthId, Long enterpriseId);
}
