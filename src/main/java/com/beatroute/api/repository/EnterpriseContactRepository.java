package com.beatroute.api.repository;

import com.beatroute.api.model.Enterprise;
import com.beatroute.api.model.EnterpriseContact;
import com.beatroute.api.model.enumeration.ContactType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnterpriseContactRepository extends JpaRepository<EnterpriseContact, Long> {

    /*@Modifying
    @Transactional
    @Query(value = "delete from enterprise_contacts where enterprise_id = :id", nativeQuery = true)
    void deleteAllOnEnterpriseId(@Param(value = "id") Long id);

    @Query(value = "select * from enterprise_contacts where enterprise_id = :id", nativeQuery = true)
    List<EnterpriseContact> findAllByEnterpriseId(@Param(value = "id") Long id);*/

    //TODO use the new function modified in Pankaj machine
    /*@Query(value = "select * from enterprise_contacts where enterprise_id = :id AND contact_type = :contactType", nativeQuery = true)
    EnterpriseContact findByEnterpriseIdONBusinessContact(@Param(value = "id") Long id, @Param(value = "contactType") int contactType);*/

    EnterpriseContact findByEnterpriseAndContactType(Enterprise enterprise, ContactType business);

}

