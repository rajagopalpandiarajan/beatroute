package com.beatroute.api.repository;

import com.beatroute.api.model.EnterprisePaymentGateway;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EnterprisePaymentGatewayRepository extends JpaRepository<EnterprisePaymentGateway, Long> {

    @Modifying
    @Transactional
    @Query(value = "delete from enterprise_payment_gateway where enterprise_id = :id", nativeQuery = true)
    void deleteAllOnEnterpriseId(@Param(value = "id") Long id);

    @Query(value = "select * from enterprise_payment_gateway where enterprise_id = :id", nativeQuery = true)
    List<EnterprisePaymentGateway> findAllByEnterpriseId(@Param(value = "id") Long id);
}
