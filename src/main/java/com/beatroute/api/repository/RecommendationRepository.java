package com.beatroute.api.repository;
;
import com.beatroute.api.model.Enterprise;
import com.beatroute.api.model.RecommendationEvents;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by robins on 22/6/17.
 */
public interface RecommendationRepository extends JpaRepository<RecommendationEvents, Long> {

    //List<RecommendationEvents> findAllByEnterpriseIdOrderByPriorityAsc(Long enterpriseId);

    List<RecommendationEvents> findAllByRecommendationNameAndEnterpriseOrderByPriorityAsc(String recommendationName,Enterprise enterprise);

    //RecommendationEvents findById(long id);

    //RecommendationEvents findOneByEventNameAndEnterprise(String eventName, Enterprise enterprise);
}
