package com.beatroute.api.repository;

import com.beatroute.api.model.Event;
import com.beatroute.api.model.Session;
import com.beatroute.api.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TicketRepository extends JpaRepository<Ticket, Long>{

    List<Ticket> findAllBySession(Session session);

    Ticket findByEventAndSession(Event event, Session session);
}
