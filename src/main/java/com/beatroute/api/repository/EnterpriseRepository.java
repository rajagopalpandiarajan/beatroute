package com.beatroute.api.repository;

import com.beatroute.api.model.Enterprise;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EnterpriseRepository extends JpaRepository<Enterprise, Long> {

    Enterprise findOneById(Long enterpriseId);

    List<Enterprise> findAllByIsActiveTrue();
}
