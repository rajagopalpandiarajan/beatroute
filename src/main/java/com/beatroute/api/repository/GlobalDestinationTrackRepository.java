package com.beatroute.api.repository;

import com.beatroute.api.model.GlobalDestinationTrack;
import com.beatroute.api.model.Venue;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GlobalDestinationTrackRepository extends JpaRepository<GlobalDestinationTrack, Long> {

    GlobalDestinationTrack findBySourceCountryAndDestinationCountry(String sourceCountry, String destinationCountry);

    @Query("SELECT g.destinationCountry from GlobalDestinationTrack g where g.sourceCountry = :sourceCountry")
    List<String> findBySourceCountry(@Param(value = "sourceCountry") String sourceCountry, Pageable pageable);

}
