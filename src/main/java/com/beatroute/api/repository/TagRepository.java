package com.beatroute.api.repository;

import com.beatroute.api.model.Tag;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Long> {

    Tag findOneByName(String name);

    //@Query("Select name from Tag t where t.name like :startsWith% order by t.name asc")
    @Query(value = "Select t.name from tags t where t.name like %:startsWith% AND t.id IN (select tag_id from events_tags where event_id IN (select event_id from events_enterprise_tenants where enterprise_id = :enterpriseId) AND event_id NOT IN (select id from events where status = false)) order by t.name asc limit 5", nativeQuery = true)
    @Cacheable(value = "tags")
    List<String> findByNameThatStartsWith(@Param(value = "startsWith") String startsWith, @Param(value = "enterpriseId") Long enterpriseId);

    @Query("Select name, description from Tag t where t.name like :startsWith% order by t.name asc")
    List<String> findByNameThatStartsWithName(@Param(value = "startsWith") String startsWith, Pageable pageable);
}
