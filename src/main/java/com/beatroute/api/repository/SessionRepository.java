package com.beatroute.api.repository;

import com.beatroute.api.model.Session;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
public interface SessionRepository extends JpaRepository<Session, Long> {
    Session findOneByName(String name);

    Session findOneBySessionReferenceId(String referenceId);

    @Query(value = "select * from sessions s where s.session_reference_id = :referenceId AND s.event_id = (select distinct(id) from events where event_reference_id = :eventId)", nativeQuery = true)
    Session findOneBySessionReferenceIdAndEventId(@Param(value = "referenceId") String referenceId, @Param(value = "eventId") String eventId);

    Session findByIdAndEventId(long id, long eventId);

    //@Query(value = "Select * from sessions where begin_date_time < :beginDate and id not in (SELECT session_id FROM transactions)", nativeQuery = true)
    //List<Session> findAllBySessionWithDate(@Param(value = "beginDate") Date beginDate, Pageable pageRequest);

    @Query("Select s from Session s where s.beginDateTime < :beginDate AND s.available = true")
    Page<Session> findAllBySessionWithDate(@Param(value = "beginDate") Date beginDate, Pageable pageRequest);

    /*@Query(value = "Select * from sessions s where (s.begin_date_time between :startDate AND :endDate) AND (s.updated_date < NOW() - INTERVAL :minute MINUTE) AND s.event_id = (SELECT e.id FROM events e where e.event_reference_id = :eventReferenceId) AND s.available = true", nativeQuery = true)
    List<Session> findAllBeginDateTimeBetweenStartDateAndEndDateUponUpdateDate(@Param(value = "eventReferenceId") String eventReferenceId, @Param(value = "startDate") Date startDate, @Param(value = "endDate") Date endDate, @Param(value = "minute") int minute);
*/

    @Query("Select s from Session s where (s.beginDateTime between :startDate AND :endDate) AND s.event = (SELECT e.id FROM Event e where e.eventReferenceId = :eventReferenceId)")
    List<Session> findAllBeginDateTimeBetweenStartDateAndEndDate(@Param(value = "eventReferenceId") String eventReferenceId, @Param(value = "startDate") Date startDate, @Param(value = "endDate") Date endDate);

    @Query("Select s from Session s where (s.beginDateTime between :startDate AND :endDate) AND s.event = (SELECT e.id FROM Event e where e.eventReferenceId = :eventReferenceId) AND s.available = true")
    List<Session> findAllBeginDateTimeBetweenStartDateAndEndDateOnAvailable(@Param(value = "eventReferenceId") String eventReferenceId, @Param(value = "startDate") Date startDate, @Param(value = "endDate") Date endDate);

    @Query(value = "select * from sessions s where s.session_reference_id LIKE :referenceId% AND s.event_id = (select distinct(id) from events where event_reference_id = :eventId)", nativeQuery = true)
    Session findOneBySessionReferenceIdAndEventIdForIngresso(@Param(value = "referenceId") String referenceId, @Param(value = "eventId") String eventId);

}
