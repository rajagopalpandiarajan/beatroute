package com.beatroute.api.repository;

import com.amazonaws.services.dynamodbv2.xspec.S;
import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SecureTicketRepository extends JpaRepository<SecureTicket, Long> {

    List<SecureTicket> findAllByPaymentReferenceId(String paymentReferenceId);

    List<SecureTicket> findAllByUser(User user);

    List<SecureTicket> findAllByUserAndLockStatus(User user, Boolean status);

    SecureTicket findByLockRequestId(String lockRequestId);

}
