package com.beatroute.api.repository;

import com.beatroute.api.model.Event;
import com.beatroute.api.model.Venue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VenueRepository extends JpaRepository<Venue, Long> {

    List<Venue> findAllBySessionId(long sessionId);

    @Query("SELECT distinct(v.country) from Venue v")
    List<Venue> findAllCountries();

    @Query("SELECT distinct(v.city) from Venue v where v.country = :country AND v.city != :country")
    List<String> findAllCities(@Param(value = "country") String country);

    @Query(value = "SELECT * FROM venues where session_id IN (:sessionIdList)", nativeQuery = true)
    List<Venue> findAllBySessionIdList(@Param(value = "sessionIdList") List<Long> sessionIdList);

    @Query("select distinct v.country from Venue v, Session s where v.session = s.id and s.event.id IN (:eventId) order by v.country")
    List<String> findAllCountriesForEnterprise(@Param(value = "eventId") List<Long> eventId);

    @Query(value = "select distinct v.country from venues v join sessions s on s.id = v.session_id join events e on s.event_id = e.id join events_enterprise_tenants eet ON eet.event_id=e.id where eet.enterprise_id = :enterpriseId AND v.country like :startsWith limit :count", nativeQuery = true)
    List<String> findAllDistinctCountriesStartswithForEnterprise(@Param(value = "enterpriseId") long enterpriseId,
                                                                 @Param(value = "startsWith") String startsWith,
                                                                 @Param(value = "count") int count);

    @Query(value = "select distinct v.city from venues v join sessions s on s.id = v.session_id join events e on s.event_id = e.id join events_enterprise_tenants eet ON eet.event_id=e.id where eet.enterprise_id = :enterpriseId AND v.city like :startsWith AND v.city != v.country limit :count", nativeQuery = true)
    List<String> findAllDistinctCitiesStartswithForEnterprise(@Param(value = "enterpriseId") long enterpriseId,
                                                              @Param(value = "startsWith") String startsWith,
                                                              @Param(value = "count") int count);

}