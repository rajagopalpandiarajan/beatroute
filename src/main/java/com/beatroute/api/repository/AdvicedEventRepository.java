package com.beatroute.api.repository;

import com.beatroute.api.model.AdvicedEvent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AdvicedEventRepository extends JpaRepository<AdvicedEvent, Long> {

    List<AdvicedEvent> findAllByEnterpriseIdOrderByPriorityAsc(Long enterpriseId);
}