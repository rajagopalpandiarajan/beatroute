package com.beatroute.api.repository;


import com.beatroute.api.model.Enterprise;
import com.beatroute.api.model.EnterpriseEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EnterpriseEventRepository extends JpaRepository<EnterpriseEvent, Long> {

    @Modifying
    @Transactional
    void deleteAllByEnterprise(Enterprise enterprise);

    List<EnterpriseEvent> findAllByEnterpriseId(long enterpriseId);

    @Query("select ee.eventReferenceId from EnterpriseEvent AS ee where ee.enterprise = :enterprise")
    List<String> findAllNameByEnterpriseId(@Param(value = "enterprise") Enterprise enterprise);

}