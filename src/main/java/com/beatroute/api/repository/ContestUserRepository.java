package com.beatroute.api.repository;

import com.beatroute.api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface ContestUserRepository extends JpaRepository<User, Long> {

    @Query(value="select * from users where password = '$2a$05$DaUhhiE6EkqIUu0VyuzDzOSB18An0wztqOei1PJbn2uAhmh.DS8da' and created_date >= curdate() and email is not null order by name" ,nativeQuery = true)
    List<User> findAllByPasswordAndCreatedDate();
}
