package com.beatroute.api.repository;

import com.beatroute.api.model.PromotedEvent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PromotedEventRepository extends JpaRepository<PromotedEvent, Long> {

    List<PromotedEvent> findAllByEnterpriseIdOrderByPriorityAsc(long enterpriseId);
}