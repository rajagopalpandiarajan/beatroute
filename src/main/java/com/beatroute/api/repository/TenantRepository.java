package com.beatroute.api.repository;

import com.beatroute.api.model.Tenant;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TenantRepository extends JpaRepository<Tenant, Long> {

    Tenant findOneByName(String name);

    @Query("Select t from Tenant t where t.name like :startsWith% order by t.name asc")
    @Cacheable(value = "tenant")
    List<Tenant> findByNameThatStartsWith(@Param(value = "startsWith") String startsWith, Pageable pageable);

    Tenant findByApiKey(String apiKey);
}
