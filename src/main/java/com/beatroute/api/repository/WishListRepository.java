package com.beatroute.api.repository;

import com.beatroute.api.model.WishList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface WishListRepository extends JpaRepository<WishList, Long> {

    WishList findByUserIdAndEventId(long userId, long eventId);

    List<WishList> findAllByUserId(long userId);
}
