package com.beatroute.api.repository;

import com.beatroute.api.model.UserEventTrack;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.List;

public interface UserEventTrackRepository extends JpaRepository<UserEventTrack, Long> {

    UserEventTrack findByUserIdAndEventReferenceId(String userId, String eventReferenceId);

    @Query("SELECT u.eventReferenceId from UserEventTrack u where u.userId = :userId")
    List<String> findByUserIdForEventReferenceId(@Param(value = "userId") String userId, Pageable pageable);

    @Query("SELECT u.eventName from UserEventTrack u where u.userId = :userId")
    List<String> findByUserId(@Param(value = "userId") String userId, Pageable pageable);

    @Query("SELECT u.eventName from UserEventTrack u")
    List<String> getAllEventNames(Pageable pageable);

    /*@Query(value = "SELECT t.name AS TagName, COUNT(t.name) AS tagsCount from user_event_track AS uet JOIN events AS e ON uet.event_reference_id = e.event_reference_id JOIN events_tags AS et ON e.id = et.event_id JOIN tags AS t ON et.tag_id = t.id WHERE uet.user_id = :userId", nativeQuery = true)*/
    @Query(value = "select t.name from events as e join events_tags as et on e.id = et.event_id join tags as t on et.tag_id = t.id where e.event_reference_id in (select uet.event_reference_id from user_event_track AS uet where uet.user_id = :userId ORDER BY uet.visit_count DESC) GROUP BY et.tag_id ORDER BY COUNT(et.tag_id) DESC limit 5", nativeQuery = true)
    ArrayList<String> getCommonTagsOfEvents(@Param(value = "userId") String userId);

    /*@Query("SELECT u.eventName from UserEventTrack u where u.userId = :userId")
    List<String> findByUserIdForLatest(@Param(value = "userId") String userId, Pageable pageable);*/

    /*@Query("SELECT DISTINCT u.eventName from UserEventTrack u where u.userId = :userId order by u.lastVisitedDate desc")
    List<String> findTop1ByUserId(@Param(value = "userId") String userId);*/

}
