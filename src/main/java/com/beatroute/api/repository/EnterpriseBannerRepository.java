package com.beatroute.api.repository;

import com.beatroute.api.model.Enterprise;
import com.beatroute.api.model.EnterpriseBanner;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EnterpriseBannerRepository extends JpaRepository<EnterpriseBanner, Long> {

    List<EnterpriseBanner> findAllByEnterprise(Enterprise enterprise);

}
