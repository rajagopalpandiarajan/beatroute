package com.beatroute.api.repository;

import com.beatroute.api.model.User;
import com.beatroute.api.model.UserBagPack;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserBagPackRepository extends JpaRepository<UserBagPack, Long>{

    List<UserBagPack> findAllByUser(User user);

    List<UserBagPack> findAllByUserId(Long id);
}
