package com.beatroute.api.repository;

import com.beatroute.api.model.DisplayCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.beatroute.api.model.Enterprise;
import java.util.Set;

public interface DisplayCategoryRepository extends JpaRepository<DisplayCategory, Long> {

    @Query(value = "SELECT GROUP_CONCAT(distinct mapped_categories SEPARATOR ',') FROM display_categories", nativeQuery = true)
    String getAllDistinctCategoryId();

    DisplayCategory findByNameAndEnterprise(String name, Enterprise enterprise);

    Set<DisplayCategory> findAllByEnterprise(Enterprise enterprise);
}