package com.beatroute.api.repository;

import com.beatroute.api.model.Enterprise;
import com.beatroute.api.model.EnterpriseExperience;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EnterpriseExperienceRepository extends JpaRepository<EnterpriseExperience, Long> {

    @Modifying
    @Transactional
    @Query(value = "delete from enterprise_experiences where enterprise_id = :id", nativeQuery = true)
    void deleteAllOnEnterpriseId(@Param(value = "id") Long id);

    @Query(value = "select * from enterprise_experiences where enterprise_id = :id", nativeQuery = true)
    List<EnterpriseExperience> findAllByEnterpriseId(@Param(value = "id") Long id);

    @Query("select distinct ee.experienceType from EnterpriseExperience AS ee where ee.enterprise = :enterprise AND ee.experienceType IS NOT NULL order by ee.experienceType")
    List<String> findAllNameByEnterpriseId(@Param(value = "enterprise") Enterprise enterprise);

    @Query("select distinct ee.country from EnterpriseExperience AS ee where ee.enterprise = :enterprise AND ee.country IS NOT NULL order by ee.country")
    List<String> findAllCountryByEnterpriseId(@Param(value = "enterprise") Enterprise enterprise);

}
