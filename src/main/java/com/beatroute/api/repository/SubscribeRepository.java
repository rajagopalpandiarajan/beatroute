package com.beatroute.api.repository;

import com.beatroute.api.model.Subscribe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by nid on 18/11/16.
 */
@Transactional
public interface SubscribeRepository extends JpaRepository<Subscribe, Long> {

    Subscribe findOneByEmailAndEnterpriseId(String email, Long enterpriseId);
}
