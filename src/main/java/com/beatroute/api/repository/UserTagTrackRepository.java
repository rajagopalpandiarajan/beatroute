package com.beatroute.api.repository;

import com.beatroute.api.model.UserDestinationTrack;
import com.beatroute.api.model.UserTagTrack;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserTagTrackRepository  extends JpaRepository<UserTagTrack, Long> {

    UserTagTrack findByUserIdAndSearchTag(String userId, String searchTag);

    /*@Query("SELECT DISTINCT(u.searchTag) from UserTagTrack u where u.userId = :userId order by u.searchCount desc")
    List<String> findTop5ByUserId(@Param(value = "userId") String userId);*/

    @Query("SELECT u.searchTag from UserTagTrack u where u.userId = :userId")
    List<String> findByUserId(@Param(value = "userId") String userId, Pageable pageable);

}
