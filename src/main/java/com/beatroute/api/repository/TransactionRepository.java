package com.beatroute.api.repository;

import com.beatroute.api.model.Transaction;
import com.beatroute.api.model.User;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    List<Transaction> findAllByUserOrderByCreatedDateDesc(User user);

    Transaction findByPaymentReferenceId(String paymentReferenceId);

    List<Transaction> findAllByPaymentReferenceId(String paymentReferenceId);

    Transaction findOneByMercuryOrderId(String mercuryOrderId);

    List<Transaction> findByMercuryOrderIdIsNotNullAndUrlIsNullAndTransactionStatusIsNotNull();
}
