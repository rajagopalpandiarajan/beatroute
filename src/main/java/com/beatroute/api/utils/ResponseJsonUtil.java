package com.beatroute.api.utils;

import lombok.experimental.UtilityClass;

import java.util.HashMap;
import java.util.Map;

@UtilityClass
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ResponseJsonUtil {

	/**
	 * Returns success JSON format
	 * 
	 * @return
	 * {
            "status": "success"
        }
	 */
    public static Map getSuccessResponseJson() {
        Map responseBuilder = new HashMap();
        responseBuilder.put("status","success");
        return responseBuilder;
    }
    
	/**
	 * Returns success JSON format for given data
	 * 
	 * @param data - value for 'data' key
	 * @return
	 * {
            "status": "success",
            "data": {
                "content": 101
            }
        }
	 */
    public static Map getSuccessResponseJson(Object data) {
        Map responseBuilder = new HashMap();
        responseBuilder.put("status","success");
        responseBuilder.put("data", data);
        return responseBuilder;
    }

    /**
     * Creates error json format
     * 
     * @param errorMsg
     * @param error
     * @return
     * {
            "status": "failure",
            "errors": {
                "INVALIDINPUTS": "Invalid values for products, email"
            }
        }
     */
	public static Map getFailedResponseJson(String errorMsg, String error) {
        Map responseBuilder = new HashMap();
        responseBuilder.put("status", "failure");
        Map errors = new HashMap();
        errors.put(errorMsg, error);
        responseBuilder.put("errors", errors);
        return responseBuilder;
    }

}
