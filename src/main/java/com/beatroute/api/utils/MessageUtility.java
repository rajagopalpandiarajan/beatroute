package com.beatroute.api.utils;

import com.beatroute.api.model.Enterprise;
import com.beatroute.api.repository.EnterpriseRepository;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@UtilityClass
@Slf4j
public class MessageUtility {

    public String getFormattedString(String message, EnterpriseRepository enterpriseRepository){
    	String formattedString=message;
    	try {
	        ContextData cd=ContextStorage.get();
	        Enterprise enterprise= enterpriseRepository.findOne(cd.getEnterpriseId());
            log.info(enterprise.getId().toString());
	        if (message.contains("Beatroute")) {
	        	formattedString=message.replace("Beatroute", enterprise.getDisplayName());
                log.info("getFormattedString - "+formattedString);
	        }
    	} catch(Exception e){
            log.error(e.getMessage(), e);
        }
    	
    	return formattedString;
    }

    public static String getTenantUrl(String srcUrl){
        String formattedString=srcUrl;
        try {
            ContextData cd=ContextStorage.get();
            log.info("OriginHost from MessageUtility.getTenantUrl - "+cd.getOriginHost());
            formattedString=srcUrl.replace("$sourceHost", cd.getOriginHost());
            log.info("formattedString from MessageUtility.getTenantUrl - "+formattedString);
        } catch(Exception e){
            log.error(e.getMessage(), e);
        }

        return formattedString;
    }
}
