package com.beatroute.api.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

import lombok.experimental.UtilityClass;

@UtilityClass
@Slf4j
public class HttpUtil {

	@Value("${app.subdomain.default}")
    private String defaultSubDomain;
	
	public String getSubdomain(String url) {
		String subDomain=defaultSubDomain;
        try {
	        String[] domainParts=url.replace("http://", "").replace("https://", "").replaceAll("/", "").split("\\.");
	        subDomain=domainParts[0];
        } catch(Exception e) {
			log.error(e.getMessage(), e);
        	subDomain=defaultSubDomain;
        }
//        log.info("SubDomain from getSubdomain - "+subDomain);
        return subDomain;
	}
}
