package com.beatroute.api.elasticsearch;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.net.InetAddress;

@Component
public class ElasticSearchConfiguration {

    @Value("${elastic.search.server.host.name}")
    private String serverName;

    @Value("${elastic.search.server.port.number}")
    private String portNumber;

    @Bean
    public Client elasticSearchClient() throws Exception {
        return TransportClient.builder().build()
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(serverName), Integer.parseInt(portNumber)));
    }

}
