package com.beatroute.api.elasticsearch.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.client.Client;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptService.ScriptType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class ElasticSearchService {

    @Value("${elastic.search.event.index}")
    private String index;

    @Value("${elastic.search.event.type}")
    private String type;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Client elasticSearchClient;
    
    private Script addEnterpriseScript;
    
    private Script removeEnterpriseScript;

    /**
     * create a Script object to add new enterprise id to enterprises id list.
     * //TODO: not tested
     * 
     * The Script will be pre-compiled with following. It will take enterprise id as param.
	 * {
		   "script" : "ctx._source.enterprises+=new_enterprise",
		   "params" : {
		      "new_enterprise" : 201
		   }
		}
	 */
    private Script getAddEnterpriseScript(Long enterpriseId) {
    	if (addEnterpriseScript == null) {
    		addEnterpriseScript=new Script("ctx._source.enterprises+=new_enterprise",
    				ScriptType.INLINE,"painless",new HashMap<String, Long>());
    	}
    	addEnterpriseScript.getParams().put("new_enterprise", enterpriseId);
    	return addEnterpriseScript;
    }
    
    /**
     * Create a Script object to remove enterprise id from enterprises id list.
     * //TODO: not tested
     * 
     * The Script will be pre-compiled with following. It will take enterprise id as param.
     * 
	 * {
		   "script" : "ctx._source.enterprises-=old_enterprise",
		   "params" : {
		      "old_enterprise" : 201
		   }
		}
	 */
    private Script getRemoveEnterpriseScript(Long enterpriseId) {
    	if (removeEnterpriseScript == null) {
    		removeEnterpriseScript=new Script("ctx._source.enterprises-=old_enterprise",
    				ScriptType.INLINE,"painless",new HashMap<String, Long>());
    	}
    	removeEnterpriseScript.getParams().put("old_enterprise", enterpriseId);
    	return removeEnterpriseScript;
        /*POST /twitter/twit/1/_update
        {
            "script": "items_to_remove = []; foreach (item : ctx._source.list) { if (item['tweet_id'] == tweet_id) { items_to_remove.add(item); } } foreach (item : items_to_remove) {ctx._source.list.remove(item);}",
                "params": {"tweet_id": "123"}
        }*/
    }
    
    public void save(Object object, String id) throws Exception {
        elasticSearchClient.prepareIndex(index, type, id).setSource(objectMapper.writeValueAsBytes(object)).get();
    }

    public void delete(String id) throws Exception {
        elasticSearchClient.prepareDelete(index, type, id).get();
    }

    public void update(Object object, String id) throws Exception {
        elasticSearchClient.prepareUpdate(index, type, id).setDoc(objectMapper.writeValueAsBytes(object)).get();
    }

    public String get(String id) throws Exception {
        return elasticSearchClient.prepareGet(index, type, id).get().getSourceAsString();
    }

    public void updateEventAddEnterprise(String id, Long enterpriseId) {
    	elasticSearchClient.prepareUpdate(index, type, id).setScript(getAddEnterpriseScript(enterpriseId)).get();
    }
    
    public void updateEventRemoveEnterprise(String id, Long enterpriseId) {
    	elasticSearchClient.prepareUpdate(index, type, id).setScript(getRemoveEnterpriseScript(enterpriseId)).get();
    }

    //TODO: provide bulk update to update the 'enterprise tenant' field of event
    public void updateEventEnterprise(String id, Long enterpriseId) {
        //elasticSearchClient.prepareBulk().
    }
    
    /*public DeleteIndexResponse deleteIndices() {
        return elasticSearchClient.admin().indices().delete(new DeleteIndexRequest(index)).actionGet();
    }*/
}
