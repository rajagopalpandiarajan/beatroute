package com.beatroute.api.model;

import com.beatroute.api.model.enumeration.RevenueModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Entity
@Table(name = "enterprise_commercials")
public class EnterpriseCommercial {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private RevenueModel revenueModel;

    @Getter
    @Setter
    private float revenuePercent;

    @Getter
    @Setter
    private String billingSlabs;

    @Getter
    @Setter
    @OneToOne
    @JoinColumn(name = "enterprise_id")
    private Enterprise enterprise;

}