package com.beatroute.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Entity
@Table(name = "tickets")
@JsonIgnoreProperties("event, session")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String ticketType;

    @Getter
    @Setter
    private String ticketCategory;

    @Getter
    @Setter
    private String ticketInclusion;

    @Getter
    @Setter
    private String ticketUsp;

    @Getter
    @Setter
    private String ticketUspIconUrl;

    @Getter
    @Setter
    private String ageValidation;

    @Getter
    @Setter
    private int totalQuantity;

    @Getter
    @Setter
    private int maxQuantity;

    @Getter
    @Setter
    private int minQuantity;

    @Getter
    @Setter
    private int ticketsRemaining;

    @Getter
    @Setter
    private Date createdDate = new Date();

    @Getter
    @Setter
    private Date updatedDate = new Date();

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "event_id")
    private Event event;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "session_id")
    private Session session;
}
