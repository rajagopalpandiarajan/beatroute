package com.beatroute.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Entity
@Table(name = "enterprise_payment_gateway")
public class EnterprisePaymentGateway {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @OneToOne
    @JoinColumn(name = "payment_gateway_id")
    private PaymentGateway paymentGateway;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "enterprise_id")
    private Enterprise enterprise;


    public static List<Long> getPaymentGatewayList(List<EnterprisePaymentGateway> paymentGatewayList){
        List<Long> gatewayList = new ArrayList<>();
        for(EnterprisePaymentGateway epg : paymentGatewayList){
            gatewayList.add(epg.getPaymentGateway().getId());
        }
        return gatewayList;
    }

}