package com.beatroute.api.model;

import com.beatroute.api.model.enumeration.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Entity
@Table(name = "enterprise_products")
public class EnterpriseProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private Product product;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "enterprise_id")
    private Enterprise enterprise;


    public static List<Integer> getProductIntegerList(List<EnterpriseProduct> enterpriseProductsList){
        List<Integer> list = new ArrayList<>();
        for(EnterpriseProduct pg : enterpriseProductsList){
            list.add(pg.getProduct().ordinal());
        }
        return list;
    }

}