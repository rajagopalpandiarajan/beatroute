package com.beatroute.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Entity
@Table(name = "global_destination_track")
public class GlobalDestinationTrack {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String sourceCountry;

    @Getter
    @Setter
    private String destinationCountry;

    @Getter
    @Setter
    private Long visitCount;


}
