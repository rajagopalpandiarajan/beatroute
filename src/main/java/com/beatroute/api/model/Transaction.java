package com.beatroute.api.model;


import com.beatroute.api.model.enumeration.TransactionStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@NoArgsConstructor
@Entity
@Table(name = "transactions")
@JsonIgnoreProperties("user")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private BigDecimal amount;

    @Getter
    @Setter
    private String currency;

    @Getter
    @Setter
    private int numberOfTickets;

    @Getter
    @Setter
    private BigDecimal ticketBookingPrice;

    @Getter
    @Setter
    private String paymentReferenceId;

    @Getter
    @Setter
    private String mercuryOrderId;

    @Getter
    @Setter
    private String url;

    @Getter
    @Setter
    private TransactionStatus transactionStatus;

    @Getter
    @Setter
    private TransactionStatus paymentStatus;

    @Getter
    @Setter
    private Date createdDate = new Date();

    @Getter
    @Setter
    private Date updatedDate = new Date();

    @Getter
    @Setter
    private String ticketReferenceNumber;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "event_id")
    private Event event;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "session_id")
    private Session session;

    @Getter
    @Setter
    @Lob
    @Column(name = "userbagpack_data")
    private String userBagPackJson;

    @Getter
    @Setter
    private BigDecimal refundAmount;

    @Getter
    @Setter
    private Date supplierPaidDate;

    @Getter
    @Setter
    private Boolean supplierPaid = false;

    @Getter
    @Setter
    private BigDecimal supplierAmount = BigDecimal.valueOf(0.0);

    @Getter
    @Setter
    private String shippingMethods;
}
