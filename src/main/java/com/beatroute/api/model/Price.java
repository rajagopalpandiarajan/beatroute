package com.beatroute.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@NoArgsConstructor
@Entity
@Table(name = "price")
@JsonIgnoreProperties("ticket, session")
public class Price {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private BigDecimal wholeSalePrice;

    @Getter
    @Setter
    private BigDecimal retailPrice;

    @Getter
    @Setter
    private String currency;

    @Getter
    @Setter
    private Date createdDate = new Date();

    @Getter
    @Setter
    private Date updatedDate = new Date();


    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "session_id")
    private Session session;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;
}
