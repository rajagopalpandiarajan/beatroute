package com.beatroute.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Entity
@Table(name = "display_categories")
@JsonIgnoreProperties("enterprise")
public class DisplayCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String icon;

    @Getter
    @Setter
    private String mappedCategories;

    @Getter
    @Setter
    private String mappedEvents;

    @Getter
    @Setter
    private Date createdDate = new Date();

    @Getter
    @Setter
    private Date updatedDate = new Date();

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "enterprise_id")
    private Enterprise enterprise;
}
