package com.beatroute.api.model.enumeration;

public enum UserType {
    GUEST_USER,
    OAUTH_FB_USER,
    OAUTH_GOOGLE_USER,
    APP_USER,
    SUPER_ADMIN,
    ENTERPRISE_ADMIN,
    CUSTOMER_SERVICE_ADMIN,
    INTERN_ADMIN,
    MARKETING_ADMIN,
    ADMIN
}
