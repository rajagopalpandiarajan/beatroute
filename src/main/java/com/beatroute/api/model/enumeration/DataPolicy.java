package com.beatroute.api.model.enumeration;

public enum DataPolicy {
    DEFAULT, BRDEFAULT, MASKED;

    public static DataPolicy getValue(int i) {
        if(i == 1) {
            return BRDEFAULT;
        }else if(i == 2){
            return MASKED;
        }
        return DEFAULT;
    }
}
