package com.beatroute.api.model.enumeration;

public enum RevenueModel {
    DEFAULT, REVENUESHARE;

    public static RevenueModel getValue(int i) {
        if (i == 1) {
            return REVENUESHARE;
        }
        return DEFAULT;
    }
}