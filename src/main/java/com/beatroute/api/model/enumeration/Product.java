package com.beatroute.api.model.enumeration;

public enum Product {
    DEFAULT, WHITELABEL, WHITELABELWITHPG, API;

    public static Product getValue(int i) {
        if(i == 1) {
            return WHITELABEL;
        }else if(i == 2){
            return WHITELABELWITHPG;
        }else if(i == 3){
            return API;
        }
        return DEFAULT;
    }
}
