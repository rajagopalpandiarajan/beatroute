package com.beatroute.api.model.enumeration;

/**
 * Created by vimal on 2/8/16.
 */
public enum DestinationType {
    DEFAULT,
    COUNTRY,
    STATE,
    CITY;

    public static DestinationType getValue(int i) {
        if (i == 1) {
            return COUNTRY;
        } else if (i == 2) {
            return STATE;
        } else if (i == 3) {
            return CITY;
        }
        return DEFAULT;
    }
}
