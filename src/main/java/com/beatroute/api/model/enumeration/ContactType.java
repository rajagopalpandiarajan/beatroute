package com.beatroute.api.model.enumeration;

public enum ContactType {
    DEFAULT, BUSINESS, ACCOUNT, HELPLINE;

    public static ContactType getValue(int i) {
        if (i == 1) {
            return BUSINESS;
        }else if (i == 2) {
            return ACCOUNT;
        }else if (i == 3) {
            return HELPLINE;
        }
        return DEFAULT;
    }
}
