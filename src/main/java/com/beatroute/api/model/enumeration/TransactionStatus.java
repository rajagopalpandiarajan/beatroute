package com.beatroute.api.model.enumeration;

public enum TransactionStatus {
    FAIL, SUCCESS, CANCELLED
}
