package com.beatroute.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Entity
@Table(name = "enterprise_experiences")
public class EnterpriseExperience {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String experienceType;

    @Getter
    @Setter
    private String country; //nullable

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "enterprise_id")
    private Enterprise enterprise;

    public static List<String> getExperiencesList(List<EnterpriseExperience> api){
        List<String> experienceList = new ArrayList<>();
        for(EnterpriseExperience enterpriseExperience : api){
            String name=enterpriseExperience.getExperienceType();
            experienceList.add(name);
        }
        return experienceList;
    }

}
