package com.beatroute.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@Entity
@Table(name = "sessions")
@JsonIgnoreProperties("event")
public class Session {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String timeZone;

    @Getter
    @Setter
    private Date displayDate;

    @Getter
    @Setter
    private Date beginDateTime;

    @Getter
    @Setter
    private Date endDateTime;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private int duration;

    @Getter
    @Setter
    private String usp;

    @Getter
    @Setter
    private String rating;

    @Getter
    @Setter
    private Date createdDate = new Date();

    @Getter
    @Setter
    private Date updatedDate = new Date();

    @Getter
    @Setter
    private String sessionReferenceId = UUID.randomUUID().toString();

    @Getter
    @Setter
    @OneToMany(mappedBy = "session", cascade = CascadeType.ALL)     //fetch = FetchType.EAGER,
    private Set<Venue> venues = new HashSet<>();

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "event_id")
    private Event event;

    @Getter
    @Setter
    private boolean available = true;

}
