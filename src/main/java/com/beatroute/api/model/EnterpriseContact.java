package com.beatroute.api.model;

import com.beatroute.api.model.enumeration.ContactType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Entity
@Table(name = "enterprise_contacts")
public class EnterpriseContact {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String contactName;

    @Getter
    @Setter
    private String contactNo;

    @Getter
    @Setter
    private String emailId;

    @Getter
    @Setter
    private ContactType contactType;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "enterprise_id")
    private Enterprise enterprise;

}
