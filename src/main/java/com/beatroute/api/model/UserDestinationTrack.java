package com.beatroute.api.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Entity
@Table(name = "user_destination_track")
public class UserDestinationTrack {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    /**
     * userId from User table in case of permanent user, else a UUID defined by cookie
     */
    private String userId;

    @Getter
    @Setter
    private String searchDestination;

    @Getter
    @Setter
    private Long searchCount;

    @Getter
    @Setter
    private Date lastSearchedDate = new Date();

}
