package com.beatroute.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Entity
@Table(name = "enterprise_events")
public class EnterpriseEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String eventReferenceId;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "enterprise_id")
    private Enterprise enterprise;

}
