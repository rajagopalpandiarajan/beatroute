package com.beatroute.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@Entity
@Table(name = "events")
public class Event {

    //NOTE: Any new fields added will need to be exposed via 'getCopy'  if needed

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL)
    private Category primaryCategory;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL)
    private Category secondaryCategory;

    @Getter
    @Setter
    private String supplierReferenceId;

    @Getter
    @Setter
    private String supplier;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private String genericDetailTitle;

    @Getter
    @Setter
    private String genericDetailDescription;

    @Getter
    @Setter
    private String genericDetailImage;

    @Getter
    @Setter
    private Date createdDate = new Date();

    @Getter
    @Setter
    private Date updatedDate = new Date();

    @Getter
    @Setter
    private String eventReferenceId = UUID.randomUUID().toString();

    @Getter
    @Setter
    private String imageGallery;

    @Getter
    @Setter
    private String venueMap;

    @Getter
    @Setter
    private String videoGallery;

    @Getter
    @Setter
    private String eventTermsAndConditions;

    @Getter
    @Setter
    private String notes;

    @Getter
    @Setter
    private boolean status = false;

    @Getter
    @Setter
    private BigDecimal lowPrice;

    @Getter
    @Setter
    private BigDecimal highPrice;

    @Getter
    @Setter
    private BigDecimal avgPrice;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String metaName;

    @Getter
    @Setter
    private String metaTags;

    @Getter
    @Setter
    private String metaDescription;

    @Getter
    @Setter
    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL)       //fetch = FetchType.EAGER,
    private Set<Session> sessions = new HashSet<>();

    @Getter
    @Setter
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "events_tags", joinColumns = @JoinColumn(name = "event_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id"))
    private Set<Tag> tags = new HashSet<>();

    @Getter
    @Setter
    @JoinTable(name = "events_tenants", joinColumns = @JoinColumn(name = "event_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "tenant_id", referencedColumnName = "id"))
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Tenant> tenants = new HashSet<>();

    @Getter
    @Setter
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "events_enterprise_tenants", joinColumns = @JoinColumn(name = "event_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "enterprise_id", referencedColumnName = "id"))
    private Set<Enterprise> eventsEnterpriseTenants = new HashSet<>();

    @Getter
    @Setter
    private Long viewCount = Long.valueOf(0);


    //NOTE: Any new fields added will need to be exposed via 'getCopy'  if needed
    public Event getCopy(Event e){
        Event event = new Event();
        event.setId(e.getId());
        event.setName(e.getName());
        event.setPrimaryCategory(e.getPrimaryCategory());
        event.setSecondaryCategory(e.getSecondaryCategory());
        event.setSupplierReferenceId(e.getSupplierReferenceId());
        event.setSupplier(e.getSupplier());
        event.setDescription(e.getDescription());
        event.setGenericDetailTitle(e.getGenericDetailTitle());
        event.setGenericDetailDescription(e.getGenericDetailDescription());
        event.setGenericDetailImage(e.getGenericDetailImage());
        event.setCreatedDate(e.getCreatedDate());
        event.setUpdatedDate(e.getUpdatedDate());
        event.setEventReferenceId(e.getEventReferenceId());
        event.setImageGallery(e.getImageGallery());
        event.setVenueMap(e.getVenueMap());
        event.setVideoGallery(e.getVideoGallery());
        event.setEventTermsAndConditions(e.getEventTermsAndConditions());
        event.setNotes(e.getNotes());
        event.setStatus(e.isStatus());
        event.setLowPrice(e.getLowPrice());
        event.setHighPrice(e.getHighPrice());
        event.setAvgPrice(e.getAvgPrice());
        event.setSessions(e.getSessions());
        event.setTags(e.getTags());
        event.setViewCount(e.getViewCount());

        return event;
    }
}
