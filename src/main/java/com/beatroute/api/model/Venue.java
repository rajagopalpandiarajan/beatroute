package com.beatroute.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "venues")
@JsonIgnoreProperties("session")
@NoArgsConstructor
public class Venue {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String city;

    @Getter
    @Setter
    private String geoLocation;

    @Getter
    @Setter
    private String state;

    @Getter
    @Setter
    private String country;

    @Getter
    @Setter
    private String address;

    @Getter
    @Setter
    private String url;

    @Getter
    @Setter
    private String venueMap;

    @Getter
    @Setter
    private String zipCode;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private Date createdDate = new Date();

    @Getter
    @Setter
    private Date updatedDate = new Date();

    @Getter
    @Setter
    private String venueReferenceId = UUID.randomUUID().toString();

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "session_id")
    private Session session;

}
