package com.beatroute.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Entity
@Table(name = "domain_tenant")
public class DomainTenant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Column(name = "sub_domain_name", unique=true)
    @Getter
    @Setter
    private String subDomainName;

    @Getter
    @Setter
    @OneToOne
    @JoinColumn(name = "enterprise_id")
    private Enterprise enterprise;
}
