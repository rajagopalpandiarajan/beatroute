package com.beatroute.api.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;


@Entity
public class TabsPrimary {

    @Id
    @Setter
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Setter
    @Getter
    private String tabs;

    @Setter
    @Getter
    private String alias;

    @Getter
    @Setter
    @OneToMany(mappedBy = "tabsPrimary", cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = TabsSecondary.class)
   // @JsonManagedReference
    private Set<TabsSecondary> secondaryTabs;

}
