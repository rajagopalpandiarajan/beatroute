package com.beatroute.api.model;

import com.beatroute.api.model.enumeration.DataPolicy;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Entity
@Table(name = "enterprise")
public class Enterprise {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String clientServicingPersonnel;

    @Getter
    @Setter
    private boolean signedAgreement = false;

    @Getter
    @Setter
    private String companyName;

    @Getter
    @Setter
    private String displayName;

    @Getter
    @Setter
    private Date contractTerminationDate = new Date();

    @Getter
    @Setter
    private String userID;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private DataPolicy dataPolicy;

    @Getter
    @Setter
    private boolean setupComplete = false;

    @Getter
    @Setter
    private String themeData;

    @Getter
    @Setter
    private boolean isActive = true;

    @Getter
    @Setter
    @OneToMany(mappedBy = "enterprise", cascade = CascadeType.ALL)
    private Set<DisplayCategory> displayCategories = new HashSet<>();

}