package com.beatroute.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Entity
@Table(name = "tenant_category")
public class TenantCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL)
    private Category primaryCategory;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL)
    private Category secondaryCategory;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL)
    private Category subCategory;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL)
    private DisplayCategory displayCategory;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL)
    private Tenant tenant;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "enterprise_id")
    private Enterprise enterprise;

}
