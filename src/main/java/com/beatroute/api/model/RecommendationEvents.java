package com.beatroute.api.model;

import com.beatroute.api.model.enumeration.RecommendationType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@JsonIgnoreProperties("enterprise")
public class RecommendationEvents {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private int priority;

    @Getter
    @Setter
    private String eventName;

    @Getter
    @Setter
    private String eventReferenceId;

    @Getter
    @Setter
    private Date createdDate =new Date();

    @Getter
    @Setter
    private Date updatedDate =new Date();

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "enterprise_id")
    private Enterprise enterprise;

    @Getter
    @Setter
    private RecommendationType recommendationType;

    @Getter
    @Setter
    private String recommendationName;

}
