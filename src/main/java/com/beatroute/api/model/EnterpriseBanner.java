package com.beatroute.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Entity
@Table(name = "enterprise_banner")
public class EnterpriseBanner {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String image;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private String buttonText;

    @Getter
    @Setter
    private String link;

    @Getter
    @Setter
    private int priority;

    @Getter
    @Setter
    private String video;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "enterprise_id")
    private Enterprise enterprise;

    @Getter
    @Setter
    private Date createdDate = new Date();

    @Getter
    @Setter
    private Date updatedDate = new Date();

}
