package com.beatroute.api.model;

import com.beatroute.api.model.enumeration.DestinationType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Entity
@Table(name = "destination_display")
public class DestinationDisplay {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String country;

    @Getter
    @Setter
    private String countryAlias;

    @Getter
    @Setter
    private String state;

    @Getter
    @Setter
    private String stateAlias;

    @Getter
    @Setter
    private String city;

    @Getter
    @Setter
    private String cityAlias;

    @Getter
    @Setter
    private String image;

    @Getter
    @Setter
    private String video;

    @Getter
    @Setter
    private String userInterestDestinations;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private DestinationType destinationType;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String metaName;

    @Getter
    @Setter
    private String metaTags;

    @Getter
    @Setter
    private String metaDescription;

    @Getter
    @Setter
    private Integer priority;

    @Getter
    @Setter
    private Date createdDate = new Date();

    @Getter
    @Setter
    private Date updatedDate = new Date();

}