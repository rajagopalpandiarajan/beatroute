package com.beatroute.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Entity
@Table(name = "user_event_track")
public class UserEventTrack {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    /**
     * userId from User table in case of permanent user, else a UUID defined by cookie
     */
    private String userId;

    @Getter
    @Setter
    private String eventName;

    @Getter
    @Setter
    private String eventReferenceId;

    @Getter
    @Setter
    private Long visitCount;

    @Getter
    @Setter
    private Date lastVisitedDate = new Date();

}
