package com.beatroute.api.model;

import com.beatroute.api.model.enumeration.UserType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private String country;

    @Getter
    @Setter
    private UserType userType;

    @Getter
    @Setter
    private String oauthId;

    @Getter
    @Setter
    private boolean authorised = false;

    @Getter
    @Setter
    private String profilePic;

    @Getter
    @Setter
    private String mobile;

    @Getter
    @Setter
    private String address;

    @Getter
    @Setter
    private String city;

    @Getter
    @Setter
    private Date createdDate = new Date();

    @Getter
    @Setter
    private Date updatedDate = new Date();

    @Getter
    @Setter
    private Date lastVisitedDate = new Date();

    @Getter
    @Setter
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = Transaction.class)
    private Set<Transaction> transactions = new HashSet<>();

    @Getter
    @Setter
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = WishList.class)
    private Set<WishList> wishlist = new HashSet<>();

    @Getter
    @Setter
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<UserBagPack> userBagPacks = new HashSet<>();

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "enterprise_id")
    private Enterprise enterprise;

    @Getter
    @Setter
    private String tabsActive;

}
