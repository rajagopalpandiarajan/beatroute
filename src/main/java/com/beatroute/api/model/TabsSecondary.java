package com.beatroute.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@JsonIgnoreProperties("tabsPrimary")
public class TabsSecondary {

    @Id
    @Setter
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Setter
    @Getter
    private String tabs;

    @Setter
    @Getter
    private String alias;


    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "tabs_primary_id")
   // @JsonBackReference
    private TabsPrimary tabsPrimary;


}
