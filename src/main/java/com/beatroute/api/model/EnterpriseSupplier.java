package com.beatroute.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Entity
@Table(name = "enterprise_suppliers")
public class EnterpriseSupplier {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "suppliers_id")
    private Tenant supplier;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "enterprise_id")
    private Enterprise enterprise;

    public static List<Long> getSuppliersList(List<EnterpriseSupplier> api){
        List<Long> supplierList = new ArrayList<>();
        for(EnterpriseSupplier enterpriseSupplier : api){
            supplierList.add(enterpriseSupplier.getSupplier().getId());
        }
        return supplierList;
    }
}


/*
create table enterprise_suppliers(
id BIGINT AUTO_INCREMENT PRIMARY KEY,
enterprise_id   BIGINT NOT NULL,
suppliers_id BIGINT NOT NULL ,
FOREIGN KEY  payment_api_enterprise(enterprise_id) REFERENCES enterprise(id),
FOREIGN KEY  payment_api_suppliers_id(suppliers_id) REFERENCES tenants(id)
);
*/

