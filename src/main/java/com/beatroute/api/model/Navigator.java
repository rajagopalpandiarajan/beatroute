package com.beatroute.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Entity
@Table(name = "navigator")
public class Navigator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL)
    private Category primaryCategory;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL)
    private Category secondaryCategory;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL)
    private Event event;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private String videoLink;

    @Getter
    @Setter
    private Integer secondaryCategoryPriority;

    @Getter
    @Setter
    private Integer eventPriority;

}
