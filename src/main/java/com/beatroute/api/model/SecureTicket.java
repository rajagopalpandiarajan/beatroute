package com.beatroute.api.model;

import com.beatroute.api.dto.TicketNetworkDetail;
import com.beatroute.api.rest.template.OrderRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@Entity
@Table(name = "secure_tickets")
public class SecureTicket {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @OneToOne
    private User user;

    @Getter
    @Setter
    private Date createdDate = new Date();

    @Getter
    @Setter
    private Date updatedDate = new Date();

    @Getter
    @Setter
    private Date expiryDateTime = new Date();

    @Getter
    @Setter
    @OneToOne
    private Event event;

    @Getter
    @Setter
    private BigDecimal ticketBookingPrice;

    @Getter
    @Setter
    private String ticketGroupId;

    @Getter
    @Setter
    private Integer noOfTickets;

    @Getter
    @Setter
    private String lockRequestId = UUID.randomUUID().toString();

    @Getter
    @Setter
    private String ticketReferenceNumber = UUID.randomUUID().toString();

    @Getter
    @Setter
    private String orderReferenceNumber;

    @Getter
    @Setter
    private Boolean lockStatus = false;

    @Getter
    @Setter
    private String lockResponse;

    @Getter
    @Setter
    private String sessionReferenceId;

    @Getter
    @Setter
    private String paymentReferenceId;

    @Getter
    @Setter
    private BigDecimal supplierAmount;

    @Getter
    @Setter
    @OneToOne
    private UserBagPack userBagPack;

    public OrderRequest getOrderRequest(TicketNetworkDetail ticketNetworkDetail) {
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setExternalPONumber(ticketNetworkDetail.getPoNumber());
        orderRequest.setShippingMethodID(ticketNetworkDetail.getShippingMethod());
        orderRequest.setShippingMethodDesc(ticketNetworkDetail.getShippingMethodDesc());
        orderRequest.setShippingPrice(ticketBookingPrice.toString());
        orderRequest.setShippingInstructions(this.user.getEmail());
        orderRequest.setRetailCustomerName(this.user.getName());
        orderRequest.setRetailCustomerPhone(this.user.getMobile());
        orderRequest.setRetailCustomerStreet1(ticketNetworkDetail.getStreetOne());
        orderRequest.setRetailCustomerStreet2(ticketNetworkDetail.getStreetTwo());
        orderRequest.setRetailCustomerCity(ticketNetworkDetail.getCity());
        orderRequest.setRetailCustomerStateProvince(ticketNetworkDetail.getState());
        orderRequest.setRetailCustomerCountry(ticketNetworkDetail.getCountry());
        orderRequest.setRetailCustomerEmail(this.getUser().getEmail());
        orderRequest.setRetailCustomerZipcode(ticketNetworkDetail.getZipCode());
        orderRequest.setRetailCustomerZipcode(ticketNetworkDetail.getZipCode());
        orderRequest.setStage1Guid(this.lockRequestId);
        orderRequest.setStage2Guid(UUID.randomUUID().toString());
        return orderRequest;
    }
}
