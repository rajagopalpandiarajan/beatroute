package com.beatroute.api.service;

import com.beatroute.api.elasticsearch.service.ElasticSearchService;
import com.beatroute.api.model.Category;
import com.beatroute.api.model.Event;
import com.beatroute.api.model.Session;
import com.beatroute.api.repository.CategoryRepository;
import com.beatroute.api.repository.EventRepository;
import com.beatroute.api.repository.SessionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Slf4j
public class SessionOptimizeService {

    @Value("${app.session.batch.size}")
    private int pageSize;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private  ElasticSearchService elasticSearchService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Value("${api.event.category.name}")
    private String eventCategoryNames;

    @Value("${api.event.disable.allowed.supplier}")
    private String supplierNames;

    public void optimizeSession() throws Exception {
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();

        // getting session list that are expired to specific date
        boolean done=false;
        // get the first page
        Page<Session> sessionList = this.sessionRepository.findAllBySessionWithDate(date, new PageRequest(0, pageSize, new Sort("id")));

        if(sessionList.getTotalElements() > 0) {
            Set<String> eventReferenceIdSet = new HashSet<>();
            List<Session> batchSession = null;
            Set<Session> updatedSession = null;
            while (!done) {
                updatedSession = new HashSet<>();
                batchSession = sessionList.getContent();
                for (Session session : batchSession) {
                    log.info("processing session id: " + session.getId()+" of event ref id "+session.getEvent().getEventReferenceId());
                    session.setAvailable(false);
                    updatedSession.add(session);

                    eventReferenceIdSet.add(session.getEvent().getEventReferenceId());
                }

                //update db
                this.sessionRepository.save(updatedSession);

                if (sessionList.hasNext()) {
                    // get the next page
                    sessionList = this.sessionRepository.findAllBySessionWithDate(date, sessionList.nextPageable());
                } else {
                    done = true;
                }
            }

            //event status update for NON GetSessions
            List<Event> disabledEvents = new ArrayList<>();
            List<String> categoryNamesList = Arrays.asList(eventCategoryNames.split(","));
            List<Category> categoryList = this.categoryRepository.findAllByNameIn(categoryNamesList);
            List<String> supplierList = Arrays.asList(supplierNames.split(","));

            List<Event> eventList = this.eventRepository.findAllByEventReferenceId(eventReferenceIdSet, categoryList, supplierList);
            if(eventList.size() > 0){
                eventList.forEach(event -> {
                    try {
                        log.info("Checking disable Events - "+event.getEventReferenceId());
                        Set<Session> sessionSet = event.getSessions();
                        int count = 0;
                        for(Session session : sessionSet){
                            if(session.isAvailable()){
                                break;
                            }
                            count++;
                        }
                        log.info("total session count - "+sessionSet.size()+" and not available count - "+count);
                        if(count == sessionSet.size()){
                            event.setStatus(false);
                            disabledEvents.add(event);
                        }
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                });
            }
            log.info("Total event list size - "+eventList.size()+" and disabling event size - "+disabledEvents.size());
            if(disabledEvents.size() > 0) {
                this.eventRepository.save(disabledEvents);
            }

            Thread.sleep(5000);
            //elastic sync
            List<Event> eventLists = this.eventRepository.findAllByEventReferenceIdIn(eventReferenceIdSet);
            log.info("Total events list size that are syncing to elastic - "+eventLists.size());
            if(eventLists.size() > 0){
                eventLists.forEach(event -> {
                    try {
                        log.info("Syncing the event - "+event.getEventReferenceId());
                        this.elasticSearchService.update(event, event.getEventReferenceId());
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                });
            }
        }
        log.info("Completed Session Optimization scheduler at "+new Date());
    }
}
