package com.beatroute.api.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@Transactional
public class SmsService{

    public String sendSms(String restRequest){
        log.info("Sending sms to user : "+restRequest);
        RestTemplate smsTemplate = new RestTemplate();
        return smsTemplate.getForObject(restRequest, String.class);
    }

}
