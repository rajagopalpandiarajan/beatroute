package com.beatroute.api.service;

import com.beatroute.api.model.Category;
import com.beatroute.api.model.DisplayCategory;
import com.beatroute.api.model.Enterprise;
import com.beatroute.api.repository.CategoryRepository;
import com.beatroute.api.repository.DisplayCategoryRepository;
import com.beatroute.api.repository.DomainTenantRepository;
import com.beatroute.api.repository.EnterpriseRepository;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import com.beatroute.api.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CategoryService {

    @Autowired
    private DisplayCategoryRepository displayCategoryRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private DomainTenantRepository domainTenantRepository;

    @Autowired
    private EnterpriseRepository enterpriseRepository;

    public Map listCategories() {

        ContextData cd= ContextStorage.get();
        log.info("NavigationService : got contextData: "+cd.toString());
        long enterpriseId = cd.getEnterpriseId();
        Enterprise enterprise  = enterpriseRepository.findOneById(enterpriseId);
        log.info("Started listing the categories");
        Set<DisplayCategory> displayCategoryList = this.displayCategoryRepository.findAllByEnterprise(enterprise);
        log.info("Completed listing the categories");

        Map data = new HashMap();
        data.put("displayCategory", displayCategoryList);
        return ResponseJsonUtil.getSuccessResponseJson(data);
    }

    public Set<Long> getIdListAsLongFromString(String eventsIdAsString) {
        Set<Long> idList = new HashSet<>();
        if(!StringUtils.isEmpty(eventsIdAsString)) {
            String[] categoriesId = eventsIdAsString.replaceAll("\\s+", "").split(",");
            if (categoriesId.length > 0) {
                idList = convertStringToLongArray(categoriesId);
            }
        }
        return idList;
    }

    private Set<Long> convertStringToLongArray(String[] numbers) {
        Set<Long> result = new HashSet<>();
        for(String s : numbers) {
            if(s != null && !StringUtils.isEmpty(s) && !s.equals("null") && !s.contains("null") && s.length() > 0) {
                result.add(Long.parseLong(s));
            }
        }
        return result;
    }

    public Set<String> getPrimaryCategoryOfDisplayCategory(String categoriesIdAsString){
        Set<Long> idList = getIdListAsLongFromString(categoriesIdAsString);
        List<Category> primaryCategoryList = categoryRepository.findAll(idList);
        Set<String> categoryNameSet = primaryCategoryList.stream().map(Category::getName).collect(Collectors.toSet());
        if(categoryNameSet.contains("NONE")){
            categoryNameSet.remove("NONE");
        }
        return categoryNameSet;
    }
}
