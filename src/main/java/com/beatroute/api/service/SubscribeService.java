package com.beatroute.api.service;

import com.beatroute.api.model.Enterprise;
import com.beatroute.api.model.Subscribe;
import com.beatroute.api.repository.EnterpriseRepository;
import com.beatroute.api.repository.SubscribeRepository;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@Transactional
public class SubscribeService {

    @Autowired
    private SubscribeRepository subscribeRepository;

    @Autowired
    private EnterpriseRepository enterpriseRepository;

    public boolean save(String email) {
        log.info("Started saving Subscription Email in Service");
        Subscribe subscribe;
        ContextData cd= ContextStorage.get();
        //checking email already present in db
        subscribe = subscribeRepository.findOneByEmailAndEnterpriseId(email,cd.getEnterpriseId());

        if(subscribe == null){
            Enterprise enterprise=enterpriseRepository.findOneById(cd.getEnterpriseId());
            log.info("New Email Found");
            subscribe = new Subscribe();
            subscribe.setEmail(email);
            subscribe.setStatus(true);
            subscribe.setEnterprise(enterprise);
            log.info("setted to subscribe "+subscribe.getEmail());
            subscribe = subscribeRepository.saveAndFlush(subscribe);
            log.info("after saving to subscribe "+subscribe.getId());
            if(subscribe == null){
                log.error("Subscription Email saving Failed for "+email);
                return false;
            }else{
                log.info("Subscription Email saved successfully for "+email +" and id generated is "+subscribe.getId());
                return true;
            }
        }else{
            log.error("Subscription Email already found for "+email);
            return false;
        }
    }

}
