package com.beatroute.api.service;

import com.beatroute.api.dto.EmailContent;
import com.beatroute.api.dto.Enquiry;
import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.Transaction;
import com.beatroute.api.model.Venue;
import com.beatroute.api.model.enumeration.TransactionStatus;
import com.beatroute.api.repository.TransactionRepository;
import com.beatroute.api.repository.VenueRepository;
import com.beatroute.api.rest.template.AdditionalInfo;
import com.beatroute.api.rest.template.CancellationPolicyDto;
import com.beatroute.api.rest.template.PaxDto;
import com.beatroute.api.rest.template.UserBagPackTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class EmailHelperService {

    @Value("${app.mail.payment.confirmation.subject}")
    private String paymentConfirmationSubject;

    @Value("${app.mail.order.download.subject}")
    private String orderDownloadSubject;

    @Value("${app.mail.order.failure.subject}")
    private String orderFailureSubject;

    @Value("${app.mail.booking.cancel.success.subject}")
    private String bookingCancelSubject;

    @Value("${api.enquiry.email.subject}")
    private String enquirySubject;

    @Value("${api.enquiry.email.to}")
    private String sendMailTo;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private VenueRepository venueRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private EnterpriseService enterpriseService;

    @Async
    public void sendMailToUser(String paymentReferenceId, List<SecureTicket> secureTicketList, String sourceHost) throws Exception {
        log.info("Sending email to user");
        List<Transaction> transactions = transactionRepository.findAllByPaymentReferenceId(paymentReferenceId);
        SimpleDateFormat formatter = new SimpleDateFormat("EEE dd MMM yyyy, hh:mm aaa");
        EmailContent emailContent = this.enterpriseService.getEnterpriseContentByHost(sourceHost);
        log.info("EmailContent DisplayName - "+emailContent.getDisplayName());
        for (Transaction transaction: transactions){
            //send mail for transactions made by user
            String userName = transaction.getUser().getName();
            String userEmail = transaction.getUser().getEmail();
            //get event name
            String eventName = transaction.getEvent().getName();
            String tNo = Integer.toString(transaction.getNumberOfTickets());
            String tToalAmount = String.valueOf(transaction.getAmount().setScale(2, BigDecimal.ROUND_UP));
            String importantNotes = null;
            String cancellationPolicies = null;
            String paxAmounts = null;
            List<CancellationPolicyDto> cancellationPoliciesList = null;
            List<PaxDto> paxAmountsList = null;
            String supplierName = transaction.getEvent().getSupplier();
            String additionalNotes = null;
            String additionalInfo = null;
            List<AdditionalInfo> additionalInfoList = null;
            if (TransactionStatus.SUCCESS.equals(transaction.getTransactionStatus())){
                //get session details
                Date sessionBeginTime = transaction.getSession().getBeginDateTime();
                List<Venue> venues = venueRepository.findAllBySessionId(transaction.getSession().getId());
                String venueName = venues.iterator().next().getName();
                String tTax = String.valueOf(transaction.getAmount().subtract(transaction.getTicketBookingPrice().multiply(new BigDecimal(transaction.getNumberOfTickets()))));
                String tId = transaction.getPaymentReferenceId();
                String tAmount = String.valueOf(transaction.getTicketBookingPrice().setScale(2, BigDecimal.ROUND_UP));
                ObjectMapper objectMapper = new ObjectMapper();
                UserBagPackTemplate userBagPackTemplate = objectMapper.readValue(transaction.getUserBagPackJson(), UserBagPackTemplate.class);
                importantNotes = userBagPackTemplate.getSupplierInfo().getImportantNotes();
                cancellationPoliciesList = userBagPackTemplate.getSupplierInfo().getCancellationPolicies();
                paxAmountsList = userBagPackTemplate.getSupplierInfo().getPaxAmounts();
                additionalNotes = userBagPackTemplate.getSupplierInfo().getAdditionalNotes();
                additionalInfoList = userBagPackTemplate.getSupplierInfo().getAdditionalInfo();
                int numAdults = userBagPackTemplate.getSupplierInfo().getTickets().getNumAdults();
                int numChildren = userBagPackTemplate.getSupplierInfo().getTickets().getNumChildren();
                if(paxAmountsList!=null && paxAmountsList.size()>0){
                    paxAmounts = transactionService.getPaxAmounts(paxAmountsList);
                }
                if(cancellationPoliciesList!=null && cancellationPoliciesList.size()>0){
                    cancellationPolicies = transactionService.getCancellationPolicies(cancellationPoliciesList);
                }
                if(additionalInfoList!=null && additionalInfoList.size()>0){
                    additionalInfo = transactionService.getAdditionalInfo(additionalInfoList);
                }
                if("Isango".equalsIgnoreCase(supplierName)){
                    userName = userBagPackTemplate.getSupplierInfo().getTravellerName();
                }
                try {
                    String confirmSubject = paymentConfirmationSubject.replace("$displayName", emailContent.getDisplayName());    //MessageUtility.getFormattedString(paymentConfirmationSubject, enterpriseRepository);
                    log.info("Payment Confirmation Subject - "+confirmSubject);
                    emailService.sendPaymentConfirmation(userName, userEmail, confirmSubject, eventName, venueName, formatter.format(sessionBeginTime), tAmount, tNo, tTax, tId, tToalAmount, importantNotes, paxAmounts, cancellationPolicies, supplierName, additionalNotes, additionalInfo, emailContent);
                    //sending download email except TicketNetwork
                    if(!supplierName.equalsIgnoreCase("TicketNetwork")){
                        String downloadSubject = orderDownloadSubject.replace("$displayName", emailContent.getDisplayName());  //MessageUtility.getFormattedString(orderDownloadSubject, enterpriseRepository);
                        log.info("Order Download Subject - "+downloadSubject);
                        emailService.sendDownloadTicket(userName, userEmail, downloadSubject, eventName, venueName, formatter.format(sessionBeginTime), tAmount, tNo, tTax, tId, tToalAmount, supplierName, importantNotes, cancellationPolicies, paxAmounts, additionalNotes, additionalInfo, numAdults, numChildren, emailContent);
                    }
                }catch (Exception ex){
                    log.error("Exception occured while sending Payment Confirmation email :: ", ex);
                }
            }else {
                try {
                    String failureSubject = orderFailureSubject.replace("$displayName", emailContent.getDisplayName());    //MessageUtility.getFormattedString(orderFailureSubject, enterpriseRepository);
                    emailService.sendOrderFailure(userName, userEmail, failureSubject + paymentReferenceId, paymentReferenceId, eventName, tNo, tToalAmount, transaction.getCreatedDate(), supplierName, importantNotes, emailContent);
                }catch (Exception ex) {
                    log.error("Exception occured when trying to send Payment Failure email :: ", ex);
                }
            }
        }
    }

    @Async
    public void sendMailForCancelBooking(Transaction transaction, EmailContent emailContent){
        String json = transaction.getUserBagPackJson();
        log.info("json: "+json);
        JSONObject jsonObject=new JSONObject(json);
        String userEmail=transaction.getUser().getEmail();
        String eventName=transaction.getEvent().getName();
        String sessionBeginDate=""+transaction.getSession().getBeginDateTime();
        String sessionEndDate=""+transaction.getSession().getEndDateTime();
        String mercuryOrderId=transaction.getMercuryOrderId();
        String travellerName=jsonObject.getJSONObject("supplierInfo").getString("travellerName");
        log.info("travellerName : "+travellerName);
        String amount=""+transaction.getAmount();
        String currency=transaction.getCurrency();
        String numberOfTickets=""+transaction.getNumberOfTickets();
        String ticketBookingPrice=""+transaction.getTicketBookingPrice();
        String paymentReferanceId=transaction.getPaymentReferenceId();
        String url=transaction.getUrl();
        String transactionStatus=String.valueOf(transaction.getTransactionStatus());
        String paymentStatus=""+transaction.getPaymentStatus();
        String createdDate=""+transaction.getCreatedDate();
        String updatedDate=""+transaction.getUpdatedDate();
        String ticketReferenceNumber=transaction.getTicketReferenceNumber();
        String user=""+transaction.getUser();
        String event=""+transaction.getUser();
        String session=""+transaction.getSession();
        String refundAmount = String.valueOf(transaction.getRefundAmount());
        String cancelSubject = bookingCancelSubject.replace("$displayName", emailContent.getDisplayName());  //MessageUtility.getFormattedString(bookingCancelSubject, enterpriseRepository);
        emailService.sendCancelBookingMail(userEmail, cancelSubject,eventName,sessionBeginDate,sessionEndDate,mercuryOrderId,travellerName,amount,currency,numberOfTickets,ticketBookingPrice,paymentReferanceId,url,transactionStatus,paymentStatus,createdDate,updatedDate,ticketReferenceNumber,user,event,session,refundAmount, emailContent); //change traveller name name
    }

    @Async
    public Map sendEnquiryEmail(Enquiry enquiry) {
        Map responseJson=new HashMap<>();
        Map errors=new HashMap();
        try {
            if(enquiry.getEmail().trim().length()>0 && enquiry.getContactNumber().trim().length()>0) {
                String message = " First Name : " +enquiry.getFirstName()+ " \n Last Name : " +enquiry.getLastName()+ " \n Email : " +enquiry.getEmail()+ " \n Contact Number : " +enquiry.getContactNumber()+ " \n Designation : " +enquiry.getDesignation()+ " \n Company Name : " +enquiry.getCompanyName()+ " \n Address : " +enquiry.getAddress()+ " \n Reg. Id : " +enquiry.getRegId()+ " \n Type of Solution : " +enquiry.getTypeOfSolution();
                emailService.sendEnquiryMail(sendMailTo, enquirySubject, message);
                responseJson.put("status", "success");
            } else{
                responseJson.put("status","failure");
                if (enquiry.getEmail().trim().length()==0){
                    errors.put("INVALDIEMAIL","Email Mandatory. Please try with valid Email Id");
                }
                if (enquiry.getContactNumber().trim().length()==0){
                    errors.put("INVALIDNUMBER","Contact Number Mandatory. Please try with Contact Numbere.");
                }
                responseJson.put("errors",errors);
            }
        } catch(Exception e) {
            log.error(e.getMessage(), e);
            responseJson.put("status","failure");
            responseJson.put("error",e.getMessage());
            return responseJson;
        }
        return responseJson;
    }
}
