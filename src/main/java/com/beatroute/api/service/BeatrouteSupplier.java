package com.beatroute.api.service;


import com.beatroute.api.model.*;
import com.beatroute.api.repository.*;
import com.beatroute.api.rest.template.OrderRequest;
import com.beatroute.api.rest.template.UserBagPackTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.Future;


@Slf4j
@Component(value = "beatroute")
@SuppressWarnings({ "unchecked", "rawtypes" })
public class BeatrouteSupplier implements Supplier{

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private PriceRepository priceRepository;

    @Autowired
    private SecureTicketService secureTicketService;

    @Autowired
    private SecureTicketRepository secureTicketRepository;

    @Autowired
    private EventRepository eventRepository;

    @Override
    public Future<String> getTickets(String id) {
        log.info("Getting tickets for Beatroute");
        //get all tickets for session
        Session session = sessionRepository.findOneBySessionReferenceId(id);
        List<Ticket> ticketList = ticketRepository.findAllBySession(session);

        String response = null;
        log.info("Completed getting tickets for Beatroute");
        try {
            response = new ObjectMapper().writeValueAsString(buildTicketJson(ticketList));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return new AsyncResult<>(response);
    }

    @Override
    public Future<String> getTickets(String id, String ticketCount) {
        return null;
    }

    @Override
    @Async
    public Future<String> lockTicket(UserBagPack userBagPack, UserBagPackTemplate userBagPackTemplate) {
        log.info("Started to lock ticket for Beatroute");
        Future<String> future = new AsyncResult<>("");
        try {
            if (!userBagPackTemplate.isExpired() && (userBagPackTemplate.getLatestTicketBookingPrice() != null)) {
                SecureTicket secureTicket = secureTicketService.getSecureTicket(userBagPack, userBagPackTemplate);
                secureTicketRepository.save(secureTicket);

                Event event = eventRepository.findOneByEventReferenceId(userBagPackTemplate.getEventId());
                Session session = sessionRepository.findOneBySessionReferenceIdAndEventId(userBagPackTemplate.getSessionReferenceId(), userBagPackTemplate.getEventId());
                Ticket ticket = ticketRepository.findByEventAndSession(event, session);
                future = new AsyncResult<>(new ObjectMapper().writeValueAsString(buildLockResponse(ticket, userBagPackTemplate, secureTicket)));
            }
        }catch (Exception e) {
            e.printStackTrace();
            log.info("Error occured while locking tickets ::" + e);
        }

        log.info("Completed locking ticket for Beatroute");
        return future;
    }

    @Override
    @Async
    public Future<String> placeOrder(OrderRequest orderRequest) {
        log.info("Started placing order for Beatroute");
        Future<String> future = null;
        try {
            future = new AsyncResult<>(new ObjectMapper().writeValueAsString(buildOrderResponse()));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            log.info("Error occured while ordering tickets ::" + e);
        }
        log.info("Completed placing order for Beatroute");
        return future;
    }

    private Object buildTicketJson(List<Ticket> ticketsList) {
        log.info("Started constructing ticket response");
        Map ticketDetails = new HashMap();
        List tickets = new ArrayList<>();
        ticketsList.forEach(ticket -> {
            Price price = getPriceDetails(ticket);
            Map<String, Object> ticketData = new HashMap<>();
            if (ticket.getTicketsRemaining() > 0) {
                if(price != null) {
                    ticketData.put("quantity", ticket.getMaxQuantity());
                    ticketData.put("splits", getSplits(ticket.getMinQuantity(), ticket.getMaxQuantity()));
                    ticketData.put("actualPrice", price.getWholeSalePrice());
                    ticketData.put("rating", null);
                    ticketData.put("description", ticket.getTicketType());
                    ticketData.put("section", ticket.getTicketType());
                    ticketData.put("wholeSalePrice", price.getWholeSalePrice());
                    ticketData.put("retailPrice", price.getRetailPrice());
                    ticketData.put("expectedArrivalDate", ticket.getAgeValidation());
                    ticketData.put("duration", ticket.getAgeValidation());
                    ticketData.put("usp", ticket.getTicketUsp());
                    ticketData.put("shippingMethods", null);
                    ticketData.put("currency", "USD");
                    ticketData.put("convertedPrice", price.getCurrency());
                    ticketData.put("row", ticket.getTicketType());
                    ticketData.put("category", ticket.getTicketType());
                    ticketData.put("vendorTicketRefId", ticket.getId().toString());
                }
            }
            tickets.add(ticketData);
                }
        );
        ticketDetails.put("tickets", tickets);
        log.info("Completed constructing ticket response");
        return ticketDetails;
    }

    private Price getPriceDetails(Ticket ticket) {
        return priceRepository.findByTicket(ticket);
    }

    private List getSplits(Integer minQuantity, Integer maxQuantity) {
        List<Integer> splits = new ArrayList<>();
        while (minQuantity <= maxQuantity) {
            splits.add(minQuantity);
            minQuantity++;
        }
        return splits;
    }

    private Object buildLockResponse(Ticket ticket, UserBagPackTemplate userBagPackTemplate, SecureTicket secureTicket) {
        Map lockResponse = new HashMap();
        if (ticket.getTicketsRemaining() > 0) {
            ticket.setTicketsRemaining(ticket.getTicketsRemaining() - Integer.valueOf(userBagPackTemplate.getNumberOfTickets()));
            ticketRepository.save(ticket);
            lockResponse.put("lockStatus", "success");
            lockResponse.put("lockRequestId", secureTicket.getLockRequestId());
        }else {
            lockResponse.put("lockStatus", "failure");
            lockResponse.put("lockRequestId", secureTicket.getLockRequestId());
        }
        return lockResponse;
    }

	private Object buildOrderResponse() {
        Map orderResponse = new HashMap();
        orderResponse.put("highSeat", "");
        orderResponse.put("invoiceID", "");
        orderResponse.put("lowSeat", "");
        orderResponse.put("mercuryOrderID", new Random().nextInt(1000000000));
        orderResponse.put("message", "");
        orderResponse.put("row", "");
        orderResponse.put("section", "");
        orderResponse.put("sellerEmail", "");
        orderResponse.put("sellerName", "");
        orderResponse.put("sellerPhone", "");
        orderResponse.put("shipByDate", "");
        orderResponse.put("shippingTrackingCreatedStatus", true);
        orderResponse.put("shippingTrackingCreatedStatusDesc", "");
        orderResponse.put("shippingTrackingID", "");
        orderResponse.put("shippingTrackingLocation", "");
        orderResponse.put("success", true);
        return orderResponse;
    }

	@Override
	public Future<String> getSessionInfo(String requestJson) {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    public Future<String> getEventSessions(String requestJson) {
        // TODO Auto-generated method stub
        return null;
    }
}
