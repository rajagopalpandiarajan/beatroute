package com.beatroute.api.service;

import com.beatroute.api.model.*;
import com.beatroute.api.repository.*;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import com.beatroute.api.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.lang.reflect.MalformedParameterizedTypeException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class NavigationService {

    @Autowired
    private HomeRepository homeRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private TenantCategoryRepository tenantCategoryRepository;

    @Autowired
    private EnterpriseRepository enterpriseRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private DisplayCategoryRepository displayCategoryRepository;

    @Autowired
    private NavigatorRepository navigator_newRepository;

    @Value("${app.db.beatroute.enterprise.id}")
    private long defaultEnterpriseId;

    public List<Event> getHomePageEvents(){
        ContextData cd= ContextStorage.get();
        log.info("NavigationService : got contextData: "+cd.toString());
        long enterpriseId=cd.getEnterpriseId();
        List<HomeEvent> homeEvents = this.homeRepository.findAllByEnterpriseIdOrderByPriorityAsc(enterpriseId);
        List<Event> events = new ArrayList<>();
        for(int i=0; i<homeEvents.size(); i++){
        	Event event=eventRepository.findOneByEventReferenceId(homeEvents.get(i).getEventReferenceId());
        	if (event != null && event.isStatus()) {
                event = event.getCopy(event);
        		events.add(event);
        	}
        }
        return events;
    }

    public Map getTenantCategory(String apiKey) {
        Map navigatorMap = new HashMap<>();
        Tenant tenant = this.tenantRepository.findByApiKey(apiKey);
        if(tenant != null){
            //List<TenantCategory> tenantCategoryList = this.tenantCategoryRepository.findAllByTenantId(tenant.getId());
            ContextData cd = ContextStorage.get();
            Enterprise enterprise = enterpriseRepository.findOne(cd.getEnterpriseId());
            List<TenantCategory> tenantCategoryList = this.tenantCategoryRepository.findAllByEnterprise(enterprise);

            if(tenantCategoryList.size() == 0){
                enterprise = this.enterpriseRepository.findOne(defaultEnterpriseId);
                tenantCategoryList = this.tenantCategoryRepository.findAllByEnterprise(enterprise);
            }

            //Using Primary Category
            /*Set<Category> primaryCategoryList = tenantCategoryList.stream().map(TenantCategory::getPrimaryCategory).collect(Collectors.toSet());
            for(Category primeCategory: primaryCategoryList ) {
                List<Navigator> temp = navigator_newRepository.findAllByPrimaryCategory(primeCategory);
                List primaryList = new ArrayList<>();
                for(int i=0; i<temp.size(); i++){
                    Event event = temp.get(i).getEvent();
                    if(event != null && event.isStatus()){
                        primaryList.add(temp.get(i));
                    }
                }
                navigatorMap.put(primeCategory.getName(), primaryList);
            }*/

            //Using Display Category
            Set<DisplayCategory> displayCategorySet = new HashSet<>();
            if(tenantCategoryList.size() == 0){
                displayCategorySet.addAll(this.displayCategoryRepository.findAll());
            }else {
                displayCategorySet.addAll(tenantCategoryList.stream().map(TenantCategory::getDisplayCategory).collect(Collectors.toSet()));
                /*for(DisplayCategory displayCategory: displayCategorySet ) {
                    String categoriesIdAsString = displayCategory.getMappedCategories();
                    Set<Long> idList = categoryService.getIdListAsLongFromString(categoriesIdAsString);
                    List<Category> primaryCategoryList = categoryRepository.findAll(idList);

                    List<Navigator> temp = navigator_newRepository.findAllByPrimaryCategory(primaryCategoryList);
                    List primaryList = new ArrayList<>();
                    for(int i=0; i<temp.size(); i++){
                        Event event = temp.get(i).getEvent();
                        if(event != null && event.isStatus()){
                            primaryList.add(temp.get(i));
                        }
                    }
                    navigatorMap.put(displayCategory.getName(), primaryList);
                }*/
            }
            List displayCategoryResponseList = new ArrayList();
            for(DisplayCategory displayCategory : displayCategorySet){
                Set<String> categoryNameSet = categoryService.getPrimaryCategoryOfDisplayCategory(displayCategory.getMappedCategories());

                Map displayCategoryMap = new HashMap();
                displayCategoryMap.put("id",displayCategory.getId());
                displayCategoryMap.put("name", displayCategory.getName());
                displayCategoryMap.put("icon", displayCategory.getIcon());
                displayCategoryMap.put("primaryCategory", categoryNameSet);

                displayCategoryResponseList.add(displayCategoryMap);
            };
            navigatorMap.put("displayCategory", displayCategoryResponseList);
        }
        return navigatorMap;
    }

    public Map getTenantApiKey() {
        String apiKey = tenantRepository.findOneByName("Beatroute").getApiKey();

        Map object = new HashMap();
        object.put("apiKey", apiKey);

        return ResponseJsonUtil.getSuccessResponseJson(object);
    }
}
