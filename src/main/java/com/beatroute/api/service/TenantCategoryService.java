package com.beatroute.api.service;

import com.beatroute.api.model.Category;
import com.beatroute.api.model.Tenant;
import com.beatroute.api.model.TenantCategory;
import com.beatroute.api.repository.CategoryRepository;
import com.beatroute.api.repository.TenantCategoryRepository;
import com.beatroute.api.repository.TenantRepository;
import com.beatroute.api.service.exception.EntityNotFoundException;
import groovy.util.logging.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TenantCategoryService {

    @Autowired
    private TenantCategoryRepository tenantCategoryRepository;

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    public void saveTenantCategory(String json) throws ParseException, EntityNotFoundException {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(json);

        Tenant tenant = tenantRepository.findOne(Long.parseLong(obj.get("tenantId").toString()));
        Category primaryCategory = categoryRepository.findOneByName(obj.get("primaryCategoryName").toString());

        if((tenant == null) || (primaryCategory == null)){
            throw new EntityNotFoundException("Could not find tenant, category with ids: " + obj.get("tenantId") + ", " + obj.get("primaryCategoryId"));
        }

        TenantCategory tenantCategory = new TenantCategory();
        tenantCategory.setPrimaryCategory(primaryCategory);
        tenantCategory.setTenant(tenant);
        tenantCategoryRepository.save(tenantCategory);
    }

    public void deleteTenantCategory(Long id) throws EntityNotFoundException, ParseException {
        /*JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(json);

        Tenant tenant = tenantRepository.findOne(Long.parseLong(obj.get("tenantId").toString()));
        Category primaryCategory = categoryRepository.findOneByName(obj.get("primaryCategoryName").toString());

        if((tenant == null) || (primaryCategory == null)){
            throw new EntityNotFoundException("Could not find tenant, category with ids: " + obj.get("tenantId") + ", " + obj.get("primaryCategoryId"));
        }

        List<TenantCategory> tenantCategoryList = tenantCategoryRepository.findAllByTenantIdAndPrimaryCategoryId(tenant.getId(), primaryCategory.getId());
        tenantCategoryRepository.delete(tenantCategoryList);*/

        this.tenantCategoryRepository.delete(id);
    }
}
