package com.beatroute.api.service;

import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.User;
import com.beatroute.api.model.UserBagPack;
import com.beatroute.api.repository.EventRepository;
import com.beatroute.api.repository.SecureTicketRepository;
import com.beatroute.api.rest.template.UserBagPackTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

@Service
@Slf4j
@Transactional
public class SecureTicketService {

    @Autowired
    private SecureTicketRepository secureTicketRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private EventRepository eventRepository;

    public List<SecureTicket> fetchSecureTickets(Long userId) throws Exception {
        log.info("Starting to fetch the secure tickets for :: " + userId);

        User user = userService.get(userId);
        return secureTicketRepository.findAllByUser(user);
    }

    public void deleteSecureTickets(Long userId) throws Exception{
        List<SecureTicket> secureTickets = this.fetchSecureTickets(userId);
        secureTicketRepository.delete(secureTickets);
    }

    public List<SecureTicket> fetchSecureTicketsWithLockTrue(Long userId) throws Exception {
        log.info("Starting to fetch the secure tickets for :: " + userId);

        User user = userService.get(userId);
        return secureTicketRepository.findAllByUserAndLockStatus(user, true);
    }

    public SecureTicket getSecureTicket(UserBagPack userBagPack, UserBagPackTemplate userBagPackTemplate) {
        log.info("Started saving secure ticket with ticketBookingPrice ::" + userBagPackTemplate.getLatestTicketBookingPrice() + " and no.of tickets ::" + userBagPackTemplate.getNumberOfTickets() + " for bagpack ::" + userBagPack.getId());
        SecureTicket SecureTicket = new SecureTicket();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 10);
        SecureTicket.setExpiryDateTime(calendar.getTime());
        SecureTicket.setUser(userBagPack.getUser());
        SecureTicket.setEvent(eventRepository.findOneByEventReferenceId(userBagPackTemplate.getEventId()));
        SecureTicket.setTicketGroupId(userBagPackTemplate.getTicketGroupId());
        SecureTicket.setTicketBookingPrice(new BigDecimal(userBagPackTemplate.getLatestTicketBookingPrice()));
        SecureTicket.setNoOfTickets(Integer.valueOf(userBagPackTemplate.getNumberOfTickets()));
        SecureTicket.setSessionReferenceId(userBagPackTemplate.getSessionReferenceId());
        SecureTicket.setUserBagPack(userBagPack);
        SecureTicket.setSupplierAmount(userBagPackTemplate.getSupplierAmount());
        return SecureTicket;
    }
}
