package com.beatroute.api.service;

import com.beatroute.api.model.enumeration.UserType;
import com.beatroute.api.repository.UserRepository;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import org.springframework.security.core.userdetails.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class AuthenticationService implements UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(AuthenticationService.class);

    @Autowired
    private UserRepository userRepository;

    boolean accountNonExpired = true;
    boolean credentialsNonExpired = true;
    boolean accountNonLocked = true;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        ContextData cd= ContextStorage.get();
        com.beatroute.api.model.User user = this.userRepository.findOneByEmailAndEnterpriseId(email,cd.getEnterpriseId());
        if(user == null){
            throw new UsernameNotFoundException("user with email: "+email+" not found");
        }


        if(user.getUserType() == null){    //user.getUserType() != UserType.APP_USER){
            throw new UsernameNotFoundException("UserType for the email: "+email+" not found");
        }

        /*
            return data : (id, empty,_dont_send_anything, isAuthorised, .. , .., .., ROLES)
         */
        return new User(user.getId().toString(), user.getPassword() , user.isAuthorised(), accountNonExpired, credentialsNonExpired, accountNonLocked, getAuthorities());
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(UserType.APP_USER.toString());
        authorities.add(authority);
        return authorities;
    }
}