package com.beatroute.api.service.exception;

public class EntityAlreadyExistsException extends Exception{

    public EntityAlreadyExistsException(String message) {
        super(message);
    }
}
