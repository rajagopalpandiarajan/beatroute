package com.beatroute.api.service.exception;

public class EntityNotFoundException extends Exception{
    String message;

    public EntityNotFoundException(String message){
        super(message);
    }
}
