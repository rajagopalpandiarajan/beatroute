package com.beatroute.api.service;

import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.UserBagPack;
import com.beatroute.api.repository.SecureTicketRepository;
import com.beatroute.api.rest.template.OrderRequest;
import com.beatroute.api.rest.template.TempLockRequest;
import com.beatroute.api.rest.template.UserBagPackTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

@Slf4j
@Component(value = "ticketnetwork")
public class TicketNetworkSupplier implements Supplier{

    @Autowired
    private GenericAsyncService genericAsyncService;

    @Autowired
    private SecureTicketRepository secureTicketRepository;

    @Autowired
    private SecureTicketService secureTicketService;

    @Override
    public Future<String> getTickets(String id) {
        log.info("Started getting events from Ticket Network");
        Future<String> response = null;
        try {
            System.out.println(id);
            response = genericAsyncService.getTickets(id);
        } catch (Exception e) {
            log.info("Error getting tickets ::" + e);
        }
        log.info("Completed getting events fro ticket network");
        return response;
    }

    @Override
    public Future<String> getTickets(String id, String ticketCount) {
        return null;
    }

    @Override
    @Async
    public Future<String> lockTicket(UserBagPack userBagPack, UserBagPackTemplate userBagPackTemplate) {
        log.info("Started to lock tickets from Ticket Network");
        Future<String> future = new AsyncResult<>("");
        try {
            if (!userBagPackTemplate.isExpired() && (userBagPackTemplate.getLatestTicketBookingPrice() != null)) {
                SecureTicket secureTicket = secureTicketService.getSecureTicket(userBagPack, userBagPackTemplate);
                secureTicketRepository.save(secureTicket);

                // lock ticket
                try {
                    log.info("Calling lock ticket");
                    future = genericAsyncService.lockTicket(getTempLockRequest(secureTicket));
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("Completed locking tickets from Ticket Network");
        return future;
    }

    @Override
    @Async
    public Future<String> placeOrder(OrderRequest orderRequest) {
        log.info("Started placing Order for TicketNetwork");
        Future<String> future = new AsyncResult<>("");
        try {
            future = genericAsyncService.createOrder(orderRequest);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.info("Completed placing order for Ticket Network");
        return future;
    }

    /*private SecureTicket getSecureTicket(UserBagPack userBagPack, UserBagPackTemplate userBagPackTemplate) {
        log.info("Started saving secure ticket with ticketBookingPrice ::" + userBagPackTemplate.getLatestTicketBookingPrice() + " and no.of tickets ::" + userBagPackTemplate.getNumberOfTickets() + " for bagpack ::" + userBagPack.getId());
        SecureTicket SecureTicket = new SecureTicket();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 10);
        SecureTicket.setExpiryDateTime(calendar.getTime());
        SecureTicket.setUser(userBagPack.getUser());
        SecureTicket.setEvent(eventRepository.findOneByEventReferenceId(userBagPackTemplate.getEventId()));
        SecureTicket.setTicketGroupId(userBagPackTemplate.getTicketGroupId());
        SecureTicket.setTicketBookingPrice(new BigDecimal(userBagPackTemplate.getLatestTicketBookingPrice()));
        SecureTicket.setNoOfTickets(Integer.valueOf(userBagPackTemplate.getNumberOfTickets()));
        SecureTicket.setSessionReferenceId(userBagPackTemplate.getSessionReferenceId());
        SecureTicket.setUserBagPack(userBagPack);
        return SecureTicket;
    }*/

    private TempLockRequest getTempLockRequest(SecureTicket secureTicket) {
        return new TempLockRequest(secureTicket.getNoOfTickets(), secureTicket.getTicketGroupId(), secureTicket.getTicketBookingPrice(), secureTicket.getLockRequestId());
    }

	@Override
    @Async
	public Future<String> getSessionInfo(String requestJson) {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    @Async
    public Future<String> getEventSessions(String requestJson) {
        // TODO Auto-generated method stub
        return null;
    }
}
