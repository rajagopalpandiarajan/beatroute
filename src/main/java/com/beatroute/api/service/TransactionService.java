package com.beatroute.api.service;

import com.beatroute.api.dto.PaymentDetail;
import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.*;
import com.beatroute.api.repository.*;
import com.beatroute.api.rest.template.AdditionalInfo;
import com.beatroute.api.rest.template.CancellationPolicyDto;
import com.beatroute.api.rest.template.PaxDto;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.*;

@Service
@Slf4j
public class TransactionService {

    public static final String _100 = "100.00";

    @Value("${asiapay.credentials.merchant.id}")
    private String merchantID;

    @Value("${asiapay.secure.hash.secret}")
    private String secureHashSecret;

    @Value("${asiapay.payment.type}")
    private String payType;

    @Value("${asiapay.currency.code}")
    private String currCode;

    /*@Value("${app.mail.payment.confirmation.subject}")
    private String paymentConfirmationSubject;*/

    @Value("${app.payment.sms.url}")
    private String smsUrl;

    @Value("${app.payment.sms.username}")
    private String smsUserName;

    @Value("${app.payment.sms.password}")
    private String smsPassword;

    @Value("${app.payment.sms.from}")
    private String smsFrom;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private PaymentDetail paymentDetail;

    public void saveUserTransactions(List<SecureTicket> secureTickets, String paymentReferenceId) throws Exception {
        log.info("Started saving user transactions");
        List<Transaction> userTransactions = new ArrayList<>();
        for(SecureTicket secureTicket : secureTickets) {
//            secureTicket.setPaymentReferenceId(paymentReferenceId);
//            userTransactions.add(getTransaction(secureTicket));
            //continue to make payment only if ticked is locked
//            if (secureTicket.getLockStatus()) {
                secureTicket.setPaymentReferenceId(paymentReferenceId);
                userTransactions.add(getTransaction(secureTicket));
//            }
        }
        transactionRepository.save(userTransactions);
        log.info("Saved all the user transactions");
        log.info("Completed saving user transactions");
    }

    public Transaction getTransaction(SecureTicket secureTicket) {
        Transaction transaction = new Transaction();
        transaction.setAmount(calculateAmount(secureTicket));
        transaction.setCurrency(paymentDetail.getCurrCode());
        transaction.setPaymentReferenceId(String.valueOf(System.nanoTime()));
        transaction.setUser(secureTicket.getUser());
        transaction.setNumberOfTickets(secureTicket.getNoOfTickets());
        transaction.setTicketBookingPrice(secureTicket.getTicketBookingPrice());
        transaction.setEvent(secureTicket.getEvent());
        transaction.setPaymentReferenceId(secureTicket.getPaymentReferenceId());
        transaction.setTicketReferenceNumber(secureTicket.getTicketReferenceNumber());
        transaction.setSession(sessionRepository.findOneBySessionReferenceIdAndEventId(secureTicket.getSessionReferenceId(), secureTicket.getEvent().getEventReferenceId()));
        transaction.setSupplierAmount(secureTicket.getSupplierAmount());
        String jsonData = secureTicket.getUserBagPack().getJsonData();
        if(jsonData != null){
            JSONObject jsonObject = new JSONObject(jsonData);
            if(jsonObject.has("shippingMethods")){
                Object shippingMethods  = jsonObject.opt("shippingMethods");
                if(shippingMethods !=null){
                    transaction.setShippingMethods(shippingMethods.toString());
                }
            }
        }
        return transaction;
    }

    private BigDecimal calculateAmount(SecureTicket secureTicket) {
        BigDecimal totalAmount = secureTicket.getTicketBookingPrice();
        totalAmount = totalAmount.multiply(new BigDecimal(secureTicket.getNoOfTickets()));
        BigDecimal extraAmount = totalAmount.multiply(new BigDecimal(paymentDetail.getPaymentBumpUpPercent()));
        totalAmount = totalAmount.add(extraAmount);
        return totalAmount;
    }

    public String getCancellationPolicies(List<CancellationPolicyDto> cancellationPoliciesList) throws Exception {
        StringBuilder sb=new StringBuilder();
        String cancelPolicy = null;
        if(cancellationPoliciesList != null && cancellationPoliciesList.size()>0){
            for (CancellationPolicyDto cancellationPolicy : cancellationPoliciesList) {
                sb.append("dateFrom :"+cancellationPolicy.getDateFrom()+"<br/>");
                sb.append("amount :"+ cancellationPolicy.getAmount()+"<br/>");
            }
            cancelPolicy = sb.toString();
            cancelPolicy = cancelPolicy.replace("dateFrom :", "Last Date this activity can be Cancelled: ").replace("amount :", "Cancellation Charges <b>$</b>");
            log.info("cancellationPolicies "+sb.toString());
        }
        return cancelPolicy;
    }

    public String getPaxAmounts(List<PaxDto> paxAmountsList) throws Exception {
        StringBuilder sb=new StringBuilder();
        if(paxAmountsList !=null && paxAmountsList.size()>0){
            for (PaxDto paxAmount : paxAmountsList) {
                sb.append("paxType :"+paxAmount.getPaxType()+"<br/>");
                sb.append("ageFrom :"+ paxAmount.getAgeFrom()+"<br/>");
                sb.append("ageTo :"+ paxAmount.getAgeTo()+"<br/>");
                sb.append("amount :"+ paxAmount.getAmount()+"<br/>");
                sb.append("boxOfficeAmount :"+ paxAmount.getBoxOfficeAmount()+"<br/><br/>");
            }
            log.info("paxAmounts "+sb.toString());
        }
        return sb.toString();
    }

    public String getAdditionalInfo(List<AdditionalInfo> additionalInfoList) {
        StringBuilder sb=new StringBuilder();
        if(additionalInfoList !=null && additionalInfoList.size()>0){
            for (AdditionalInfo additionalInfo : additionalInfoList) {
                sb.append("code :"+additionalInfo.getCode()+"<br/>");
                sb.append("text :"+ additionalInfo.getText()+"<br/>");
                sb.append("answer :"+ additionalInfo.getAnswer()+"<br/>");
                sb.append("required :"+ additionalInfo.getRequired()+"<br/><br/>");
            }
            log.info("paxAmounts "+sb.toString());
        }
        return sb.toString();
    }

}
