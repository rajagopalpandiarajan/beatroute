package com.beatroute.api.service;

import com.beatroute.api.model.*;
import com.beatroute.api.repository.SecureTicketRepository;
import com.beatroute.api.repository.SessionRepository;
import com.beatroute.api.repository.UserBagPackRepository;
import com.beatroute.api.rest.template.SupplierInfo;
import com.beatroute.api.rest.template.TicketDto;
import com.beatroute.api.rest.template.TicketsDto;
import com.beatroute.api.rest.template.UserBagPackTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.Future;

@Service
@Slf4j
@SuppressWarnings({ "unchecked", "rawtypes" })
public class UserBagPackService {

    @Autowired
    private UserService userService;

    @Autowired
    private UserBagPackRepository userBagPackRepository;

    @Value("${payment.bump.up.percentage}")
    private String bumpUpValue;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private GenericAsyncService genericAsyncService;

    @Autowired
    private SecureTicketRepository secureTicketRepository;

    @Autowired
    private SupplierFactory supplierFactory;

    public Long addToUserBagPack(String jsonData) throws EntityNotFoundException, Exception {
        log.info("Started saving user bag pack");
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(jsonData);

        UserBagPack userBagPack = new UserBagPack();
        User user = userService.get(Long.parseLong(obj.get("userId").toString()));
        if (user == null) {
            throw new EntityNotFoundException("user with id not found: " + jsonData);
        }
        userBagPack.setUser(user);

        ObjectMapper objectMapper = new ObjectMapper();
        userBagPack.setJsonData(objectMapper.writeValueAsString(obj.get("jsonData")));

        userBagPackRepository.save(userBagPack);

        log.info("Completed saving user bag pack");
        return userBagPack.getId();
    }

    public void deleteUserBagPack(Long id) throws EntityNotFoundException, Exception {
        log.info("Started deleting bagpack for user");

        User user = userService.get(id);
        List<UserBagPack> userBagPack = userBagPackRepository.findAllByUser(user);
        if (userBagPack == null) {
            throw new EntityNotFoundException("Bagpack not found for userId:" + id);
        }

        userBagPackRepository.delete(userBagPack);
        log.info("Completed deleting bagpack for user");
    }

    public void deleteEventFromUserBagPack(Long bagPackId) throws EntityNotFoundException, Exception {
        log.info("Deleting event from  bag pack for user");
        UserBagPack userBagPack = userBagPackRepository.findOne(bagPackId);
        if (userBagPack == null) {
            throw new EntityNotFoundException("Bagpack not found with id:" + bagPackId);
        }
        //Deleting tickets that were locked but never proceeded for payment
        log.info("Deleting stale tickets for user");
        List<SecureTicket> secureTickets = secureTicketRepository.findAllByUser(userBagPack.getUser());
        if (secureTickets.size() > 0) {
            secureTicketRepository.delete(secureTickets);
        }
        log.info("Completed deleting stale tickets for user");
        userBagPackRepository.delete(userBagPack);
        log.info("Completed deleting event from bagpack for user");
    }

    public List<UserBagPack> fetchBagPack(Long userId) throws Exception {
        log.info("Starting to fetch the user backpack for :: " + userId);
        return userBagPackRepository.findAllByUserId(userId);
    }


    /**
     * @return JSON
     *
    {
    "totalAmount": 100.00,
    "bagPackData": [
    {
    "bagPackId": 1,
    "jsonData": {
    "eventId": "13952",
    "eventName": "Wicked",
    ...
    }
    },
    {
    "bagPackId": 2,
    "jsonData": {
    "eventId": "13952",
    "eventName": "Wicked",
    ...
    }
    },
    ....]
    }
     * */
    /*public Map<Object, Object> checkAvailabilityOfTickets(List<UserBagPack> userBagPacks) throws Exception{
        Long startTime =  System.currentTimeMillis();
        log.info("Checking for availability of tickets");

        Map jsonMap = new HashMap();
        BigDecimal grandTotal = new BigDecimal(0);
        List<Object> jsondata = new ArrayList<>();
        Collection<Future<String>> futures = new ArrayList<Future<String>>();

        //for each bagpack make a call to Ticket Network (ASYNC CALLS)
        for (UserBagPack bagPack: userBagPacks) {
            Map json = new HashMap();
            json.put("bagPackId", bagPack.getId());

            ObjectMapper objectMapper = new ObjectMapper();
            UserBagPackTemplate userBagPackTemplate = objectMapper.readValue(bagPack.getJsonData(), UserBagPackTemplate.class);

            //update events that are not yet expired
            Session session = sessionRepository.findOneBySessionReferenceIdAndEventId(userBagPackTemplate.getSessionReferenceId());
            if (session.getBeginDateTime().after(new Date())) {

                //async REST call to Ticket Network
                log.info("Making a call to Ticket Network for fetching tickets");
                futures.add(genericAsyncService.getTickets(userBagPackTemplate.getSessionReferenceId()));
                log.info("Got Response from Ticket Network");

            }else {
                // event date is already over
                log.info("Event is already over");
                json.put("jsonData", getUserBagPackTemplate(userBagPackTemplate, session, null, true));
                bagPack.setJsonData(objectMapper.writeValueAsString(json.get("jsonData")));
                jsondata.add(json);
                userBagPackRepository.save(bagPack);
            }
        }

        *//************************************************************************************//*

        for (UserBagPack bagPack: userBagPacks) {
            Map json = new HashMap();
            json.put("bagPackId", bagPack.getId());

            ObjectMapper objectMapper = new ObjectMapper();
            UserBagPackTemplate userBagPackTemplate = objectMapper.readValue(bagPack.getJsonData(), UserBagPackTemplate.class);

            boolean responseNotReceived = true;
            for(Future<String> future: futures) {
                while (responseNotReceived) {
                    responseNotReceived = !future.isDone();
                }

                String response = future.get();
                // parse the response
                JSONParser parser = new JSONParser();
                JSONObject obj = (JSONObject) parser.parse(response);

                // updating ticket booking price
                Session session = sessionRepository.findOneBySessionReferenceIdAndEventId(userBagPackTemplate.getSessionReferenceId());
                int size = ((List<Map<String,Object>>)obj.get("tickets")).size();
                List<Map<String,Object>> tickets = (List<Map<String,Object>>)obj.get("tickets");
                for(int i = 0; i < size; i++){
                    if(userBagPackTemplate.getTicketGroupId().equals(tickets.get(i).get("vendorTicketRefId"))) {
                        log.info("Ticket is available...updating price of ticket");
                        json.put("jsonData", getUserBagPackTemplate(userBagPackTemplate, session, new BigDecimal(tickets.get(i).get("wholeSalePrice").toString()).setScale(2, BigDecimal.ROUND_UP).toString(), false));
                        bagPack.setJsonData(objectMapper.writeValueAsString(json.get("jsonData")));
                        userBagPackRepository.save(bagPack);
                        if (!jsondata.contains(json)) {
                            grandTotal = grandTotal.add(new BigDecimal(userBagPackTemplate.getTicketBookingPrice()).multiply(new BigDecimal(userBagPackTemplate.getNumberOfTickets())));
                            jsondata.add(json);
                        }
                        break;
                    }
                }
            }
        }

        *//************************************************************************************//*

        jsonMap.put("bagPackData", jsondata);
        BigDecimal markUpValue = grandTotal.multiply(new BigDecimal(bumpUpValue));
        grandTotal = grandTotal.add(markUpValue);
        grandTotal = grandTotal.setScale(2, BigDecimal.ROUND_UP);
        jsonMap.put("totalAmount", grandTotal);

        log.info("Completed updating tickets");

        Long endTime = System.currentTimeMillis();
        log.info("Time taken:" + (endTime - startTime));
        return jsonMap;
    }
*/
    private UserBagPackTemplate getUserBagPackTemplate(UserBagPackTemplate userBagPackTemplate, Session session, String latestTicketBookingPrice, Boolean status){
        if(session != null && session.getId() > 0) {
            Venue venue = session.getVenues().iterator().next();
            userBagPackTemplate.setCity(venue.getCity());
            userBagPackTemplate.setCountry(venue.getCountry());
            userBagPackTemplate.setAddress(venue.getAddress());
            userBagPackTemplate.setImageGallery(session.getEvent().getImageGallery());
            userBagPackTemplate.setSessionDate(String.valueOf(session.getBeginDateTime().getTime()));
        }
        userBagPackTemplate.setLatestTicketBookingPrice(latestTicketBookingPrice);
        userBagPackTemplate.setExpired(status);
        return userBagPackTemplate;

    }


    public Map<Object, Object> getUserBagPack(Long id) throws EntityNotFoundException, Exception {
        log.info("Started getting events from bag pack for user");
        Map jsonMap = new HashMap();
        try {
            List<UserBagPack> userBagPacks = this.fetchBagPack(id);
            //   log.info("*** Size of the List userBagPacks is "+userBagPacks.size());
            BigDecimal grandTotal = new BigDecimal(0);
            List<Object> jsondata = new ArrayList<>();
            ObjectMapper mapper = new ObjectMapper();

            for (UserBagPack bagPack : userBagPacks) {
                UserBagPackTemplate userBagPackTemplate = mapper.readValue(bagPack.getJsonData(), UserBagPackTemplate.class);
                Session session = sessionRepository.findOneBySessionReferenceIdAndEventId(userBagPackTemplate.getSessionReferenceId(), userBagPackTemplate.getEventId());
                if(session != null) {
                    Map json = new HashMap();
                    json.put("bagPackId", bagPack.getId());
                    json.put("jsonData", getUserBagPackTemplate(userBagPackTemplate, session, userBagPackTemplate.getLatestTicketBookingPrice(), userBagPackTemplate.isExpired()));

                    String latestPrice = null;
                    if (userBagPackTemplate.getLatestTicketBookingPrice() != null) {
                        latestPrice = new BigDecimal(userBagPackTemplate.getLatestTicketBookingPrice()).setScale(2, BigDecimal.ROUND_UP).toString();
                    }else{
                        latestPrice = userBagPackTemplate.getTicketBookingPrice();
                    }
                    grandTotal = grandTotal.add(new BigDecimal(latestPrice).multiply(new BigDecimal(userBagPackTemplate.getNumberOfTickets())));
                    jsondata.add(json);
                }
            }
            jsonMap.put("bagPackData", jsondata);
            BigDecimal markUpValue = grandTotal.multiply(new BigDecimal(bumpUpValue));
            grandTotal = grandTotal.add(markUpValue);
            grandTotal = grandTotal.setScale(2, BigDecimal.ROUND_UP);
            jsonMap.put("totalAmount", grandTotal);
            log.info("Completed getting events from bagpack for user");

        }catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return jsonMap;
    }

    public Map<Long, Future<String>> checkAvailabilityOfSupplierTickets(List<UserBagPack> userBagPacks) throws IOException {
        log.info("Checking for availability of tickets");
//        Collection<Future<String>> futures = new ArrayList<Future<String>>();
        Map<Long, Future<String>> futures = new HashMap<Long, Future<String>>();

        for (UserBagPack bagPack: userBagPacks) {
            ObjectMapper objectMapper = new ObjectMapper();
            UserBagPackTemplate userBagPackTemplate = objectMapper.readValue(bagPack.getJsonData(), UserBagPackTemplate.class);

            //update events that are not yet expired
            Session session = sessionRepository.findOneBySessionReferenceIdAndEventId(userBagPackTemplate.getSessionReferenceId(), userBagPackTemplate.getEventId());
            if (session != null && session.getBeginDateTime().after(new Date())) {
                //identify supplier
                Supplier supplier = supplierFactory.getSupplier(userBagPackTemplate.getSupplierName());

                //lock tickets based on supplier (Asynchronous call)
                if (userBagPackTemplate.getSupplierName().equalsIgnoreCase("Isango")) {
                	//need to create the getTicket requestJson
                	/**
                	 * {
				     * 		"eventId": "33985-10900",
				            "sessionId": "1-E20-X-011100027",
				            "sessionDate": 1480636800000,
				            "tickets": {
				                   "quantity": 4,
				                   "numAdults": 2,
				                   "numChildren": 2,
				                   "adultAges": [30, 30],
				                   "childrenAges": [4, 5]
				               }
				     * }
                	 */
                	HashMap<String, Object> requestMap=new HashMap<String, Object>();
                	requestMap.put("eventId", userBagPackTemplate.getEventId());
                	requestMap.put("sessionId", userBagPackTemplate.getSessionReferenceId());
                	requestMap.put("sessionDate", userBagPackTemplate.getSessionDate());

                	SupplierInfo si=userBagPackTemplate.getSupplierInfo();
                	requestMap.put("additionalInfo", si.getAdditionalInfo());
                	requestMap.put("tickets", si.getTickets());

                	Future<String> ticketResponse=supplier.getTickets(objectMapper.writeValueAsString(requestMap));
                	try {
						/*TicketsDto responseTickets=objectMapper.readValue(ticketResponse.get(), TicketsDto.class);
						if (responseTickets!=null && responseTickets.getTickets() != null) {
							TicketDto ticket=responseTickets.getTickets().get(0);
							userBagPackTemplate.setTicketGroupId(ticket.getVendorTicketRefId());
							bagPack.setJsonData(objectMapper.writeValueAsString(userBagPackTemplate));
                            userBagPackRepository.save(bagPack);
						}*/

                        TicketsDto responseTickets=objectMapper.readValue(ticketResponse.get(), TicketsDto.class);
                        if (responseTickets!=null && responseTickets.getTickets() != null) {
                            for (TicketDto ticket: responseTickets.getTickets()) {
                                //find the ticket for same row item
                                //NOTE : need to compare with section , since UI is setting "section" in template row!
                                if (userBagPackTemplate.getRow().equalsIgnoreCase(ticket.getSection())) {
                                    userBagPackTemplate.setTicketGroupId(ticket.getVendorTicketRefId());
                                    userBagPackTemplate.setSupplierAmount(ticket.getSupplierAmount());
                                    bagPack.setJsonData(objectMapper.writeValueAsString(userBagPackTemplate));
                                    userBagPackRepository.save(bagPack);
                                    log.info("bagPack :: " + objectMapper.writeValueAsString(bagPack));
                                }
                            }
                        }
					} catch (Exception e) {
						log.error("Failed to fetch ticket response!", e);
					}
                    futures.put(bagPack.getId(), ticketResponse);
                } else {
                    if (userBagPackTemplate.getSupplierName().equalsIgnoreCase("LondonTheatreDirect")) {
                        futures.put(bagPack.getId(), supplier.getTickets(session.getSessionReferenceId(), userBagPackTemplate.getNumberOfTickets()));
                    }else{
                        futures.put(bagPack.getId(), supplier.getTickets(session.getSessionReferenceId()));
                    }
                }
            }
        }
        return futures;
    }



    /*public Map<Object, Object> calculateUserBagPack1(List<UserBagPack> bagPackList, Collection<Future<String>> futures) throws Exception {
        log.info("Started calculating the total for the bagpack");
        Map jsonMap = new HashMap();
        List<Object> jsondata = new ArrayList<>();
        BigDecimal grandTotal = new BigDecimal(0);
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            for (UserBagPack bagPack: bagPackList) {

                UserBagPackTemplate userBagPackTemplate = objectMapper.readValue(bagPack.getJsonData(), UserBagPackTemplate.class);

                // in case session is expired handle session expiration
                Session session = sessionRepository.findOneBySessionReferenceIdAndEventId(userBagPackTemplate.getSessionReferenceId());
                if(session!=null) {
                    if(isSessionExpired(session)) {
                        processExpiredSessions(session, userBagPackTemplate, bagPack, jsondata);
                    } else {
                        grandTotal = grandTotal.add(processActiveSessions(futures, session, userBagPackTemplate, bagPack, jsondata));
                        log.info("Grand Total --> "+grandTotal);
                    }
                }
            }
        }catch (Exception e) {
            log.error(e.getMessage(),e);
        }

        calculateMarkUpValue(jsonMap, jsondata, grandTotal);
        log.info("Completed calculating the total for the bagpack");

        return jsonMap;
    }

    private void calculateMarkUpValue(Map jsonMap, List<Object> jsondata, BigDecimal grandTotal) {
        jsonMap.put("bagPackData", jsondata);
        BigDecimal markUpValue = grandTotal.multiply(new BigDecimal(bumpUpValue));
        grandTotal = grandTotal.add(markUpValue);
        grandTotal = grandTotal.setScale(2, BigDecimal.ROUND_UP);
        jsonMap.put("totalAmount", grandTotal);
    }

    private BigDecimal processActiveSessions(Collection<Future<String>> futures, Session session, UserBagPackTemplate userBagPackTemplate, UserBagPack bagPack, List<Object> jsondata) throws Exception {
        List<String> responses = getFutureResponses(futures);

        for(String response : responses){
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(response);

            int size = ((List<Map<String, Object>>) obj.get("tickets")).size();
            if (size <= 0) {
                // Tickets are not available for the event at all. A case of tickets sold out.
                processExpiredSessions(session, userBagPackTemplate, bagPack, jsondata);
            }else{
                return checkTicketAvailable(session, userBagPackTemplate, bagPack, jsondata, size, obj);
            }
        }
        return null;
    }

    private BigDecimal checkTicketAvailable(Session session, UserBagPackTemplate userBagPackTemplate, UserBagPack bagPack, List<Object> jsondata, int size, JSONObject obj) throws Exception {
        List<Map<String, Object>> tickets = (List<Map<String, Object>>) obj.get("tickets");
        boolean ticketFound = false;
        BigDecimal grandTotal = new BigDecimal(0);
        for (int i = 0; i < size; i++) {
            String ticketVendorRefId = (String) tickets.get(i).get("vendorTicketRefId");
            String bagPackTicketGroupId = userBagPackTemplate.getTicketGroupId();
            String supplierName = userBagPackTemplate.getSupplierName();

            if("Ingresso".equals(supplierName)){
                ticketVendorRefId = (String) tickets.get(i).get("section"); //ticketVendorRefId.split("INGRESSO")[0];
                bagPackTicketGroupId = userBagPackTemplate.getRow();    //bagPackTicketGroupId.split("INGRESSO")[0];
            }

            //NOTE: Isango always provides new rateKey (ticketVendorRefId). So we cannot use this comparison
            if("Ingresso".equals(supplierName) || "Isango".equals(supplierName)){
                ticketVendorRefId = (String) tickets.get(i).get("section");//ticketVendorRefId.split("INGRESSO")[0];
                bagPackTicketGroupId = userBagPackTemplate.getRow();//bagPackTicketGroupId.split("INGRESSO")[0];
            }
            log.info("bagPackTicketGroupId :: " +bagPackTicketGroupId+ " ticketVendorRefId :: "+ticketVendorRefId);
            if(bagPackTicketGroupId.equals(ticketVendorRefId)) {
                ticketFound = ticketAvailable(tickets.get(i), session, userBagPackTemplate, bagPack, jsondata);

                if(ticketFound && userBagPackTemplate.getLatestTicketBookingPrice() != null){
                    grandTotal = calculateGrandTotal(userBagPackTemplate);
                    break;
                }
            }
        }
        if (!ticketFound && userBagPackTemplate.getLatestTicketBookingPrice() == null) {
            // Tickets not available for the user selected category
            processExpiredSessions(session, userBagPackTemplate, bagPack, jsondata);
        }
        return grandTotal;
    }

    private BigDecimal calculateGrandTotal(UserBagPackTemplate userBagPackTemplate) {
        BigDecimal grandTotal = new BigDecimal(0);
        String numberOfTickets = userBagPackTemplate.getNumberOfTickets();
        String ticketBookingPrice = userBagPackTemplate.getTicketBookingPrice();
        log.info("************ Grand Total 1 - "+grandTotal);
        grandTotal = grandTotal.add(new BigDecimal(ticketBookingPrice).multiply(new BigDecimal(numberOfTickets)));
        log.info("************ Grand Total 2 - "+grandTotal);
        return grandTotal;
    }

    private boolean ticketAvailable(Map<String, Object> tickets, Session session, UserBagPackTemplate userBagPackTemplate, UserBagPack bagPack, List<Object> jsondata) throws Exception {
        //Tickets available for the category selected by user.
        Map json = new HashMap();
        json.put("bagPackId", bagPack.getId());
        ObjectMapper objectMapper = new ObjectMapper();
        String latestPrice = null;
        log.info("Ticket is available...updating ticket price for vendorTicketRefId is "+userBagPackTemplate.getTicketGroupId());
        latestPrice = new BigDecimal(tickets.get("wholeSalePrice").toString()).setScale(2, BigDecimal.ROUND_UP).toString();
        json.put("jsonData", getUserBagPackTemplate(userBagPackTemplate, session, latestPrice, false));
        bagPack.setJsonData(objectMapper.writeValueAsString(json.get("jsonData")));
        userBagPackRepository.save(bagPack);
        if (!jsondata.contains(json)) {
            jsondata.add(json);
        }
        return true;
    }

    public List<String> getFutureResponses(Collection<Future<String>> futures) throws Exception {
        List<String> responses = new ArrayList<>();
        for (Future<String> future: futures) {
            log.info("Future looping");
            boolean responseNotReceived = true;
            while (responseNotReceived) {
                Thread.sleep(2000);
                responseNotReceived = !future.isDone();
            }
            String response = future.get();
            //if(StringUtils.isEmpty(response)){
                responses.add(response);
            //}
        }
        return responses;
    }

    private void processExpiredSessions(Session session, UserBagPackTemplate userBagPackTemplate, UserBagPack bagPack, List<Object> jsondata) throws Exception {
        // Session has expired
        Map json = new HashMap();
        json.put("bagPackId", bagPack.getId());
        ObjectMapper objectMapper = new ObjectMapper();
        json.put("jsonData", getUserBagPackTemplate(userBagPackTemplate, session, null, true));
        bagPack.setJsonData(objectMapper.writeValueAsString(json.get("jsonData")));
        userBagPackRepository.save(bagPack);
        if (!jsondata.contains(json)) {
            jsondata.add(json);
        }
    }

    private boolean isSessionExpired(Session session) {
        boolean sessionExpired = true;
        if(session!=null) {
            ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
            Date beginDate = session.getBeginDateTime();
            Date currentDate = Date.from(utc.toInstant());
            // session date is equal to or less than todays date
            if(beginDate.compareTo(currentDate) > 0 || beginDate.compareTo(currentDate) == 0){
                sessionExpired = false;
            }
        }
        return sessionExpired;
    }
    */

	public Map<Object, Object> calculateUserBagPack(List<UserBagPack> bagPackList, Map<Long, Future<String>> futures) throws Exception {
        Map jsonMap = new HashMap();
        BigDecimal grandTotal = new BigDecimal(0);
        List<Object> jsondata = new ArrayList<>();

        for (UserBagPack bagPack: bagPackList) {
            Map json = new HashMap();
            json.put("bagPackId", bagPack.getId());

            ObjectMapper objectMapper = new ObjectMapper();
            UserBagPackTemplate userBagPackTemplate = objectMapper.readValue(bagPack.getJsonData(), UserBagPackTemplate.class);

            boolean responseNotReceived = true;
            boolean isExpired = false;
            String latestPrice = null;
            ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
            Future<String> future = futures.get(bagPack.getId());
            if(future != null){
                log.info("Future looping");
                while (responseNotReceived) {
                    Thread.sleep(2000);
                    responseNotReceived = !future.isDone();
                }

                String response = future.get();
                // parse the response
                JSONParser parser = new JSONParser();
                JSONObject obj = (JSONObject) parser.parse(response);

                log.info("session ref id = " + obj.get("sessionReferenceId"));

                // updating ticket booking price
                Session session = sessionRepository.findOneBySessionReferenceIdAndEventId(userBagPackTemplate.getSessionReferenceId(), userBagPackTemplate.getEventId());
                // Compare with now time in UTC
                //log.info("session begin date time - "+session.getBeginDateTime());
                if (session != null && session.getBeginDateTime().compareTo(Date.from(utc.toInstant())) > 0 || session.getBeginDateTime().compareTo(Date.from(utc.toInstant())) == 0) {
                    int size = ((List<Map<String, Object>>) obj.get("tickets")).size();
                    if (size > 0) {
                        log.info("size = " + size);
                        List<Map<String, Object>> tickets = (List<Map<String, Object>>) obj.get("tickets");
                        boolean ticketFound = false;
                        for (int i = 0; i < size; i++) {
                            String ticketVendorRefId = String.valueOf(tickets.get(i).get("vendorTicketRefId"));
                            String bagPackTicketGroupId = userBagPackTemplate.getTicketGroupId();
                            String supplierName = userBagPackTemplate.getSupplierName();
                            log.info("User bag pack session ref id = " + userBagPackTemplate.getSessionReferenceId());
                            if ("Ingresso".equals(supplierName) || "Isango".equals(supplierName)) {
                                ticketVendorRefId = String.valueOf(tickets.get(i).get("section"));
                                bagPackTicketGroupId = userBagPackTemplate.getRow();
                            }
                            /*if ("Ingresso".equals(supplierName)) {
                                log.info("ticketGroupId - "+userBagPackTemplate.getTicketGroupId()+" ----- "+tickets.get(i).get("vendorTicketRefId"));
                                ticketVendorRefId = String.valueOf(tickets.get(i).get("vendorTicketRefId")).split("INGRESSO")[0];
                                bagPackTicketGroupId = userBagPackTemplate.getTicketGroupId().split("INGRESSO")[0];
                            }*/
                            log.info("bagPackTicketGroupId :: " + bagPackTicketGroupId + " ticketVendorRefId :: " + ticketVendorRefId);
                            if (bagPackTicketGroupId.equals(ticketVendorRefId)) {
                                //log.info("Tickets size iteration: i " + i);
                                //Tickets available for the category selected by user.
                                ticketFound = true;
                                log.info("Ticket is available...updating price of ticket for vendorTicketRefId is " + userBagPackTemplate.getTicketGroupId());
                                Double supAmt = new Double(0);
                                if(tickets.get(i).containsKey("supplierAmount")){
                                    supAmt = new Double(String.valueOf(tickets.get(i).get("supplierAmount")));
                                }
                                userBagPackTemplate.setSupplierAmount(BigDecimal.valueOf(supAmt));
                                latestPrice = new BigDecimal(tickets.get(i).get("wholeSalePrice").toString()).setScale(2, BigDecimal.ROUND_UP).toString();
                                userBagPackTemplate = processSessionsJsonData(session, userBagPackTemplate, bagPack, jsondata, latestPrice, isExpired);
                                log.info("************ Grand Total - "+grandTotal+" and LatestPrice - "+latestPrice);
                            }
                            if (ticketFound && StringUtils.isNotEmpty(latestPrice)){    //userBagPackTemplate.getLatestTicketBookingPrice() != null) {
                                //log.info("************ Grand Total 1 - "+grandTotal);
                                grandTotal = grandTotal.add(new BigDecimal(latestPrice).multiply(new BigDecimal(userBagPackTemplate.getNumberOfTickets())));
                                //log.info("************ Grand Total 2 - "+grandTotal);
                                break;
                            }
                        }
                        if (!ticketFound && userBagPackTemplate.getLatestTicketBookingPrice() == null) {
                            // Tickets not available for the user selected category
                            processSessionsJsonData(session, userBagPackTemplate, bagPack, jsondata, latestPrice, isExpired);
                        }
                    } else {
                        // Tickets are not available for the event at all. A case of tickets sold out.
                        processSessionsJsonData(session, userBagPackTemplate, bagPack, jsondata, latestPrice, isExpired);
                    }
                } else {
                    // Session has expired or not found
                    isExpired = true;
                    processSessionsJsonData(session, userBagPackTemplate, bagPack, jsondata, latestPrice, isExpired);
                }
            } else {
                // Session has expired or not found
                isExpired = true;
                Session session = null;
                processSessionsJsonData(session, userBagPackTemplate, bagPack, jsondata, latestPrice, isExpired);
            }
        }

        jsonMap.put("bagPackData", jsondata);
        BigDecimal markUpValue = grandTotal.multiply(new BigDecimal(bumpUpValue));
        grandTotal = grandTotal.add(markUpValue);
        grandTotal = grandTotal.setScale(2, BigDecimal.ROUND_UP);
        jsonMap.put("totalAmount", grandTotal);

        log.info("Completed updating tickets");
        return jsonMap;
    }

    private UserBagPackTemplate processSessionsJsonData(Session session, UserBagPackTemplate userBagPackTemplate, UserBagPack bagPack, List<Object> jsondata, String latestTicketBookingPrice, Boolean isExpired) throws Exception {
        // Session has expired
        Map json = new HashMap();
        json.put("bagPackId", bagPack.getId());
        ObjectMapper objectMapper = new ObjectMapper();
        userBagPackTemplate = getUserBagPackTemplate(userBagPackTemplate, session, latestTicketBookingPrice, isExpired);
        json.put("jsonData", userBagPackTemplate);
        bagPack.setJsonData(objectMapper.writeValueAsString(json.get("jsonData")));
        userBagPackRepository.save(bagPack);
        if (!jsondata.contains(json)) {
            jsondata.add(json);
        }
        return userBagPackTemplate;
    }

    /*public Map<Object, Object> calculateUserBagPack(List<UserBagPack> bagPackList, Collection<Future<String>> futures) throws IOException, ExecutionException, InterruptedException, ParseException {
        Map jsonMap = new HashMap();
        BigDecimal grandTotal = new BigDecimal(0);
        List<Object> jsondata = new ArrayList<>();

        for (UserBagPack bagPack: bagPackList) {
            Map json = new HashMap();
            json.put("bagPackId", bagPack.getId());

            ObjectMapper objectMapper = new ObjectMapper();
            UserBagPackTemplate userBagPackTemplate = objectMapper.readValue(bagPack.getJsonData(), UserBagPackTemplate.class);

            boolean responseNotReceived = true;
            for (Future<String> future: futures) {
                while (responseNotReceived) {
                    responseNotReceived = !future.isDone();
                }

                String response = future.get();
                // parse the response
                JSONParser parser = new JSONParser();
                JSONObject obj = (JSONObject) parser.parse(response);

                // updating ticket booking price
                Session session = sessionRepository.findOneBySessionReferenceIdAndEventId(userBagPackTemplate.getSessionReferenceId());
                int size = ((List<Map<String,Object>>)obj.get("tickets")).size();
                List<Map<String,Object>> tickets = (List<Map<String,Object>>)obj.get("tickets");
                for(int i = 0; i < size; i++){
                    if(userBagPackTemplate.getTicketGroupId().equals(tickets.get(i).get("vendorTicketRefId"))) {
                        log.info("Ticket is available...updating price of ticket");
                        json.put("jsonData", getUserBagPackTemplate(userBagPackTemplate, session, new BigDecimal(tickets.get(i).get("wholeSalePrice").toString()).setScale(2, BigDecimal.ROUND_UP).toString(), false));
                        bagPack.setJsonData(objectMapper.writeValueAsString(json.get("jsonData")));
                        userBagPackRepository.save(bagPack);
                        if (!jsondata.contains(json)) {
                            grandTotal = grandTotal.add(new BigDecimal(userBagPackTemplate.getTicketBookingPrice()).multiply(new BigDecimal(userBagPackTemplate.getNumberOfTickets())));
                            jsondata.add(json);
                        }
                        break;
                    }
                    else {
                        json.put("jsonData", getUserBagPackTemplate(userBagPackTemplate, session, null, false));
                        bagPack.setJsonData(objectMapper.writeValueAsString(json.get("jsonData")));
                        userBagPackRepository.save(bagPack);
                        if (!jsondata.contains(json)) {
                            jsondata.add(json);
                        }
                    }
                }
            }
        }

        jsonMap.put("bagPackData", jsondata);
        BigDecimal markUpValue = grandTotal.multiply(new BigDecimal(bumpUpValue));
        grandTotal = grandTotal.add(markUpValue);
        grandTotal = grandTotal.setScale(2, BigDecimal.ROUND_UP);
        jsonMap.put("totalAmount", grandTotal);

        log.info("Completed updating tickets");
        return jsonMap;
    }*/

}

