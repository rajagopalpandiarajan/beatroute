package com.beatroute.api.service;

import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.UserBagPack;
import com.beatroute.api.repository.SecureTicketRepository;
import com.beatroute.api.rest.template.OrderRequest;
import com.beatroute.api.rest.template.TempLockRequest;
import com.beatroute.api.rest.template.UserBagPackTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

@Slf4j
@Component(value = "Ingresso")
public class IngressoSupplier implements Supplier{

    @Autowired
    private GenericAsyncService genericAsyncService;

    @Autowired
    private SecureTicketService secureTicketService;

    @Autowired
    private SecureTicketRepository secureTicketRepository;

    @Override
    public Future<String> getTickets(String id) {
        log.info("Started getting events from Ingresso");
        Future<String> response = null;
        try {
            response = genericAsyncService.getTicketsIngresso(id);
        } catch (Exception e) {
            log.info("Error getting tickets ::" + e);
        }
        log.info("Completed getting events from Ingresso");
        return response;
    }

    @Override
    public Future<String> getTickets(String id, String ticketCount) {
        return null;
    }

    @Override
    @Async
    public Future<String> lockTicket(UserBagPack userBagPack, UserBagPackTemplate userBagPackTemplate) {
        log.info("Started to lock tickets from Ingresso");
        Future<String> future = new AsyncResult<>("");
        try {
            if (!userBagPackTemplate.isExpired() && (userBagPackTemplate.getLatestTicketBookingPrice() != null)) {
                SecureTicket secureTicket = secureTicketService.getSecureTicket(userBagPack, userBagPackTemplate);
                secureTicketRepository.save(secureTicket);

                // lock ticket
                try {
                    log.info("Calling lock ticket Ingresso");
                    future = genericAsyncService.lockTicketIngresso(getTempLockRequest(secureTicket));
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("Completed locking tickets from Ingresso");
        return future;
    }

    @Override
    @Async
    public Future<String> placeOrder(OrderRequest orderRequest) {
        log.info("Started placing Order for Ingresso");
        Future<String> future = new AsyncResult<>("");
        try {
            future = genericAsyncService.createOrderIngresso(orderRequest);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.info("Completed placing order for Ingresso");
        return future;
    }

    private TempLockRequest getTempLockRequest(SecureTicket secureTicket) {
        return new TempLockRequest(secureTicket.getNoOfTickets(), secureTicket.getTicketGroupId(), secureTicket.getTicketBookingPrice(), secureTicket.getLockRequestId());
    }

    @Override
    @Async
	public Future<String> getSessionInfo(String requestJson) {
		// Auto-generated method stub
		return null;
	}

    @Override
    @Async
    public Future<String> getEventSessions(String requestJson) {
        log.info("Started getting event session for Ingresso");
        Future<String> future = new AsyncResult<>("");
        try {
            future = genericAsyncService.getEventSessionIngresso(requestJson);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.info("Completed getting event session for Ingresso");
        return future;
    }

}
