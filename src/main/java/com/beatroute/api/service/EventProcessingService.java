package com.beatroute.api.service;

import com.beatroute.api.model.*;
import com.beatroute.api.repository.CategoryRepository;
import com.beatroute.api.repository.SessionRepository;
import com.beatroute.api.repository.TagRepository;
import com.beatroute.api.repository.TenantRepository;
import com.beatroute.api.rest.template.PrimaryCategory;
import com.beatroute.api.rest.template.SecondaryCategory;
import com.beatroute.api.rest.template.TagTemplate;
import com.beatroute.api.rest.template.TenantTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class EventProcessingService {

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private SessionRepository sessionRepository;

    public boolean checkTagIfAbsent(String name){
        Tag t = this.tagRepository.findOneByName(name);
        if(t == null){
            return true;
        }
        return false;
    }

    public Tag getTagByName(String name){
        return this.tagRepository.findOneByName(name);
    }

    public boolean checkTenantIfAbsent(String name){
        Tenant t = this.tenantRepository.findOneByName(name);
        if(t == null){
            return true;
        }
        return false;
    }

    public Tenant getTenantByName(String name){
        return this.tenantRepository.findOneByName(name);
    }

    public Category getCategory(String name, String description){
        Category category = this.categoryRepository.findByNameThatStartsWith(name);     //findOneByName(name);
        if(category == null){
            category = new Category();
            category.setName(name);
            category.setDescription(description);

            category = categoryRepository.save(category);
        }

        return category;
    }

    public boolean checkSessionIfAbsent(String sessionRefId, Event event){
        String eventReferenceId = event.getEventReferenceId();
        Session session = null;
        if(event.getSupplier().equalsIgnoreCase("Ingresso")) {
            String sessionReferenceId = sessionRefId.split("INGRESSO")[0];
            session = this.sessionRepository.findOneBySessionReferenceIdAndEventIdForIngresso(sessionReferenceId, eventReferenceId);
        }else{
            session = this.sessionRepository.findOneBySessionReferenceIdAndEventId(sessionRefId, eventReferenceId);
        }

        if(session == null){
            return true;
        }
        return false;
    }

    public Session getSessionByRefId(String sessionRefId, String eventReferenceId){
        return this.sessionRepository.findOneBySessionReferenceIdAndEventId(sessionRefId, eventReferenceId);
    }

    public Set<Tag> getTagsFromTemplate(List<TagTemplate> tagTemplates){
        Set<Tag> tags = new HashSet<>();
        for(TagTemplate t : tagTemplates){
            if(checkTagIfAbsent(t.getName())){
                Tag tag = new Tag();
                tag.setName(t.getName());
                tag.setDescription(t.getDescription());

                tags.add(tag);
            }else{
                Tag tag = getTagByName(t.getName());
                tags.add(tag);
            }
        }
        return tags;
    }

    public Set<Tenant> getTenantsFromTemplate(List<TenantTemplate> tenantTemplates){
        Set<Tenant> tenants = new HashSet<>();

        for(TenantTemplate t: tenantTemplates){
            if(checkTenantIfAbsent(t.getName())){
                Tenant tenant = new Tenant();
                tenant.setName(t.getName());
                tenant.setApiKey(t.getApiKey());

                tenants.add(tenant);
            }else{
                Tenant tenant = getTenantByName(t.getName());
                tenants.add(tenant);
            }
        }

        return tenants;
    }

    public Category getPrimaryCategory(PrimaryCategory category){
        return getCategory(category.getName(), category.getDescription());
    }

    public Category getSecondaryCategory(SecondaryCategory category){
        return getCategory(category.getName(), category.getDescription());
    }
}
