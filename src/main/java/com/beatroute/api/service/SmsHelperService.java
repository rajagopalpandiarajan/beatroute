package com.beatroute.api.service;

import com.beatroute.api.dto.EmailContent;
import com.beatroute.api.dto.SmsRequest;
import com.beatroute.api.model.Transaction;
import com.beatroute.api.model.Venue;
import com.beatroute.api.model.enumeration.TransactionStatus;
import com.beatroute.api.repository.TransactionRepository;
import com.beatroute.api.repository.VenueRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class SmsHelperService{

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private SmsService smsService;

    @Autowired
    private SmsRequest smsRequest;

    /*@Autowired
    private SmsRequest smsRequestCancel;*/

    @Autowired
    private VenueRepository venueRepository;

    @Value("${app.payment.sms.text.after.booking}")
    private String smsTextAfterBooking;

    @Value("${app.sendSms.booking.cancel}")
    private String smsTextAfterCancelBooking;

    @Autowired
    private EnterpriseService enterpriseService;

    @Async
    public void sendSmsToUser(String paymentReferenceId, String sourceHost) {
        log.info("Sending sms to user");

        List<Transaction> transactions = transactionRepository.findAllByPaymentReferenceId(paymentReferenceId);

        EmailContent emailContent = this.enterpriseService.getEnterpriseContentByHost(sourceHost);

        for (Transaction transaction : transactions) {
            //send sms for transactions with status SUCCESS only
            log.info("Transaction Status - "+transaction.getTransactionStatus());
            if (TransactionStatus.SUCCESS.equals(transaction.getTransactionStatus())){
                //get user phone number
                String sendSmsTo = transaction.getUser().getMobile();
                smsRequest.setSendSmsTo(sendSmsTo);

                //get event name
                String eventName = transaction.getEvent().getName();
                smsRequest.setEventName(eventName);

                //get session begin time for event booked by user
                Date eventSession = transaction.getSession().getBeginDateTime();

                List<Venue> venues = venueRepository.findAllBySessionId(transaction.getSession().getId());
                String country = venues.iterator().next().getCountry();

                LocalDateTime localDateTime = LocalDateTime.ofInstant(eventSession.toInstant(), ZoneId.systemDefault());
                smsRequest.setEventSessionDate(localDateTime.toLocalDate());
                smsRequest.setEventSessionTime(localDateTime.toLocalTime());
                smsRequest.setSmsReturnUrl(emailContent.getHost());
                //set sms text
                String bookingText = smsTextAfterBooking.replace("$displayName", emailContent.getDisplayName());
                log.info("booking text - "+bookingText);
                smsRequest.setText(smsTextAfterBooking + paymentReferenceId + " for " + eventName + ", " + country + "  has been received. Please check your email for details.");
                String response = smsService.sendSms(smsRequest.toRestRequest());
                log.info("Sms response: " + response);
            }
        }
    }

    /*@Async
    public void sendSmsToUserForCancelBooking(Transaction transaction) {
        log.info("Sending sms to user for cancel booking");

        ContextData cd= ContextStorage.get();
        String smsReturnUrl = cd.getOriginHost();

        if (TransactionStatus.CANCELLED.equals(transaction.getTransactionStatus())){
            String sendSmsTo = transaction.getUser().getMobile();
            smsRequestCancel.setSendSmsTo(sendSmsTo);

            String eventName = transaction.getEvent().getName();
            smsRequestCancel.setEventName(eventName);

            Date eventSession = transaction.getSession().getBeginDateTime();

            List<Venue> venues = venueRepository.findAllBySessionId(transaction.getSession().getId());
            String country = venues.iterator().next().getCountry();

            LocalDateTime localDateTime = LocalDateTime.ofInstant(eventSession.toInstant(), ZoneId.systemDefault());
            smsRequestCancel.setEventSessionDate(localDateTime.toLocalDate());
            smsRequestCancel.setEventSessionTime(localDateTime.toLocalTime());
            smsRequestCancel.setSmsReturnUrl(smsReturnUrl);
            //set sms text
            smsRequestCancel.setText(smsTextAfterCancelBooking + transaction.getPaymentReferenceId() + " for " + eventName + ", " + country + " has been cancelled. Pls check your email for details.");
            String response = smsService.sendSms(smsRequestCancel.toRestRequest());
            System.out.println("response for cancel : "+response);
            log.info("cancellation sms response : " +response);
        }
    }*/

}
