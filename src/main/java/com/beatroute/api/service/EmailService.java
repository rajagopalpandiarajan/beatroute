package com.beatroute.api.service;

import com.beatroute.api.dto.EmailContent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private MailContentBuilder mailContentBuilder;

    @Value("${spring.mail.username}")
    private String fromUsername;

    @Value("${cancellation.mail.bcc.usernames}")
    private String cancellationBccUsername;

    @Async
    public void sendUserRegistartionEmail(String userName, String userEmail, String activationLink, String subject, EmailContent emailContent) throws MailException, InterruptedException {
        log.info("Started sending user registartion email");
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(userEmail);
            messageHelper.setSubject(subject);
            String content = mailContentBuilder.buildUserRegistrationTemplate(userName, activationLink, emailContent);
            messageHelper.setText(content, true);
        };
        javaMailSender.send(messagePreparator);
        log.info("Completed sending user registartion email");
    }

    @Async
    public void sendForgotPasswordEmail(String userName, String userEmail, String activationLink, String subject, EmailContent emailContent) throws MailException, InterruptedException {
        log.info("Started sending forgot password email");
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(userEmail);
            messageHelper.setSubject(subject);
            String content = mailContentBuilder.buildForgotPasswordTemplate(userName, activationLink, emailContent);
            messageHelper.setText(content, true);
        };
        javaMailSender.send(messagePreparator);
        log.info("Completed sending reset password email");
    }

    /*@Async
    public void sendResetPasswordEmail(String userName, String userEmail, String subject) throws MailException, InterruptedException {
        log.info("Started sending reset password email");
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(userEmail);
            messageHelper.setSubject(subject);
            String content = mailContentBuilder.buildResetPasswordTemplate(userName);
            messageHelper.setText(content, true);
        };
        javaMailSender.send(messagePreparator);
        log.info("Completed starting ending payment confirmation email");
    }*/

    @Async
    public void sendPaymentConfirmation(String userName, String userEmail, String subject, String eventName, String venueName, String sessionDate, String tAmount, String tNo, String tTax, String tId, String tToalAmount, String importantNotes, String paxAmounts, String cancellationPolicies, String supplierName, String additionalNotes, String additionalInfo, EmailContent emailContent) throws MailException, InterruptedException {
        log.info("Started sending payment confirmation email");
        if(supplierName.equalsIgnoreCase("Isango")){
            supplierName = "Activities Bank";
        }
        String finalSupplierName = supplierName;
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(userEmail);
            messageHelper.setSubject(subject);
            String content = mailContentBuilder.buildPaymentConfirmationTemplate(userName, eventName, venueName, sessionDate, tAmount, tNo, tTax, tId, tToalAmount, importantNotes, paxAmounts, cancellationPolicies, finalSupplierName, additionalNotes, additionalInfo, emailContent);
            messageHelper.setText(content, true);
        };
        javaMailSender.send(messagePreparator);
        log.info("Completed sending payment confirmation email");
    }

    @Async
    public void sendCancelBookingMail(String userEmail, String subject, String eventName, String sessionBeginDate, String sessionEndDate, String mercuryOrderId, String travellerName, String amount, String currency, String numberOfTickets, String ticketBookingPrice, String paymentReferanceId, String url, String transactionStatus, String paymentStatus, String createdDate, String updatedDate, String ticketReferenceNumber, String user, String event, String session, String refundAmount, EmailContent emailContent)
    {
        log.info("sendMailToUser for cancel ticket");
        String bccUsers[] = cancellationBccUsername.split(",");
        MimeMessagePreparator mimeMessagePreparator=mimeMessage -> {
            MimeMessageHelper messageHelper=new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(fromUsername);
            messageHelper.setBcc(bccUsers);
            messageHelper.setTo(userEmail);
            messageHelper.setSubject(subject);
            String content=mailContentBuilder.buildBookingCancelTemplet(userEmail,subject, eventName, sessionBeginDate,  sessionEndDate, mercuryOrderId, travellerName,amount,currency,numberOfTickets,ticketBookingPrice,paymentReferanceId,url,transactionStatus,paymentStatus,createdDate,updatedDate,ticketReferenceNumber,user,event,session,refundAmount, emailContent);
            messageHelper.setText(content,true);
        };

        javaMailSender.send(mimeMessagePreparator);
        log.info("completed booking success");
    }

    @Async
    public void sendOrderFailure(String userName, String userEmail, String subject, String paymentReferenceId, String eventName, String noOfTickets, String totalAmount, Date transactionDate, String supplierName, String importantNotes, EmailContent emailContent) {
        log.info("Started sending order failure email");
        if(supplierName.equalsIgnoreCase("Isango")){
            supplierName = "Activities Bank";
        }
        String finalSupplierName = supplierName;
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(userEmail);
            messageHelper.setSubject(subject);
            String content = mailContentBuilder.buildOrderFailureTemplate(userName, paymentReferenceId, eventName, noOfTickets, totalAmount, transactionDate, finalSupplierName, importantNotes, emailContent);
            messageHelper.setText(content, true);
        };
        javaMailSender.send(messagePreparator);
        log.info("Completed sending order failure email");
    }

    @Async
    public void sendDownloadTicket(String userName, String userEmail, String subject, String eventName, String venueName, String sessionDate, String tAmount, String tNo, String tTax, String tId, String tToalAmount, String supplierName, String importantNotes, String cancellationPolicies, String paxAmounts, String additionalNotes, String additionalInfo, int numAdults, int numChildren, EmailContent emailContent) throws MailException, InterruptedException {
        log.info("Started download ticket email");
        if(supplierName.equalsIgnoreCase("Isango")){
            supplierName = "Activities Bank";
        }
        String finalSupplierName = supplierName;
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(userEmail);
            messageHelper.setSubject(subject);
            String content = mailContentBuilder.buildDownloadTicketTemplate(userName, eventName, venueName, sessionDate, tAmount, tNo, tTax, tId, tToalAmount, finalSupplierName, importantNotes, cancellationPolicies, paxAmounts, additionalNotes, additionalInfo, numAdults, numChildren, emailContent);
            messageHelper.setText(content, true);
        };
        javaMailSender.send(messagePreparator);
        log.info("Completed sending download ticket email");
    }

    public void sendEnquiryMail(String sendMailTo, String enquirySubject, String msg) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(sendMailTo);
            message.setText(msg);
            message.setSubject(enquirySubject);
            javaMailSender.send(message);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
