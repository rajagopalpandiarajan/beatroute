package com.beatroute.api.service;

import com.beatroute.api.rest.template.OrderRequest;
import com.beatroute.api.rest.template.TempLockDetailedRequest;
import com.beatroute.api.rest.template.TempLockRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;

@Service
@Slf4j
public class GenericAsyncService {

    public static final String SUCCESS = "success";

    public static final String FAILURE = "failure";

    @Value("${max.pool.size}")
    private String maxPoolSize;

    @Value("${core.pool.size}")
    private String corePoolSize;

    @Value("${max.queue.size}")
    private String maxQueueSize;

    @Value("${app.ticketnetwork.createOrder}")
    private String createOrderUrl;

    @Value("${app.ticketnetwork.getTicket}")
    private String ticketUrl;

    @Value("${app.ticketnetwork.getTicketLock}")
    private String ticketLockUrl;

    @Value("${app.ingresso.createOrder}")
    private String ingressoCreateOrderUrl;

    @Value("${app.ingresso.getTicket}")
    private String ingressoTicketUrl;

    @Value("${app.ingresso.getTicketLock}")
    private String ingressoTicketLockUrl;

    @Value("${app.ingresso.getEventSession}")
    private String ingressoEventSessionInfoUrl;


    @Value("${app.londonTheatreDirect.getTicket}")
    private String londonTheatreDirectTicketUrl;

    @Value("${app.londonTheatreDirect.getTicketLock}")
    private String londonTheatreDirectTicketLockUrl;


    @Value("${app.isango.getEventSession}")
    private String isangoEventSessionInfoUrl;

    @Value("${app.isango.getSessionInfo}")
    private String isangoSessionInfoUrl;
    
    @Value("${app.isango.getTicket}")
    private String isangoTicketUrl;
    
    @Value("${app.isango.getTicketLock}")
    private String isangoTicketLockUrl;
    
    @Value("${app.isango.createOrder}")
    private String isangoCreateOrderUrl;
    
    @Autowired
    private RestTemplate networkTemplate;

    @Bean
    public RestTemplate ticketNetworkTemplate() {
        return new RestTemplate();
    }

    @Bean(name = "threadPoolTaskExecutor")
    public Executor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setMaxPoolSize(Integer.parseInt(maxPoolSize));
        threadPoolTaskExecutor.setCorePoolSize(Integer.parseInt(corePoolSize));
        threadPoolTaskExecutor.setThreadNamePrefix("PRT - ");
        threadPoolTaskExecutor.setQueueCapacity(Integer.parseInt(maxQueueSize));
        return threadPoolTaskExecutor;
    }

    @Async("threadPoolTaskExecutor")
    public Future<String> createOrder(OrderRequest orderRequest) throws Exception {
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Creating order for event");
        try {
            Thread.sleep(2000);
            String response = networkTemplate.postForEntity(createOrderUrl, orderRequest, String.class).getBody();
            asyncResult = new AsyncResult<>(response);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed order creation");
        return asyncResult;
    }

    @Async("threadPoolTaskExecutor")
    public Future<String> createOrderIngresso(OrderRequest orderRequest) throws Exception {
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Creating order for event Ingresso");
        try {
            Thread.sleep(2000);
            String response = networkTemplate.postForEntity(ingressoCreateOrderUrl, orderRequest, String.class).getBody();
            asyncResult = new AsyncResult<>(response);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed order creation Ingresso");
        return asyncResult;
    }

    @Async("threadPoolTaskExecutor")
    public Future<String> createOrderIsango(OrderRequest orderRequest) throws Exception {
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Creating order for event Isango");
        try {
            Thread.sleep(2000);
            String response = networkTemplate.postForEntity(isangoCreateOrderUrl, orderRequest, String.class).getBody();
            asyncResult = new AsyncResult<>(response);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed order creation Isango");
        return asyncResult;
    }
    
    @Async("threadPoolTaskExecutor")
    public Future<String> getTickets(String id) throws Exception{
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Started fetching tickets from ticket network");
        try {
            Thread.sleep(2000);
            String response = networkTemplate.getForObject(ticketUrl+id, String.class);
            asyncResult = new AsyncResult<>(response);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed getting tickets");
        return asyncResult;
    }

    @Async("threadPoolTaskExecutor")
    public Future<String> getTicketsIngresso(String id) throws Exception{
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Started fetching tickets from Ingresso");
        try {
            Thread.sleep(2000);
            String response = networkTemplate.getForObject(ingressoTicketUrl+id, String.class);
            asyncResult = new AsyncResult<>(response);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed getting tickets Ingresso");
        return asyncResult;
    }

    @Async("threadPoolTaskExecutor")
    public Future<String> getSessionInfoIsango(String requestJson) throws Exception{
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Started fetching session info from Isango");
        try {
            Thread.sleep(2000);
            String response = networkTemplate.postForEntity(isangoSessionInfoUrl, requestJson, String.class).getBody();
            asyncResult = new AsyncResult<>(response);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed getting session info from Isango");
        return asyncResult;
    }
    
    @Async("threadPoolTaskExecutor")
    public Future<String> getTicketsIsango(String requestJson) throws Exception{
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Started fetching tickets from Isango");
        try {
            Thread.sleep(2000);
            String response = networkTemplate.postForEntity(isangoTicketUrl, requestJson, String.class).getBody();
            asyncResult = new AsyncResult<>(response);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed getting tickets Isango");
        return asyncResult;
    }
    
    @Async("threadPoolTaskExecutor")
    public Future<String> lockTicket(TempLockRequest tempLockRequest) throws Exception {
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Started locking tickets at " + new Date());
        try {
            Thread.sleep(2000);
            String response = networkTemplate.postForEntity(ticketLockUrl, tempLockRequest, String.class).getBody();
            asyncResult = new AsyncResult<>(response);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed locking tickets");
        return asyncResult;
    }

    @Async("threadPoolTaskExecutor")
    public Future<String> lockTicketIngresso(TempLockRequest tempLockRequest) throws Exception {
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Started locking tickets at Ingresso :" + new Date());
        try {
            Thread.sleep(2000);
            String response = networkTemplate.postForEntity(ingressoTicketLockUrl,tempLockRequest, String.class).getBody();
            asyncResult = new AsyncResult<>(response);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed locking tickets at Ingresso");
        return asyncResult;
    }
    
    @Async("threadPoolTaskExecutor")
    public Future<String> lockTicketIsango(TempLockDetailedRequest tempLockRequest) throws Exception {
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Started locking tickets at Isango :" + new Date());
        try {
            Thread.sleep(2000);
            String response = networkTemplate.postForEntity(isangoTicketLockUrl,tempLockRequest, String.class).getBody();
            asyncResult = new AsyncResult<>(response);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed locking tickets at Isango");
        return asyncResult;
    }

    @Async("threadPoolTaskExecutor")
    public Future<String> getEventSessionIngresso(String eventIdDate) {
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Started fetching event session info from Ingresso");
        try {
            Thread.sleep(2000);
            String response = networkTemplate.getForObject(ingressoEventSessionInfoUrl+"?eventIdDate={eventIdDate}", String.class, eventIdDate);//.getBody();
            asyncResult = new AsyncResult<>(response);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed getting event session info from Ingresso");
        return asyncResult;
    }

    @Async("threadPoolTaskExecutor")
    public Future<String> getEventSessionIsango(String eventIdDate) {
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Started fetching event session info from Isango");
        try {
            Thread.sleep(2000);
            String response = networkTemplate.getForObject(isangoEventSessionInfoUrl+"?eventIdDate={eventIdDate}", String.class, eventIdDate);//.getBody();
            asyncResult = new AsyncResult<>(response);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed getting event session info from Ingresso");
        return asyncResult;
    }

    //London Theatre Direct
    @Async("threadPoolTaskExecutor")
    public Future<String> getTicketsLondonTheatreDirect(String id, String ticketCount) throws Exception{
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Started fetching tickets from LondonTheatreDirect");
        try {
            Thread.sleep(2000);
            String response = networkTemplate.getForObject(londonTheatreDirectTicketUrl.replace("$sessionId",id).replace("$requiredTicketsCount", ticketCount), String.class);
            asyncResult = new AsyncResult<>(response);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed getting tickets LondonTheatreDirect");
        return asyncResult;
    }

    @Async("threadPoolTaskExecutor")
    public Future<String> lockTicketLondonTheatreDirect(TempLockRequest tempLockRequest) throws Exception {
        AsyncResult<String> asyncResult = new AsyncResult<>(SUCCESS);
        log.info("Started locking tickets at LondonTheatreDirect :" + new Date());
        try {
            Thread.sleep(2000);
            String response = networkTemplate.postForEntity(londonTheatreDirectTicketLockUrl,tempLockRequest, String.class).getBody();
            asyncResult = new AsyncResult<>(response);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            asyncResult = new AsyncResult<>(FAILURE);
        }
        log.info("Completed locking tickets at LondonTheatreDirect");
        return asyncResult;
    }

}
