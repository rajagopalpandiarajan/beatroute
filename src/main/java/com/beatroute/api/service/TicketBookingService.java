package com.beatroute.api.service;

import com.beatroute.api.model.User;
import com.beatroute.api.model.UserBagPack;
import com.beatroute.api.repository.UserBagPackRepository;
import com.beatroute.api.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class TicketBookingService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserBagPackRepository userBagPackRepository;

    public void bookTicket(Long userId){
            User user = userRepository.findOneById(userId);
            List<UserBagPack> userBagPackList = userBagPackRepository.findAllByUser(user);
        }
}
