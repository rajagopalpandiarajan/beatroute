package com.beatroute.api.service;

import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.User;
import com.beatroute.api.model.UserBagPack;
import com.beatroute.api.repository.SecureTicketRepository;
import com.beatroute.api.rest.template.OrderRequest;
import com.beatroute.api.rest.template.TempLockDetailedRequest;
import com.beatroute.api.rest.template.UserBagPackTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

@Slf4j
@Component(value = "Isango")
public class IsangoSupplier implements Supplier{

    @Autowired
    private GenericAsyncService genericAsyncService;

    @Autowired
    private SecureTicketService secureTicketService;

    @Autowired
    private SecureTicketRepository secureTicketRepository;

    @Override
    public Future<String> getTickets(String id) {
        log.info("Started getting events from Isango");
        Future<String> response = null;
        try {
            response = genericAsyncService.getTicketsIsango(id);
        } catch (Exception e) {
            log.info("Error getting tickets ::" + e);
        }
        log.info("Completed getting events from Isango");
        return response;
    }

    @Override
    public Future<String> getTickets(String id, String ticketCount) {
        return null;
    }

    @Override
    @Async
    public Future<String> lockTicket(UserBagPack userBagPack, UserBagPackTemplate userBagPackTemplate) {
        log.info("Started to lock tickets from Isango");
        Future<String> future = new AsyncResult<>("");
        try {
            if (!userBagPackTemplate.isExpired() && (userBagPackTemplate.getLatestTicketBookingPrice() != null)) {
                SecureTicket secureTicket = secureTicketService.getSecureTicket(userBagPack, userBagPackTemplate);
                secureTicketRepository.save(secureTicket);

                // lock ticket
                try {
                    log.info("Calling lock ticket Isango");
                    future = genericAsyncService.lockTicketIsango(getTempLockRequest(secureTicket, userBagPackTemplate));
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("Completed locking tickets from Isango");
        return future;
    }

    @Override
    @Async
    public Future<String> placeOrder(OrderRequest orderRequest) {
        log.info("Started placing Order for Isango");
        Future<String> future = new AsyncResult<>("");
        try {
            future = genericAsyncService.createOrderIsango(orderRequest);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.info("Completed placing order for Isango");
        return future;
    }

    private TempLockDetailedRequest getTempLockRequest(SecureTicket secureTicket, UserBagPackTemplate userBagPackTemplate) {
    	User user=secureTicket.getUser();
        return new TempLockDetailedRequest(secureTicket.getNoOfTickets(), secureTicket.getTicketGroupId(), 
        		secureTicket.getTicketBookingPrice(), secureTicket.getLockRequestId(), user.getName(),
        		user.getEmail(), user.getMobile(), userBagPackTemplate.getSessionDate(), userBagPackTemplate.getSupplierInfo());
    }

    @Override
    @Async
	public Future<String> getSessionInfo(String requestJson) {
    	log.info("Started getting session info from Isango");
        Future<String> response = null;
        try {
            response = genericAsyncService.getSessionInfoIsango(requestJson);
        } catch (Exception e) {
            log.info("Error getting session info ::" + e);
        }
        log.info("Completed getting session info from Isango");
        return response;
	}

    @Override
    @Async
    public Future<String> getEventSessions(String requestJson) {
        log.info("Started getting event session for Isango");
        Future<String> future = new AsyncResult<>("");
        try {
            future = genericAsyncService.getEventSessionIsango(requestJson);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.info("Completed getting event session for Ingresso");
        return future;
    }


}
