package com.beatroute.api.service;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.beatroute.api.dto.EmailContent;
import com.beatroute.api.model.Transaction;
import com.beatroute.api.model.Venue;
import com.beatroute.api.model.enumeration.TransactionStatus;
import com.beatroute.api.repository.TransactionRepository;
import com.beatroute.api.repository.VenueRepository;
import com.beatroute.api.rest.template.AdditionalInfo;
import com.beatroute.api.rest.template.CancellationPolicyDto;
import com.beatroute.api.rest.template.PaxDto;
import com.beatroute.api.rest.template.UserBagPackTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
@Slf4j
public class AmazonS3Service {

    public static final String JPG = "jpg";

    public static final String IMAGE_JPEG = "image/jpeg";

    public static final String APPLICATION_TEXT = "text/plain";

    public static final String TXT = "txt";

    @Autowired
    TicketService ticketService;

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    private AmazonS3Client amazonS3Client;

    @Value("${aws.s3.bucket}")
    private String bucketName;

    @Value("${aws.watermark.location}")
    private String watermark;

    @Value("${aws.image.size.length}")
    private String length;

    @Value("${aws.image.size.breadth}")
    private String breadth;

    @Value("${app.ticketnetwork.download.ticket}")
    private String downloadUrl;

    @Autowired
    private EmailService emailService;

    @Autowired
    private  TransactionService transactionService;

    @Autowired
    private VenueRepository venueRepository;

    @Autowired
    private EnterpriseService enterpriseService;

    @Bean
    public AmazonS3Client amazonS3Client(@Value("${aws.credentials.access.key}") String accessKey, @Value("${aws.credentials.secret.key}") String secretKey) {
        return new AmazonS3Client(new BasicAWSCredentials(accessKey, secretKey));
    }

    public String upload(String imageUrl) throws Exception {
        log.info("Started uploading image at " + imageUrl);
        String url = null;
        try {
            String key = UUID.randomUUID().toString() + "." + JPG;
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentType(IMAGE_JPEG);
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, getThumbnailImage(imageUrl), objectMetadata);
            putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
            amazonS3Client.putObject(putObjectRequest);
            url = amazonS3Client.getResourceUrl(bucketName, key);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.info("Completed uploading image");
        return url;
    }

    private InputStream getThumbnailImage(String imageUrl) throws Exception {
        log.info("Creating image thumbnail");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Thumbnails
                .of(new URL(imageUrl))
                .size(Integer.parseInt(length), Integer.parseInt(breadth))
                .outputFormat(JPG)
                //.watermark(Positions.CENTER, ImageIO.read(new URL(watermark)), 0.5f)
                .outputQuality(0.8)
                .toOutputStream(byteArrayOutputStream);
        InputStream inputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        log.info("Completed converting to byte array input stream");
        return inputStream;
    }

    public void uploadTicket(){
        List<Transaction> transactionList = this.transactionRepository.findByMercuryOrderIdIsNotNullAndUrlIsNullAndTransactionStatusIsNotNull();

        for(Transaction transaction: transactionList){

            if(transaction.getEvent().getSupplier().equals("TicketNetwork")){
                try{
                    if(transaction.getTransactionStatus() == TransactionStatus.SUCCESS){
                        RestTemplate restTemplate = new RestTemplate();

                        JSONObject jsonObject = restTemplate.getForEntity(downloadUrl+transaction.getMercuryOrderId(), JSONObject.class).getBody();

                        String data = jsonObject.get("downloadStream").toString();
                        String key = UUID.randomUUID().toString() + "." + TXT;
                        String url = "";

                        ObjectMetadata objectMetadata = new ObjectMetadata();
                        objectMetadata.setContentType(APPLICATION_TEXT);
                        byte[] downloadStream = data.getBytes();

                        InputStream inputStream = new ByteArrayInputStream(downloadStream);
                        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, inputStream, objectMetadata);
                        putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
                        amazonS3Client.putObject(putObjectRequest);
                        url = amazonS3Client.getResourceUrl(bucketName, key);

                        transaction.setUrl(url);
                        this.transactionRepository.save(transaction);

                        try{
                            EmailContent emailContent = this.enterpriseService.getEnterpriseContent();
                            log.info("mail sending started");
                            String username = transaction.getUser().getName();
                            String email = transaction.getUser().getEmail();
                            String subject = "Download Ticket";
                            String eventName = transaction.getEvent().getName();
                            String eventVenue = "";
                            Date date = transaction.getSession().getBeginDateTime();
                            String ticketPrice = String.valueOf(transaction.getTicketBookingPrice());
                            String numberOfTickets = String.valueOf(transaction.getNumberOfTickets());
                            String transactionId = transaction.getPaymentReferenceId();
                            String totalAmount = String.valueOf(transaction.getAmount());
                            String tax = "0";
                            String supplierName = transaction.getEvent().getSupplier();
                            List<CancellationPolicyDto> cancellationPoliciesList = null;
                            List<PaxDto> paxAmountsList = null;
                            String importantNotes = null;
                            String cancellationPolicies = null;
                            String paxAmounts = null;
                            String additionalNotes = null;
                            List<AdditionalInfo> additionalInfoList = null;
                            String additionalInfo = null;
                            int numAdults = 0;
                            int numChildren = 0;

                            LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
                            String eventTime = localDateTime.toLocalDate().toString() + " " +  localDateTime.toLocalTime();

                            List<Venue> venues = this.venueRepository.findAllBySessionId(transaction.getSession().getId());
                            eventVenue = venues.get(0).getName();

                            ObjectMapper objectMapper = new ObjectMapper();
                            UserBagPackTemplate userBagPackTemplate = objectMapper.readValue(transaction.getUserBagPackJson(), UserBagPackTemplate.class);
                            importantNotes = userBagPackTemplate.getSupplierInfo().getImportantNotes();
                            cancellationPoliciesList = userBagPackTemplate.getSupplierInfo().getCancellationPolicies();
                            paxAmountsList = userBagPackTemplate.getSupplierInfo().getPaxAmounts();

                            if(paxAmountsList.size()>0){
                                paxAmounts = transactionService.getPaxAmounts(paxAmountsList);
                            }

                            if(cancellationPoliciesList.size()>0){
                                cancellationPolicies = transactionService.getCancellationPolicies(cancellationPoliciesList);
                            }

                            if(additionalInfoList.size()>0){
                                additionalInfo = transactionService.getAdditionalInfo(additionalInfoList);
                            }

                            this.emailService.sendDownloadTicket(
                                    username,
                                    email,
                                    subject,
                                    eventName,
                                    eventVenue,
                                    eventTime,
                                    ticketPrice,
                                    numberOfTickets,
                                    tax,
                                    transactionId,
                                    totalAmount,
                                    supplierName, importantNotes, cancellationPolicies, paxAmounts, additionalNotes, additionalInfo, numAdults, numChildren,emailContent);
                            log.info("mail sent");
                        }catch (Exception e){
                            log.error("mail sending failed "+e.getMessage());
                        }

                        log.info("ticket uploaded for mercury id : "+transaction.getMercuryOrderId());
                    }
                }catch (Exception e){
                    log.error("ticket upload failed for mercury id :"+transaction.getMercuryOrderId());
                }
            }else{
                log.info("ticket upload has been skipped for mercury id : "+transaction.getMercuryOrderId());
            }
        }
    }
}
