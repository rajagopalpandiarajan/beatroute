package com.beatroute.api.service;

import com.beatroute.api.model.UserBagPack;
import com.beatroute.api.rest.template.OrderRequest;
import com.beatroute.api.rest.template.UserBagPackTemplate;
import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.Future;

public interface Supplier {

    // view/get tickets
    Future<String> getTickets(String id);

    // lock tickets
    @Async
    Future<String> lockTicket(UserBagPack userBagPack, UserBagPackTemplate userBagPackTemplate);

    // place order
    @Async
    Future<String> placeOrder(OrderRequest orderRequest);
    
    // get session info
    Future<String> getSessionInfo(String requestJson);

    Future<String> getEventSessions(String requestJson);

    // view/get tickets
    Future<String> getTickets(String id, String ticketCount);
}
