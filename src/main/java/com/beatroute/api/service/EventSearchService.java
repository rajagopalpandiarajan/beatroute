package com.beatroute.api.service;

import com.beatroute.api.dto.EventElasticDto;
import com.beatroute.api.dto.EventHitsDto;
import com.beatroute.api.repository.DestinationRepository;
import com.beatroute.api.repository.EventRepository;
import com.beatroute.api.repository.VenueRepository;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import com.beatroute.api.rest.template.AdvanceSearchFilter;
import com.beatroute.api.rest.template.ElasticSearchQuery;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
@Slf4j
@Transactional
public class EventSearchService {

    @Value("${elastic.search.url}")
    private String elasticSearchServerUrl;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private VenueRepository venueRepository;

    @Autowired
    private DestinationRepository destinationRepository;

    @Autowired
    private ObjectMapper objectMapper;

    RestTemplate restTemplate = new RestTemplate();

    @Value("${elastic.search.count.url}")
    private String elasticSearchServerCountUrl;

    public Map getAutoCompleteEvents(String startsWith) {
        ContextData cd = ContextStorage.get();
        long enterpriseId = cd.getEnterpriseId();
        // List<Event> names = eventRepository.findByNameThatStartsWith("%"+startsWith+"%", new PageRequest(0,5));
        List<Object> names = eventRepository.findByNameThatStartsWithForEnterprise(enterpriseId,"%"+startsWith+"%", 5);

        Map<String, List> response = new HashMap();
        List<Map> eventData = new ArrayList<>();
        /*for(Object eventObject : names){
            Map temp = new HashMap<>();
            String event[]=(String[]) eventObject;
            String name = event[0];
            String eventReferenceId = event[1];

            temp.put("name", doHighLight(name.toLowerCase(), startsWith.toLowerCase()));
            temp.put("title", name);
            temp.put("id", eventReferenceId);
            temp.put("type", "title");
            eventData.add(temp);
        }*/

        org.json.JSONArray jsonArray = new org.json.JSONArray(names);
        org.json.JSONArray jsonArrayNew;
        for (int i = 0; i < jsonArray.length(); i++) {
            jsonArrayNew = jsonArray.getJSONArray(i);
            if(jsonArrayNew.length() == 2) {
                String name = jsonArrayNew.getString(0);
                String eventReferenceId = jsonArrayNew.getString(1);

                Map temp = new HashMap<>();
                temp.put("name", doHighLight(name.toLowerCase(), startsWith.toLowerCase()));
                temp.put("title", name);
                temp.put("id", eventReferenceId);
                temp.put("type", "title");
                eventData.add(temp);
            }
        }
        response.put("event", eventData);

        List<String> countryNames = venueRepository.findAllDistinctCountriesStartswithForEnterprise(enterpriseId, "%"+startsWith+"%", 5);
        List<String> cityNames = venueRepository.findAllDistinctCitiesStartswithForEnterprise(enterpriseId, "%"+startsWith+"%", 5);
       //Include alias for search
        List<Object[]> countryAliasObject=destinationRepository.findAllDistinctCountriesAliasStartsWith("%"+startsWith+"%",5);
        List<Object[]> cityAliasObject=destinationRepository.findAllDistinctCitiesAliasStartsWith("%"+startsWith+"%",5);
        List<String> aliasCountry= new ArrayList<String>();
        List<String> aliasCity= new ArrayList<String>();


        List<String> copyCountryAlias=countryNames;
        List<String> copyCityAlias=cityNames;

        countryAliasObject.stream().forEach((record) -> {

            for(int i=0;i<copyCountryAlias.size();i++){

                if(record[1].toString().toLowerCase().contains(copyCountryAlias.get(i).toLowerCase())){
                    countryNames.remove(copyCountryAlias.get(i));
                }

            }
            aliasCountry.add(record[0].toString());

        });
        cityAliasObject.stream().forEach((record) -> {

            for(int i=0;i<copyCityAlias.size();i++){

                if(record[1].toString().toLowerCase().contains(copyCityAlias.get(i).toLowerCase())){
                    cityNames.remove(copyCityAlias.get(i));
                }
            }
            aliasCity.add(record[0].toString());
        });



        countryNames.addAll(aliasCountry);
        cityNames.addAll(aliasCity);

        List<Map> destinationData = new ArrayList<>();



        //remove duplicates
        Set<String> countryNameIncludeAlias= new HashSet<String>(countryNames);
        Set<String> cityNameIncludeAlias= new HashSet<String>(cityNames);


        countryNameIncludeAlias.forEach(s -> {
            Map temp = new HashMap<>();
            temp.put("name", doHighLight(s.toLowerCase(), startsWith.toLowerCase()));
            temp.put("country", s);
            temp.put("type", "country");
            destinationData.add(temp);
        });

        cityNameIncludeAlias.forEach(s -> {
            Map temp = new HashMap<>();
            temp.put("name", doHighLight(s.toLowerCase(), startsWith.toLowerCase()));
            temp.put("city", s);
            temp.put("type", "city");
            destinationData.add(temp);
        });
        response.put("destination", destinationData);


        /*for(Event e : names){
            Map temp = new HashMap<>();
            temp.put("name", doHighLight(e.getName().toLowerCase(), startsWith.toLowerCase()));
            temp.put("title", e.getName());
            temp.put("id", e.getEventReferenceId());
            temp.put("type", "title");
            eventData.add(temp);
        }*/
        return response;
    }

    private String doHighLight(String fullText, String highLight){
        int start = fullText.indexOf(highLight);
        int end = fullText.indexOf(" ", (start + highLight.length()));

        StringBuilder sb = new StringBuilder();

        if(start == -1){
            return fullText;
        }

        if(fullText.indexOf(" ", (start + highLight.length())) == -1){
            sb.append(fullText.substring(0, start))
                    .append("<em>")
                    .append(fullText.substring(start, fullText.length()))
                    .append("</em>");
        }else{
            sb.append(fullText.substring(0, start))
                    .append("<em>")
                    .append(fullText.substring(start, end))
                    .append("</em>")
                    .append(fullText.substring(end));
        }

        return sb.toString();
    }

    /**
     *
     * @param filter : {"country": "", "city": ""}
     * @return
     */
    public Long getEventsCount(String filter) {
        org.json.JSONObject jsonObject = new org.json.JSONObject(filter);

        String country = "";
        String city = "";
        if(jsonObject.has("country")) {
            country = (String) jsonObject.get("country");
        }
        if(jsonObject.has("city")) {
            city = (String) jsonObject.get("city");
        }

        AdvanceSearchFilter advanceSearchFilter = new AdvanceSearchFilter();
        if(StringUtils.isNotEmpty(country) && StringUtils.isNotEmpty(city)){
            advanceSearchFilter.setEventsCount(country, "country");
            advanceSearchFilter.setEventsCount(city, "city");

        } else if(StringUtils.isNotEmpty(country)){
            advanceSearchFilter.setEventsCount(country, "country");
        }else if(StringUtils.isNotEmpty(city)){
            advanceSearchFilter.setEventsCount(city, "city");
        }else{
            return Long.valueOf(0);
        }

        ElasticSearchQuery query = new ElasticSearchQuery();
        query.addToMust(advanceSearchFilter.getEventsCountQuery());

        org.json.simple.JSONObject masterQuery = query.getAllMasterQuery();
        log.info("Master Query - "+masterQuery);

        org.json.simple.JSONObject response = restTemplate.postForEntity(this.elasticSearchServerCountUrl, masterQuery, org.json.simple.JSONObject.class).getBody();
        Long count = Long.valueOf((Integer)response.get("count"));

        return count;
    }

    /**
     *
     * @param filter : {"country": "", "primaryCategory": ""}
     * @return
     */
    public Long getExperienceCount(String filter) {
        org.json.JSONObject jsonObject = new org.json.JSONObject(filter);

        String country = "";
        String primaryCategory = "";
        String city="";

        if(jsonObject.has("country")) {
            country = (String) jsonObject.get("country");
        }
        if(jsonObject.has("primaryCategory")) {
            primaryCategory = (String) jsonObject.get("primaryCategory");
        }
        if(jsonObject.has("city")) {
            city = (String) jsonObject.get("city");
        }


        AdvanceSearchFilter advanceSearchFilter = new AdvanceSearchFilter();
        if((StringUtils.isNotEmpty(country) || StringUtils.isNotEmpty(city))  && StringUtils.isNotEmpty(primaryCategory)){
            if(country!=null){advanceSearchFilter.setExperiencesCount(country, "country");}
            if(city!=null){advanceSearchFilter.setExperiencesCount(city, "city");}

            advanceSearchFilter.setExperiencesCount(primaryCategory, "primaryCategory");
        } else{
            return Long.valueOf(0);
        }

        ElasticSearchQuery query = new ElasticSearchQuery();
        query.addToMust(advanceSearchFilter.setExperiencesCountQuery());

        org.json.simple.JSONObject masterQuery = query.getAllMasterQuery();
        log.info("Master Query - "+masterQuery);

        org.json.simple.JSONObject response = restTemplate.postForEntity(this.elasticSearchServerCountUrl, masterQuery, org.json.simple.JSONObject.class).getBody();
        Long count = Long.valueOf((Integer)response.get("count"));

        return count;
    }

    //TODO: to work around this fix applied. Need to do code cleanup.
    public Object dateFilters(String filter) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (JSONObject) parser.parse(filter);
        log.info("filter input - "+jsonObject);

        String page = jsonObject.get("from").toString();
        String pageSize = jsonObject.get("size").toString();
        AdvanceSearchFilter advanceSearchFilter = new AdvanceSearchFilter((Map) jsonObject.get("filters"));
        ElasticSearchQuery query = new ElasticSearchQuery();
        query.setFrom("0");
        query.setSize("5000");
        query.addToMust(advanceSearchFilter.getQuery());

        JSONObject masterQuery = query.getMasterQuery();
        log.info("Master Query - "+masterQuery);

        //this gives only date filtered events
        JSONObject response = restTemplate.postForEntity(this.elasticSearchServerUrl, masterQuery, JSONObject.class).getBody();


        EventElasticDto eventElasticDto = objectMapper.readValue(response.toString(), EventElasticDto.class);
        int initialSize = eventElasticDto.getHits().getTotal();

        Set<EventHitsDto> eventHitsDtosSet = new LinkedHashSet<>();
        eventHitsDtosSet.addAll(eventElasticDto.getHits().getHits());


        //this is to get all the events
        Map mapObject = (Map) jsonObject.get("filters");
        JSONObject dateJSONObject = new JSONObject();
        dateJSONObject.put("from", "");
        dateJSONObject.put("to", "");
        dateJSONObject.put("time_zone", "");

        mapObject.remove("date");
        mapObject.put("date", dateJSONObject);

        advanceSearchFilter = new AdvanceSearchFilter(mapObject);
        query = new ElasticSearchQuery();
        query.setFrom("0");
        query.setSize("10000");//jsonObject.get("size").toString()
        query.addToMust(advanceSearchFilter.getQuery());

        masterQuery = query.getMasterQuery();
        log.info("Master Query - "+masterQuery);

        //this gives only date filtered events
        response = restTemplate.postForEntity(this.elasticSearchServerUrl, masterQuery, JSONObject.class).getBody();

        EventElasticDto eventElasticDtoAddition = objectMapper.readValue(response.toString(), EventElasticDto.class);
        //int responseSize = eventElasticDtoAddition.getHits().getTotal();
        List<EventHitsDto> eventHitsDtosListAddition = eventElasticDtoAddition.getHits().getHits();

        //combining two responses of event
        eventHitsDtosSet.addAll(eventHitsDtosListAddition);

        List<EventHitsDto> eventHitsDtosList = new ArrayList<>();
        eventHitsDtosList.addAll(eventHitsDtosSet);

        List<EventHitsDto> responseEventHitsDto = new ArrayList<>();

        //based on size send data to the UI
        int fromIndex = Integer.parseInt(page) * Integer.parseInt(pageSize);
        int toIndex = fromIndex + Integer.parseInt(pageSize);
        responseEventHitsDto = eventHitsDtosList.subList(fromIndex, toIndex);

        //updating the response object
        List<EventHitsDto> eventHitsDtos = new ArrayList<>();
        eventHitsDtos.addAll(responseEventHitsDto);
        eventElasticDto.getHits().setHits(eventHitsDtos);

        return eventElasticDto;
    }

}
