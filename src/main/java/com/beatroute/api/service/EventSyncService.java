package com.beatroute.api.service;

import com.beatroute.api.model.Event;
import com.beatroute.api.repository.EventRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class EventSyncService {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventService eventService;

    public long syncEvents() {
        long eventSize = this.eventRepository.findCountOfEvents();
        log.info("total events " + eventSize);

        long updatedEvents = 1L;
        List<Event> events = eventRepository.findAll();

        for (Event event : events) {
            log.info(event.getName() + " updated");
            try {
                eventService.save(event);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
            updatedEvents++;
        }

        log.info("total synced events : " + updatedEvents);
        return updatedEvents;
    }
}
