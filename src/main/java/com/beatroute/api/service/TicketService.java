package com.beatroute.api.service;

import com.beatroute.api.dto.OrderResponse;
import com.beatroute.api.dto.TicketNetworkDetail;
import com.beatroute.api.model.Event;
import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.Transaction;
import com.beatroute.api.model.UserBagPack;
import com.beatroute.api.repository.EventRepository;
import com.beatroute.api.repository.SecureTicketRepository;
import com.beatroute.api.repository.TransactionRepository;
import com.beatroute.api.rest.template.OrderRequest;
import com.beatroute.api.rest.template.UserBagPackTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Service
@Slf4j
public class TicketService {

    public static final String LOCK_STATUS = "lockStatus";

    public static final String SUCCESS = "success";

    @Value("${app.ticketnetwork.getTicket}")
    private String ticketUrl;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventService eventService;

    @Autowired
    private UserBagPackService userBagPackService;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private TicketNetworkDetail ticketNetworkDetail;

    @Autowired
    private SecureTicketRepository secureTicketRepository;

    @Autowired
    private SecureTicketService secureTicketService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private GenericAsyncService genericAsyncService;

    @Autowired
    private SupplierFactory supplierFactory;

    /*public List<SecureTicket> lockTickets(List<UserBagPack> userBagPacks) throws Exception {
        log.info("Started locking tickets for the user bag pack");
        List<SecureTicket> lockedTickets = new ArrayList<>();
        Collection<Future<String>> futures = new ArrayList<Future<String>>();

        for (UserBagPack userBagPack : userBagPacks) {
            // from user bag pack get details required for locking the ticket
            // #TODO investigate the mapper readvalue
            UserBagPackTemplate userBagPackTemplate = mapper.readValue(userBagPack.getJsonData(), TypeFactory.defaultInstance().constructType(UserBagPackTemplate.class));

            if (!userBagPackTemplate.isExpired() && (userBagPackTemplate.getLatestTicketBookingPrice() != null)) {
                SecureTicket secureTicket = getSecureTicket(userBagPack, userBagPackTemplate);
                secureTicketRepository.save(secureTicket);

                // lock tickets
                try {
                    log.info("Calling lock tickets");
                    futures.add(genericAsyncService.lockTicket(getTempLockRequest(secureTicket)));
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }

            }
        }
        *//**********************************************************************************************//*


        //for each bagpack, update the ticket status from the lock response
        for (UserBagPack userBagPack : userBagPacks) {
//            UserBagPackTemplate userBagPackTemplate = mapper.readValue(userBagPack.getJsonData(), TypeFactory.defaultInstance().constructType(UserBagPackTemplate.class));

                boolean responseNotReceived = true;
                for (Future<String> future: futures) {
                    while (responseNotReceived) {
                        responseNotReceived = !future.isDone();
                    }
                    log.info("Started saving secure ticket for bagpack:: " + userBagPack.getId());
                    String ticketLockResponse = future.get();
                    log.info(ticketLockResponse);

                    JSONParser parser = new JSONParser();
                    JSONObject obj = (JSONObject) parser.parse(ticketLockResponse);

                    SecureTicket secureTicket = secureTicketRepository.findByLockRequestId(obj.get("lockRequestId").toString());
                    if (secureTicket != null && !lockedTickets.contains(secureTicket)) {
                        updateTicketStatus(secureTicket, ticketLockResponse);
                        lockedTickets.add(secureTicket);
                        secureTicketRepository.save(secureTicket);
                    }
                }

            log.info("Secure tickets saved for backpack :: " + userBagPack.getId());

        }


        *//**********************************************************************************************//*
        log.info("Completed locking tickets for the user bag pack");
        return lockedTickets;
    }

    public List<OrderResponse> placeOrder(List<SecureTicket> secureTickets) throws Exception {
        log.info("Placing order for total tickets "+secureTickets.size());
        List<OrderResponse> orderResponses = new ArrayList<>();
        Collection<Future<String>> futures = new ArrayList<Future<String>>();

        for(SecureTicket secureTicket : secureTickets) {
            OrderRequest orderRequest = secureTicket.getOrderRequest(ticketNetworkDetail);
            log.info("Sending order process request for :: "+secureTicket.getEvent().getName());
            futures.add(genericAsyncService.createOrder(orderRequest));
        }

        for(SecureTicket secureTicket : secureTickets) {
            boolean responseNotReceived = true;
            for (Future<String> future: futures) {
                while (responseNotReceived) {
                    responseNotReceived = !future.isDone();
                }
                String orderResponseJson = future.get();
                log.info("Order response :: " + orderResponseJson);
                OrderResponse orderResponse = mapper.readValue(orderResponseJson, TypeFactory.defaultInstance().constructType(OrderResponse.class));
                secureTicket.setOrderReferenceNumber(String.valueOf(orderResponse.getMercuryOrderID()));
                orderResponses.add(orderResponse);
                break;
            }
        }
        // save secure tickets
        secureTicketRepository.save(secureTickets);
        log.info("Saved secure tickets");

        return orderResponses;
    }*/

    private void updateTicketStatus(SecureTicket secureTicket, String ticketLockResponse) throws ParseException {
        log.info("Updating ticket status");
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(ticketLockResponse);
        if (obj.get(LOCK_STATUS)!=null && SUCCESS.equals(obj.get(LOCK_STATUS).toString())) {
            secureTicket.setLockStatus(true);
        }else {
            secureTicket.setLockStatus(false);
        }
        secureTicket.setLockResponse(ticketLockResponse);
        log.info("Completed updating ticket status");
    }

    /*private TempLockRequest getTempLockRequest(SecureTicket secureTicket) {
        return new TempLockRequest(secureTicket.getNoOfTickets(), secureTicket.getTicketGroupId(), secureTicket.getTicketBookingPrice(), secureTicket.getLockRequestId());
    }*/

    /*private SecureTicket getSecureTicket(UserBagPack userBagPack, UserBagPackTemplate userBagPackTemplate) {
        SecureTicket SecureTicket = new SecureTicket();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 10);
        SecureTicket.setExpiryDateTime(calendar.getTime());
        SecureTicket.setUser(userBagPack.getUser());
        SecureTicket.setEvent(eventRepository.findOneByEventReferenceId(userBagPackTemplate.getEventId()));
        SecureTicket.setTicketGroupId(userBagPackTemplate.getTicketGroupId());
//        SecureTicket.setTicketBookingPrice(new BigDecimal(userBagPackTemplate.getTicketBookingPrice()));
        SecureTicket.setTicketBookingPrice(new BigDecimal(userBagPackTemplate.getLatestTicketBookingPrice()));
        SecureTicket.setNoOfTickets(Integer.valueOf(userBagPackTemplate.getNumberOfTickets()));
        SecureTicket.setSessionReferenceId(userBagPackTemplate.getSessionReferenceId());
        SecureTicket.setUserBagPack(userBagPack);
        return SecureTicket;
    }*/

    public Collection<Future<String>> lockTicketsForSupplier(List<UserBagPack> bagPackList) throws IOException {
        Collection<Future<String>> futures = new ArrayList<Future<String>>();
        for (UserBagPack bagPack: bagPackList) {
            ObjectMapper mapper = new ObjectMapper();
            UserBagPackTemplate userBagPackTemplate = mapper.readValue(bagPack.getJsonData(), UserBagPackTemplate.class);

            //identify supplier
            Supplier supplier = supplierFactory.getSupplier(userBagPackTemplate.getSupplierName());

            //lock tickets based on supplier (Asynchronous call)
            futures.add(supplier.lockTicket(bagPack, userBagPackTemplate));
        }
        return futures;
    }

    public List<SecureTicket> getSecureTickets(List<UserBagPack> bagPackList, Collection<Future<String>> futures) throws Exception {
        List<SecureTicket> lockedTickets = new ArrayList<>();
        List<String> futureResponse = this.getSecureTicketsFutureResponses(futures);
        int i = 0;
    //    if(bagPackList.size() == futureResponse.size()){
            for (UserBagPack userBagPack: bagPackList) {
                log.info(userBagPack.getId().toString());
                log.info(userBagPack.getJsonData());
                boolean responseNotReceived = true;
                boolean secureTicketAdded = false;

            /*for (Future<String> future: futures) {
                while (responseNotReceived) {
                    responseNotReceived = !future.isDone();
                }
            */
                log.info("Started saving secure ticket for bagpack:: " + userBagPack.getId());
                String ticketLockResponse = futureResponse.get(i);
                log.info(ticketLockResponse);

                if(StringUtils.isNotEmpty(ticketLockResponse)){
                    JSONParser parser = new JSONParser();
                    JSONObject obj = (JSONObject) parser.parse(ticketLockResponse);

                    if (obj!=null && obj.get("lockRequestId")!=null ) {
                        SecureTicket secureTicket = secureTicketRepository.findByLockRequestId(obj.get("lockRequestId").toString());
                        if (secureTicket != null && !lockedTickets.contains(secureTicket)) {
                            updateTicketStatus(secureTicket, ticketLockResponse);
                            lockedTickets.add(secureTicket);
                            secureTicketRepository.save(secureTicket);
                            secureTicketAdded = true;
                            //break;
                        }
                    }
                }
                //}
                if(!secureTicketAdded){
                    SecureTicket secureTicket = new SecureTicket();
                    ObjectMapper objectMapper = new ObjectMapper();
                    UserBagPackTemplate userBagPackTemplate = objectMapper.readValue(userBagPack.getJsonData(), UserBagPackTemplate.class);
                    log.info(userBagPackTemplate.getEventId());
                    log.info(userBagPackTemplate.getEventName());
                    Event event = eventService.getByName(userBagPackTemplate.getEventName());
                    secureTicket.setLockStatus(false);
                    secureTicket.setSupplierAmount(userBagPackTemplate.getSupplierAmount());
                    secureTicket.setEvent(event);
                    secureTicket.setUserBagPack(userBagPack);
                    secureTicket.setTicketBookingPrice(new BigDecimal(userBagPackTemplate.getTicketBookingPrice()));
                    secureTicket.setNoOfTickets(Integer.parseInt(userBagPackTemplate.getNumberOfTickets()));
                    lockedTickets.add(secureTicket);
                }
                log.info("Secure tickets saved for backpack :: " + userBagPack.getId());
                i++;
            }
    //    }
        return lockedTickets;
    }

    public List<String> getSecureTicketsFutureResponses(Collection<Future<String>> futures) {
        List<String> responses = new ArrayList<>();
        for (Future<String> future: futures) {
            log.info("Future looping");
            boolean responseNotReceived = true;
            String response = null;
            String responseFailure = "{\"quantity\":null,\"wholeSalePrice\":null,\"lockCompletion\":null,\"lockExpirationSeconds\":null,\"lockStatus\":\"failure\",\"lockStatusDescription\":null,\"lockRequestId\":null,\"externalPONumber\":null}";
            try {
                // long startTime = System.currentTimeMillis(); //fetch starting time
                // (System.currentTimeMillis()-startTime) < 20000) - 20 sec
                while (responseNotReceived) {
                    Thread.sleep(2000);
                    responseNotReceived = !future.isDone();
                }
                response = future.get();
            } catch (Exception e) {
                log.error(String.valueOf(e.getStackTrace()));
                response = responseFailure;
            } finally {
                if(StringUtils.isNotEmpty(response)){
                    responses.add(response);
                }else{
                    response = responseFailure;
                    responses.add(response);
                }
            }
        }
        return responses;
    }

    public Collection<Future<String>> placeOrderForSupplier(String paymentReferenceId) throws Exception {
        List<SecureTicket> secureTickets = secureTicketRepository.findAllByPaymentReferenceId(paymentReferenceId);
        Collection<Future<String>> futures = new ArrayList<Future<String>>();
        for (SecureTicket secureTicket: secureTickets) {
            OrderRequest orderRequest = secureTicket.getOrderRequest(ticketNetworkDetail);

            String supplierName = secureTicket.getEvent().getSupplier();
            log.info("Sending order process request for :: "+secureTicket.getEvent().getName());

            if("Isango".equalsIgnoreCase(supplierName)){
                JSONParser parser = new JSONParser();
                JSONObject obj = (JSONObject) parser.parse(secureTicket.getLockResponse());
                orderRequest.setExternalPONumber((String) obj.get("externalPONumber"));
            }

            //place order based on supplier
            Supplier supplier = supplierFactory.getSupplier(supplierName);
            if("Ingresso".equalsIgnoreCase(supplierName)){
                JSONParser parser = new JSONParser();
                JSONObject obj = (JSONObject) parser.parse(secureTicket.getLockResponse());
                orderRequest.setWebConfigId((String) obj.get("cryptoBlock"));
            }
            futures.add(supplier.placeOrder(orderRequest));
        }
        return futures;
    }

    public void getOrderResponse(String paymentReferenceId, Collection<Future<String>> futures) throws ExecutionException, InterruptedException, IOException {
        List<SecureTicket> secureTickets = secureTicketRepository.findAllByPaymentReferenceId(paymentReferenceId);
        int i = 0;
        Object[] futureArr = futures.toArray();
        for (SecureTicket secureTicket : secureTickets) {
            Future<String> future = (Future<String>) futureArr[i];
            boolean responseNotReceived = true;
            while (responseNotReceived) {
                Thread.sleep(2000);
                responseNotReceived = !future.isDone();
            }
            String orderResponseJson = future.get();
            log.info("Order response :: " + orderResponseJson);
            OrderResponse orderResponse = mapper.readValue(orderResponseJson, TypeFactory.defaultInstance().constructType(OrderResponse.class));
            secureTicket.setOrderReferenceNumber(orderResponse.getMercuryOrderID());
            i++;
        }
        // save secure tickets
        secureTicketRepository.save(secureTickets);
        log.info("Saved secure tickets");
    }

    public Transaction get(long id) {
        Transaction transaction=transactionRepository.findOne(id);
        return transaction;
    }
}
