package com.beatroute.api.service;

import com.beatroute.api.model.*;
import com.beatroute.api.model.enumeration.DestinationType;
import com.beatroute.api.repository.*;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import com.beatroute.api.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class DestinationService {

    @Autowired
    private DestinationRepository destinationRepository;

    @Autowired
    private DestinationMappingRepository destinationMappingRepsotitory;

    @Autowired
    private EventSearchService eventSearchService;

    @Autowired
    private UserService userService;

    @Autowired
    private VenueRepository venueRepository;

    @Autowired
    private EnterpriseRepository enterpriseRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EnterpriseExperienceRepository enterpriseExperienceRepository;

    @Autowired
    private DisplayCategoryRepository displayCategoryRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    public Map listDestinations() throws Exception {
        log.info("Started listing the destinations");

        List<DestinationDisplay> destinationList = this.destinationRepository.findByDestinationType(DestinationType.COUNTRY);

        List destinationCountryList = new ArrayList<>();
        destinationList.forEach(destinationDisplay -> {
            Map country = new HashMap();
            country.put("id", destinationDisplay.getId());
            country.put("country",destinationDisplay.getCountry());
            country.put("countryAlias", destinationDisplay.getCountryAlias());
            country.put("image",destinationDisplay.getImage());
            country.put("video", destinationDisplay.getVideo());
            country.put("userInterestDestinations",destinationDisplay.getUserInterestDestinations());
            country.put("description", destinationDisplay.getDescription());
            country.put("eventCount", eventSearchService.getEventsCount("{\"country\":\""+destinationDisplay.getCountry()+"\"}"));
            destinationCountryList.add(country);
        });
        log.info("Completed listing the destinations");

        Map data = new HashMap();
        data.put("destinationDisplay", destinationCountryList);
        return ResponseJsonUtil.getSuccessResponseJson(data);
    }

    public Map getPrioritizedDestinations(String country) throws Exception {
        log.info("Started listing the prioritized destinations");

        DestinationDisplay destinationDisplay = this.destinationRepository.findOneByCountryName(country);
        ContextData cd = ContextStorage.get();
        long enterpriseId = cd.getEnterpriseId();
        Enterprise enterprise=this.enterpriseRepository.findOneById(enterpriseId);
        if(destinationDisplay != null) {
            List<DestinationMapping> destinationList = this.destinationMappingRepsotitory.getCityMappingOrderByPriorityAsc(destinationDisplay,enterprise);
            List destinationDisplayList = new ArrayList();
            destinationList.stream().forEach((record) -> {
                Map destinationMap = new HashMap();
                destinationMap.put("destinationId", record.getId());
                destinationMap.put("destination", record.getCity().getCity());
                destinationMap.put("image", record.getCity().getImage());
                destinationMap.put("priority", record.getPriority());
                destinationMap.put("eventCount", eventSearchService.getEventsCount("{\"city\":\"" + record.getCity().getCity() + "\"}"));
                destinationDisplayList.add(destinationMap);
            });
            log.info("Completed listing the destinations");

            Map data = new HashMap();
            data.put("destinationDisplay", destinationDisplayList);
            return ResponseJsonUtil.getSuccessResponseJson(data);
        }else{
            throw new Exception("Country Not Found");
        }
    }

    public List<String> getCountries() {
        ContextData cd = ContextStorage.get();
        Enterprise enterprise = enterpriseRepository.findOne(cd.getEnterpriseId());

        List<String> countryList = this.enterpriseExperienceRepository.findAllCountryByEnterpriseId(enterprise);
        if(countryList.size() > 0){
            return countryList;
        }else {
            List<Long> eventIdList = userService.getEventIdList(enterprise.getId());
            return this.venueRepository.findAllCountriesForEnterprise(eventIdList);
        }
    }

    public Map getCities(String country) {
        List<Map<String,String>> venues=new ArrayList<Map<String,String>>();
        List<String> venueList = this.venueRepository.findAllCities(country);
        venues=this.getVenuesMap(venueList,country);
        DestinationDisplay destinationDisplay=this.destinationRepository.findOneByCountryName(country);
        if(destinationDisplay!=null) {
            String aliasCountry[] = destinationDisplay.getCountryAlias().toString().split(",");
            for (int i = 0; i < aliasCountry.length; i++) {
                List<String> destinationDisplayVenue=this.venueRepository.findAllCities(aliasCountry[i]);
                venues.addAll(this.getVenuesMap(destinationDisplayVenue,aliasCountry[i]));
            }
        }
        List responseList = new ArrayList();
        venues.forEach(s -> {
            s.forEach((k,v)-> {
                Map mapObject = new HashMap();
                Long eventCount = eventSearchService.getEventsCount("{\"city\":\""+v+"\",\"country\":\""+k+"\"}");
                if(eventCount > 0) {
                    mapObject.put("city", v);
                    mapObject.put("eventCount", eventCount);

                    responseList.add(mapObject);
                }


            });

        });

        return ResponseJsonUtil.getSuccessResponseJson(responseList);
    }

    private List getVenuesMap(List<String> venueList,String country){
        List<Map<String,String>> venues=new ArrayList<Map<String,String>>();
        for(String venue:venueList){
            Map venueMap=new HashMap();
            venueMap.put(country,venue);
            venues.add(venueMap);
        }
        return venues;
    }

    public Map getExperiences(String country,String city) {
        /*ContextData cd = ContextStorage.get();
        Enterprise enterprise = enterpriseRepository.findOne(cd.getEnterpriseId());

        List<String> experienceList = this.enterpriseExperienceRepository.findAllNameByEnterpriseId(enterprise);
        if(experienceList.size() > 0){
            return experienceList;
        }else {
            List<Long> eventIdList = getEventIdList(enterprise.getId());
            return this.categoryRepository.findAllPrimaryCategoryForEnterprise(eventIdList);
        }
        */
        List<Object[]> experienceList = null;
        DestinationDisplay destinationDisplay = null;
        if(country!=null) {
            experienceList = this.eventRepository.findAllPrimaryCategoryByCountry(country);
            destinationDisplay = this.destinationRepository.findOneByCountryName(country);
        }else{
            experienceList = this.eventRepository.findAllPrimaryCategoryByCity(city);
            destinationDisplay = this.destinationRepository.findOneByCityName(city);
        }
        if(destinationDisplay!=null) {
            String alias[] =null;
            if(country!=null && destinationDisplay.getCountryAlias()!=null){
                alias=destinationDisplay.getCountryAlias().toString().split(",");
            }
            if(city!=null && destinationDisplay.getCityAlias()!=null){
                alias=destinationDisplay.getCity().toString().split(",");

            }
            if(alias!=null) {
                for (int i = 0; i < alias.length; i++) {
                    boolean b = (country != null) ? experienceList.addAll(this.eventRepository.findAllPrimaryCategoryByCountry(country)) : experienceList.addAll(this.eventRepository.findAllPrimaryCategoryByCity(city));
                }
            }
        }
        List responseList = new ArrayList();
        Map displayCategoryMap=new HashMap();
        List<DisplayCategory> displayCategoriesList=displayCategoryRepository.findAll();

        experienceList.forEach(s -> {
            Long eventCount =Long.valueOf(0);

            eventCount=(country!=null)?eventSearchService.getExperienceCount("{\"country\":\""+country+"\", \"primaryCategory\":\"" + s[0].toString() + "\"}"):eventSearchService.getExperienceCount("{\"city\":\""+city+"\", \"primaryCategory\":\"" + s[0].toString() + "\"}");

            if(eventCount > 0) {

                for(DisplayCategory dc:displayCategoriesList){
                    String[] categoriesIds=dc.getMappedCategories().split(",");
                    if(ArrayUtils.contains(categoriesIds,s[1].toString())){
                        Category category=categoryRepository.findById(Long.valueOf(s[1].toString()));
                        if(displayCategoryMap.containsKey(dc.getName())){
                            long count=Long.valueOf(displayCategoryMap.get(dc.getName()).toString());
                            count=count+eventCount;
                            displayCategoryMap.put(dc.getName(),count);
                        }else{
                            displayCategoryMap.put(dc.getName(),eventCount);
                        }
                    }
                }
            }

        });

        displayCategoryMap.forEach((k,v) -> {
            Map responseMap=new HashMap();
            ContextData cd = ContextStorage.get();
            long enterpriseId = cd.getEnterpriseId();
            Enterprise enterprise = enterpriseRepository.getOne(enterpriseId);
            DisplayCategory displayCategory=this.displayCategoryRepository.findByNameAndEnterprise(k.toString(), enterprise);
            List<Long> categoryIdsList = Stream.of(displayCategory.getMappedCategories().split(","))
                    .map(Long::parseLong)
                    .collect(Collectors.toList());
            HashSet primaryCategoryIds=new HashSet<Long>(categoryIdsList);
            List<String> primaryCategories=this.categoryRepository.getNameByIdIn(primaryCategoryIds);

            responseMap.put("displayCategory", k);
            responseMap.put("eventCount", v);
            responseMap.put("primaryCategory",StringUtils.join(primaryCategories, ','));

            responseList.add(responseMap);

        });

        return ResponseJsonUtil.getSuccessResponseJson(responseList);
    }

}
