package com.beatroute.api.service;

import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.UserBagPack;
import com.beatroute.api.repository.SecureTicketRepository;
import com.beatroute.api.rest.template.OrderRequest;
import com.beatroute.api.rest.template.TempLockRequest;
import com.beatroute.api.rest.template.UserBagPackTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

/**
 * Created by rakshith on 3/7/17.
 */
@Slf4j
@Component(value = "LondonTheatreDirect")
public class LondonTheatreDirectSupplier implements Supplier{

    @Autowired
    private GenericAsyncService genericAsyncService;

    @Autowired
    private SecureTicketService secureTicketService;

    @Autowired
    private SecureTicketRepository secureTicketRepository;

    @Override
    public Future<String> getTickets(String id) {
        return null;
    }

    @Override
    public Future<String> getTickets(String id, String ticketCount) {
        log.info("Started getting events from LondonTheatreDirect");
        Future<String> response = null;
        try {
            response = genericAsyncService.getTicketsLondonTheatreDirect(id, ticketCount);
        } catch (Exception e) {
            log.info("Error getting tickets ::" + e);
        }
        log.info("Completed getting events from LondonTheatreDirect");
        return response;
    }

    @Override
    @Async
    public Future<String> lockTicket(UserBagPack userBagPack, UserBagPackTemplate userBagPackTemplate) {
        log.info("Started to lock tickets from LondonTheatreDirect");
        Future<String> future = new AsyncResult<>("");
        try {
            if (!userBagPackTemplate.isExpired() && (userBagPackTemplate.getLatestTicketBookingPrice() != null)) {
                SecureTicket secureTicket = secureTicketService.getSecureTicket(userBagPack, userBagPackTemplate);
                secureTicketRepository.save(secureTicket);

                // lock ticket
                try {
                    log.info("Calling lock ticket LondonTheatreDirect");
                    future = genericAsyncService.lockTicketLondonTheatreDirect(getTempLockRequest(secureTicket));
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("Completed locking tickets from LondonTheatreDirect");
        return future;
    }

    private TempLockRequest getTempLockRequest(SecureTicket secureTicket) {
        return new TempLockRequest(secureTicket.getNoOfTickets(), secureTicket.getTicketGroupId(), secureTicket.getTicketBookingPrice(), secureTicket.getLockRequestId());
    }

    @Override
    @Async
    public Future<String> placeOrder(OrderRequest orderRequest) {
        /*log.info("Started placing Order for LondonTheatreDirect");
        Future<String> future = new AsyncResult<>("");
        try {
           // future = genericAsyncService.createOrderLondonTheatreDirect(orderRequest);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.info("Completed placing order for LondonTheatreDirect");
        return future;*/
        // Auto-generated method stub
        return null;
    }

    @Override
    @Async
    public Future<String> getSessionInfo(String requestJson) {
        // Auto-generated method stub
        return null;
    }

    @Override
    @Async
    public Future<String> getEventSessions(String requestJson) {
        // Auto-generated method stub
        return null;
    }

}
