package com.beatroute.api.service;

import com.beatroute.api.elasticsearch.service.ElasticSearchService;
import com.beatroute.api.model.*;
import com.beatroute.api.repository.*;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import com.beatroute.api.rest.template.AdvanceSearchFilter;
import com.beatroute.api.rest.template.ElasticSearchQuery;
import com.beatroute.api.rest.template.SessionTemplate;
import com.beatroute.api.rest.template.TicketNetworkEvent;
import com.beatroute.api.service.exception.EntityNotFoundException;
import com.beatroute.api.utils.ResponseJsonUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Future;

@Service
@Slf4j
@Transactional
@SuppressWarnings({ "rawtypes", "unchecked" })
public class EventService {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventProcessingService eventProcessingService;

    @Autowired
    private ElasticSearchService elasticSearchService;

    @Autowired
    private SupplierFactory supplierFactory;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private EnterpriseRepository enterpriseRepository;

    @Autowired
    private EnterpriseExperienceRepository enterpriseExperienceRepository;

    @Autowired
    private EnterpriseEventRepository enterpriseEventRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private RecommendationRepository recommendationRepository;

    @Value("${elastic.search.url}")
    private String elasticSearchServerUrl;

    RestTemplate restTemplate = new RestTemplate();

    @Value("${api.event.category.name}")
    private String eventCategoryNames;

    @Value("${api.reindex.events.enterprises.date.subtraction.value}")
    private String reindexSubtractionDate;

    @Value("${api.event.session.date.subtraction.value}")
    private String subtractionDate;

    @Value("${api.event.get.new.event.date.subtraction.value}")
    private String dateToGetNewEvents;

    @Value("${api.event.session.date.addition.value}")
    private String additionDate;

    @Value("${api.event.session.last.update.date.minute.value}")
    private String updateMinuteValue;

    @Value("${api.event.category.name}")
    private String categoryNames;

    @Value("${api.event.supplier.name}")
    private String supplierNames;

    @Value("${app.session.batch.size}")
    private int pageSize;

    @Value("${app.populate.session.batch.size}")
    private int populateSessionsPageSize;

    @Value("${app.db.beatroute.enterprise.id}")
    private long defaultEnterpriseId;

    @Value("${app.elastic.search.batch.size}")
    private int elasticBatchSize;

    public void save(Event event) throws Exception {
        log.info("Started saving the event: "+event.getEventReferenceId());
        eventRepository.save(event);
        elasticSearchService.save(event, event.getEventReferenceId());
        log.info("Completed saving the event: "+event.getEventReferenceId());
    }

    public void delete(Long eventId, String eventReferenceId) throws Exception {
        log.info("Started deleting the event: "+eventReferenceId);
        eventRepository.delete(eventId);
        elasticSearchService.delete(eventReferenceId);
        log.info("Completed deleting the event: "+eventReferenceId);
    }

    public Object get(String id) throws Exception {
        //updating the view count in the event table
        boolean check = this.updateViewCount(id);
        if(!check) {
            log.info("Event Status is False or Found Null");
            return ResponseJsonUtil.getFailedResponseJson("invalidInput","Event NOT Found");
        }

        //get the updated event from elastic search
        String eventString = elasticSearchService.get(id);
        Event event = objectMapper.readValue(eventString, Event.class);
        event = event.getCopy(event);
        return event;
    }

    private boolean updateViewCount(String eventReferenceId) throws Exception {
        Event event = this.eventRepository.findOneByEventReferenceIdStatusFalseOrTrue(eventReferenceId);
        if(event != null) {
            Long viewCount = event.getViewCount();
            viewCount++;
            event.setViewCount(viewCount);
            this.update(event);
            return true;
        }
        return false;
    }

    public void update(Event event) throws Exception {
        log.info("Started updating the event: "+event.getEventReferenceId());
        event = eventRepository.save(event);
        elasticSearchService.update(event, event.getEventReferenceId());
        log.info("Completed updating the event: "+event.getEventReferenceId());
    }

    public boolean checkIfEventAbsent(String name) {
        Event event = this.eventRepository.findOneByName(name);
        if (event == null) {
            return true;
        }
        return false;
    }

    public Event findById(long id) throws EntityNotFoundException {
        Event event = this.eventRepository.findOne(id);
        if (event == null) {
            throw new EntityNotFoundException("event not found with id:" + id);
        }
        return event;
    }

    public Event getByName(String name) {
        return this.eventRepository.findOneByName(name);
    }

    public List getPrimaryCategory() {
        return this.eventRepository.findAllPrimaryCategory();
    }

    public void saveApiEvent(TicketNetworkEvent ticketNetworkEvent) {
        try {
        	boolean update=false;
            Event event = null;
            List<SessionTemplate> sessionTemplates = ticketNetworkEvent.getSessions();
            Set<Session> sessions = new HashSet<>();
            Set<Tag> tags = this.eventProcessingService.getTagsFromTemplate(ticketNetworkEvent.getTags());
            Set<Tenant> tenants = this.eventProcessingService.getTenantsFromTemplate(ticketNetworkEvent.getTenants());
            Category primaryCategory = this.eventProcessingService.getPrimaryCategory(ticketNetworkEvent.getPrimaryCategory());
            Category secondaryCategory = this.eventProcessingService.getSecondaryCategory(ticketNetworkEvent.getSecondaryCategory());
            Enterprise enterprise=this.enterpriseRepository.findOne(defaultEnterpriseId);

            event = this.eventRepository.findOneByName(ticketNetworkEvent.getName());
            if (event == null) {
            	event = new Event();
                event.setName(ticketNetworkEvent.getName());
                event.setPrimaryCategory(primaryCategory);
                event.setSecondaryCategory(secondaryCategory);
                event.setSupplierReferenceId(ticketNetworkEvent.getSupplierReferenceId());
                event.setVenueMap(ticketNetworkEvent.getVenueMap());
                event.setSupplier(ticketNetworkEvent.getSupplier());
                event.setDescription(ticketNetworkEvent.getDescription());
                event.setGenericDetailTitle(ticketNetworkEvent.getGenericDetail());
                event.setImageGallery(ticketNetworkEvent.getImageGallery());
                event.setVideoGallery(ticketNetworkEvent.getVideoGallery());
                event.setEventTermsAndConditions(ticketNetworkEvent.getEventTermsAndConditions());
                event.setEventReferenceId(ticketNetworkEvent.getEventReferenceId());
                event.setNotes(ticketNetworkEvent.getNotes());
                event.setTags(tags);
                if (ticketNetworkEvent.getHighPrice() != null) {
                    event.setHighPrice(new BigDecimal(ticketNetworkEvent.getHighPrice()));
                }
                if (ticketNetworkEvent.getLowPrice() != null) {
                    event.setLowPrice(new BigDecimal(ticketNetworkEvent.getLowPrice()));
                }
                if (ticketNetworkEvent.getAvgPrice() != null) {
                    event.setAvgPrice(new BigDecimal(ticketNetworkEvent.getAvgPrice()));
                }
                event.setTenants(tenants);
                //by default add Beatroute enterprise to enterpriseTenants
                Set<Enterprise> enterpriseSet = event.getEventsEnterpriseTenants();
                if(enterpriseSet==null) {
                    enterpriseSet = new HashSet<>();
                }
                enterpriseSet.add(enterprise);
                event.setEventsEnterpriseTenants(enterpriseSet);

                if(ticketNetworkEvent.getSupplier().equalsIgnoreCase("LondonTheatreDirect")){
                    //setting event start date
                    event.setCreatedDate(ticketNetworkEvent.getCreatedDate());
                }

            } else {
            	update=true;
            	//by default add Beatroute enterprise to enterpriseTenants
                Set<Enterprise> enterpriseSet = event.getEventsEnterpriseTenants();
                if(enterpriseSet==null) {
                    enterpriseSet = new HashSet<>();
                }
                enterpriseSet.add(enterprise);
                event.setEventsEnterpriseTenants(enterpriseSet);
                if(ticketNetworkEvent.getVenueMap() != null){
                    event.setVenueMap(ticketNetworkEvent.getVenueMap());
                }
                if (ticketNetworkEvent.getHighPrice() != null) {
                    event.setHighPrice(new BigDecimal(ticketNetworkEvent.getHighPrice()));
                }
                if (ticketNetworkEvent.getLowPrice() != null) {
                    event.setLowPrice(new BigDecimal(ticketNetworkEvent.getLowPrice()));
                }
                if (ticketNetworkEvent.getAvgPrice() != null) {
                    event.setAvgPrice(new BigDecimal(ticketNetworkEvent.getAvgPrice()));
                }
                if(ticketNetworkEvent.getImageGallery() != null){
                    event.setImageGallery(ticketNetworkEvent.getImageGallery());
                }
                if(ticketNetworkEvent.getVideoGallery() != null){
                    event.setVideoGallery(ticketNetworkEvent.getVideoGallery());
                }
                if(ticketNetworkEvent.getDescription() != null){
                    event.setDescription(ticketNetworkEvent.getDescription());
                }
            }

            Calendar cal1 = Calendar.getInstance();
            Date currentDate = cal1.getTime();

            for (SessionTemplate s : sessionTemplates) {
                Session session;

                if (s.getSessionReferenceId().length() > 0 && this.eventProcessingService.checkSessionIfAbsent(s.getSessionReferenceId(), event)) {
                    session = new Session();
                    session.setName(s.getName());
                    session.setBeginDateTime(s.getBeginDateTime());
                    session.setEndDateTime(s.getEndDateTime());
                    session.setDescription(s.getDescription());
                    session.setDuration(s.getDuration());
                    session.setUsp(s.getUsp());
                    session.setRating(s.getRating());
                    session.setEvent(event);
                    session.setSessionReferenceId(s.getSessionReferenceId());

                    if(event.getSupplier().equalsIgnoreCase("TicketNetwork")){
                        log.info("Setting session status - "+s.isAvailable()+" for the event - "+s.getSessionReferenceId());
                        session.setAvailable(s.isAvailable());
                    }

                    if (currentDate.after(s.getBeginDateTime())) {
                        //log.info("currentDate is after session begin date");
                        session.setAvailable(false);
                    }

                    Set<Venue> venues = s.getVenues();
                    venues.forEach(v -> {
                        v.setSession(session);
                    });

                    session.setVenues(venues);
                } else {
                	session = this.eventProcessingService.getSessionByRefId(s.getSessionReferenceId(), event.getEventReferenceId());
                    session.setEvent(event);

                    Set<Venue> venues = s.getVenues();
                    venues.forEach(v -> {
                        v.setSession(session);
                    });

                    session.setVenues(venues);
                }

                sessions.add(session);
            }
            event.setSessions(sessions);

            boolean imageExists = event.getImageGallery() != null;
            if (imageExists) {
                event.setStatus(true);
            }

            if(event.getSupplier().equalsIgnoreCase("TicketNetwork") && imageExists){
                log.info("Setting event status - "+ticketNetworkEvent.isStatus()+" for the event - "+ticketNetworkEvent.getEventReferenceId());
                event.setStatus(ticketNetworkEvent.isStatus());
            }
            log.info("Event status: "+event.isStatus()+" Event Image :"+ event.getImageGallery());
            if (update) {
            	update(event);
            } else {
                if(event.getSessions().size() > 0) {
                    save(event);
                }else{
                    log.info("No Sessions for the event - "+event.getEventReferenceId());
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private final String SUB = "SUB";
    private final String ADD = "ADD";

	public Map getSessions(String id, String dateString, boolean populateSessions) {
        Map<String, Object> response = new HashMap<>();
        try {
            Event event = this.eventRepository.findOneByEventReferenceId(id);
            if (event != null && StringUtils.isNotEmpty(dateString)) {
                String startDate = getDateFormatted(dateString, -Integer.parseInt(subtractionDate), SUB);
                log.info("START DATE - " + startDate);
                String endDate = getDateFormatted(dateString, Integer.parseInt(additionDate), ADD);
                log.info("END DATE - " + endDate);
                java.sql.Date dateStartDate = getSqlDateFormat(startDate);
                java.sql.Date dateEndDate = getSqlDateFormat(endDate);

                List<String> categoryList = Arrays.asList(eventCategoryNames.split(","));
                boolean contains = categoryList.contains(event.getPrimaryCategory().getName());

                if (contains) {
                    //categories which not required api call
                    log.info("Event Primary category - " + event.getPrimaryCategory().getName());
                    response = getUpdatedSessionsFromDB(id, dateStartDate, dateEndDate);
                    return response;
                }else {
                    //calling supplier api
                    Future<String> getTicketsResponse = getSupplierSessionResponse(event.getSupplier(), id, startDate, endDate);

                    if (getTicketsResponse != null && StringUtils.isNotEmpty(getTicketsResponse.get())) {
                        String futureResponse = getTicketsResponse.get();
                        log.info("data received - " + futureResponse);
                        // parse the response
                        JSONObject obj = new JSONObject(futureResponse);
                        if (obj != null && String.valueOf(obj.get("eventReferenceId")) != null) {
                            JSONArray sessionsJson = obj.getJSONArray("sessions");
                            if (sessionsJson.length() > 0) {
                                //update sessions and set available to true/false and also updatedDate
                                response = updateSessions(sessionsJson, event, id, dateStartDate, dateEndDate, populateSessions);

                                event = this.eventRepository.findOneByEventReferenceId(id);
                                log.info("event sessions Size : " + event.getSessions().size());
                                log.info("Started to update elastic search for Event Reference Id - " + event.getEventReferenceId());
                                elasticSearchService.update(event, event.getEventReferenceId());
                                log.info("Completed to update elastic search for Event Reference Id - " + event.getEventReferenceId());
                                return response;
                            } else {
                                log.error("Sessions found Null");
                                return createSessionResponse(id, new ArrayList<>());
                            }
                        } else {
                            log.error("Future response Object Null");
                            return createSessionResponse(id, new ArrayList<>());
                        }
                    } else {
                        log.error("Future response null");
                        return createSessionResponse(id, new ArrayList<>());
                    }
                }
            } else {
                log.error("Event NOT Found");
                return createSessionResponse(id, new ArrayList<>());
            }
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return createSessionResponse(id, new ArrayList<>());
        }
    }

    //get events from eventRepository which are not in the catrgory list in app.pro
    public void populateSessions() throws Exception{
        log.info("################### started to event sessions update @ : " + new Date() +" ############ ");
        List<String> supplierNamesList = Arrays.asList(supplierNames.split(","));
        List<String> categoryNamesArray = Arrays.asList(categoryNames.split(","));

        List<Category> categoryNamesList = this.categoryRepository.findAllByNameIn(categoryNamesArray);

        Page<Event> events = this.eventRepository.findAllBySupplierInAndPrimaryCategoryNotInAndStatusTrue(supplierNamesList, categoryNamesList, new PageRequest(0, populateSessionsPageSize));
        log.info("################### Events Size : " + events.getNumberOfElements() +" ############ ");

        Date d = Calendar.getInstance().getTime(); // Current time
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy"); // Set your date format
        String currentDate = sdf.format(d); // Get Date String according to date format
        String modifiedCurrentDate = getDateFormatted(currentDate, 15, ADD);
        String modifiedCurrentDateByTwoMonths = getDateFormatted(modifiedCurrentDate, 60, ADD);

        events.forEach(e -> {
            if(e.getSessions().size() == 0){
                try{
                    log.info("================ Fetching sessions for Date : " + modifiedCurrentDate +" ================ ");
                    log.info("================ e.getEventReferenceId() : " + e.getEventReferenceId() +" ============= ");
                    Map getSessionResponse = getSessions(e.getEventReferenceId(), modifiedCurrentDate, true);
                    List responseObjectSessions = (List) getSessionResponse.get("sessions");

                    if(responseObjectSessions.size() == 0){
                        log.info("================ Re-fetching sessions for Date : " + modifiedCurrentDateByTwoMonths  +" ================ ");
                        getSessions(e.getEventReferenceId(), modifiedCurrentDateByTwoMonths, true);
                    }

                    Thread.sleep(5000);
                } catch (Exception ex){
                    log.error(ex.getMessage(), ex);
                }
            }
        });
        log.info("################### completed event sessions update @ : " + new Date() +" ############ ");
    }

    private Map updateSessions(JSONArray sessionsJson, Event event, String id, java.sql.Date dateStartDate, java.sql.Date dateEndDate, boolean populateSessions) throws Exception {
        //TODO: 15 min check need to be implemented.
        List<Session> sessionToSave = new ArrayList<>();
        List<Session> sessionOldToUpdate = new ArrayList<>();
        //DB sessions list
        List<Session> batchSession = this.sessionRepository.findAllBeginDateTimeBetweenStartDateAndEndDate(id, dateStartDate, dateEndDate);
        log.info("Sessions from DB - "+batchSession.size());

        Map<Date, Session> batchSessionMap = new HashMap<>();
        if(batchSession.size() > 0) {
            //IF sessions are already available in DB
            batchSession.forEach(s -> {
                s.setAvailable(false);
                s.setUpdatedDate(new Date());
                batchSessionMap.put(s.getBeginDateTime(), s);
            });
        }

        for (int i = 0; i < sessionsJson.length(); i++) {
            //response session
            Session session = objectMapper.readValue(sessionsJson.get(i).toString(), Session.class);
            session.setEvent(event);

            if(batchSessionMap.containsKey(session.getBeginDateTime())){
                Session s = batchSessionMap.get(session.getBeginDateTime());
                session.setId(s.getId());
                session.setVenues(s.getVenues());

                batchSessionMap.remove(session.getBeginDateTime());         //removing the key from map object
            }else{
                Set<Venue> venues = session.getVenues();
                venues.forEach(v -> {
                    v.setSession(session);
                });
                session.setVenues(venues);
            }
            session.setAvailable(true);
            session.setUpdatedDate(new Date());
            sessionToSave.add(session);
        }

        //map object which doesn't come from supplier, but already in DB.
        if(batchSessionMap.size() > 0){
            log.info("Size of Map Object - "+batchSessionMap.size());
            sessionOldToUpdate.addAll(batchSessionMap.values());
        }

        log.info("session list size - " + sessionToSave.size());
        if(populateSessions){
            return savePopulateSessionsList(sessionToSave, id, sessionOldToUpdate, event);
        } else {
            return saveSessionList(sessionToSave, id, sessionOldToUpdate);
        }
    }

    private Map savePopulateSessionsList(List<Session> sessionList, String id, List<Session> sessionOldToUpdate, Event event) {
        if(sessionOldToUpdate.size() > 0){
            // TODO: saving old sessions need to be handled
            sessionOldToUpdate = sessionRepository.save(sessionOldToUpdate);
        }
        if (sessionList.size() > 0) {
            //save updated sessions
            //sessionList = sessionRepository.save(sessionList);
            Set<Session> sessionSet = event.getSessions();
            sessionSet.addAll(sessionList);
            event.setSessions(sessionSet);
            event = eventRepository.save(event);
            sessionList.clear();
            sessionList.addAll(event.getSessions());
            log.info("Successfully saved the newer sessions");
            eventRepository.flush();
            return createSessionResponse(id, sessionList);
        } else {
            log.error("Sessions found Null - While saving SessionList");
            return createSessionResponse(id, new ArrayList<>());
        }
    }

    private Map<String,Object> saveSessionList(List<Session> sessionList, String id, List<Session> sessionOldToUpdate) {
        if(sessionOldToUpdate.size() > 0){
            // TODO: saving old sessions need to be handled
            sessionOldToUpdate = sessionRepository.save(sessionOldToUpdate);
            sessionRepository.flush();
        }
        if (sessionList.size() > 0) {
            //save updated sessions
            sessionList = sessionRepository.save(sessionList);
            log.info("Successfully saved the newer sessions");
            sessionRepository.flush();
            return createSessionResponse(id, sessionList);
        } else {
            log.error("Sessions found Null - While saving SessionList");
            return createSessionResponse(id, new ArrayList<>());
        }
    }

    private Map<String, Object> getUpdatedSessionsFromDB(String id, java.sql.Date dateStartDate, java.sql.Date dateEndDate) {
        Map<String, Object> response = new HashMap<>();
        List<Session> sessionList = this.sessionRepository.findAllBeginDateTimeBetweenStartDateAndEndDateOnAvailable(id, dateStartDate, dateEndDate);
        if(sessionList.size() > 0) {
            log.error("Sessions found ");
            response.put("eventReferenceId", id);
            response.put("sessions", sessionList);
            return response;
        }else {
            log.error("Sessions found Null - While getting UpdatedSessionsFromDB");
            return createSessionResponse(id, new ArrayList<>());
        }
    }

    private Future<String> getSupplierSessionResponse(String supplierName, String id, String startDate, String endDate) throws Exception {
        Future<String> getTicketsResponse = null;
        //based on supplier call get sessions
        Supplier supplier = supplierFactory.getSupplier(supplierName);

        String requestJson = "{\"eventReferenceID\":\"" + id + "\", \"startDate\":\"" + startDate + "\", \"endDate\":\"" + endDate + "\"}";
        log.info("requestJson - " + requestJson);

        //get session info
        getTicketsResponse = supplier.getEventSessions(requestJson);
        boolean responseNotReceived = true;
        while (responseNotReceived) {
            Thread.sleep(2000);
            responseNotReceived = !getTicketsResponse.isDone();
        }
        return getTicketsResponse;
    }

    private Map<String, Object> createSessionResponse(String id, List list) {
        Map<String, Object> response = new HashMap<>();
        response.put("eventReferenceId", id);
        response.put("sessions", list);
        return response;
        /*
            {
                "eventReferenceId": "3Q92",
                "sessions": []
            }
        */
    }

    private java.sql.Date getSqlDateFormat(String dateReceived) throws Exception {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(df.parse(dateReceived));

        java.sql.Timestamp timestamp = new java.sql.Timestamp(cal1.getTime().getTime());
        java.sql.Date date1 = new java.sql.Date(timestamp.getTime());

        return date1;
    }

    private String getDateFormatted(String dateString, int daysCount, String check) throws Exception {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date selectedDate = df.parse(dateString);

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(selectedDate);
        cal1.add(Calendar.DATE, daysCount); // date before 3 days.

        Date date = cal1.getTime();

        if(check.equalsIgnoreCase(SUB)) {
            Date date1 = date;
            Date currentDate = new Date();

            log.info("date1 : " +date1);
            log.info("currentDate : " +currentDate);

            if (date1.after(currentDate) || DateUtils.isSameDay(date1, currentDate)) {
                log.info("Date1 is after or equal currentDate - " + date1);
                return df.format(date1);
            } else {
                //if (date1.before(currentDate) || date1.equals(currentDate))
                log.info("Date1 is before currentDate  - " + currentDate);
                return df.format(selectedDate);
            }
        }else{
            return df.format(date);
        }
    }

    /**
     * Re-indexes the event for enterprise.
     * Performs:
     * - delete from event enterprise tenant table all entries for enterpriseId (done)
     * - fetch all  enterpriseCountries, enterpriseExperience, enterpriseEventsRefId for enterpriseId (done)
     * - search on these criteria (done)
     * - save all event Id in event enterprise tenant table (done)
     * - After all the relevant events are updated for enterprise, run a elastic search update to re-index
     *
     * @param enterpriseId
     */
    public void indexEnterpriseEvents(Long enterpriseId) throws Exception {
        log.info("Started for indexing Events For Particular Enterprise "+enterpriseId+" at "+new Date());
        if(enterpriseId > 0) {
            Enterprise enterprise = this.enterpriseRepository.findOneById(enterpriseId);
            if(enterprise != null && enterprise.isActive()) {
                updateEnterpriseEvents(enterprise);
                log.info("Updated Event enterprise tenants for " + enterpriseId);

                //update the elasticsearch with enterpriseTenant to eventId
                updateIndexForEnterprise(enterprise);
            }
        }else{
            log.error("Invalid enterprise ID: "+enterpriseId);
        }
        log.info("Completed for indexing Events For Particular Enterprise "+enterpriseId+" at "+new Date());
    }

    public void reIndexEventsForAllEnterprises() throws Exception {
        log.info("Started for indexing Events For All Enterprises at "+new Date());
        List<Enterprise> enterpriseList = this.enterpriseRepository.findAllByIsActiveTrue();

        enterpriseList.forEach(enterprise -> {
        	if (enterprise != null && enterprise.getId()==defaultEnterpriseId) {
        		log.info("Skipping Indexing for Enterprise id - "+enterprise.getId());
        	} else {
	            log.info("Indexing for Enterprise id - "+enterprise.getId());
	            if(enterprise != null && enterprise.isActive()) {
	                updateEventsDBForAllEnterprises(enterprise);
	            }
        	}
        });

        updateIndexForAllEvents();
        log.info("Completed for indexing Events For All Enterprises at "+new Date());
    }

    private void updateEventsDBForAllEnterprises(Enterprise enterprise) {
        log.info("Started updating DB for Enterprise id - "+enterprise.getId());

        // fetch all enterpriseCountries, enterpriseExperience, enterpriseEventsRefId for enterpriseId
        List<String> enterpriseCountry = this.enterpriseExperienceRepository.findAllCountryByEnterpriseId(enterprise);
        enterpriseCountry = new ArrayList<>(new HashSet(enterpriseCountry));
        List<String> enterpriseExperiences = this.enterpriseExperienceRepository.findAllNameByEnterpriseId(enterprise);
        List<String> enterpriseEvents = this.enterpriseEventRepository.findAllNameByEnterpriseId(enterprise);

        // search for matching events based on these criteria
        //TODO: optional: search directly the eventId instead of whole index doc
        boolean loop = true;
        int elasticFrom = 0;
        while(loop) {
            ElasticSearchQuery query = getQuery(enterpriseCountry, enterpriseExperiences, enterpriseEvents, elasticFrom, true);
            org.json.simple.JSONObject masterQuery = query.getAllMasterQuery();
            log.info("masterQuery - " + masterQuery);
            org.json.simple.JSONObject response = restTemplate.postForEntity(this.elasticSearchServerUrl, masterQuery, org.json.simple.JSONObject.class).getBody();

            // saving all event Id in event enterprise tenant table
            loop = saveEventEnterpriseTenants(response, enterprise);

            //update the 'from' for the elastic search
            elasticFrom = elasticFrom + elasticBatchSize;
        }
        log.info("Completed updating DB for Enterprise id - "+enterprise.getId());
    }

    private void updateEnterpriseEvents(Enterprise enterprise) {
        log.info("Started updating DB for Enterprise id - "+enterprise.getId());

        // delete from event enterprise tenant table all entries for enterpriseId
        List<BigInteger> eventIdList = this.eventRepository.findAllEnterpriseTenantsByEnterprise(enterprise.getId());
        int deleteCount = this.eventRepository.deleteAllEnterpriseTenantsByEnterprise(enterprise.getId());
        eventRepository.flush();
        log.info("Deleted Count - "+deleteCount);

        //update the elasticsearch with enterpriseTenant to eventId
        updateIndexForEnterpriseAfterDelete(eventIdList);

        // fetch all enterpriseCountries, enterpriseExperience, enterpriseEventsRefId for enterpriseId
        List<String> enterpriseCountry = this.enterpriseExperienceRepository.findAllCountryByEnterpriseId(enterprise);
        enterpriseCountry = new ArrayList<>(new HashSet(enterpriseCountry));
        List<String> enterpriseExperiences = this.enterpriseExperienceRepository.findAllNameByEnterpriseId(enterprise);
        List<String> enterpriseEvents = this.enterpriseEventRepository.findAllNameByEnterpriseId(enterprise);

        // search for matching events based on these criteria
        //TODO: optional: search directly the eventId instead of whole index doc
        boolean loop = true;
        int elasticFrom = 0;
        while(loop) {
            ElasticSearchQuery query = getQuery(enterpriseCountry, enterpriseExperiences, enterpriseEvents, elasticFrom, false);
            org.json.simple.JSONObject masterQuery = query.getAllMasterQuery();
            log.info("masterQuery - " + masterQuery);
            org.json.simple.JSONObject response = restTemplate.postForEntity(this.elasticSearchServerUrl, masterQuery, org.json.simple.JSONObject.class).getBody();

            // saving all event Id in event enterprise tenant table
            loop = saveEventEnterpriseTenants(response, enterprise);

            //update the 'from' for the elastic search
            elasticFrom = elasticFrom + elasticBatchSize;
        }
        log.info("Completed updating DB for Enterprise id - "+enterprise.getId());
    }

    private void updateIndexForEnterpriseAfterDelete(List<BigInteger> eventIdList) {
        List<Long> eventIdList1 = new ArrayList<>();
        eventIdList.forEach(l -> {
            //log.info("id - "+l.longValue());
            eventIdList1.add(l.longValue());
        });
        List<Event> eventList = this.eventRepository.findAll(eventIdList1);
        boolean done = false;
        updateIndex(eventList);
    }

    private void updateIndexForEnterprise(Enterprise enterprise) throws Exception {
        log.info("Started updating index For Enterprise "+enterprise.getCompanyName()+" at "+new Date());
        Page<Event> eventPage = this.eventRepository.findAllByEventsEnterpriseTenants(enterprise, new PageRequest(0, pageSize, new Sort("id")));
        boolean done = false;
        List<Event> eventList = null;
        if(eventPage.getTotalElements() > 0) {
            while (!done) {
                eventList = eventPage.getContent();
                log.info("EventPage no - "+eventPage.getNumber()+" and eventPage Contents - "+eventPage.getContent().size()+" and totalContents - "+eventPage.getTotalElements());
                Thread.sleep(2000);
                updateIndex(eventList);

                if (eventPage.hasNext()) {
                    // get the next page
                    eventPage = this.eventRepository.findAllByEventsEnterpriseTenants(enterprise, eventPage.nextPageable());
                } else {
                    done = true;
                }
            }
        }
        log.info("Completed updating index For Enterprise "+enterprise.getCompanyName()+" at "+new Date());
    }

    private void updateIndexForAllEvents() throws Exception {
        log.info("Started updating index For All Events at "+new Date());
        Date date = getDateForReIndex();
        //log.info("date - "+date);
        Page<Event> eventPage = this.eventRepository.findAll(date, new PageRequest(0, pageSize, new Sort("id")));
        boolean done = false;
        List<Event> eventList = null;
        if(eventPage.getTotalElements() > 0) {
            while (!done) {
                eventList = eventPage.getContent();
                log.info("EventPage no - "+eventPage.getNumber()+" and eventPage Contents - "+eventPage.getContent().size()+" and totalContents - "+eventPage.getTotalElements());
                Thread.sleep(2000);
                updateIndex(eventList);

                if (eventPage.hasNext()) {
                    // get the next page
                    eventPage = this.eventRepository.findAll(date, eventPage.nextPageable());
                } else {
                    done = true;
                }
            }
        }
        log.info("Completed updating index For All Events at "+new Date());
    }

    private void updateIndex(List<Event> eventList) {
        eventList.forEach(e -> {
            try {
                log.info("index update for eventReferenceId - "+e.getEventReferenceId());
                this.elasticSearchService.update(e, e.getEventReferenceId());
            } catch (Exception e1) {
                log.error(e1.getMessage(), e1);
            }
        });
    }

    private boolean saveEventEnterpriseTenants(org.json.simple.JSONObject response, Enterprise enterprise) {
        Map responseMap = (Map) response.get("hits");
        List responseList = (List) responseMap.get("hits");
        if(responseList.size() > 0) {
            log.info("Response hits Size - "+responseList.size()+" and Total - "+responseMap.get("total"));
            Set<Event> eventSet = new HashSet<>();
            Map mapObject = null;
            Map eventMap = null;
            boolean saveRequired = false;
            for (Object resp : responseList) {
                mapObject = (Map) resp;
                eventMap = (Map) mapObject.get("_source");
                try {
                    String eventReferenceId = String.valueOf(eventMap.get("eventReferenceId"));
                    Event event = this.eventRepository.findOneByEventReferenceIdWithStatusTrueOrFalse(eventReferenceId);
                    if (event != null) {
                        Set<Enterprise> enterpriseSet = new HashSet<>();  // = event.getEventsEnterpriseTenants();
                        if (event.getEventsEnterpriseTenants() != null && event.getEventsEnterpriseTenants().size() > 0) {
                            enterpriseSet.addAll(event.getEventsEnterpriseTenants());
                        }
                        enterpriseSet.add(enterprise);
                        event.setEventsEnterpriseTenants(enterpriseSet);
                        eventSet.add(event);

                        if (responseList.size() > 50) {
                            if (eventSet.size() > 50) {
                                this.eventRepository.save(new ArrayList<>(eventSet));
                                eventSet.clear();
                            }
                        } else {
                            saveRequired = true;
                        }
                    }else{
                        log.error("Event NOT found for the Event Reference Id - "+eventReferenceId);
                    }
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    continue;
                }
            }
            if (saveRequired) {
                this.eventRepository.save(new ArrayList<>(eventSet));
            }
            return true;
        }
        return false;
    }

    /*
        INPUT: {"filters": { "countries": [], "experiences": [], "events": []}}
    */
    private ElasticSearchQuery getQuery(List<String> enterpriseCountry, List<String> enterpriseExperiences, List<String> enterpriseEvents, int elasticFrom, boolean allEventsIndex) {
        List<String> eventRefIdSet = new ArrayList<>();
        List<String> event_IdSet = new ArrayList<>();
        for(String eventRefId : enterpriseEvents){
            if(eventRefId.contains("-") || eventRefId.contains("~")){
                event_IdSet.add(eventRefId.toUpperCase());
            }else{
                eventRefIdSet.add(eventRefId);
            }
        }

        Map<String, Object> searchFilter = new HashMap();
        Map<String, Object> filter = new HashMap();
        filter.put("countries", enterpriseCountry);
        filter.put("experiences", enterpriseExperiences);
        filter.put("events", eventRefIdSet);
        filter.put("_id", event_IdSet);
        searchFilter.put("filters", filter);
        log.info("Input: "+String.valueOf(searchFilter));

        AdvanceSearchFilter advanceSearchFilter = new AdvanceSearchFilter();
        advanceSearchFilter.getEventsOnEnterpriseCriteria(filter);
        ElasticSearchQuery query = new ElasticSearchQuery();
        query.setFrom(String.valueOf(elasticFrom));
        query.setSize(String.valueOf(elasticBatchSize));   //("size").toString());
        query.addToShould(advanceSearchFilter.getIndexEventsQuery());

        //if reIndex for ALL the events within 24 hours
        if(allEventsIndex){
            Map<String, Object> filter1 = new HashMap();
            Date d = getDateForReIndex();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String fromDate = sdf.format(d);
            log.info("after date - "+fromDate);

            //adding created date param for filter
            filter1.put("createdDate", fromDate);

            AdvanceSearchFilter advanceSearchFilter1 = new AdvanceSearchFilter();
            advanceSearchFilter1.getEventsOnEnterpriseCriteria(filter1);
            query.addToMustNot(advanceSearchFilter1.getIndexAllEventsQuery());
        }

        return query;
    }

    private Date getDateForReIndex() {
        Calendar cal1 = Calendar.getInstance();
        log.info("before date - "+cal1.getTime());
        cal1.add(Calendar.DATE, -Integer.parseInt(reindexSubtractionDate)); // date before 1 day/ 24 hours.
        return cal1.getTime();
    }

    public Map getNewEvents() throws Exception {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String currentDateString = df.format(new Date());
        String currentDate = getDateFormatted(currentDateString, 1, ADD);
        String startDate = getDateFormatted(currentDate, -Integer.parseInt(dateToGetNewEvents), ADD);
        log.info("START DATE - " + startDate);
        String endDate = currentDate;
        log.info("END DATE - " + endDate);
        java.sql.Date dateStartDate = getSqlDateFormat(startDate);
        java.sql.Date dateEndDate = getSqlDateFormat(endDate);

        /*List<Event> eventList = this.eventRepository.findAllInBetweenCreatedDate(dateStartDate, dateEndDate);
        List<Event> eventResponseList = new ArrayList<>();
        eventList.forEach(event -> {
            eventResponseList.add(event.getCopy(event));
        });*/

        Map<String, Object> filter1 = new HashMap();
        //adding created date param for filter
        filter1.put("createdFromDate", dateStartDate);
        filter1.put("createdToDate", dateEndDate);
        filter1.put("time_zone", "+05:30");

        AdvanceSearchFilter advanceSearchFilter = new AdvanceSearchFilter();
        advanceSearchFilter.getEventsOnCreatedDateCriteria(filter1);
        ElasticSearchQuery query = new ElasticSearchQuery();
        query.setFrom("0");
        query.setSize("20");
        query.addToMust(advanceSearchFilter.getCreatedDateRangeQuery());

        org.json.simple.JSONObject masterQuery = query.getAllMasterQuery();
        log.info("Master Query - "+masterQuery);

        org.json.simple.JSONObject response = restTemplate.postForEntity(this.elasticSearchServerUrl, masterQuery, org.json.simple.JSONObject.class).getBody();

        Map responseMap = (Map) response.get("hits");
        List responseList = (List) responseMap.get("hits");
        Set eventSet = new HashSet<>();
        if(responseList.size() > 0) {
            log.info("Response Total - "+responseMap.get("total"));
            Map mapObject = null;
            Map eventMap = null;
            for (Object resp : responseList) {
                mapObject = (Map) resp;
                eventMap = (Map) mapObject.get("_source");
                eventSet.add(eventMap);
            }
        }
        return ResponseJsonUtil.getSuccessResponseJson(eventSet);
    }

    public Map getRecommendationPageEvents(String recommendationName){

        ContextData cd = ContextStorage.get();
        Enterprise enterprise = enterpriseRepository.findOneById(cd.getEnterpriseId());
        Set<String> eventReferenceIds = new HashSet<>();
        List<RecommendationEvents> recommendationEvents= this.recommendationRepository.findAllByRecommendationNameAndEnterpriseOrderByPriorityAsc(recommendationName,enterprise);
        List<Event> events = new ArrayList<Event>();
        if(recommendationEvents != null && recommendationEvents.size() > 0){
            for (RecommendationEvents recommendation : recommendationEvents){
                eventReferenceIds.add(recommendation.getEventReferenceId());
            }
            events = eventRepository.findAllByEventReferenceIdIn(eventReferenceIds);
        }
        return ResponseJsonUtil.getSuccessResponseJson(events);
    }
}
