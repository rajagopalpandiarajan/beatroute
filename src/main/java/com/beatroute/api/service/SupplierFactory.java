package com.beatroute.api.service;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SupplierFactory implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    public Supplier getSupplier(String supplierName) {
        Supplier supplier = null;

        if (supplierName.equals("Beatroute")) {
            supplier = getBeatrouteSupplier();
        }else if (supplierName.equals("TicketNetwork")) {
            supplier = getTicketNetworkSupplier();
        }else if("Ingresso".equals(supplierName)){
            supplier = getIngressoSupplier();
        }else if("Isango".equals(supplierName)){
            supplier = getIsangoSupplier();
        }

        return supplier;
    }

    public Supplier getBeatrouteSupplier() {
        return (Supplier) applicationContext.getBean("beatroute");
    }

    public Supplier getTicketNetworkSupplier() {
        return (Supplier) applicationContext.getBean("ticketnetwork");
    }

    public Supplier getIngressoSupplier(){ return (Supplier) applicationContext.getBean("Ingresso");}

    public Supplier getIsangoSupplier(){ return (Supplier) applicationContext.getBean("Isango");}

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
