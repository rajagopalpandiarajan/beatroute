package com.beatroute.api.service;

import com.beatroute.api.model.*;
import com.beatroute.api.repository.*;
import com.beatroute.api.service.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class RecommendationService {

    @Autowired
    private UserEventTrackRepository userEventTrackRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private UserDestinationTrackRepository userDestinationTrackRepository;

    @Autowired
    private UserTagTrackRepository userTagTrackRepository;

    @Autowired
    private GlobalDestinationTrackRepository globalDestinationTrackRepository;
    /**
     * Returns the recommended events, destinations and tags for specified user.
     *
     * @param userId
     * @return JSON format:
     * {
        eventNames: ["","",""],
        destinations: ["", "", ""],
        tags: ["", "", ""]
        }
     */
    public Map getUserRecommendations(String userId) {
        // get 5 most searched events, 5 latest searched events
        //List<String> eventReferenceIdOnVisitCount = this.userEventTrackRepository.findByUserId(userId, new PageRequest(0,5, new Sort(Sort.Direction.DESC,"visitCount")));
        List<String> eventReferenceIdOnVisitCount = this.userEventTrackRepository.findByUserIdForEventReferenceId(userId, new PageRequest(0,5, new Sort(Sort.Direction.DESC,"visitCount")));
        log.info(" 5 Most Searched Events size - "+eventReferenceIdOnVisitCount.size());

        List<String> eventReferenceIdForLatest = this.userEventTrackRepository.findByUserIdForEventReferenceId(userId, new PageRequest(0,5, new Sort(Sort.Direction.DESC,"lastVisitedDate")));
        log.info(" 5 Latest Searched Events size - "+eventReferenceIdForLatest.size());

        Set<String> eventReferenceId = new HashSet();
        eventReferenceId.addAll(eventReferenceIdOnVisitCount);
        eventReferenceId.addAll(eventReferenceIdForLatest);
        log.info(" User Searched Events size - "+eventReferenceId.size());

        List<Event> recommendedEvent = new ArrayList<>();
        if(eventReferenceId.size() > 0){
            recommendedEvent.addAll(this.eventRepository.findAllByEventReferenceIdIn(eventReferenceId));
        }
        List<Event> recommendedEventList = new ArrayList<>();
        recommendedEvent.forEach(event -> {
            recommendedEventList.add(event.getCopy(event));
        });

        //get 5 most searched destinations (order descending count + lastSearched date)
        List<String> destinations = this.userDestinationTrackRepository.findByUserId(userId, new PageRequest(0,5, new Sort(Sort.Direction.DESC,"searchCount", "lastSearchedDate")));
        /*List<String> destinations = this.userDestinationTrackRepository.findTop5ByUserId(userId);*/
        log.info(" 5 Most Searched Destinations size - "+destinations.size());


        //get 5 most searched tags
        List<String> tags = this.userTagTrackRepository.findByUserId(userId, new PageRequest(0,5, new Sort(Sort.Direction.DESC,"searchCount")));
        /*List<String> tags = this.userTagTrackRepository.findTop5ByUserId(userId);*/
        log.info(" 5 Most Searched Tags size - "+tags.size());


        //get all searched events and get their common tags. whichever tags are most common among events, pick 5
        List<String> userInterestTags = getUserInterestTags(userId);


        Map<String, Object> response = new HashMap<>();
        response.put("eventNames", recommendedEventList);
        response.put("destinations", destinations);
        response.put("tags", tags);
        response.put("userInterestTags", userInterestTags);

        return response;
    }

    /**
     * Interests are tracked in 2 ways:
     1. By which tags the user is searching for - already done in tags
     2. Commonality of tags in the events that the user has search.
     For example, if user has visited 10 experiences, out of which Rock common in 6 of the 10 then,
     we'll assume that one of his interests are Rock. followed by other common tags. say Blues, Football, etc .

     * @param userId
     * @return
     */
    private List<String> getUserInterestTags(String userId) {
        //get all searched events and get their common tags. whichever tags are most common among events
        List<String> tags = this.userEventTrackRepository.getCommonTagsOfEvents(userId);
        log.info(" Searched Events And Get Their Common Tags size - "+tags.size());

        return tags;
    }

    /**
     * Saves all the usage info to DB.
     *
     *
     * @param contentJson - JSON map with format:
     *  {
            userId: "", //registerd userId/cookie UUID
            currentLocation: "", "india"
            events: [
                {
                    name: "Wicked",
                    referenceId: "",
                    visits: 3,
                },
                {
                    name: "Riverdance",
                    referenceId: "",
                    visits: 4,
                }
            ],
            destinations: [
                {
                    name: "Spain",
                    count: 5
                },
                {
                    name: "India",
                    count: 2
                }
            ],
            tags: [
                {
                    name: "Rock",
                    count: 5
                },
                {
                    name: "Music",
                    count: 2
                }
            ]
        }
     *
     *
     * @throws EntityNotFoundException - if user or cookie ID not found
     */
    public void setUsageInfo(String contentJson) throws EntityNotFoundException, Exception {
        //get events info from input and add to repo
        /**
         * Find the entry for given event. If exists, add input visit count to entry visit count.
         * If doesn't exist add entry with input visit count.
         */

        log.info("Stated to Get events info from input and add to repo");

        JSONObject obj = new JSONObject(contentJson);
        JSONArray events = obj.getJSONArray("events");

        saveUserEventTrack(obj, events);


        //get searched destinations info from input and add to repo
        /**
         * Find the entry for given destination. If exists, add input visit count to entry visit count.
         * If doesn't exist add entry with input visit count.
         */
        JSONArray destinations = obj.getJSONArray("destinations");

        saveUserDestinationTrack(obj, destinations);


        //get searched tags info from input and add to repo
        /**
         * Find the entry for given tag. If exists, add input visit count to entry visit count.
         * If doesn't exist add entry with input visit count.
         */
        JSONArray tags = obj.getJSONArray("tags");

        saveUserTagTrack(obj, tags);

        log.info("Completed to Get events info from input and add to repo");

        return;

    }

    public Map getGlobalEvents() throws EntityNotFoundException{
        Map<String, Object> response = new HashMap<>();

        List<String> allEventNameOnVisitCount = this.userEventTrackRepository.getAllEventNames(new PageRequest(0,5, new Sort(Sort.Direction.DESC,"visitCount")));

        log.info("################### All Event Names on Visit Count ###################");
        log.info(allEventNameOnVisitCount.toString());

        response.put("eventNames", allEventNameOnVisitCount);

        return response;
    }

    public Map getGlobalDestinations(String sourceCountry) throws EntityNotFoundException{
        Map<String, Object> response = new HashMap<>();

        List<String> allDestinations = this.globalDestinationTrackRepository.findBySourceCountry(sourceCountry, new PageRequest(0,5, new Sort(Sort.Direction.DESC,"visitCount")));

        log.info("################### All Destinations For Source Country ###################");
        log.info(allDestinations.toString());

        response.put("destinations", allDestinations);

        return response;
    }


    private void saveUserTagTrack(JSONObject obj, JSONArray tags) {
        if(tags != null && tags.length() > 0){
            for(int i=0; i< tags.length(); i++){
                JSONObject tag = tags.getJSONObject(i);
                UserTagTrack userTagTrack = this.userTagTrackRepository.findByUserIdAndSearchTag(obj.getString("userId"), tag.getString("name"));
                if(userTagTrack == null){
                    userTagTrack = new UserTagTrack();
                    userTagTrack.setUserId(obj.getString("userId"));
                    userTagTrack.setSearchTag(tag.getString("name"));
                    userTagTrack.setSearchCount(tag.getLong("count"));
                }else{
                    long searchCount = userTagTrack.getSearchCount();
                    searchCount = searchCount + tag.getLong("count");
                    userTagTrack.setSearchCount(searchCount);
                }

                userTagTrack.setLastSearchedDate(new Date());
                this.userTagTrackRepository.save(userTagTrack);
            }
        }
    }

    private void saveUserEventTrack(JSONObject obj, JSONArray events) {
        if(events != null && events.length() > 0){
            for(int i=0; i< events.length(); i++){
                JSONObject event = events.getJSONObject(i);
                UserEventTrack userEventTrack = this.userEventTrackRepository.findByUserIdAndEventReferenceId(obj.getString("userId"), event.getString("referenceId"));
                if(userEventTrack == null){
                    userEventTrack = new UserEventTrack();
                    userEventTrack.setUserId(obj.getString("userId"));
                    userEventTrack.setEventName(event.getString("name"));
                    userEventTrack.setEventReferenceId(event.getString("referenceId"));
                    userEventTrack.setVisitCount(event.getLong("visits"));
                } else {
                    long visitCount = userEventTrack.getVisitCount();
                    visitCount = visitCount + event.getLong("visits");
                    userEventTrack.setVisitCount(visitCount);
                }

                userEventTrack.setLastVisitedDate(new Date());
                this.userEventTrackRepository.save(userEventTrack);


            }
        }
    }

    private void saveUserDestinationTrack(JSONObject obj, JSONArray destinations) {

        String currentLocation = obj.getString("currentLocation");
        if(destinations != null && destinations.length() > 0){

            for(int i=0; i< destinations.length(); i++){
                JSONObject destination = destinations.getJSONObject(i);
                UserDestinationTrack userDestinationTrack = this.userDestinationTrackRepository.findByUserIdAndSearchDestination(obj.getString("userId"), destination.getString("name"));
                if(userDestinationTrack == null){
                    userDestinationTrack = new UserDestinationTrack();
                    userDestinationTrack.setUserId(obj.getString("userId"));
                    userDestinationTrack.setSearchDestination(destination.getString("name"));
                    userDestinationTrack.setSearchCount(destination.getLong("count"));
                }else{
                    long searchCount = userDestinationTrack.getSearchCount();
                    searchCount = searchCount + destination.getLong("count");
                    userDestinationTrack.setSearchCount(searchCount);
                }

                userDestinationTrack.setLastSearchedDate(new Date());
                this.userDestinationTrackRepository.save(userDestinationTrack);

                if(currentLocation != null && !currentLocation.isEmpty()){
                    GlobalDestinationTrack globalDestinationTrack = this.globalDestinationTrackRepository.findBySourceCountryAndDestinationCountry(currentLocation, destination.getString("name"));

                    if(globalDestinationTrack == null){
                        globalDestinationTrack = new GlobalDestinationTrack();
                        globalDestinationTrack.setSourceCountry(currentLocation);
                        globalDestinationTrack.setDestinationCountry(destination.getString("name"));
                        globalDestinationTrack.setVisitCount(destination.getLong("count"));

                    } else {
                        long visitCount = globalDestinationTrack.getVisitCount();
                        globalDestinationTrack.setVisitCount(visitCount+destination.getLong("count"));

                    }

                    this.globalDestinationTrackRepository.save(globalDestinationTrack);
                }

            }
        }


    }


}
