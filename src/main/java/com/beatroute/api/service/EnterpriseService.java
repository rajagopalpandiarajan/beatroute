package com.beatroute.api.service;

import com.beatroute.api.dto.EmailContent;
import com.beatroute.api.model.DomainTenant;
import com.beatroute.api.model.Enterprise;
import com.beatroute.api.model.EnterpriseBanner;
import com.beatroute.api.model.EnterpriseContact;
import com.beatroute.api.model.enumeration.ContactType;
import com.beatroute.api.repository.DomainTenantRepository;
import com.beatroute.api.repository.EnterpriseBannerRepository;
import com.beatroute.api.repository.EnterpriseContactRepository;
import com.beatroute.api.repository.EnterpriseRepository;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import com.beatroute.api.utils.HttpUtil;
import com.beatroute.api.utils.ResponseJsonUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@SuppressWarnings("rawtypes")
public class EnterpriseService {

    @Autowired
    private EnterpriseRepository enterpriseRepository;

    @Autowired
    private EnterpriseContactRepository enterpriseContactRepository;

    @Autowired
    private DomainTenantRepository domainTenantRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private EnterpriseBannerRepository enterpriseBannerRepository;

    @Value("${app.mail.images.url.path}")
    private String imagePathUrl;

    @Value("${api.enterprise.paths.uploadedFiles}")
    private String uploadPath;


    /**
     * Fetches the enterprise info for domain from context
     *
     * @return
     *  {
            "domainTenantId": 1,
            "name": "Infosys Technologies Pvt. Ltd.",
            "displayName" : "Infosys",
            "contact": {
	            "email" : "support@Infosys.com",
	            "contactNo" : "+65 84809775"
            }
        }
     */
	public Map getEnterpriseInfo() {
    	try {
    		ContextData cd=ContextStorage.get();
        	if ((cd == null) || (cd.getEnterpriseId() == null)) {
        		return ResponseJsonUtil.getFailedResponseJson("domainNotFound", "Domain context not initialized!");
        	}
    		log.info("EnterpriseService: got contextData: "+cd.toString());

	        Enterprise enterprise = enterpriseRepository.findOne(cd.getEnterpriseId());
            EnterpriseContact enterpriseContact = enterpriseContactRepository.findByEnterpriseAndContactType(enterprise, ContactType.HELPLINE);

            Map<String, Object> response = new HashMap<>();
            Map<String, Object> contact = new HashMap<>();
            response.put("domainTenantId", enterprise.getId());
            response.put("name", enterprise.getCompanyName());
            response.put("displayName", enterprise.getDisplayName());
            contact.put("email", enterpriseContact.getEmailId());
            contact.put("contactNo", enterpriseContact.getContactNo());
            response.put("contact", contact);

            log.info("response: " + response);
            return response;
        } catch (Exception e){
        	log.error("Failed to get enterprise!", e);
            return ResponseJsonUtil.getFailedResponseJson("domainNotFound", "Domain tenant not found!");
        }
    }

    public EmailContent getEnterpriseContent(){
    	EmailContent emailContent = null;
        try {
	        ContextData cd = ContextStorage.get();
	        Enterprise enterprise = enterpriseRepository.findOne(cd.getEnterpriseId());
	        emailContent = fillContentFromEnterprise(enterprise, cd.getOriginHost(), cd.getOriginSubDomain());
        } catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return emailContent;
    }

    private EmailContent fillContentFromEnterprise(Enterprise enterprise, String host, String subDomain){
        EmailContent emailContent = new EmailContent();
        EnterpriseContact enterpriseContact = enterpriseContactRepository.findByEnterpriseAndContactType(enterprise, ContactType.HELPLINE);
        emailContent.setDisplayName(enterprise.getDisplayName());
        emailContent.setCompanyName(enterprise.getCompanyName());
        emailContent.setSupportContactName(enterpriseContact.getContactName());
        emailContent.setSupportContactNo(enterpriseContact.getContactNo());
        emailContent.setSupportEmailId(enterpriseContact.getEmailId());

        emailContent.setHost(host);     //TODO: uncomment and comment other one. (for testing purpose locally)
        //emailContent.setHost("http://beta-infy.beatroute.com");
        emailContent.setSubDomain(subDomain);   //TODO: uncomment and comment other one. (for testing purpose locally)
        //emailContent.setSubDomain("beta-infy");

        //String imageUrl= emailContent.getHost() + imagePathUrl.replace("$subDomain",emailContent.getSubDomain());
        String imageUrl=host + imagePathUrl.replace("$subDomain",subDomain);
        log.info("image url - "+imageUrl);
        emailContent.setImageUrl(imageUrl);

        return emailContent;
    }

    public EmailContent getEnterpriseContentByHost(String host){
        EmailContent emailContent = null;
        try {
            String subDomain = HttpUtil.getSubdomain(host);
            log.info("subDomain in getEnterpriseContentByHost is "+subDomain+" for the host - "+host);
            DomainTenant domainTenant = domainTenantRepository.findBySubDomainName(subDomain);
            emailContent = fillContentFromEnterprise(domainTenant.getEnterprise(), host, subDomain);
        } catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return emailContent;
    }

    public Map listGenericPages(){
        Map data = new HashMap();
        ContextData cd= ContextStorage.get();
        long enterpriseId=cd.getEnterpriseId();
        String path = uploadPath+enterpriseId+ File.separatorChar+"pages"+File.separatorChar;
        File file = new File(path);
        List<String> fileList = this.getPagesFileList(file);
        data.put("files", fileList);
        return ResponseJsonUtil.getSuccessResponseJson(data);
    }

    public List<String> getPagesFileList(File file){
        List<String> list=new ArrayList<>();
        if (file.exists() && file.isDirectory()){
            File []files=file.listFiles();
            for(File f :files){
                list.add(f.getName());
            }
            return list;
        }else{
            return list;
        }
    }

    public Map enterpriseBannerList() {
        log.info("Started listing the enterprise banner");
        ContextData cd= ContextStorage.get();
        long enterpriseId=cd.getEnterpriseId();
        Enterprise enterprise = this.enterpriseRepository.findOne(enterpriseId);
        if(enterprise != null) {
            List<EnterpriseBanner> enterpriseBannerList = this.enterpriseBannerRepository.findAllByEnterprise(enterprise);
            enterpriseBannerList.forEach(banner -> {
                banner.setEnterprise(null);
            });
            log.info("Completed listing the enterprise banner");
            return ResponseJsonUtil.getSuccessResponseJson(enterpriseBannerList);
        }else {
            log.error("Enterprise Not Found");
            return ResponseJsonUtil.getFailedResponseJson("invalidInput", "Enterprise Not Found");
        }
    }

}
