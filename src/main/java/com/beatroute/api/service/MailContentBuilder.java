package com.beatroute.api.service;

import com.beatroute.api.dto.EmailContent;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class MailContentBuilder {

    @Value("${download.ticket.url}")
    private String url;

    @Autowired
    private TemplateEngine templateEngine;

    public String buildUserRegistrationTemplate(String userName, String activationLink, EmailContent emailContent) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("userName", userName);
        variables.put("activationLink", activationLink);
        variables.put("emailContent", emailContent);
        return build(variables, "userRegistration");
    }

    public String buildForgotPasswordTemplate(String userName, String activationLink, EmailContent emailContent) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("userName", userName);
        variables.put("activationLink", activationLink);
        variables.put("emailContent", emailContent);
        return build(variables, "forgotPassword");
    }

    /*public String buildResetPasswordTemplate(String userName) {
        Map<String, String> variables = new HashMap<String, String>();
        variables.put("userName", userName);
        return build(variables, "passwordReset");
    }*/

    private String build(Map<String, Object> map, String templateName) {
        Context context = new Context();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            context.setVariable(entry.getKey(), entry.getValue());
        }
        return templateEngine.process(templateName, context);
    }

    public String buildPaymentConfirmationTemplate(String userName, String eventName, String venueName, String sessionDate, String tAmount, String tNo, String tTax, String tId, String tToalAmount, String importantNotes, String paxAmounts, String cancellationPolicies, String supplierName, String additionalNotes, String additionalInfo, EmailContent emailContent) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("userName", userName);
        variables.put("eventName",eventName);
        variables.put("venueName",venueName);
        variables.put("sessionDate",sessionDate);
        variables.put("tAmount", tAmount);
        variables.put("tNo", tNo);
        variables.put("tTax",tTax);
        variables.put("tId",tId);
        variables.put("tToalAmount",tToalAmount);
        variables.put("importantNotes",importantNotes);
        variables.put("cancellationPolicies",cancellationPolicies);
        variables.put("paxAmounts",paxAmounts);
        variables.put("supplierName", supplierName);
        variables.put("additionalNotes", additionalNotes);
        variables.put("additionalInfo", additionalInfo);
        variables.put("emailContent", emailContent);

        for (Map.Entry entry : variables.entrySet()) {
            System.out.println(entry.getKey() + ", " + entry.getValue());
        }

        return build(variables, "paymentConfirmation");
    }

    public String buildOrderFailureTemplate(String userName, String paymentReferenceId, String eventName, String noOfTickets, String totalAmount, Date transactionDate, String supplierName, String importantNotes, EmailContent emailContent) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("userName", userName);
        variables.put("paymentReferenceId", paymentReferenceId);
        variables.put("eventName", eventName);
        variables.put("noOfTickets", noOfTickets);
        variables.put("totalAmount", totalAmount);
        variables.put("transactionDate", transactionDate.toString());
        variables.put("supplierName",supplierName);
        variables.put("importantNotes",importantNotes);
        variables.put("emailContent", emailContent);

        return build(variables, "orderFailure");
    }

    public String buildDownloadTicketTemplate(String userName, String eventName, String venueName, String sessionDate, String tAmount, String tNo, String tTax, String tId, String tToalAmount, String supplierName, String importantNotes, String paxAmounts, String cancellationPolicies, String additionalNotes, String additionalInfo, int numAdults, int numChildren, EmailContent emailContent) {
        //ContextData cd= ContextStorage.get();
        //String siteUrl = cd.getOriginHost().concat(url);

        String siteUrl = emailContent.getHost().concat(url);
        log.info("SITE URL - "+siteUrl);

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("userName", userName);
        variables.put("eventName",eventName);
        variables.put("venueName",venueName);
        variables.put("sessionDate",sessionDate);
        variables.put("tAmount", tAmount);
        variables.put("tNo", tNo);
        variables.put("tTax",tTax);
        variables.put("tId",tId);
        variables.put("url", siteUrl);
        variables.put("tToalAmount",tToalAmount);
        variables.put("importantNotes",importantNotes);
        variables.put("cancellationPolicies",cancellationPolicies);
        variables.put("paxAmounts",paxAmounts);
        variables.put("supplierName", supplierName);
        variables.put("additionalNotes", additionalNotes);
        variables.put("additionalInfo", additionalInfo);
        variables.put("numAdults", String.valueOf(numAdults));
        variables.put("numChildren", String.valueOf(numChildren));
        variables.put("emailContent", emailContent);

        log.info("coming here new log");

        return build(variables, "downloadTicket");
    }

    public String buildBookingCancelTemplet(String userEmail, String subject, String eventName, String sessionBeginDate, String sessionEndDate, String mercuryOrderId, String travellerName, String amount, String currency, String numberOfTickets, String ticketBookingPrice, String paymentReferanceId, String url, String transactionStatus, String paymentStatus, String createdDate, String updatedDate, String ticketReferenceNumber, String user, String event, String session, String refundAmount, EmailContent emailContent)
    {
        Map<String, Object> variables=new HashMap<String,Object>();
        variables.put("userEmail", userEmail);
        variables.put("subject",subject);
        variables.put("eventName", eventName);
        variables.put("sessionBeginDate",sessionBeginDate);
        variables.put("sessionEndDate",sessionEndDate);
        variables.put("mercuryOrderId", mercuryOrderId);
        variables.put("travellerName",travellerName);
        variables.put("amount", amount);
        variables.put("currency", currency);
        variables.put("numberOfTickets", numberOfTickets);
        variables.put("ticketBookingPrice",ticketBookingPrice);
        variables.put("paymentReferanceId",paymentReferanceId);
        variables.put("url",url);
        variables.put("transactionStatus",transactionStatus);
        variables.put("paymentStatus",paymentStatus);
        variables.put("createdDate",createdDate);
        variables.put("updatedDate",updatedDate);
        variables.put("ticketReferenceNumber",ticketReferenceNumber);
        variables.put("user",user);
        variables.put("event",event);
        variables.put("session",session);
        variables.put("refundAmount",refundAmount);
        variables.put("emailContent", emailContent);

        return build(variables, "bookingCancellation");

    }

}
