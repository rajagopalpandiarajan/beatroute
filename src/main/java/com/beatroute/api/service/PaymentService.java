package com.beatroute.api.service;

import com.beatroute.api.dto.PaymentDetail;
import com.beatroute.api.dto.PaymentResponse;
import com.beatroute.api.model.SecureTicket;
import com.beatroute.api.model.Transaction;
import com.beatroute.api.model.enumeration.TransactionStatus;
import com.beatroute.api.repository.SecureTicketRepository;
import com.beatroute.api.repository.TransactionRepository;
import com.beatroute.api.repository.UserBagPackRepository;
import com.beatroute.api.utils.MessageUtility;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
public class PaymentService {

    public static final String PIPE = "|";

    public static final String AMPERSAND="&";

    public static final String SHA_1 = "SHA-1";

    @Value("${asiapay.directPayment.url}")
    private String asiaPayDirectPaymetUrl;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private PaymentDetail paymentDetail;

    @Autowired
    private SecureTicketRepository secureTicketRepository;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private SecureTicketService secureTicketService;

    @Autowired
    private UserBagPackRepository userBagPackRepository;

    @Autowired
    private EmailHelperService emailHelperService;

    @Autowired
    private SmsHelperService smsHelperService;
    
    /*public void processSuccessfulPayment(String paymentReferenceId) throws Exception {

        // confirm order and update mercury id and download details in transaction
        List<SecureTicket> secureTickets = secureTicketRepository.findAllByPaymentReferenceId(paymentReferenceId);
        ticketService.placeOrder(secureTickets);
        log.info("Placed order successfully");

        // get all the transactions and update the status to success
        List<Transaction> transactions = transactionRepository.findAllByPaymentReferenceId(paymentReferenceId);
        Long userId = 0L;
        for(Transaction transaction : transactions) {
            String mercuryId = getMercuryOrderId(secureTickets, transaction);
            if(Integer.parseInt(mercuryId) > 0) {
                transaction.setTransactionStatus(TransactionStatus.SUCCESS);
                transaction.setMercuryOrderId(getMercuryOrderId(secureTickets, transaction));
            } else {
                transaction.setTransactionStatus(TransactionStatus.FAIL);
            }
            userId = transaction.getUser().getId();
        }
        transactionRepository.save(transactions);
        log.info("Saved transactions successfully");

        //get secure tickets with lock status true
        List<SecureTicket> secureTicketList = secureTicketService.fetchSecureTicketsWithLockTrue(userId);

        //delete all secure tickets for user
        secureTicketService.deleteSecureTickets(userId);

        //delete bagpack
        userBagPackRepository.delete(secureTicketList.stream().map(SecureTicket::getUserBagPack).collect(Collectors.toList()));
        log.info("Cleaned up user backpack successfully");
    }*/

    private String getMercuryOrderId(List<SecureTicket> secureTickets, Transaction transaction) {
        String mercuryId = "0";
        for(SecureTicket secureTicket : secureTickets) {
            if(secureTicket.getTicketReferenceNumber().equals(transaction.getTicketReferenceNumber())){
                mercuryId = secureTicket.getOrderReferenceNumber();
            }
        }
        return mercuryId;
    }

    public PaymentResponse processPayment(List<SecureTicket> secureTickets) throws Exception {
        log.info("Started processing the payments");
        String paymentReferenceId = String.valueOf(System.nanoTime());
        transactionService.saveUserTransactions(secureTickets, paymentReferenceId);
        log.info("Saved all transactions");
        secureTicketRepository.save(secureTickets);
        log.info("Saved all securetickets");
        String secureHash = computeSecureHash(secureTickets, paymentReferenceId);
        return getPaymentResponse(paymentReferenceId, secureHash, secureTickets);
    }

    private PaymentResponse getPaymentResponse(String paymentReferenceId, String secureHash, List<SecureTicket> secureTickets) {
        PaymentResponse paymentResponse = new PaymentResponse();
        paymentResponse.setUrl(paymentDetail.getPaymentUrl());
        paymentResponse.setMerchantId(paymentDetail.getMerchantID());
        paymentResponse.setOrderRef(paymentReferenceId);
        paymentResponse.setCurrCode(paymentDetail.getCurrCode());
        paymentResponse.setMpsMode(paymentDetail.getMpsMode());
        paymentResponse.setCancelUrl(MessageUtility.getTenantUrl(paymentDetail.getCancelUrl()));
        paymentResponse.setSuccessUrl(MessageUtility.getTenantUrl(paymentDetail.getSuccessUrl()));
        paymentResponse.setFailUrl(MessageUtility.getTenantUrl(paymentDetail.getFailureUrl()));
        paymentResponse.setPayType(paymentDetail.getPayType());
        paymentResponse.setLang(paymentDetail.getLanguage());
        paymentResponse.setPayMethod(paymentDetail.getPayMethod());
        paymentResponse.setSecureHash(secureHash);
        paymentResponse.setTemplateId(paymentDetail.getTemplateId());
        paymentResponse.setRedirect(paymentDetail.getRedirect());
        BigDecimal totalAmount = new BigDecimal("0");
        totalAmount.setScale(2, BigDecimal.ROUND_UP);
        for(SecureTicket secureTicket : secureTickets) {
            totalAmount = totalAmount.add(calculateAmount(secureTicket));
        }
        BigDecimal extraAmount = totalAmount.multiply(new BigDecimal(paymentDetail.getPaymentBumpUpPercent()));
        paymentResponse.setAmount(totalAmount.add(extraAmount).toString());
        return paymentResponse;
    }

    private String computeSecureHash(List<SecureTicket> secureTickets, String paymentReferenceId) throws Exception {
        log.info("Started creating secure hash");
        log.info("Order reference is:" + paymentReferenceId);
        // calculate total amount
        BigDecimal totalAmount = new BigDecimal("0");
        totalAmount.setScale(2, BigDecimal.ROUND_UP);
        for(SecureTicket secureTicket : secureTickets) {
//            if (secureTicket.getLockStatus()) {
                totalAmount = totalAmount.add(calculateAmount(secureTicket));
//            }
        }

        // "merchantID|merchantRef|currCode|amount|paymentType|secureHashSecret"
        log.info("Amount to be paid is:" + totalAmount);
        StringBuilder hashBuilder = new StringBuilder();
        hashBuilder
                .append(paymentDetail.getMerchantID())
                .append(PIPE)
                .append(paymentReferenceId)
                .append(PIPE)
                .append(paymentDetail.getCurrCode())
                .append(PIPE)
                .append(totalAmount.toString())
                .append(PIPE)
                .append(paymentDetail.getPayType())
                .append(PIPE)
                .append(paymentDetail.getSecureHashSecret());
        byte[] hashseq = hashBuilder.toString().getBytes();
        StringBuffer hexString = new StringBuffer();
        MessageDigest algorithm = MessageDigest.getInstance(SHA_1);
        algorithm.reset();
        algorithm.update(hashseq);
        byte[] messageDigest = algorithm.digest();
        for (int i = 0; i < messageDigest.length; i++) {
            String hex = Integer.toHexString(0xFF & messageDigest[i]);
            if (hex.length() == 1)
                hexString.append("0");
            hexString.append(hex);
        }
        log.info("Completed creating secure hash :: " + hexString);
        return hexString.toString();
    }

    private BigDecimal calculateAmount(SecureTicket secureTicket) {
        BigDecimal totalAmount = secureTicket.getTicketBookingPrice();
        totalAmount = totalAmount.multiply(new BigDecimal(secureTicket.getNoOfTickets()));
        log.info("Ticket Booking price is ::" + secureTicket.getTicketBookingPrice());
        log.info("No of tickets ::" + secureTicket.getNoOfTickets());
        log.info("Amount calculated is ::" + totalAmount);
        return totalAmount;
    }

    public void updatePaymentStatus(String paymentReferenceId, TransactionStatus transactionStatus) throws Exception{
        // get all the transactions and update the payment status
        List<Transaction> transactions = transactionRepository.findAllByPaymentReferenceId(paymentReferenceId);
        transactions.forEach(transaction -> transaction.setPaymentStatus(transactionStatus));
        transactionRepository.save(transactions);
    }

    public List<SecureTicket> updateTransactionStatus(String paymentReferenceId) throws Exception {
        // get all the transactions and update the status to success
        List<Transaction> transactions = transactionRepository.findAllByPaymentReferenceId(paymentReferenceId);
        List<SecureTicket> secureTickets = secureTicketRepository.findAllByPaymentReferenceId(paymentReferenceId);
        Long userId = 0L;
        for(Transaction transaction : transactions) {
            String mercuryId = getMercuryOrderId(secureTickets, transaction);
            if(!"".equals(mercuryId) && mercuryId != null) { //Integer.parseInt(mercuryId) > 0
                transaction.setTransactionStatus(TransactionStatus.SUCCESS);
                transaction.setMercuryOrderId(mercuryId);
            } else {
                transaction.setTransactionStatus(TransactionStatus.FAIL);
            }
            userId = transaction.getUser().getId();
        }
        transactionRepository.save(transactions);
        log.info("Saved transactions successfully");

        //get secure tickets with lock status true
        List<SecureTicket> secureTicketList = secureTicketService.fetchSecureTicketsWithLockTrue(userId);

        //save json data from userbagpack to transaction table
        saveUserBagPackJson(paymentReferenceId, secureTicketList);

        //delete all secure tickets for user
        secureTicketService.deleteSecureTickets(userId);

        //delete bagpack
        userBagPackRepository.delete(secureTicketList.stream().map(SecureTicket::getUserBagPack).collect(Collectors.toList()));
        log.info("Cleaned up user backpack successfully");

        return secureTicketList;
    }

    private void saveUserBagPackJson(String paymentReferenceId, List<SecureTicket> secureTicketList) throws Exception {
        List<Transaction> transactions = transactionRepository.findAllByPaymentReferenceId(paymentReferenceId);
        for(Transaction transaction : transactions) {
            for (SecureTicket secureTicket: secureTicketList) {
                if (paymentReferenceId.equals(secureTicket.getPaymentReferenceId())) {
                    String jsonData = secureTicket.getUserBagPack().getJsonData();
                    transaction.setUserBagPackJson(jsonData);
                    transactionRepository.save(transaction);
                    log.info("Saved transactions userBagPackJson successfully");
                }
            }
        }
    }

    public void updateTransactionUrl(String ref, Collection<Future<String>> futures)  throws Exception {
        // get the transaction and update the ticket url
        for (Future<String> future: futures) {
            log.info("Future looping for update Transaction Url for refId - "+ref);

            String response = future.get();
            // parse the response
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(response);

            List<Transaction> transactions = transactionRepository.findAllByPaymentReferenceId(ref);
            String url = (String) obj.get("ticketUrl");
            String mercuryID = String.valueOf(obj.get("mercuryOrderID"));
            if(!StringUtils.isEmpty(url) && !StringUtils.isEmpty(mercuryID)){
                log.info("Ticket Download URL is "+url+" For Transaction "+mercuryID);
                for(Transaction transaction : transactions) {
                    if("Ingresso".equals(transaction.getEvent().getSupplier()) || "Isango".equals(transaction.getEvent().getSupplier())){
                        if(mercuryID.equals(transaction.getMercuryOrderId())){
                            transaction.setUrl(url);
                            transactionRepository.save(transaction);
                            log.info("Saved transactions url successfully");
                        }
                    }
                }
            }
        }

    }
    public String initiatePayment(String paymentRequestJson) throws Exception {
        org.json.JSONObject paymentRequestObj=new org.json.JSONObject(paymentRequestJson);
        Long userId = paymentRequestObj.getLong("userId");
        PaymentResponse paymentResponse = new PaymentResponse();
        // get secure tickets for user
        List<SecureTicket> secureTickets = secureTicketService.fetchSecureTicketsWithLockTrue(userId);
        // if all tickets are locked successfully, proceed to payment, otherwise return back to the user
        // and provide a message to remove the cart items, which are not successful
        paymentResponse = processPayment(secureTickets);

        //build the pay doller payment string and make a post request to it
        String response=makePayment(paymentResponse,paymentRequestObj);
        return response;
    }

    public String makePayment(PaymentResponse paymentResponse, org.json.JSONObject paymentRequestObj) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        StringBuilder hashBuilder = new StringBuilder();
        hashBuilder
                .append("merchantId="+paymentResponse.getMerchantId())
                .append(AMPERSAND)
                .append("orderRef="+paymentResponse.getOrderRef())
                .append(AMPERSAND)
                .append("amount="+paymentResponse.getAmount())
                .append(AMPERSAND)
                .append("currCode="+paymentResponse.getCurrCode())
                .append(AMPERSAND)
                .append("payMethod="+paymentRequestObj.getString("payMethod"))
                .append(AMPERSAND)
                .append("epMonth="+paymentRequestObj.getString("epMonth"))
                .append(AMPERSAND)
                .append("epYear="+paymentRequestObj.getString("epYear"))
                .append(AMPERSAND)
                .append("cardNo="+paymentRequestObj.getString("cardNo"))
                .append(AMPERSAND)
                .append("cardHolder="+paymentRequestObj.getString("cardholder"))
                .append(AMPERSAND)
                .append("remark="+paymentRequestObj.getString("remark"))
                .append(AMPERSAND)
                .append("lang="+paymentResponse.getLang())
                .append(AMPERSAND)
                .append("securityCode="+paymentRequestObj.getString("securityCode"))
                .append(AMPERSAND)
                .append("payType="+paymentRequestObj.getString("payType"))
        ;

        String response = restTemplate.postForEntity(asiaPayDirectPaymetUrl, hashBuilder.toString(), String.class).getBody();
        
        /**
         * //TODO: parse response and get the successcode and PayRef.
         * if successcode=0, invoke processPaymentSuccess() with the PayRef value and ContextStorage.get().getOriginHost() and
         * return ResponseJsonUtil.getSuccessResponseJson()
         * if successcode=-1, send the errMsg value to UI as failure : 
         * ResponseJsonUtil.getFailedResponseJson("processPaymentFailed", response.errMsg)
         *
         * Sample response from paydollar: 
         * successcode=0&Ref=Test&PayRef=4780&Amt=1.0&Cur=344&prc=0&src=0&Ord=6697090&Holder=edward&AuthId=123456&TxTime=2003-10-07 17:48:02.0&errMsg=Transaction completed
         */
        
        return response;
    }


    /*
    * This method is dummy method. to test the payment with paydollar site added this method*/
    public String initiatePaymentTesting() throws Exception {

        log.info("########### Inside 'initiatePaymentTesting' service method ############");

        RestTemplate restTemplate = new RestTemplate();
        StringBuilder hashBuilder = new StringBuilder();

        log.info("########### Started hashBuilder ############");

        hashBuilder
                .append("merchantId=12104978")
                .append(AMPERSAND)
                .append("orderRef=9859341150678")
                .append(AMPERSAND)
                .append("amount=0.15")
                .append(AMPERSAND)
                .append("currCode=840")
                .append(AMPERSAND)
                .append("payMethod=VISA")
                .append(AMPERSAND)
                .append("epMonth=07")
                .append(AMPERSAND)
                .append("epYear=2020")
                .append(AMPERSAND)
                .append("cardNo=4918914107195005")
                .append(AMPERSAND)
                .append("cardHolder=VISA Card")
                .append(AMPERSAND)
                .append("remark=Testing")
                .append(AMPERSAND)
                .append("lang=E")
                .append(AMPERSAND)
                .append("securityCode=123")
                .append(AMPERSAND)
                .append("payType=N")
        ;


        log.info("########### Completed hashBuilder ############");
        log.info("########### Started to post the data to the paydoller URL ############");

        String response = restTemplate.postForEntity(asiaPayDirectPaymetUrl, hashBuilder.toString(), String.class).getBody();

        log.info("########### Completed to post the data to the paydoller URL ############");

        log.info("########### Response" + response + "############");

        return response;
    }

    public void processPaymentSuccess(String ref, String sourceHost) {
        Collection<Future<String>> futures = new ArrayList<Future<String>>();
        log.info("SourceHost - "+sourceHost+" for the reference - "+ref);
        try {
            //update payment status success
            this.updatePaymentStatus(ref, TransactionStatus.SUCCESS);

            /*//place order and update transaction status
            paymentService.processSuccessfulPayment(ref);*/

            //place order and update transaction status
            futures = ticketService.placeOrderForSupplier(ref);

            //get order response
            ticketService.getOrderResponse(ref, futures);

            //update transactions and delete from bagpack
            List<SecureTicket> secureTicketList = this.updateTransactionStatus(ref);

            //update transaction ticket url
            this.updateTransactionUrl(ref, futures);

            //send sms
            smsHelperService.sendSmsToUser(ref, sourceHost);

            //send email
            emailHelperService.sendMailToUser(ref, secureTicketList, sourceHost);

        }catch (Exception e) {
            log.error(e.getMessage(),e);
        }
    }
}
