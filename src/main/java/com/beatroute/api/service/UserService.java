package com.beatroute.api.service;

import com.beatroute.api.dto.EmailContent;
import com.beatroute.api.model.*;
import com.beatroute.api.model.enumeration.UserType;
import com.beatroute.api.repository.*;
import com.beatroute.api.rest.configuration.ContextData;
import com.beatroute.api.rest.configuration.ContextStorage;
import com.beatroute.api.service.exception.EntityAlreadyExistsException;
import com.beatroute.api.service.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.*;

@Service
@Slf4j
@SuppressWarnings("rawtypes")
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WishListRepository wishListRepository;

    @Autowired
    private EventService eventService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private EnterpriseService enterpriseService;

    @Autowired
    private EnterpriseRepository enterpriseRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private VenueRepository venueRepository;

    @Autowired
    private EnterpriseExperienceRepository enterpriseExperienceRepository;

    @Value("${app.mail.register.user.subject}")
    private String registerUserSubject;

    @Value("${app.mail.register.user.body}")
    private String registerUserMessage;

    @Value("${app.mail.reset.password.subject}")
    private String resetPasswordSubject;

    @Value("${app.mail.forgot.password.subject}")
    private String forgotPasswordSubject;

    @Value("${app.mail.forgot.password.body}")
    private String forgotPasswordBody;

    public User save(User user){
        return this.userRepository.save(user);
    }

    public User get(long id) throws EntityNotFoundException {
        User user = this.userRepository.findOne(id);
        if(user == null) {
            throw new EntityNotFoundException("user not found with id :"+id);
        }
        return user;
    }

    public User addWishList(long userId, long eventId) throws EntityAlreadyExistsException , EntityNotFoundException {
        WishList wishList = this.wishListRepository.findByUserIdAndEventId(userId, eventId);
        if(wishList != null){
            throw new EntityAlreadyExistsException("Event already added to your wishlist.");
        }
        User user = get(userId);
        Event event = eventService.findById(eventId);
        wishList = new WishList();
        wishList.setUser(user);
        wishList.setEvent(event);
        user.getWishlist().add(this.wishListRepository.save(wishList));
        return this.userRepository.save(user);
    }

    public User removeFromWishList(long userId, long eventId) throws EntityNotFoundException {
        WishList wishList = this.wishListRepository.findByUserIdAndEventId(userId, eventId);
        if(wishList == null){
            throw new EntityNotFoundException("event is not in user wishlist");
        }
        User user = get(userId);
        user.getWishlist().remove(wishList);
        this.wishListRepository.delete(wishList.getId());
        return this.userRepository.save(user);
    }

    public void delete(long id){
        this.userRepository.delete(id);
    }

    public User resetPassword(String password , long id) throws EntityNotFoundException, InterruptedException {
        User user = null;
        try {
            user = this.userRepository.findOne(id);
            if(user == null){
                throw new EntityNotFoundException("User not found with id:"+id);
            }
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(5);
            String hashedPassword = passwordEncoder.encode(password);
            user.setPassword(hashedPassword);
            user.setUpdatedDate(new Date());
            user = this.userRepository.save(user);
//            this.emailService.sendResetPasswordEmail(user.getName(), user.getEmail(), resetPasswordSubject);
        }catch( Exception e ){
            //todo: add log here
            log.error(e.getMessage(), e);
        }
        return user;
    }

    public boolean isUserAuthorised(User user){
        String country = user.getCountry();
        String email = user.getEmail();
        ContextData cd= ContextStorage.get();
        Enterprise e=user.getEnterprise();
        if (e!=null) {
        	int compareResult=cd.getEnterpriseId().compareTo(e.getId());
        	if (compareResult!=0) {
                return false;
            }
        }
        
        if(country == null || email == null){
            return false;
        }
        //if user is appuser and he is still not authorised through mail
        if(user.getUserType() == UserType.APP_USER && !user.isAuthorised()){
            return false;
        }
        return true;
    }

    public User register(User user) throws InterruptedException {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(5);
        String hashedPassword = passwordEncoder.encode(user.getPassword());
        user.setName(user.getName());
        user.setPassword(hashedPassword);
        user.setAuthorised(isUserAuthorised(user));
        user.setEmail(user.getEmail());
        user.setCountry(user.getCountry());
        user.setUserType(UserType.APP_USER);
        //adding enterprise to user
        ContextData cd= ContextStorage.get();
        user.setEnterprise(enterpriseRepository.findOne(cd.getEnterpriseId()));
        user = this.userRepository.save(user);

        EmailContent emailContent = this.enterpriseService.getEnterpriseContent();
        String activationLink = emailContent.getHost().concat(registerUserMessage);

        String registerSubject = registerUserSubject.replace("$displayName", emailContent.getDisplayName());  //MessageUtility.getFormattedString(registerUserSubject, enterpriseRepository);

        this.emailService.sendUserRegistartionEmail(user.getName(), user.getEmail(), activationLink+user.getId(), registerSubject, emailContent);
        return user;
    }

    public boolean validateEmail(String email) {
        ContextData cd= ContextStorage.get();
        User user = this.userRepository.findOneByEmailAndEnterpriseId(email,cd.getEnterpriseId());
        if(user == null){
            return true;
        }else{
            return false;
        }
    }

	public Map getUserType(String email) {
        Map<String, String> data = new HashMap<>();
        ContextData cd= ContextStorage.get();
        User user = this.userRepository.findOneByEmailAndEnterpriseId(email,cd.getEnterpriseId());
        if(user == null){
            data.put("canUseEmail", "true");
            data.put("userType", "");
        }else{
            //data.put("canUseEmail", "false");
            data.put("canUseEmail", "true");
            //data.put("userType", String.valueOf(user.getUserType()));
        }
        return data;
    }

    public User forgotPassword(String email) throws EntityNotFoundException, InterruptedException {
        ContextData cd= ContextStorage.get();
        User user = this.userRepository.findOneByEmailAndEnterpriseId(email,cd.getEnterpriseId());
        if(user == null){
            throw new EntityNotFoundException("User not found with email:"+email);
        }
        // Creating a random UUID (Universally unique identifier).
        UUID uuidOne = UUID.randomUUID();
        String randomUUIDOne = uuidOne.toString().replaceAll("-", "");
        UUID uuidTwo = UUID.randomUUID();
        String randomUUIDTwo = uuidTwo.toString().replaceAll("-", "");
        long dateTimeStamp = new Date().getTime();
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(5);
        String hashedPassword = passwordEncoder.encode(user.getId().toString());
        user.setPassword(hashedPassword);
        user = this.userRepository.save(user);

        EmailContent emailContent = this.enterpriseService.getEnterpriseContent();
        String activationLink = emailContent.getHost().concat(forgotPasswordBody);
        log.info("activation link - "+activationLink);

        String forgotSubject  = forgotPasswordSubject.replace("$displayName", emailContent.getDisplayName());  //MessageUtility.getFormattedString(forgotPasswordSubject, enterpriseRepository);

        this.emailService.sendForgotPasswordEmail(user.getName(), user.getEmail(), activationLink+randomUUIDOne+"-"+"fcyu"+user.getId()+"uko"+"-"+randomUUIDTwo+"-"+dateTimeStamp, forgotSubject, emailContent);
        log.info("completed forgot password");
        return user;
    }

    public User authorizeUser(long id) throws EntityNotFoundException, EntityAlreadyExistsException {
        User user = this.userRepository.findOne(id);
        if(user == null){
            throw new EntityNotFoundException("User not found with id:"+id);
        }
        if(user.isAuthorised()){
            throw new EntityAlreadyExistsException("user already authorised");
        }
        user.setAuthorised(true);
        user = this.userRepository.save(user);
        return user;
    }

    /*
        Input: {"name": "", "userType": "", "email":"will_not_get_fb",  "profilePic": "", "oauthId": "this_will_unique_oauth_id"}
     */
    public User oauthLogin(User oauthUser) throws Exception{
        User user = null;
        ContextData cd= ContextStorage.get();
        user = this.userRepository.findOneByOauthIdAndEnterpriseId(oauthUser.getOauthId(),cd.getEnterpriseId());
        if(user == null){
            if(oauthUser.getEmail() == null){
                oauthUser.setAuthorised(true);
                //adding enterprise to user
                oauthUser.setEnterprise(enterpriseRepository.findOne(cd.getEnterpriseId()));
                user = this.userRepository.save(oauthUser);
            }else{
                user = userRepository.findOneByEmailAndEnterpriseId(oauthUser.getEmail(),cd.getEnterpriseId());
                if(user == null){
                    oauthUser.setAuthorised(true);
                    //adding enterprise to user
                    oauthUser.setEnterprise(enterpriseRepository.findOne(cd.getEnterpriseId()));
                    user = this.userRepository.save(oauthUser);
                }else{
                    /*user.setUserType(oauthUser.getUserType());
                    user = userRepository.save(user);*/
                    user.setProfilePic(oauthUser.getProfilePic());
                    user.setUpdatedDate(new Date());
                    user = userRepository.save(user);
                    return user;
                }
            }
        }else{
            //  log.info("oauth user present");
            user.setUserType(oauthUser.getUserType());
            //  log.info("oauth user type "+user.getUserType());
            user = userRepository.save(user);
        }
        return user;
    }

    public User login(User loginUser) throws EntityNotFoundException, UsernameNotFoundException {
        try{
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(5);
            UserDetails user = this.authenticationService.loadUserByUsername(loginUser.getEmail());
            if(encoder.matches(loginUser.getPassword(), user.getPassword())){
                long id = Long.parseLong(user.getUsername());
                User authenticatedUser = this.userRepository.findOne(id);

                if(!isUserAuthorised(authenticatedUser)){
                    throw new EntityNotFoundException("Verify Account");
                }
                return authenticatedUser;
            }else{
                throw new EntityNotFoundException("Invalid Credentials");
            }
        } catch (Exception e){
            throw new EntityNotFoundException("Invalid Credentials");
        }
    }

    public User changePassword(String userJson) throws EntityNotFoundException, UsernameNotFoundException {
        try{
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(5);
            JSONObject obj = new JSONObject(userJson);
            String userName=obj.getString("userName");
            String oldPassword=obj.getString("oldPassword");
            String newPassword=obj.getString("newPassword");

            UserDetails user = this.authenticationService.loadUserByUsername(userName);
            if(encoder.matches(oldPassword,user.getPassword())){
                long id = Long.parseLong(user.getUsername());
                User authenticatedUser = this.userRepository.findOne(id);

                if(!isUserAuthorised(authenticatedUser)){
                    throw new EntityNotFoundException("Verify Account");
                }
                String hashedPassword = encoder.encode(newPassword);
                authenticatedUser.setPassword(hashedPassword);
                authenticatedUser=this.userRepository.save(authenticatedUser);
                log.info("Password Changed Successfully for the user: "+userName);
                return authenticatedUser;
            }else{
                throw new EntityNotFoundException("Invalid Credentials");
            }
        } catch (UsernameNotFoundException e){
            throw new EntityNotFoundException("Invalid Credentials");
        }
    }

    public User update(User updateUser) throws EntityNotFoundException {
        long id = updateUser.getId();
        ContextData cd= ContextStorage.get();
        Enterprise e=this.enterpriseRepository.findOneById(cd.getEnterpriseId());
        User user = this.userRepository.findOne(id);
        if(user == null){
            throw new EntityNotFoundException("User not found with id:"+id);
        }
        user.setEnterprise(e);
        return this.userRepository.save(updateUser);
    }

    public List<Transaction> getTransactions(long id) throws EntityNotFoundException {
        User user = get(id);
        return this.transactionRepository.findAllByUserOrderByCreatedDateDesc(user);
    }

    public List<WishList> getWishList(long id) throws EntityNotFoundException {
        @SuppressWarnings("unused")
		User user = get(id);
        return this.wishListRepository.findAllByUserId(id);
    }

    public List<Long> getEventIdList(Long id) {
        List<BigInteger> eventIdBigInt = this.eventRepository.findAllEnterpriseTenantsByEnterprise(id);
        List<Long> eventIdList = new ArrayList<>();
        eventIdBigInt.forEach(l -> {
            eventIdList.add(l.longValue());
        });
        return eventIdList;
    }


}
